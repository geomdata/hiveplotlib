# build_logo.py

"""
Generates hiveplotlib logo for documentation.

Note, this runner should be run from the root of the repository e.g. ``python runners/build_logo.py``.
"""

import matplotlib.pyplot as plt
import numpy as np

from hiveplotlib import Axis, BaseHivePlot, Node
from hiveplotlib.viz import axes_viz, edge_viz

hp = BaseHivePlot()

twist = -30
# build axes
axis0 = Axis(axis_id="A", start=1, end=5, angle=0 + twist)
axis1 = Axis(axis_id="B", start=1, end=5, angle=120 + twist)
axis2 = Axis(axis_id="C", start=1, end=5, angle=240 + twist)

axes = [axis0, axis1, axis2]

hp.add_axes(axes)

# build nodes

num_nodes = 20
rng = np.random.default_rng(0)

nodes = []
for i in range(num_nodes):
    temp_node = Node(
        unique_id=str(i),
        data={"a": rng.uniform(), "b": rng.uniform(), "c": rng.uniform()},
    )
    nodes.append(temp_node)
hp.add_nodes(nodes)

# shuffle nodes and assign at random
node_ids = np.arange(num_nodes).astype(str)
rng.shuffle(node_ids)

vmin = 0
vmax = 1
hp.place_nodes_on_axis(
    unique_ids=node_ids[: num_nodes // 3],
    axis_id="A",
    sorting_feature_to_use="a",
    vmin=vmin,
    vmax=vmax,
)
hp.place_nodes_on_axis(
    unique_ids=node_ids[num_nodes // 3 : 2 * num_nodes // 3],
    axis_id="B",
    sorting_feature_to_use="b",
    vmin=vmin,
    vmax=vmax,
)
hp.place_nodes_on_axis(
    unique_ids=node_ids[2 * num_nodes // 3 :],
    axis_id="C",
    sorting_feature_to_use="c",
    vmin=vmin,
    vmax=vmax,
)

# dummy of from, to pairs
rng = np.random.default_rng(0)
num_edges = 80
connections = rng.choice(np.arange(num_nodes).astype(str), size=num_edges * 2).reshape(
    -1, 2
)

edge_permutations = [("A", "B"), ("A", "C"), ("B", "C")]
for i, (a0, a1) in enumerate(edge_permutations):
    hp.connect_axes(
        edges=connections,
        axis_id_1=a0,
        axis_id_2=a1,
        c=f"C{i}",
        lw=3,
        alpha=1,
        zorder=1.5,
    )

fig, ax = axes_viz(
    hp, show_axes_labels=False, figsize=(10, 10), lw=5, alpha=1, zorder=2
)

edge_viz(
    hp,
    fig=fig,
    ax=ax,
    # c=f"C{i}", plot_edges_both_directions=True, lw=3, alpha=1, zorder=1.5
)
ax.text(
    -33,
    -2,
    "hivepl",
    fontsize=450,
    style="oblique",
    weight="black",
    variant="small-caps",
    zorder=-1,
    c="midnightblue",
)
ax.text(
    2,
    -2,
    "tlib",
    fontsize=450,
    style="oblique",
    weight="black",
    variant="small-caps",
    zorder=-1,
    c="midnightblue",
)
plt.savefig(
    "./docs/source/_static/hiveplotlib.svg", bbox_inches="tight", transparent=True
)
