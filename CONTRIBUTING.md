# Contributing

## Developers

For the moment, we will restrict developers to employees of Geometric Data Analytics. If it becomes appropriate
given interest in contributing or growth of the project, we will (happily) revise this policy later. If you have
recommendations for changes / extensions or discover bugs, we would still appreciate your thoughts
in the [Issue Tracker](https://gitlab.com/geomdata/hiveplotlib/-/issues).

Local development has been streamlied to work with the `Makefile` in this repository.

## Feature Requests / Bug Reports

Use the [Issue Tracker](https://gitlab.com/geomdata/hiveplotlib/-/issues) for feature requests or reporting bugs.

## Install the Developer Environment

Developers need the `uv` virtual environment and associated `jupyter` kernel called `hiveplotlib`
to get started. These can both be created by running:

```{bash}
cd <path/to/repository>
make install
```

## Testing

Tests for the `hiveplotlib` package should be run with the `uv` virtual environment and the
associated `hiveplotlib` `jupyter` kernel. This will be done automatically via the `Makefike` runs
discussed below, as long as you've already run `make install` as discussed in the section above.

### Unit Tests

Unit tests alone can be run via:

```{bash}
cd <path/to/repository>
make test
```

The resulting `pytest-html` output can be viewed in your default browser by running:

```{bash}
cd <path/to/repository>
make view-tests
```

Any code added to the `hiveplotlib` package should be unit tested, as evidenced by the coverage report
visible in the upper right of a clicked on "hiveplotlib" job in each pipeline, which should state "Coverage: 100%".
Coverage checks can also be looked at locally after running `make test` by opening:

`<path/to/repository>/data/pytest/hiveplotlib_coverage.html/index.html`

### Example (Notebook) Tests

Example (notebook) tests alone can be run via:

```{bash}
cd <path/to/repository>
make test-nb
```

The resulting `pytest-html` output can be viewed in your default browser by running:

```{bash}
cd <path/to/repository>
make view-tests-nb
```

### Run All Tests

To run both the unit and example tests and spin up a `pytest-html` output for each, run:

```{bash}
cd <path/to/repository>
make test-all
```

## Code Hygiene Checks

[Ruff](https://docs.astral.sh/ruff/) is used for linting and formatting code.

Both linting and formatting can be triggered locally by running:

```{bash}
cd <path/to/repository>
make format
```

### Linting

Linting refers to a static analysis of the codebase to check for efficient and compliant python code. The default checks
are documented in the [pyproject.toml](pyproject.toml) files. Ruff can be run from the command line via `ruff check` or
by installing ruff in an IDE. Certain errors in ruff can be automatically fixed by running `ruff check --fix`.

Any errors that pop up (e.g. E401) can be expanded on by executing `ruff rule <error>`, as in `ruff rule E401`.

If added code is compliant, then running `ruff check` should not print out any warnings.

This is tested in each CI/CD pipeline in the `lint_check` job.

### Formatting

Formatting refers to adjusting the physical appearance of the code (adding new lines, commas, etc.), without modifying
any of the intent or logic. Ruff can automatically format a specific file or an entire repository by executing
`ruff format <path>/<to>/<file>` or `ruff format`.

To pass pipelines, this must be run before pushing code.

This is tested in each CI/CD pipeline in the `format_check` job.

## Docs

The documentation can be built locally by running:

```{bash}
cd <path/to/repository>
make docs
```

The resulting docs can be viewed in your default browser by running:

```{bash}
cd <path/to/repository>
make view-docs
```

## Notebooks

Any `jupyter` notebooks  added to `examples` are automatically tested in the "examples" job in each CI/CD pipeline to confirm
that they run start to finish without error.

### Adding Notebooks to Documentation

These notebooks can also easily be added to the ReadTheDocs
[documentation](https://hiveplotlib.readthedocs.io/stable/) by including the name of your notebook (without the `.ipynb`) in
`docs/source/index.rst` (don't worry about generating your notebook as an `.rst` or `html` file, this is automatically handled by the CI or `make docs` if running a local build).

Note, the new notebooks will not be visible in the public documentation until the code is merged back to `master` and the documentation
is rebuilt.
