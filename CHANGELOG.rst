.. _changes:

Hiveplotlib Changelog
=====================

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.1.0/>`_,

As of January, 2024, this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

Version 0.26.0 (released WIP)
--------------------------------------

This release includes several major enhancements in the user API and flexibility in generating hive plots.

First, there is a new :py:class:`~hiveplotlib.HivePlot` class with major improvements for user-friendly,
object-oriented modifiability.
New flexibility here includes setting a partition variable to dictate the axes, choosing sorting variables, rotating the
hive plot, adding / changing repeat axes, changing the axes order, a ``plot()`` method, and the ability to set a viz
backend with any of the supported ``hiveplotlib`` visualization backends. The documentation has been updated to
demonstrate this improved functionality, with a reference for how to preserve the old functionality as well as migration
to using the new ``HivePlot`` class.


The ``HivePlot`` class from previous releases lives on, but has been renamed to :py:class:`~hiveplotlib.BaseHivePlot`,
as its functionality is now the basis for the higher-level functionality of the new ``HivePlot`` class. Aside from the
name change, this class can still be used in almost exactly the same way as the previous releases.

Second, there are new, ``pandas.DataFrame``-friendly node and edge data structures
(the :py:class:`~hiveplotlib.NodeCollection` and :py:class:`~hiveplotlib.Edges` classes, respectively).

Third, users can now include hover information for the interactive viz backends.

This release also drops support for use in Python 3.8 and adds support for Python 3.13, following the
timeline of Python's supported versions: https://devguide.python.org/versions/.

Finally, this release revises our CI/CD framework from using `conda` environments to `uv` virtual environments.

Added
~~~~~

- There is a completely new :py:class:`~hiveplotlib.HivePlot` class. The
  class from previous releases is still available for use with minimal changs (see Changed section below), but has been
  renamed to the lower-level :py:class:`~hiveplotlib.BaseHivePlot` class.

- Add :py:class:`~hiveplotlib.node.NodeCollection` class to create collections of nodes from ``pandas`` dataframes.

- Add :py:class:`~hiveplotlib.edges.Edges` class to create edges from ``pandas`` dataframes or ``numpy`` arrays.

- Add hover information support for ``node_viz()``, ``edge_viz()``, ``axes_viz()``, and ``hive_plot_viz()`` with the
  ``bokeh``, ``plotly``, and ``holoviews`` (with ``bokeh``, not ``matplotlib``) backends.
  This capability is enabled by default, but can be disabled by setting ``hover=False`` in the function calls.
  Specific hover information can also be turned on in ``hive_plot_viz()`` by providing one or more of
  ``["nodes", "edges", "axes"]`` as inputs to the ``hover`` parameter.
  Hover information for nodes will include provided node data and metadata in the ``HivePlot.nodes`` attribute.
  Hover information for edges will include provided edge data and metadata in the ``HivePlot.edges`` attribute.
  Hover information for axes will include settings from each ``Axis`` instance in the ``HivePlot.axes`` attribute.
  Hovering should be comparable between the three hover-supporting backends, with the exception that axis hover
  information with the ``plotly`` backend will show up on the axis label, *not* the axis itself.
  Note: hover information is only currently supported for Hive Plots, not P2CPs.

- Add :py:func:`hiveplotlib.node.node_collection_from_node_list()` to create :py:class:`~hiveplotlib.NodeCollection`
  from a list of :py:class:`~hiveplotlib.Node` values.

- Add :py:func:`hiveplotlib.node.subset_node_collection_by_unique_ids()` to create a subset dataframe of node data
  corresponding to a list of unique node IDs.

- Add :py:func:`hiveplotlib.datasets.example_node_data()` to generate an example ``pandas.DataFrame`` of node data.
  :py:class:`~hiveplotlib.NodeCollection` instance.

- Add :py:func:`hiveplotlib.datasets.example_node_collection()` to generate an example
  :py:class:`~hiveplotlib.NodeCollection` instance.

- Add :py:func:`hiveplotlib.datasets.example_edges()` to generate an example :py:class:`~hiveplotlib.Edges` instance.

- Add :py:mod:`hiveplotlib.exceptions` module to generate custom exceptions for better error-handling.

- Add ``overwrite_existing_kwargs`` and ``reset_existing_kwargs`` flags to
  :py:meth:`hiveplotlib.BaseHivePlot.add_edge_kwargs()` and :py:meth:`hiveplotlib.BaseHivePlot.connect_axes()`.
  Default behavior gives same results as before. Setting ``reset_existing_kwargs`` to ``True``
  allows a user to drop all previously-set edge kwargs for the user-specified subset of edges
  dictated by the other parameters. Setting ``overwrite_existing_kwargs`` to ``False`` prevents overwriting any
  already-provided edge kwargs for the user-specified subset of edges dictated by the other parameters.

- Add ``warn_on_no_edges`` flag to :py:meth:`hiveplotlib.BaseHivePlot.add_edge_kwargs()`.
  Default behavior ``True`` leaves behavior unchanged from before. Setting to ``False`` silences warnings to user about
  setting edge kwargs for nonexistent edges.

Changed
~~~~~~~

Breaking Changes
^^^^^^^^^^^^^^^^

- The ``HivePlot`` class of past releases has been renamed to :py:class:`~hiveplotlib.BaseHivePlot`. This class is
  still available for use, but is now the basis for the new, higher-level :py:class:`~hiveplotlib.HivePlot` class.

- :py:meth:`hiveplotlib.BaseHivePlot.place_nodes_on_axis()` now works off of a *dataframe* of node data and metadata via the
  new ``node_df`` parameter, not lists or 1d ``numpy`` arrays of unique IDs. The old ``unique_ids`` parameter will now
  raise a ``TypeError``, pointing the user to the ``node_df`` parameter. This was a necessary change to support hover
  information for the interactive viz backends.

- The hive plot edges constructed in :py:class:`~hiveplotlib.BaseHivePlot` are now stored under the ``hive_plot_edges``
  attribute instead of the ``edges`` attribute, which is now reserved for storing edges with the higher-level
  :py:class:`~hiveplotlib.Edges` class.

- :py:func:`hiveplotlib.datasets.example_hive_plot()` has been revised to randomly generate a
  :py:class:`~hiveplotlib.HivePlot` instance. The original ``example_hive_plot()`` function persists, but has been
  renamed :py:func:`hiveplotlib.datasets.example_base_hive_plot()`, in accordance with renaming the previous hive plot
  class :py:class:`~hiveplotlib.BaseHivePlot`.

- :py:class:`~hiveplotlib.Axis` ``node_placement_label`` attribute renamed to ``sorting_variable. Also, the two private
  methods have been renamed as public methods (``_set_node_placements()`` now called
  ``set_node_placements()`` and ``_set_node_placement_label()`` now called ``set_sorting_variable()``). The parameter
  inputs for the now-named :py:meth:`hiveplotlib.Axis.set_node_placements()` have also been revised to a dataframe and a
  unique ID column name.

Other Changes
^^^^^^^^^^^^^

- ``hiveplotlib.Axis.node_placements``, a dataframe attribute that originally only stored *positional* infromation, now
  also stores the associated node metadata. This was done to allow for adding node hover information for the interactive
  viz backends. The ``unique_id`` column in this dataframe may now have a different name as a function of the specified
  name of unique IDs when creating nodes via the :py:class:`~hiveplotlib.NodeCollection` class. Note:
  :py:meth:`hiveplotlib.BaseHivePlot.to_json()` will still always call node unique IDs ``"unique_id"`` in the resulting JSON
  data.

- :py:class:`~hiveplotlib.BaseHivePlot` class now tracks its ``nodes`` attribute of as type
  :py:class:`~hiveplotlib.NodeCollection` instead of a dictionary with unique ID keys and
  :py:class:`~hiveplotlib.Node` values.

- :py:meth:`hiveplotlib.BaseHivePlot.add_nodes()` now supports adding a list of :py:class:`~hiveplotlib.Node` values (as it
  did before) or a :py:class:`~hiveplotlib.NodeCollection`.

- :py:func:`hiveplotlib.viz.holoviews` axis visualization now backed by a
  `holoviews.Segments() <https://holoviews.org/reference/elements/bokeh/Segments.html>`__ call. This was originally a
  ``holoviews.Curve()`` call. This may affect axes-related keyword arguments when plotting with either of the supported
  ``holoviews`` back ends.

- :py:func:`hiveplotlib.viz.holoviews` edge visualization now backed by a
  `holoviews.Contours() <https://holoviews.org/reference/elements/bokeh/Contours.html>`__ call. This was originally a
  ``holoviews.Path()`` call. This may affect edge-related keyword arguments when plotting with either of the supported
  ``holoviews`` back ends. When using the ``holoviews-bokeh`` back end, edge colors also MUST be specified via the
  ``"color"`` parameter (``"line_color"`` will not be accepted, now raising a ``ValueError``).

- :py:class:`~hiveplotlib.Axis` repr and str representations now includes more metadata about current settings for a
  given axis.

- :py:class:`~hiveplotlib.Axis` now tracks the ``vmin`` and ``vmax`` values used to place nodes on the ``Axis`` via
  :py:meth:`hiveplotlib.HivePlot.place_nodes_on_axis()`. Additionally, this tracks whether the ``vmin`` and / or
  ``vmax`` values were inferred, storing those boolean values as the new ``inferred_vmin`` and ``inferred_vmax``
  attributes in the ``Axis`` instance.

Fixed
~~~~~

- :py:meth:`hiveplotlib.BaseHivePlot.reset_edges()` used to trigger an error if requesting the deletion of edges that do not
  exist. This has been fixed (the method now does nothing when asked to delete edges that do not exist).

- ``hive_plot_viz()`` calls with different viz backends on empty hive plots (a lack of axes in particular) used to
  return ``None`` values and / or error out in strange ways. This has now been standardized to return the expected
  figure types without error. The sole exception is :py:func:`hiveplotlib.viz.datashader.datashade_hive_plot_mpl()`,
  which will now return a ``matplotlib`` figure, axis, and two ``None`` values (one per lack of datashaded image for
  both nodes and edges) when plotting an empty hive plot.

- Fixed incorrect type hints in :py:mod:`hiveplotlib.viz.datashader` module for ``reduction`` parameter.

Tooling Changes
~~~~~~~~~~~~~~~

- Drop support for Python 3.8 and add support for Python 3.13.

- Developer environment and CI/CD framework revised from using `conda` environments to `uv` virtual environments.

- Developers can now test multiple installations and python versions through CI/CD as optional jobs on feature branch
  pipelines. 

- Added `Makefile` for local developers.

Deprecated
~~~~~~~~~~

- :py:class:`hiveplotlib.hive_plot_n_axes` is set to be removed in version ``0.28.0``. It's functionality has been fully
  incorporated into the revised ``HivePlot`` class.

Version 0.25.1 (released Jul 5, 2024)
-------------------------------------

This bugfix release adds ``numpy>=2.0.0`` support.

Fixed
~~~~~

- Compatibility with ``numpy 2.0.0``.


Version 0.25.0 (released Mar 23, 2024)
--------------------------------------

Added
~~~~~

- Added :py:mod:`hiveplotlib.viz.holoviews` module supporting hive plot and P2CP viz in
  `holoviews <https://holoviews.org/index.html>`__.

- :py:func:`hiveplotlib.datasets.example_p2cp()` added for generating P2CP with Gaussian four blobs example.

Version 0.24.0 (Released Feb 18, 2024)
--------------------------------------

Added
~~~~~

- :py:func:`hiveplotlib.datasets.example_nodes_and_edges()` added for generating random nodes, node splits, and edges
  for an arbitrary number of axes.

- :py:meth:`hiveplotlib.HivePlot.connect_axes()` now allows users to specify ``control_rho_scale`` and
  ``control_angle_shift``, which allow users to modify the positioning of the control point for the Bézier curve of each
  edge. By default, the control point for an edge between two nodes is the polar midpoint of the two curves, but users
  can now scale the polar distance from the origin (``control_rho_scale``) or shift the angle counterclockwise
  (``control_angle_shift``) for each control point *relative to* its default median polar position.

Changed
~~~~~~~

- :py:meth:`hiveplotlib.HivePlot.add_axes()` now supports adding a single ``Axis`` instance (before, a single ``Axis``
  needed to be added as a list with one element).

- :py:mod:`hiveplotlib.viz.matplotlib` when setting ``center_plot=True`` now performs ``ax.axis("equal")`` for the
  provided / resulting ``matplotlib`` axis. Before, the global effect ``plt.axis("equal")`` was being called.

- Code hygiene (linting and formatting) revised from ``flake8`` and ``black``, respectively, to
  `ruff <https://docs.astral.sh/ruff/>`_.

- Documentation moved over to ReadTheDocs:
  https://hiveplotlib.readthedocs.io/en/stable/hive_plot_viz_outside_matplotlib.html.

Fixed
~~~~~

- :py:func:`hiveplotlib.viz.matplotlib.hive_plot_viz()` and :py:func:`hiveplotlib.viz.matplotlib.p2cp_viz()` had a bug
  from the ``0.23.0`` release that made the function calls non-responsive to changing the ``center_plot``, ``buffer``,
  and ``axes_off`` parameters.

- :py:mod:`hiveplotlib.viz.matplotlib` had a bug that made composing viz functions from this module and calling
  ``axes_off=False`` for a later function call unable to cancel out the ``axes_off=True`` behavior of an earlier
  function call. 

Removed
~~~~~~~

- :py:meth:`hiveplotlib.HivePlot.to_json()` revised to generate a more D3-friendly JSON structure. The ``"curves"`` key
  originally contained multiple lines in a single list of ``[x, y]`` values separated by ``[null, null]`` values. The
  lines are now split into their own lists. This proved more compatible for plotting in JavaScript. The original
  JSON structure could still be created for now by running with ``deprecated_version=True`` in versions ``0.22.0`` and
  ``0.23.0``. The old JSON structure was deprecated in version ``0.22.0`` and has been removed in this version.

Many thanks to:
  - @besiansejdiu for ideas, discussion, and code review for exposing the changing of control points of edge Bézier
    curves. 
  - @michael.catanzaro for assistance on migrating to ``ruff``.
  - @abrahamdavidsmith for assistance on automating Gitlab releases.

Version 0.23.0 (released Feb 2, 2024)
-------------------------------------

Added
~~~~~

- Added :py:mod:`hiveplotlib.viz.plotly` module supporting hive plot and P2CP viz in
  `plotly <https://plotly.com/python/>`__.

- :py:class:`hiveplotlib.HivePlot` now tracks the radius corresponding to the largest polar endpoint of all axes with
  the ``max_polar_end`` parameter.

Changed
~~~~~~~

- All occurances of ``mpl_axes_off`` in :py:mod:`hiveplotlib.viz.matplotlib` and :py:mod:`hiveplotlib.viz.datashader`
  renamed to ``axes_off`` for consistency between different viz back ends.

- Many redundant viz warnings used for all viz back ends pulled out of individual viz functions into
  ``hiveplotlib.viz.input_checks.input_check()`` to avoid redundant code, especially as the repository expands
  support for more visualization packages. User experience will not be changed by this.

- Other redundant, viz package-agnostic code pulled out from individual viz functions into the
  ``hiveplotlib.viz.base`` sub-module as well as into private ``_fig_setup`` methods. User experience will not be
  changed by either of these revisions.

- For all occurances of ``axes_labels_fontsize`` in :py:mod:`hiveplotlib.viz.bokeh`, the default value has been raised
  from ``"13px"`` to ``"16px"``.

- For all occurances of ``buffer`` in :py:mod:`hiveplotlib.viz.bokeh`, the default value has been raised from ``"0.2"``
  to ``"0.3"`` (to compensate for the larger default font for axes labels).

- For all occurances of ``center_plot`` in ``hiveplotlib.viz``, if the provided hive plot / P2CP has no axes, then no
  centering will run. (This would previously trigger an error.)

- :py:func:`hiveplotlib.datasets.example_hive_plot()` no longer defaults to putting the kwarg ``alpha=1`` on all edges
  for viz, as this does not generalize to the :py:mod:`hiveplotlib.viz.plotly` use case. Kwargs now exposed for the
  underlying :py:func:`hiveplotlib.hive_plot_n_axes()` call.

Version 0.22.0 (released Jan 23, 2024)
--------------------------------------

Added
~~~~~

- :py:func:`hiveplotlib.datasets.example_hive_plot()` added for generating a hive plot with random nodes and edges.

Changed
~~~~~~~

- :py:meth:`hiveplotlib.HivePlot.to_json()` revised to generate a more D3-friendly JSON structure. The ``"curves"`` key
  originally contained multiple lines in a single list of ``[x, y]`` values separated by ``[null, null]`` values. The
  lines are now split into their own lists. This proved more compatible for plotting in JavaScript. The original
  JSON structure can still be created for now by running with ``deprecated_version=True``. Running this way will raise a
  ``DeprecationWarning`` and will be removed in version ``0.24.0``.

- Moving to full `semantic versioning <https://semver.org/spec/v2.0.0.html>`_ (e.g. ``x.y.z``) this release and onward. 

Version 0.21 (released Mar 20, 2023)
------------------------------------

Enhancements
~~~~~~~~~~~~
- ``hiveplotlib.hive_plot_n_axes()`` now accepts a ``None`` value as one of the values in the ``axes_assignments``
  input. The ``None`` axis takes all otherwise-unassigned nodes from the ``node_list`` input and assigns
  them to that axis.

- Restructure ``hiveplotlib.__init__.py`` to split out capabilities into submodules. Now have ``hiveplotlib.node``,
  ``hiveplotlib.axis``, ``hiveplotlib.hiveplot``, and ``hiveplotlib.p2cp`` modules. High-level imports have been left
  unchanged.

- ``split_nodes_on_variable()``, a node-specific utility function, moved from ``hiveplotlib.utils`` moved into
  ``hiveplotlib.node``.

- The two ``P2CP``-specific utility functions in ``hiveplotlib.p2cp_utils`` -- ``split_df_on_variable()`` and
  ``indices_for_unique_values()`` -- have been moved into ``hiveplotlib.p2cp``. The ``hiveplotlib.p2cp_utils``
  module has been deleted.

- Added ``hiveplotlib.node.dataframe_to_node_list()`` for converting rows of a dataframe into a list of ``Node``
  instances.

- Added ``hiveplotlib.viz.datashader.datashade_hive_plot_mpl()`` for higher-level call to do fully-datashaded hive
  plots (node and edges plus the standard ``axes_viz()``).

- Add a ``buffer`` parameter to the datashading calls to have their extent go slightly beyond axis extents by default
  (and can be increased further by the user as desired).

- Add some international trade network data to the package, and a reader function
  ``hiveplotlib.datasets.international_trade_data()``. Also added generalized runner with instructions for how to make
  additional trade datasets from the Harvard Growth Lab dataset in the hiveplotlib repository under
  ``./runners/make_trade_network_dataset.py``.

- Improved type hinting throughout the package.

- Fix / clarify some of the ``hiveplotlib.viz.datashader`` function docstrings.

- Formally add ``seaborn`` to dependencies for ``hiveplotlib[datashader]``. Switch default edge colormap for
  ``hiveplotlib.viz.datashader.datashade_edges_mpl()`` from ``"viridis"`` to the ``seaborn`` colormap used in the "Hive
  Plots for Large Networks" example notebook.

- All remaining ``np.random.seed()`` removed from repository (some of this was still in the test suite). These have all
  been replaced with ``rng = np.random.default_rng(0)``.

- Added notebook called "Hive Plots with More Than 3 Groups," documenting examples of the new ``None`` axis capability
  as well as other 4+ group hive plot options, specifically a multi-layered axes hive plots, and a
  Hive Plot Matrix (HPM).

- Major refurbishment of the "Bitcoin Trader Network" notebook.

Bug Fixes
~~~~~~~~~
- Fix to the ``hiveplotlib.viz.datashader`` functions, which did not correctly compose. If datashading onto an existing
  figure and axis without any hive plot axes, the image did not span the correct extent. Now, when datashading onto an
  existing figure and and axis, the datashader images will now always auto-scale to the extent of the hive plot axes.

Removed Deprecations
~~~~~~~~~~~~~~~~~~~~

- The ``_mpl`` named viz functions, which raised a ``DeprecationWarning`` in version ``0.20``, have been removed
  in version ``0.21``. For example, ``hiveplotlib.viz.hive_plot_viz_mpl()`` is now called
  ``hiveplotlib.viz.hive_plot_viz``. Although the ``matplotlib`` backend is available by default, different backends
  can be accessed within the ``hiveplotlib.viz`` module. For example an explicit ``matplotlib`` backend hive plot
  visualization can now be called via ``hiveplotlib.viz.matplotlib.hive_plot_viz()``, and a ``bokeh`` backend hive plot
  visualization can now be called via ``hiveplotlib.viz.bokeh.hive_plot_viz()``.

  .. note::
      ``hiveplotlib.viz.datashader.datashade_edges_mpl()`` and
      ``hiveplotlib.viz.datashader.datashade_nodes_mpl`` have been left with their ``_mpl`` naming for now,
      as datashading could eventually be done via multiple backends.

Version 0.20 (released Jan 19, 2023)
------------------------------------

Enhancements
~~~~~~~~~~~~
- ``hiveplotlib.viz.bokeh`` submodule of ``bokeh``-backend viz functions for hive plots and P2CPs added.

- ``hiveplotlib.viz`` default ``matplotlib``-backend viz functions have been renamed without the ``_mpl`` ending.
  ``matplotlib``-backend viz code is now contained in the
  ``hiveplotlib.viz.matplotlib`` sub-module, making the ``_mpl`` redundant. Default viz called under
  ``hiveplotlib.viz``, however, will still use the ``matplotlib`` backend.

- Improvements to ``hiveplotlib.viz`` docstrings.

- Exposed kwargs that allow the modification of axes labels in the ``hiveplotlib.viz`` module.

- ``hiveplotlib`` now being checked against ``flake8-docstrings``. (``flake8`` and documentation-building checks
  also incorporated into the CI pipeline). Many subtle documentation revisions from this change.

- ``hiveplotlib.viz`` functions that are exposed to *both* ``HivePlot`` and ``P2CP`` instances now
  use the ``hiveplotlib.viz.input_checks.input_check()`` call to validate proper inputs. This will scale better
  if the code ever supports additional instances or further checks on said instances. In the least,
  this made the viz code a little more DRY.

.. note::
    The ``_mpl`` named viz functions will raise a ``DeprecationWarning`` in version ``0.20``, with support to be removed
    in version ``0.21``.

    ``hiveplotlib.viz.datashader.datashade_edges_mpl()`` and
    ``hiveplotlib.viz.datashader.datashade_nodes_mpl`` have been left with their ``_mpl`` naming for now,
    as datashading could eventually be done via multiple backends.


Bug Fixes
~~~~~~~~~
- ``pytest`` warnings in the CI about a coming deprecation resolved.

Version 0.19 (released Sept 6, 2022)
------------------------------------

Enhancements
~~~~~~~~~~~~
- Added ``hiveplotlib.viz.datashader`` module added, plus documentation explaining and demonstrating datashading
  capabilities for both hive plots and P2CPs.

- ``hiveplotlib.viz.edge_viz_mpl()``, ``hiveplotlib.viz.hive_plot_viz_mpl()`` and ``hiveplotlib.viz.p2cp_viz_mpl()``
  now have an exposed ``tags`` parameter that allows plotting a subset of tags (with default behavior to plot all
  tags).

- Improved type hints for ``hiveplotlib.viz`` module.

- ``hiveplotlib.viz.p2cp_legend_mpl()`` revised ``tags_to_include`` parameter to ``tags`` to be more consistent with
  the rest of the API.

- License revised from GNU AGPL to BSD License (BSD-3-Clause).

Bug Fixes
~~~~~~~~~
- Major bug fix in ``hiveplotlib.viz.edge_viz_mpl()``, which made incorrect assumptions about how one can compose
  lines in ``matplotlib``. Revised to use ``LineCollection`` on the backend.

- Incorrect assumptions also found in notebooks demonstrating viz in other packages. These have been corrected.

- Fixed ``hiveplotlib.viz.axes_viz_mpl()`` not responding to ``mpl_axes_of=False`` use case.

Version 0.18 (released Aug 17, 2022)
------------------------------------

Enhancements
~~~~~~~~~~~~
- ``HivePlot.to_json()`` and ``P2CP.to_json()`` implemented.

- Documentation notebook showing how to visualize hive plots and P2CPs with other viz libraries off of the JSON output.

- Started this change log (and filled in a little bit of it looking back at some previous versions).

- Switched the spacers between discretized edges in the ``(n, 2)`` ``numpy`` arrays to being ``np.nan`` instead of
  ``None``. This allows us to preserve ``float``-typed arrays instead of being forced to switch to ``Object``-typed
  arrays, which speeds up the code and improves memory usage.

Bug Fixes
~~~~~~~~~
- ``HivePlot.add_edge_kwargs()`` doesn't need to be run with ``a2_to_a1=False`` to avoid errors with axis <-> repeat axis
  anymore.

- Generally more case control / useful warnings for ``HivePlot.add_edge_kwargs()`` when trying to put kwargs on impossible
  edge sets.

- Revised ``DeprecationWarning`` issues that were arising from using ``pytest.warns(None)`` throughout the test suite.

- Revised random draws in ``hiveplotlib.datasets.four_gaussian_blobs_3d()`` to use ``np.random.default_rng()`` instead
  of ``np.random.seed()``.

Version 0.17 (released Feb 8, 2022)
-----------------------------------
- A couple of bug fixes in ``hive_plot_n_axes()``, additional utility functions, pandas futurewarning resolved,
  lots more documentation.

Enhancements
~~~~~~~~~~~~
- Additional Utility Functions

Bug Fixes
~~~~~~~~~
- A couple of bug fixes in ``hive_plot_n_axes()``.

Version 0.16 (released Dec 21, 2021)
------------------------------------
- Started a change log!
- Added in `Polar Parallel Coordinate Plots (P2CPs) <https://arxiv.org/abs/2109.10193>`_.