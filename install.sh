#!/bin/bash
# kill any old, existing env and notebook kernel
bash uninstall.sh || echo "Failed to Uninstall"
# uv overwrites existing venv by default
uv venv --python 3.12
source .venv/bin/activate && \
  uv pip install -e .[bokeh,datashader,dev,docs,holoviews,networkx,plotly,ruff,testing] && \
  uv run ipython kernel install --user --name=hiveplotlib
