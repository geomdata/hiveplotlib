Hive Plots
----------

High-Level Hive Plot API
========================

.. autoclass:: hiveplotlib.NodeCollection
    :members:
.. autoclass:: hiveplotlib.Edges
    :members:
.. autoclass:: hiveplotlib.HivePlot
    :members:

Low-Level Hive Plot API
=======================

.. autoclass:: hiveplotlib.Node
    :members:
.. autoclass:: hiveplotlib.Axis
    :members:
.. autoclass:: hiveplotlib.BaseHivePlot
    :members:

(Deprecated) Quick Hive Plots
=============================

``hive_plot_n_axes()`` is deprecated and will be reomved from ``hiveplotlib`` in the ``0.28.0`` release.

.. autofunction:: hiveplotlib.hive_plot_n_axes

Converters
==========
.. automodule:: hiveplotlib.converters
   :members:

Utility Functions
=================

Node-Specific
~~~~~~~~~~~~~

Helper static methods for working with node data.

.. autofunction:: hiveplotlib.node.dataframe_to_node_list

.. autofunction:: hiveplotlib.node.node_collection_from_node_list

.. autofunction:: hiveplotlib.node.subset_node_collection_by_unique_ids

.. autofunction:: hiveplotlib.node.split_nodes_on_variable

Other
~~~~~

.. automodule:: hiveplotlib.utils
   :members:

Polar Parallel Coordinates Plots
--------------------------------

P2CP Class
==========
.. autoclass:: hiveplotlib.P2CP
   :members:

Quick P2CPs
================
.. autofunction:: hiveplotlib.p2cp_n_axes

Utility Functions
=================

Helper static methods for generating and working with ``P2CP`` instances.

.. autofunction:: hiveplotlib.p2cp.indices_for_unique_values

.. autofunction:: hiveplotlib.p2cp.split_df_on_variable

Visualization
-------------

Matplotlib
==========
.. automodule:: hiveplotlib.viz.matplotlib
   :members:

Bokeh
=====
.. automodule:: hiveplotlib.viz.bokeh
   :members:

Holoviews
=========
.. automodule:: hiveplotlib.viz.holoviews
   :members:

Plotly
======
.. automodule:: hiveplotlib.viz.plotly
   :members:

Datashader in Matplotlib
========================
.. automodule:: hiveplotlib.viz.datashader
   :members:

Example Datasets
----------------
.. automodule:: hiveplotlib.datasets
   :members:

Exceptions
----------
.. automodule:: hiveplotlib.exceptions
   :members: