"""
Script for making custom python badges.

Intended to be run from root of repository (``python ./.gitlab/make_badges.py``).
"""

from pybadges import badge

PACKAGES = {
    "holoviews": {
        "file": ".gitlab/hv_logo.png",
        "color": "#bb222c",
    },
    "matplotlib": {
        "file": ".gitlab/mpl_logo.png",
        "color": "#2B63FF",
    },
    "bokeh": {
        "file": ".gitlab/bokeh_logo.png",
        "color": "#00A650",
    },
    "plotly": {
        "file": ".gitlab/plotly_logo.png",
        "color": "#0F1113",
    },
    "datashader": {
        "file": ".gitlab/datashader_logo.png",
        "color": "#785099",
    },
}

for name in PACKAGES:
    badge_string = badge(
        left_text="",
        right_text=f"{name.capitalize()} Support",
        embed_logo=True,
        logo=PACKAGES[name]["file"],
        right_color=PACKAGES[name]["color"],
    )

    with open(f".gitlab/{name}_badge.svg", "w") as f:
        f.write(badge_string)

# python badge
python_badge_string = badge(
    left_text="Python",
    right_text="3.9 | 3.10 | 3.11 | 3.12 | 3.13",
    embed_logo=True,
    logo="https://dev.w3.org/SVG/tools/svgweb/samples/svg-files/python.svg",
)
with open(".gitlab/python_badge.svg", "w") as f:
    f.write(python_badge_string)
