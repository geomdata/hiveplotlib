"""Run some release steps. The official gitlab tool isn't quite flexible enough."""
# This file is adapted from GDA-Cookiecutter, version 2.1.9

import argparse
import os
import re

import gitlab  # type: ignore

parser = argparse.ArgumentParser()

ver_re = re.compile(
    r"v(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?"
)

parser.add_argument(
    "--list",
    action="store_true",
    help="List available releases, one per line, and exit. no 'tag' required.",
)

parser.add_argument(
    "--tag",
    type=str,
    default="",
    help=f"Release tag to check, create, or update. Should match {ver_re.pattern}",
)

parser.add_argument(
    "--is_new",
    action="store_true",
    help="Error unless release is new.",
)

parser.add_argument(
    "--create",
    action="store_true",
    help="Create a release.",
)

parser.add_argument(
    "--link_name",
    type=str,
    help="Add a link with this name.  Also need --link_url. Optional --link_type.",
)

parser.add_argument(
    "--link_url",
    type=str,
    help="Add a link with this url.  Also need --link_name. Optional --link_type.",
)

parser.add_argument(
    "--stable",
    action="store_true",
    help="Get the current stable release.",
)


args = parser.parse_args()

if (
    "CI_SERVER_URL" not in os.environ
    or "CI_JOB_TOKEN" not in os.environ
    or "CI_PROJECT_ID" not in os.environ
):
    raise ValueError(
        "Not in CI? You need the variables CI_SERVER_URL, CI_JOB_TOKEN, CI_PROJECT_ID all set."
    )

url = f"{os.environ['CI_SERVER_URL']}"
token = os.environ["CI_JOB_TOKEN"]
g = gitlab.Gitlab(url=url, job_token=token)  # use private_token if running outside CI
project = g.projects.get(os.environ["CI_PROJECT_ID"], lazy=True)


def get_releases() -> set:
    """Get the current set of release tags."""
    return {
        r.tag_name
        for r in project.releases.list(get_all=True)
        if ver_re.fullmatch(r.tag_name) is not None
    }


if args.stable:
    all_releases = [
        r
        for r in project.releases.list(get_all=True)
        if ver_re.fullmatch(r.tag_name) is not None
    ]
    all_releases.sort(key=lambda x: x.released_at, reverse=True)
    if all_releases:
        print(all_releases[0].tag_name)
    else:
        print("")
    exit(0)


if args.list:
    for r in get_releases():
        print(r)
    exit(0)

if ver_re.fullmatch(args.tag) is None:
    raise ValueError(f"Your tag {args.tag} does not match {ver_re.pattern}")

VER = args.tag[1:]
print(f"Using version {VER} with tag {args.tag}")

if args.is_new:
    if args.tag in get_releases():
        raise ValueError(f"Sorry. Release {args.tag} exists already.")
    exit(0)

if args.create:
    release = project.releases.create(
        {
            "ref": os.environ["CI_DEFAULT_BRANCH"],
            "name": args.tag,
            "tag_name": args.tag,
            "description": f"version {VER}",
        }
    )

if args.tag not in get_releases():
    raise ValueError(f"Cannot update release {args.tag}. It doesn't exist.")

release = project.releases.get(id=args.tag)

# Now we append on the release.
if args.link_name is not None or args.link_url is not None:
    if args.link_name is None or args.link_url is None:
        raise ValueError(
            "Both link_name and link_url must be specified. link_type is optional."
        )

    release.links.create(
        {
            "url": args.link_url,
            "name": args.link_name,
            "link_type": "other",
        }
    )
    print(f"link added: {args.link_name}")

print(release._links["self"])
