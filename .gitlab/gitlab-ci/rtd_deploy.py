"""
Deploy docs to ReadTheDocs and finish when deployment complete.
"""

import argparse
import sys
import time

import requests

parser = argparse.ArgumentParser()

parser.add_argument(
    "--token",
    type=str,
    help="API Token for ReadTheDocs.",
)

parser.add_argument(
    "--version",
    type=str,
    default="latest",
    help="Which version of ReadTheDocs to build (i.e. 'latest', 'stable', 'vX.Y.Z'). Default 'latest'.",
)

parser.add_argument(
    "--timeout",
    type=str,
    default="600",
    help="Number of seconds before job times out for CI failure. "
    "Note, the build job will keep running on ReadTheDocs until success or failure. "
    "Default times out after 600 seconds (10 minutes).",
)

args = parser.parse_args()


URL = f"https://readthedocs.org/api/v3/projects/hiveplotlib/versions/{args.version}/builds/"
HEADERS = {"Authorization": f"token {args.token}"}

# trigger job
job_response = requests.post(URL, headers=HEADERS)

print(f"Building docs for:\n{job_response.json()['build']['_links']['version']}")

# grab build status GET URL
build_url = job_response.json()["build"]["_links"]["_self"] + "?expand=config"

# track how long things are running to avoid infinite loop
start_time = time.time()

while True:
    # check our build status
    check = requests.get(build_url, headers=HEADERS)
    build_status = check.json()["state"]["name"]
    # print status for awareness
    print("Build Status: ", build_status)

    # job finished, either successfully or not
    if build_status == "Finished":
        # did the job succeed or fail (True / False)?
        success = check.json()["success"]
        print(f"Job Finished with success parameter = {success}")
        exit_status = 0 if success else 1
        break

    # cancelled jobs should fail pipeline
    elif build_status == "Cancelled":
        print("Job was cancelled")
        exit_status = 1
        break

    # add timeout to avoid unexpected corner case infinite loop
    elif time.time() - start_time >= int(args.timeout):
        print(f"Job timed out after more than {args.timeout} seconds")
        exit_status = 1
        break

    # sleep between checks to not spam
    time.sleep(10)

stop_time = time.time()
print(f"Job ran in {int(stop_time - start_time)} seconds")

# raise a successful (0) or fail (1) exit status so CI/CD knows job is complete
sys.exit(exit_status)
