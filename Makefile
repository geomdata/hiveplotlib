.PHONY: badges format docs test test-nb view-docs view-tests

.ONESHELL:
SHELL := /bin/bash

badges:
	source .venv/bin/activate
	python .gitlab/make_badges.py

format:
	source .venv/bin/activate
	uv pip install -U ruff
	ruff format
	ruff check --fix

cleandocs:
	rm -rf public
	rm -f docs/source/*.ipynb

docs: cleandocs
	source .venv/bin/activate
	bash build_sphinx_docs.sh

docs-strict: cleandocs
	source .venv/bin/activate
	bash build_sphinx_docs -W

install:
	bash install.sh

test:
	source .venv/bin/activate
	pytest -c pytest.ini

test-all: test view-tests test-nb view-tests-nb

test-nb:
	source .venv/bin/activate
	pytest -c tests/pytest_examples.ini

uninstall:
	bash uninstall.sh

view-docs:
	xdg-open public/index.html

view-tests:
	xdg-open data/pytest/report_hiveplotlib.html

view-tests-nb:
	xdg-open data/pytest/report_examples.html
