# edges.py
"""
``Edges`` instance and helper methods for edge construction.
"""

from typing import Hashable, Optional, Union

import numpy as np
import pandas as pd


class Edges:
    """
    Multi-edge aggregator with helper methods useful for downstream hive plots.

    An edge is specificed with respect to its starting node unique ID and ending node unique ID.

    The ``Edge`` class ingests an input ``pandas.DataFrame`` or ``(n, 2)`` ``numpy.ndarray`` (``data``) with
    specification for which data columns correspond to the starting node IDs (``from_column_name``) and ending node IDs
    (``to_column_name``).

    By providing a ``pandas.DataFrame`` input, additional edge metadata can be provided for later use (e.g. subsetting
    edges by metadata, keyword arguments for plotting edges with different thickness / color, etc.).

    Users can provide edge-plotting keyword arguments via the ``edge_viz_kwargs`` parameter in two ways.

    1. By providing a string value corresponding to a column name if a DataFrame is provided for edges, in which case
    that column data would be used for that plotting keyword argument in an ``edge_viz()`` call.

    2. By providing explicit keyword arguments (e.g. ``cmap="viridis"``), in which case that keyword argument would
    be used as-is in an ``edge_viz()`` call.

    ``edge_kwargs`` can also be updated (or overwritten) after instantiation via the
    :py:meth:`~hiveplotlib.Edges.update_edge_viz_kwargs()` method.

    :param data: data to store as edges.
    :param from_column_name: name of the edge origin column, whose values correspond to node IDs where a given edge
        starts.
    :param to_column_name: name of the edge destination column, whose values correspond to node IDs where a given edge
        ends.
    :param edge_viz_kwargs: keyword arguments to provide to an ``edge_viz()`` call. Users can provide names according
        to column names in the ``data`` attribute or explicit values, as discussed in (1) and (2) above.

    .. note::
        If providing an array input for the ``data`` parameter, then it is required that the first column be the
        starting node IDs and the second column be the ending node IDs.

        Array inputs will be stored in the ``data`` attribute as a ``pandas.DataFrame`` with column names ``"from"`` and
        ``"to"``.

        Provided keyword argument values will be checked *first* against column names in
        ``Edges.data`` (i.e. (1) above) before falling back to (2) and setting the keyword argument
        explicitly.

        The appropriate keyword argument names should be chosen as a function of your choice of visualization back
        end (e.g. ``matplotlib``, ``bokeh``, ``datashader``, etc.).
    """

    def __repr__(self) -> str:
        """
        Make printable representation (repr) for ``Edges`` instance.
        """
        return f"hiveplotlib.Edges of {self.data.shape[0]} edges."

    def __len__(self) -> int:
        """
        Allow ``len()`` to correspond to the number of nodes in the ``Edges``.

        :return: number of edges (i.e. rows) in ``Edges.data``.
        """
        return self.data.shape[0]

    def __init__(
        self,
        data: Union[pd.DataFrame, np.ndarray],
        from_column_name: Hashable = "from",
        to_column_name: Hashable = "to",
        edge_viz_kwargs: Optional[dict] = None,
    ) -> None:
        """Initialize."""
        if isinstance(data, np.ndarray):
            assert data.shape[1] == 2, (
                f"`numpy.ndarray` input `data` must be shape (n, 2) but input has {data.shape[1]} columns."
            )
            data = pd.DataFrame(data, columns=["from", "to"])

        self.data = data
        self.from_column_name = from_column_name
        self.to_column_name = to_column_name

        self.edge_viz_kwargs = {} if edge_viz_kwargs is None else edge_viz_kwargs.copy()

        # nested dictionary that stores boolean 1d arrays of which edges are relevant to which set of edges plotted
        #  edges are plotted in chunks of "from axis", "to axis", and "tag", so this stores 1 boolean array nested in
        #  `relevant_edges[from_axis_id][to_axis_id][tag]`
        self.relevant_edges = {}

    def export_edge_array(self) -> np.ndarray:
        """
        Return an ``(n, 2)`` array of [from, to] edges.

        :return: array of [from, to] edges.
        """
        return self.data.loc[:, [self.from_column_name, self.to_column_name]].to_numpy()

    def update_edge_viz_kwargs(
        self,
        reset_kwargs: bool = False,
        **edge_viz_kwargs,
    ) -> None:
        """
        Update keyword arguments for plotting edges in a ``edge_viz()`` call.

        Users can either provide values in two ways.

        1. By providing a string value corresponding to a column name, in which case that column data would be used for
        that plotting keyword argument in a ``edge_viz()`` call.

        2. By providing explicit keyword arguments (e.g. ``cmap="viridis"``), in which case that keyword argument would
        be used as-is in a ``edge_viz()`` call.

        .. note::
            Provided keyword argument values will be checked *first* against column names in
            ``Edges.data`` (i.e. (1) above) before falling back to (2) and setting the keyword argument
            explicitly.

            The appropriate keyword argument names should be chosen as a function of your choice of visualization back
            end (e.g. ``matplotlib``, ``bokeh``, ``datashader``, etc.).

        :param reset_kwargs: whether to drop the existing keyword arguments before adding the provided keyword arguments
            to the ``edge_viz_kwargs`` attribute. Existing values are preserved by default (i.e.
            ``reset_kwargs=False``).
        :param edge_viz_kwargs: keyword arguments to provide to a ``edge_viz()`` call. Users can provide names according
            to column names in the ``data`` attribute or explicit values, as discussed in (1) and (2) above.
        :return: ``None``.
        """
        if reset_kwargs:
            self.edge_viz_kwargs = edge_viz_kwargs
        else:
            self.edge_viz_kwargs |= edge_viz_kwargs
        return
