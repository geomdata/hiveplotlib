"""
Tests for the ``hiveplotlib.edges``.
"""

import numpy as np
import pandas as pd
import pytest

from hiveplotlib.datasets import example_edges, example_node_collection
from hiveplotlib.edges import Edges

pytestmark = pytest.mark.unmarked


class TestEdges:
    """
    Tests for ``hiveplotlib.Edges`` class.
    """

    def test_instantiate_edges(self) -> None:
        """
        Test instantiating a ``Edges`` instance.
        """
        node_collection = example_node_collection()
        example_edges(nodes=node_collection)
        assert True

    def test_repr(self) -> None:
        """
        Make sure ``__repr__`` is as expected.
        """
        num_edges = 27
        node_collection = example_node_collection()
        edges = example_edges(nodes=node_collection, num_edges=num_edges)
        assert f"{num_edges}" in edges.__repr__()

    def test_len(self) -> None:
        """
        Make sure ``__len__`` is as expected.
        """
        num_edges = 27
        node_collection = example_node_collection()
        edges = example_edges(nodes=node_collection, num_edges=num_edges)
        assert len(edges) == num_edges

    def test_export_edge_array(self) -> None:
        """
        Make sure ``export_edge_array()`` method behaves as expected.
        """
        from_edges = np.arange(10)
        to_edges = np.arange(10) + 10

        expected_edge_arr = np.column_stack([from_edges, to_edges])
        edge_df = pd.DataFrame(
            np.column_stack([to_edges, from_edges]), columns=["to", "from"]
        )
        edges = Edges(data=edge_df, from_column_name="from", to_column_name="to")
        assert np.array_equal(edges.export_edge_array(), expected_edge_arr)
