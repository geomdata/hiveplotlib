# viz_plotly_test.py

"""
Tests for ``hiveplotlib.viz.plotly.py``, the ``plotly``-backend visualization functions.
"""

import traceback
import warnings
from typing import Tuple, Union

import numpy as np
import pandas as pd
import pytest

from hiveplotlib import P2CP, BaseHivePlot, Node
from hiveplotlib.datasets import example_hive_plot
from hiveplotlib.exceptions import InvalidHoverVariableError
from hiveplotlib.node import subset_node_collection_by_unique_ids

pytestmark = pytest.mark.plotly


@pytest.mark.parametrize(
    "viz_function", ["axes_viz", "label_axes", "node_viz", "edge_viz", "hive_plot_viz"]
)
def test_viz_functions_run_hiveplot(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray], viz_function: str
) -> None:
    """
    Make sure the baseline viz functions can run without error when run for hive plots.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    import hiveplotlib.viz.plotly

    viz_function = getattr(hiveplotlib.viz.plotly, viz_function)
    hp, edges = three_axis_basehiveplot_example

    # hardcoded partition of nodes assignment, placed on axes
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["0", "1"]),
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["2", "3"]),
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="C",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["4", "5"]),
        sorting_feature_to_use="c",
        vmin=vmin,
        vmax=vmax,
    )

    # connect some edges
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="C")
    # throw in an edge kwarg for coverage
    hp.connect_axes(edges=edges, axis_id_1="C", axis_id_2="B", color="blue")

    try:
        viz_function(hp)
        viz_function(
            hp, axes_off=False
        )  # to get full testing coverage, make sure this runs too
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail(f"`{viz_function}` should have run, but failed for some reason.")


def test_fill_in_empty_edge_kwargs(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray],
) -> None:
    """
    Make sure entirely unspecified edge kwargs doesn't break ``hiveplotlib.viz.edge_viz()``.
    """
    from hiveplotlib.viz.plotly import edge_viz

    hp, edges = three_axis_basehiveplot_example

    # hardcoded partition of nodes assignment, placed on axes
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["0", "1"]),
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["2", "3"]),
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="C",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["4", "5"]),
        sorting_feature_to_use="c",
        vmin=vmin,
        vmax=vmax,
    )

    # connect some edges without ever touching kwargs
    #  (`HivePlot.connect_axes()` will add empty kwargs if none included)
    tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")
    hp.add_edge_curves_between_axes(axis_id_1="A", axis_id_2="B", tag=tag)

    try:
        edge_viz(instance=hp)
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail("`edge_viz` should have run, but failed for some reason.")


@pytest.mark.parametrize("viz_function", ["axes_viz", "label_axes"])
def test_warnings_no_axes_hive_plot(viz_function: str) -> None:
    """
    Make sure the baseline viz functions hits a warning when we are missing things when run for hive plots.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    import hiveplotlib.viz.plotly

    viz_function = getattr(hiveplotlib.viz.plotly, viz_function)
    hp = BaseHivePlot()

    with pytest.warns() as record:
        viz_function(hp)

    # confirm we get a single warning
    assert len(record) == 1, (
        f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"
    )


@pytest.mark.parametrize("viz_function", ["node_viz", "edge_viz", "hive_plot_viz"])
def test_warnings_missing_content_hive_plot(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray], viz_function: str
) -> None:
    """
    Make sure the baseline viz functions hits a warning when we are missing things when run for hive plots.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    import hiveplotlib.viz.plotly

    viz_function = getattr(hiveplotlib.viz.plotly, viz_function)
    hp, edges = three_axis_basehiveplot_example

    # only place nodes on 2 of the 3 axes, explicitly excluding one axis to get a warning
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["0", "1"]),
        sorting_feature_to_use="a",
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["2", "3"]),
        sorting_feature_to_use="b",
    )

    with pytest.warns() as record:
        viz_function(hp)

    # confirm we get a single warning
    if viz_function == hiveplotlib.viz.plotly.hive_plot_viz:
        assert len(record) == 2, (
            f"Expected 2 warnings for no edges AND no nodes, got:\n{[i.message.args for i in record]}"
        )
    else:
        assert len(record) == 1, (
            f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"
        )


def test_warnings_repeat_edge_kwarg(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray],
) -> None:
    """
    Make sure ``hiveplotlib.viz.edge_viz()`` hits a warning when including a kwarg redundant to existing kwargs.

    (To warn the user that their kwarg will be ignored in deference to the existing kwarg.)
    """
    from hiveplotlib.viz.plotly import edge_viz

    hp, edges = three_axis_basehiveplot_example
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
    hp.connect_axes(edges=edges, axis_id_1="C", axis_id_2="B")
    hp.connect_axes(
        edges=edges, axis_id_1="A", axis_id_2="C", a2_to_a1=False, color="blue"
    )
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="C", a1_to_a2=False)

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        edge_viz(instance=hp)

    with pytest.warns() as record:
        edge_viz(instance=hp, color="red")

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )


@pytest.mark.parametrize(
    "viz_function", ["axes_viz", "node_viz", "edge_viz", "p2cp_viz"]
)
def test_viz_functions_run_p2cp(
    three_axis_p2cp_example: P2CP, viz_function: str
) -> None:
    """
    Make sure the baseline viz functions can run without error when run for p2cps.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    import hiveplotlib.viz.plotly

    viz_function = getattr(hiveplotlib.viz.plotly, viz_function)
    p2cp = three_axis_p2cp_example

    try:
        with pytest.warns() as record:
            viz_function(
                p2cp, hover=True
            )  # make sure we warn as expected if inappropriately adding hover info here
        assert len(record) == 1, (
            f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
        )
        assert "Hover info not yet supported for P2CPs" in record[0].message.args[0]
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail(f"`{viz_function}` should have run, but failed for some reason.")


@pytest.mark.parametrize("tags", [None, "two", ["two"], ["one", "three"]])
def test_p2cp_legend(
    three_axis_p2cp_example: P2CP, tags: Union[str, list, None]
) -> None:
    """
    Make sure the p2cp viz functions can run without error when run for p2cps.

    :param tags: which tags of data to add to the p2cp.
    """
    from hiveplotlib.viz.plotly import p2cp_legend, p2cp_viz

    p2cp = three_axis_p2cp_example
    p2cp.build_edges(tag="one")
    p2cp.build_edges(tag="two")
    p2cp.build_edges(tag="three", width=0.5)
    fig = p2cp_viz(p2cp, tags=tags)
    try:
        p2cp_legend(p2cp, fig=fig, tags=tags)
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail("`p2cp_legend` should run, but failed for some reason.")


def test_warnings_repeat_edge_kwarg_p2cp(three_axis_p2cp_example: P2CP) -> None:
    """
    Make sure ``hiveplotlib.viz.edge_viz()`` hits a warning when including a kwarg redundant to existing kwargs.

    (To warn the user that their kwarg will be ignored in deference to the existing kwarg.)
    """
    from hiveplotlib.viz.plotly import edge_viz

    p2cp = three_axis_p2cp_example
    p2cp.add_edge_kwargs(color="blue")

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        edge_viz(
            p2cp,
            hover=False,  # no need to test hover here
        )

    with pytest.warns() as record:
        edge_viz(
            instance=p2cp,
            color="red",
            hover=False,  # no need to test hover here
        )

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )


@pytest.mark.parametrize(
    "viz_function", ["axes_viz", "label_axes", "node_viz", "edge_viz"]
)
def test_expected_errors_wrong_types(viz_function: str) -> None:
    """
    Viz functions should yell if tried on non-``P2CP`` or ``HivePlot`` instances.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    import hiveplotlib.viz.plotly

    viz_function = getattr(hiveplotlib.viz.plotly, viz_function)
    node = Node(unique_id=10)

    try:
        viz_function(node)
        pytest.fail(
            f"`{viz_function}` should have failed due to invalid input, but it ran."
        )
    # supposed to fail
    except NotImplementedError:
        assert True


def test_warnings_node_viz_p2cp() -> None:
    """
    Make sure ``hiveplotlib.viz.node_viz()`` hits a single warning when nodes are unspecified.

    (If done correctly, this avoids sending a P2CP user a hive plot-specific internal warning that shouldn't
    matter. Even worse, if done wrong, the user would get the same warning *per axis* hence the check for a *single*
    warning.)
    """
    from hiveplotlib.viz.plotly import node_viz

    rng = np.random.default_rng(0)

    num_nodes = 50

    # a and b columns move in sync with eventual node id (index), c is random
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            rng.uniform(low=0, high=10, size=num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    p2cp = P2CP(data=data)

    with pytest.warns() as record:
        node_viz(
            instance=p2cp,
            hover=False,  # no need to test hover here
        )

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )

    # set the axes, then should be fine
    p2cp.set_axes(list(data.columns.values))

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        node_viz(
            p2cp,
            hover=False,  # no need to test hover here
        )


@pytest.mark.parametrize("viz_function", ["axes_viz", "label_axes"])
def test_warnings_no_axes_p2cp(viz_function: str) -> None:
    """
    Make sure the baseline viz functions hits a warning when we are missing things when run for p2cps.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    import hiveplotlib.viz.plotly

    viz_function = getattr(hiveplotlib.viz.plotly, viz_function)
    p2cp = P2CP()

    with pytest.warns() as record:
        viz_function(p2cp)

    # confirm we get a single warning
    assert len(record) == 1, (
        f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"
    )


def test_label_axes_alignments() -> None:
    """
    Make sure ``hiveplotlib.viz.label_axes()`` does a "reasonable" alignment of axes labels for many angles.

    Note, this test exists primarily for two reasons: 1) test coverage and 2) a quick reference to make a scalable,
    multi-axis figure to observe all the alignments.
    """
    from hiveplotlib.viz.plotly import axes_viz

    rng = np.random.default_rng(0)

    num_nodes = 50

    # number of axes on final p2cp
    num_axes = 26

    # axes names will be strings of integers 0, 1, 2, ...
    #  to make names longer (to see how alignment changes), repeat each string in each title this many times
    num_repeats_per_title = 5

    np_data = [np.sort(rng.uniform(low=0, high=10, size=num_nodes))] * num_axes
    data = pd.DataFrame(
        np.column_stack(np_data),
        columns=[i * num_repeats_per_title for i in np.arange(num_axes).astype(str)],
    )

    p2cp = P2CP(data=data)

    p2cp.set_axes(data.columns.values)

    try:
        axes_viz(
            p2cp,
            hover=False,  # no need to test hover here
        )
        assert True
    except:  # noqa: E722
        pytest.fail("`axes_viz` failed for some reason, but should have worked.")


def test_warnings_edge_viz_p2cp() -> None:
    """
    Make sure ``hiveplotlib.viz.edge_viz()`` hits a single warning when nodes are unspecified.

    (If done correctly, this avoids sending a P2CP user a hive plot-specific internal warning that shouldn't
    matter. Even worse, if done wrong, the user would get the same warning *per axis pair* hence the check for a
    *single* warning.)
    """
    from hiveplotlib.viz.plotly import edge_viz

    rng = np.random.default_rng(0)

    num_nodes = 50

    # a and b columns move in sync with eventual node id (index), c is random
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            rng.uniform(low=0, high=10, size=num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    p2cp = P2CP(data=data)
    p2cp.set_axes(list(data.columns.values))

    with pytest.warns() as record:
        edge_viz(
            instance=p2cp,
            hover=False,  # no need to test hover here
        )

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )

    # build the edges, then should be fine
    p2cp.build_edges()

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        edge_viz(
            p2cp,
            hover=False,  # no need to test hover here
        )


def test_hiveplot_viz_backend_plotly() -> None:
    """
    Test that we can call the viz backend set as ``"plotly"`` on a ``HivePlot`` instance.
    """
    hp = example_hive_plot(
        backend="plotly",
    )
    hp.plot()
    assert True


def test_plot_empty_hive_plot() -> None:
    """
    Test outputs when plotting an empty (no axes) ``HivePlot`` instance.
    """
    hp = example_hive_plot(
        backend="plotly",
    )
    # kill all existing axes in hive plot (thus killing all edges and nodes too)
    hp.set_partition(
        "partition_0",
        sorting_variables="low",
        build_hive_plot=False,
    )

    with pytest.warns() as record:
        out = hp.plot()
    assert len(record) == 4, (
        "Expected only 4 warnings ('intermediate changes', no axes, no edges, no nodes), "
        f"got warnings of:\n{[i.message.args for i in record]}"
    )
    for i in record:
        assert (
            "Run the `build_hive_plot()` method on your `HivePlot` instance"
            in i.message.args[0]
            or "`HivePlot.build_hive_plot()`" in i.message.args[0]
        ), f"{i.message.args}"

    import plotly.graph_objects as go

    assert isinstance(out, go.Figure)


@pytest.mark.parametrize("update_node_viz_kwargs", [True, False])
def test_plot_custom_node_kwargs_from_data(update_node_viz_kwargs: bool) -> None:
    """
    Test plotting a hive plot where we update the node viz kwargs with data and non-data values.

    This test is mainly for coverage and a reference for making a highly-interpretable visual example of changing node
    kwargs.

    :param update_node_viz_kwargs: whether to update the node viz kwargs via ``HivePlot.nodes.update_node_viz_kwargs()``
        or to call the node kwarg update via the ``node_kwargs`` parameter in ``HivePlot.plot()``.
    """
    hp = example_hive_plot(
        repeat_axes=True,
        backend="plotly",
    )

    hp.nodes.data["size"] = hp.nodes.data["low"].to_numpy() + 5

    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()

    node_kwargs = {
        "line_color": "black",
        "color": "low",  # based on data
        "size": "size",  # reference to "size" variable in node data
        "colorscale": "Magma",
        "cmin": 0,
        "cmax": 10,
        "showscale": True,
        "colorbar": {
            "title": "Node Variable: 'Low'",
        },
    }

    if update_node_viz_kwargs:
        hp.update_node_viz_kwargs(**node_kwargs)

        fig = hp.plot(
            # fig_kwargs={"width": 650, "height": 600},  # when including color bar, make wider to maintain 1:1 aspect
        )
    else:
        fig = hp.plot(node_kwargs=node_kwargs)

    node_scatter_pts = [i for i in fig.data if i.mode == "markers"]

    for node_scatter_pt in node_scatter_pts:
        assert node_scatter_pt.marker.cmin == node_kwargs["cmin"]
        assert node_scatter_pt.marker.cmax == node_kwargs["cmax"]
        assert node_scatter_pt.marker.line.color == node_kwargs["line_color"]
        assert node_scatter_pt.marker.showscale == node_kwargs["showscale"]
        assert len(set(node_scatter_pt.marker.size.tolist())) > 1, (
            "Should have multiple sizes for multiple nodes"
        )


@pytest.mark.parametrize("update_edge_viz_kwargs", [True, False])
def test_plot_custom_edge_kwargs_from_data(update_edge_viz_kwargs: bool) -> None:
    """
    Test plotting a hive plot where we update the edge viz kwargs with data and non-data values.

    This test is mainly for coverage and a reference for making a highly-interpretable visual example of changing edge
    kwargs.

    :param update_edge_viz_kwargs: whether to update the edge viz kwargs via ``HivePlot.update_edge_viz_kwargs()``
        or to call the edge kwarg update via the ``edge_kwargs`` parameter in ``HivePlot.plot()``.
    """
    import matplotlib as mpl
    # import plotly.graph_objects as go

    hp = example_hive_plot(
        repeat_axes=True,
        backend="plotly",
    )

    # need to pre-compute our line colors to work with plotly backend
    cmap = mpl.colormaps.get_cmap("cividis")
    norm = mpl.colors.Normalize(vmin=0, vmax=10)
    color_list = [mpl.colors.rgb2hex(cmap(norm(val))) for val in hp.edges.data["low"]]

    hp.edges.data["width"] = hp.edges.data["low"] / 3
    hp.edges.data["color"] = (
        color_list  # have to manually calculate the color strings first
    )
    hp.edges.data["opacity"] = [norm(val) for val in hp.edges.data["low"]]

    # propagate extra edge data changes through to data on axes
    hp.build_hive_plot()

    edge_kwargs = {
        "color": "color",  # based on data, but color strings must be created above
        "line_width": "width",  # reference to custom width handling in hiveplotlib.viz.plotly module
        "opacity": "opacity",  # reference to custom opacity handling in hiveplotlib.viz.plotly module
    }

    if update_edge_viz_kwargs:
        hp.update_edge_viz_kwargs(**edge_kwargs)

        fig = hp.plot(
            # width=650, height=600,  # when including color bar, make wider to maintain 1:1 aspect
        )
    else:
        fig = hp.plot(**edge_kwargs)

    # manually add colorbar
    # colorbar_trace = go.Scatter(
    #     x=[None],
    #     y=[None],
    #     mode="markers",
    #     marker={
    #         "colorscale": "cividis",
    #         "showscale": True,
    #         "cmin": 0,
    #         "cmax": 10,
    #         "colorbar": {"title": "Edge Variable: 'Low'"},
    #     },
    # )
    # fig.add_trace(colorbar_trace)

    line_scatter_pts = [i for i in fig.data if i.mode == "lines"]

    # all lines are separately customized
    #  so just testing that line colors and widths are different between a couple edges
    assert line_scatter_pts[0].line.width != line_scatter_pts[1].line.width, (
        "Should have different widths for each line."
    )

    def rgba_string_to_tuple(rgba_string: str) -> tuple[float, float, float, float]:
        """Convert an RGBA string to a tuple of floats."""
        import re

        match = re.match(r"rgba\((.*)\)", rgba_string)
        if match:
            values = match.group(1).split(",")
            return tuple(float(value.strip()) for value in values)
        raise ValueError("Invalid RGBA string format")

    line_color_0 = rgba_string_to_tuple(line_scatter_pts[0].line.color)
    line_color_1 = rgba_string_to_tuple(line_scatter_pts[1].line.color)

    assert line_color_0[:3] != line_color_1[:3], (
        "Should have different colors for each line."
    )
    assert line_color_0[-1] != line_color_1[-1], (
        "Should have different opacity for each line."
    )


def test_plotting_edge_kwarg_hierarchy_overwrite_data_kwargs() -> None:
    """
    Test that plotting kwargs from various edge kwargs play nice together.

    Specifically, test the interaction of an ``update_edges()`` call and the ``edge_viz_kwargs`` attribute edge kwargs,
    with overwriting of edge data-based kwargs.
    """
    import matplotlib as mpl

    from hiveplotlib.viz.plotly import _opacity_color_handler

    hp = example_hive_plot(repeat_axes=True, backend="plotly")

    cmap = mpl.colormaps.get_cmap("cividis")
    norm = mpl.colors.Normalize(vmin=0, vmax=10)
    color_list = [mpl.colors.rgb2hex(cmap(norm(val))) for val in hp.edges.data["low"]]

    hp.edges.data["linewidth"] = hp.edges.data["low"] / 3
    hp.edges.data["opacity"] = hp.edges.data["low"] / 20
    hp.edges.data["color"] = color_list

    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()

    edge_kwargs = {
        "color": "color",  # based on data, but must be created above
        "line_width": "linewidth",  # reference to "linewidth" variable in edge data
        "opacity": 0.6,
    }

    hp.update_edge_viz_kwargs(**edge_kwargs)

    red_kwargs = {"color": "red", "line_width": 2}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a1_to_a2=False,
        #     short_arc=False,
        #     control_rho_scale=2.4,
        #     control_angle_shift=30,
        **red_kwargs,
    )
    blue_kwargs = {"color": "blue", "line_width": 10}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a2_to_a1=False,
        control_rho_scale=1.2,
        #     control_angle_shift=-30,
        **blue_kwargs,
    )

    fig = hp.plot()

    line_scatter_pts = [i for i in fig.data if i.mode == "lines"]

    for line_scatter_pt in line_scatter_pts:
        color = _opacity_color_handler(
            color=line_scatter_pt.line.color,
            opacity=edge_kwargs["opacity"],
        )
        if color == _opacity_color_handler(
            color="red",
            opacity=edge_kwargs["opacity"],
        ):
            assert line_scatter_pt.line.width == red_kwargs["line_width"]
        elif color == _opacity_color_handler(
            color="blue",
            opacity=edge_kwargs["opacity"],
        ):
            assert line_scatter_pt.line.width == blue_kwargs["line_width"]
        elif color == _opacity_color_handler(
            color="black",
            opacity=1.0,
        ):
            # these are hive plot axes
            continue
        else:
            assert line_scatter_pt.line.width not in [
                blue_kwargs["line_width"],
                red_kwargs["line_width"],
            ], (
                "Should have custom, data-based line width for this edge. "
                f"Found color {color} and width {line_scatter_pt.line.width}"
            )


def test_plotting_edge_kwarg_hierarchy_plot_overwrite() -> None:
    """
    Test that plotting kwargs from various edge kwargs play nice together.

    Specifically, test the interaction of an ``update_edges()`` call, the ``edge_viz_kwargs`` attribute, and ``plot()``
    edge kwargs.
    """
    from hiveplotlib.viz.plotly import _opacity_color_handler

    hp = example_hive_plot(repeat_axes=True, backend="plotly")

    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()

    edge_kwargs = {
        "color": "green",
        "line_width": 1,
        "opacity": 0.6,
    }

    hp.update_edge_viz_kwargs(**edge_kwargs)

    red_kwargs = {"color": "red", "line_width": 2}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a1_to_a2=False,
        #     short_arc=False,
        #     control_rho_scale=2.4,
        #     control_angle_shift=30,
        **red_kwargs,
    )
    blue_kwargs = {"color": "blue", "line_width": 10}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a2_to_a1=False,
        control_rho_scale=1.2,
        #     control_angle_shift=-30,
        **blue_kwargs,
    )

    with pytest.warns() as record:
        fig = hp.plot(line_width=1)

    # confirm we get two warnings
    assert len(record) == 2, (
        f"Expected only 2 warnings, got warnings of:\n{[i.message.args for i in record]}"
    )

    for r in record:
        assert "line_width" in r.message.args[0], (
            "warning should yell about repeated line_width not being overwritten."
        )

    line_scatter_pts = [i for i in fig.data if i.mode == "lines"]

    for line_scatter_pt in line_scatter_pts:
        color = _opacity_color_handler(
            color=line_scatter_pt.line.color,
            opacity=edge_kwargs["opacity"],
        )
        if color == _opacity_color_handler(
            color="red",
            opacity=edge_kwargs["opacity"],
        ):
            assert line_scatter_pt.line.width == red_kwargs["line_width"]
        elif color == _opacity_color_handler(
            color="blue",
            opacity=edge_kwargs["opacity"],
        ):
            assert line_scatter_pt.line.width == blue_kwargs["line_width"]
        elif color == _opacity_color_handler(
            color="green",
            opacity=edge_kwargs["opacity"],
        ):
            assert line_scatter_pt.line.width == edge_kwargs["line_width"]
        elif color == _opacity_color_handler(
            color="black",
            opacity=1.0,
        ):
            # these are hive plot axes
            continue
        else:
            pytest.fail(
                f"Should have had red, blue, or green edges only, but got {line_scatter_pt.line.color}"
            )


@pytest.mark.parametrize(
    "hover",
    [
        True,
        False,
        "nodes",
        "edges",
        "axes",
        ["nodes", "edges"],
        ["axes"],
        "axis",
        ["axis", "nodes"],
    ],
)
def test_hive_plot_hover_tools(
    hover: Union[str, bool, list[str]],
) -> None:
    """
    Test plotting hive plot with possible (and impossible) ``"hover"`` settings.

    :param hover: hover setting.
    """
    hp = example_hive_plot(backend="plotly")

    if hover == "axis" or hover == ["axis", "nodes"]:
        try:
            fig = hp.plot(hover=hover)
            pytest.fail(
                "Should have raised `InvalidHoverVariableError` error (needs to be 'axes' not 'axis')."
            )
        except InvalidHoverVariableError:
            assert True

    else:
        fig = hp.plot(hover=hover)

        for glyph in fig.data:
            if hover is False:
                assert glyph.hovertemplate is None, (
                    "Should not have any hover tooltips when `hover=False`"
                )
            elif hover == "nodes":
                if glyph.mode == "markers":
                    assert "Node: " in glyph.hovertemplate, (
                        "Should have hover tooltip for node when `hover='nodes'`"
                    )
                else:
                    assert glyph.hovertemplate is None, (
                        "Should not have any hover tooltips when `hover='nodes` for non-node glyph"
                    )
            elif hover == "edges":
                if glyph.mode == "lines" and glyph.x.size > 2:
                    assert "Edge: " in glyph.hovertemplate, (
                        "Should have hover tooltip for edge when `hover='edges'`"
                    )
                else:
                    assert glyph.hovertemplate is None, (
                        "Should not have any hover tooltips when `hover='edges` for non-edge glyph"
                    )
            elif hover == "axes" or hover == ["axes"]:
                if glyph.mode == "text":
                    assert "Axis: " in glyph.hovertemplate, (
                        "Should have hover tooltip for axis text when `hover='axes'`"
                    )
                else:
                    assert glyph.hovertemplate is None, (
                        "Should not have any hover tooltips when `hover='axes` for non-axis text glyph"
                    )
            elif hover is True:
                if glyph.mode == "markers":
                    assert "Node: " in glyph.hovertemplate, (
                        "Should have hover tooltip for node when `hover=True`"
                    )
                elif glyph.mode == "lines" and glyph.x.size > 2:
                    assert "Edge: " in glyph.hovertemplate, (
                        "Should have hover tooltip for edge when `hover=True`"
                    )
                elif glyph.mode == "lines" and glyph.x.size == 2:
                    assert glyph.hovertemplate is None, (
                        "Should have no hover tooltip for axis when `hover=True`; hover info should be on axis text"
                    )
                elif glyph.mode == "text":
                    assert "Axis: " in glyph.hovertemplate, (
                        "Should have hover tooltip for axis text when `hover=True`"
                    )
                else:
                    raise NotImplementedError(
                        f"Shouldn't have any other glyph types, but got {glyph}"
                    )
            elif hover == ["nodes", "edges"]:
                if glyph.mode == "markers":
                    assert "Node: " in glyph.hovertemplate, (
                        "Should have hover tooltip for node when `hover=['nodes', 'edges']`"
                    )
                elif glyph.mode == "lines" and glyph.x.size > 2:
                    assert "Edge: " in glyph.hovertemplate, (
                        "Should have hover tooltip for node when `hover=['nodes', 'edges']`"
                    )
                elif glyph.mode == "lines" and glyph.x.size == 2:
                    assert glyph.hovertemplate is None, (
                        "Should have no hover tooltip for axis when `hover=['nodes', 'edges']` for axis glyphs; "
                        "hover info should be on axis text if at all"
                    )
                elif glyph.mode == "text":
                    assert glyph.hovertemplate is None, (
                        "Should have no hover tooltip for axis text when `hover=['nodes', 'edges']` for axis glyphs"
                    )
                else:
                    raise NotImplementedError(
                        f"Shouldn't have any other glyph types, but got {glyph}"
                    )
            else:
                raise NotImplementedError
