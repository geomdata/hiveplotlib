"""
Testing to make sure ``pyproject.toml`` has dependencies as expected.
"""

import toml


def test_dev_dependencies_coverage() -> None:
    """
    Test to confirm our [dev] dependencies cover all non-testing / docs / linting packages.

    This allows us to promise developers can always install ``[dev]`` to be able to test anything.

    (And guarantees unit tests will work as expected in CI.)
    """
    keys_to_ignore = ["docs", "ruff", "testing"]
    optional_dependencies = toml.load("./pyproject.toml")["project"][
        "optional-dependencies"
    ]
    dev_deps = set(optional_dependencies["dev"])
    other_deps = []
    for k in optional_dependencies:
        if k not in keys_to_ignore + ["dev"]:
            other_deps += optional_dependencies[k]
    other_deps = set(other_deps)
    assert len(other_deps.difference(dev_deps)) == 0, (
        f"All other deps in optional-dependencies (excluding {keys_to_ignore}) should be covered by [dev]\n"
        f"The following dependencies are not in dev but should be: {other_deps.difference(dev_deps)}"
    )
