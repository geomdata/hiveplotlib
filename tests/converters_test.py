# converters_test.py

"""
Tests for ``hiveplotlib.converters.py``.
"""

import numpy as np
import pytest

pytestmark = pytest.mark.networkx


def test_networkx_to_nodes_edges() -> None:
    """
    Make sure ``hiveplotlib.converters.networkx_to_nodes_edges()`` behaves as expected.
    """
    import networkx as nx

    from hiveplotlib.converters import networkx_to_nodes_edges

    num_nodes = 5

    # single chain graph with degree 1, 2, ..., 2, 1 (depending on size)
    graph = nx.path_graph(num_nodes)
    # add degree info to nodes before converting
    degrees = dict(graph.degree)
    for k in degrees:
        graph.nodes.data()[k]["degree"] = degrees[k]

    nodes, edges = networkx_to_nodes_edges(graph)

    assert len(nodes) == num_nodes
    assert edges.shape[0] == num_nodes - 1

    # edges should go 0 -> 1, 1 -> 2, etc.
    assert list(np.unique(edges[:, 1] - edges[:, 0])) == [1], (
        "Each edge set should be [i, i + 1]"
    )

    # check node data is as expected
    node_data = [n.data for n in nodes]

    keys = [list(k.keys()) for k in node_data]

    assert list(np.unique(keys)) == ["degree"], "Should have only added one key of data"

    degrees = [n.data["degree"] for n in nodes]

    assert degrees[0] == 1, "First degree measures should be 1 in a path graph."

    assert degrees[-1] == 1, "Last degree measures should be 1 in a path graph."

    assert list(np.unique(degrees[1:-1])) == [2], "Middle degree measures should be 2"
