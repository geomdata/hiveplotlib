# hiveplot_test.py

"""
Tests for ``hiveplotlib.HivePlot`` and ``hiveplotlib.hive_plot_n_axes``.
"""

import json
import warnings
from itertools import combinations
from typing import Literal, Tuple, Union

import numpy as np
import pandas as pd
import pytest
from matplotlib import pyplot as plt

from hiveplotlib import (
    Axis,
    BaseHivePlot,
    HivePlot,
    Node,
    NodeCollection,
    hive_plot_n_axes,
)
from hiveplotlib.datasets import (
    example_base_hive_plot,
    example_edges,
    example_hive_plot,
    example_node_collection,
    example_node_data,
    example_nodes_and_edges,
)
from hiveplotlib.exceptions import (
    InvalidAxesOrderError,
    InvalidAxisNameError,
    InvalidEdgeKwargHierarchyError,
    InvalidPartitionVariableError,
    InvalidSortingVariableError,
    MissingSortingVariableError,
    RepeatInPartitionAxisNameError,
)
from hiveplotlib.node import (
    node_collection_from_node_list,
    subset_node_collection_by_unique_ids,
)
from hiveplotlib.utils import cartesian2polar, polar2cartesian

pytestmark = pytest.mark.unmarked


def three_axis_basehiveplot_example() -> Tuple[BaseHivePlot, np.ndarray]:
    """
    3 axis example for use in multiple tests later and simulated edges.

    (No edges added yet.)

    :return: ``BaseHivePlot`` instance, (n, 2) numpy ndarray of edges (ids).
    """
    hp = BaseHivePlot()

    # build nodes
    node0 = Node(unique_id="0", data={"a": 1, "b": 2, "c": 3})
    node1 = Node(unique_id="1", data={"a": 2, "b": 3, "c": 4})
    node2 = Node(unique_id="2", data={"a": 3, "b": 4, "c": 5})
    node3 = Node(unique_id="3", data={"a": 4, "b": 5, "c": 6})
    node4 = Node(unique_id="4", data={"a": 5, "b": 6, "c": 7})
    node5 = Node(unique_id="5", data={"a": 6, "b": 7, "c": 8})

    nodes = [node0, node1, node2, node3, node4, node5]

    hp.add_nodes(nodes)

    # build axes
    axis0 = Axis(axis_id="A", start=1, end=5, angle=0)
    axis1 = Axis(axis_id="B", start=1, end=5, angle=120)
    axis2 = Axis(axis_id="C", start=1, end=5, angle=240)

    axes = [axis0, axis1, axis2]

    hp.add_axes(axes)

    # hardcoded partition of nodes assignment for now
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["0", "1"]),
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["2", "3"]),
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="C",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["4", "5"]),
        sorting_feature_to_use="c",
        vmin=vmin,
        vmax=vmax,
    )

    # dummy of from, to pairs
    rng = np.random.default_rng(0)
    edges = rng.choice(np.arange(5).astype(str), size=100).reshape(-1, 2)

    return hp, edges


def two_axis_zero_crossing_basehiveplot_example() -> Tuple[BaseHivePlot, np.ndarray]:
    """
    2 axis example with axes on either side of 0 degrees.

    :return: ``BaseHivePlot`` instance, (n, 2) numpy ndarray of edges (ids).
    """
    hp = BaseHivePlot()

    # build nodes
    node0 = Node(unique_id="0", data={"a": 1, "b": 2, "c": 3})
    node1 = Node(unique_id="1", data={"a": 2, "b": 3, "c": 4})
    node2 = Node(unique_id="2", data={"a": 3, "b": 4, "c": 5})
    node3 = Node(unique_id="3", data={"a": 4, "b": 5, "c": 6})
    node4 = Node(unique_id="4", data={"a": 5, "b": 6, "c": 7})
    node5 = Node(unique_id="5", data={"a": 6, "b": 7, "c": 8})

    nodes = [node0, node1, node2, node3, node4, node5]

    hp.add_nodes(nodes)

    # build axes
    axis0 = Axis(axis_id="A", start=1, end=5, angle=20)
    axis1 = Axis(axis_id="B", start=1, end=5, angle=340)

    axes = [axis0, axis1]

    hp.add_axes(axes)

    # hardcoded partition of nodes assignment for now
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["0", "1", "2"]),
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["3", "4", "5"]),
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )

    # dummy of from, to pairs
    rng = np.random.default_rng(0)
    edges = rng.choice(np.arange(5).astype(str), size=100).reshape(-1, 2)

    return hp, edges


def repeated_nodes_basehiveplot_example() -> Tuple[BaseHivePlot, np.ndarray]:
    """
    2 axis example with some repeated nodes on either side of 0 degrees.

    :return: ``BaseHivePlot`` instance, (n, 2) numpy ndarray of edges (ids).
    """
    hp = BaseHivePlot()

    # build nodes
    node0 = Node(unique_id="0", data={"a": 1, "b": 2, "c": 3})
    node1 = Node(unique_id="1", data={"a": 2, "b": 3, "c": 4})
    node2 = Node(unique_id="2", data={"a": 3, "b": 4, "c": 5})
    node3 = Node(unique_id="3", data={"a": 4, "b": 5, "c": 6})
    node4 = Node(unique_id="4", data={"a": 5, "b": 6, "c": 7})
    node5 = Node(unique_id="5", data={"a": 6, "b": 7, "c": 8})

    nodes = [node0, node1, node2, node3, node4, node5]

    hp.add_nodes(nodes)

    # build axes
    axis0 = Axis(axis_id="A", start=1, end=5, angle=20)
    axis1 = Axis(axis_id="B", start=1, end=5, angle=340)

    axes = [axis0, axis1]

    hp.add_axes(axes)

    # hardcoded partition of nodes with overlap
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(
            hp.nodes, ids=["0", "1", "2", "3"]
        ),
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(
            hp.nodes, ids=["2", "3", "4", "5"]
        ),
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )

    # dummy of from, to pairs
    rng = np.random.default_rng(0)
    edges = rng.choice(np.arange(5).astype(str), size=100).reshape(-1, 2)

    return hp, edges


def repeat_axis_basehiveplot_example() -> Tuple[BaseHivePlot, np.ndarray]:
    """
    Hive plot example with 2 axes, 1 a repeat of the other.

    :return: ``BaseHivePlot`` instance, (n, 2) numpy ndarray of edges (ids).
    """
    hp = BaseHivePlot()

    # build axes

    axis0 = Axis(axis_id="A", start=1, end=5, angle=40)
    axis1 = Axis(axis_id="B", start=1, end=5, angle=320)

    axes = [axis0, axis1]

    hp.add_axes(axes)

    # build nodes

    num_nodes = 100
    rng = np.random.default_rng(0)

    nodes = []
    for i in range(num_nodes):
        temp_node = Node(
            unique_id=str(i),
            data={"a": rng.uniform(), "b": rng.uniform(), "c": rng.uniform()},
        )
        nodes.append(temp_node)
    hp.add_nodes(nodes)

    node_ids = list(np.arange(num_nodes).astype(str))

    # repeated nodes
    vmin = 0
    vmax = 1
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=node_ids),
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=node_ids),
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )

    # dummy of from, to pairs
    num_edges = 10
    edges = rng.choice(np.arange(num_nodes).astype(str), size=num_edges * 2).reshape(
        -1, 2
    )

    return hp, edges


class TestBaseHivePlot:
    """
    Tests for hiveplotlib.BaseHivePlot.
    """

    def test_add_axes_example(self) -> None:
        """
        Check that ``BaseHivePlot.add_axes()`` behaves as expected on a "nice" example.
        """
        axis0 = Axis(axis_id="a0", angle=10)
        axis1 = Axis(axis_id="a1", angle=30)

        hp = BaseHivePlot()
        hp.add_axes([axis0, axis1])

        assert hp.axes["a0"].angle == 10
        assert hp.axes["a1"].angle == 30

    def test_add_axes_hostile_input(self) -> None:
        """
        Check that ``BaseHivePlot.add_axes()`` fails when adding redundant IDs.
        """
        axis0 = Axis(axis_id="a0", angle=10)
        axis1 = Axis(axis_id="a1", angle=20)
        axis2 = Axis(axis_id="a0", angle=30)

        hp = BaseHivePlot()
        try:
            hp.add_axes([axis0, axis1, axis2])
            pytest.fail("Should have failed on trying to add axes with redundant IDs.")
        except AssertionError:
            assert True

    def test_add_axes_hostile_overlap(self) -> None:
        """
        Check that ``BaseHivePlot.add_axes()`` fails when IDs redundant with existing IDs.
        """
        axis0 = Axis(axis_id="a0", angle=10)
        axis1 = Axis(axis_id="a1", angle=20)
        axis2 = Axis(axis_id="a0", angle=30)

        hp = BaseHivePlot()
        hp.add_axes([axis0, axis1])
        try:
            hp.add_axes([axis2])
            pytest.fail(
                "Should have failed due to trying to add an axis with a rendundant ID."
            )
        except AssertionError:
            assert True

    def test_add_nodes_example(self) -> None:
        """
        Check that ``BaseHivePlot.add_nodes()`` behaves as expected on a "nice" example.
        """
        node0 = Node(unique_id="n0", data={"a": 0})
        node1 = Node(unique_id="n1", data={"a": 1})
        node2 = Node(unique_id="n2", data={"a": 2})

        hp = BaseHivePlot()
        hp.add_nodes(nodes=[node0, node1, node2])

        for i in range(3):
            assert (
                subset_node_collection_by_unique_ids(
                    node_collection=hp.nodes, ids=f"n{i}"
                )["a"].to_numpy()[0]
                == i
            )

    def test_add_nodes_hostile_input(self) -> None:
        """
        Check that ``BaseHivePlot.add_nodes()`` fails when adding redundant IDs.
        """
        node0 = Node(unique_id="n0", data={"a": 0})
        node1 = Node(unique_id="n1", data={"a": 1})
        node2 = Node(unique_id="n0", data={"a": 2})

        hp = BaseHivePlot()
        try:
            hp.add_nodes([node0, node1, node2])
            pytest.fail("Should have failed trying to add nodes with redundant IDs.")
        except AssertionError:
            assert True

    def test_add_nodes_hostile_overlap(self) -> None:
        """
        Check that ``BaseHivePlot.add_nodes()`` fails when IDs redundant with existing IDs.
        """
        node0 = Node(unique_id="n0", data={"a": 0})
        node1 = Node(unique_id="n1", data={"a": 1})
        node2 = Node(unique_id="n0", data={"a": 2})

        hp = BaseHivePlot()
        hp.add_nodes([node0, node1])
        try:
            hp.add_nodes([node2])
            pytest.fail("Should have failed trying to add nodes with redundant IDs.")
        except AssertionError:
            assert True

    def test__allocate_nodes_to_axis_example(self) -> None:
        """
        Check that ``BaseHivePlot._allocate_nodes_to_axis()`` behaves as expected with a "nice" example.
        """
        axis0 = Axis(axis_id="a0", angle=10)
        axis1 = Axis(axis_id="a1", angle=20)
        axis2 = Axis(axis_id="a2", angle=30)
        axes = [axis0, axis1, axis2]

        node0 = Node(unique_id="n0", data={"a": 0})
        node1 = Node(unique_id="n1", data={"a": 1})
        node2 = Node(unique_id="n2", data={"a": 2})
        nodes = [node0, node1, node2]

        hp = BaseHivePlot()
        hp.add_nodes(nodes=nodes)
        hp.add_axes(axes=axes)

        hp._allocate_nodes_to_axis(
            node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["n0", "n1"]),
            axis_id="a0",
        )
        hp._allocate_nodes_to_axis(
            node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["n1", "n2"]),
            axis_id="a1",
        )
        hp._allocate_nodes_to_axis(
            node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["n2", "n0"]),
            axis_id="a2",
        )

        assert hp.node_assignments["a0"][hp.nodes.unique_id_column].to_list() == [
            "n0",
            "n1",
        ]
        assert hp.node_assignments["a1"][hp.nodes.unique_id_column].to_list() == [
            "n1",
            "n2",
        ]
        assert hp.node_assignments["a2"][hp.nodes.unique_id_column].to_list() == [
            "n0",
            "n2",
        ]

    @pytest.mark.parametrize("angle", [0, 120])
    def test_place_nodes_on_axis_example(self, angle: float) -> None:
        """
        Check that ``BaseHivePlot.place_nodes_on_axis()`` behaves as expected with a "nice" example.

        :param angle: angle of ``Axis`` instance.
        """
        node0 = Node(unique_id="n0", data={"a": 0})
        node1 = Node(unique_id="n1", data={"a": 1})
        node2 = Node(unique_id="n2", data={"a": 2})
        nodes = [node0, node1, node2]

        start = 1
        end = 5
        axis0 = Axis(axis_id="a0", start=start, end=end, angle=angle)

        hp = BaseHivePlot()
        hp.add_nodes(nodes=nodes)
        hp.add_axes(axes=[axis0])

        hp.place_nodes_on_axis(
            axis_id="a0",
            node_df=subset_node_collection_by_unique_ids(
                hp.nodes, ids=["n0", "n1", "n2"]
            ),
            sorting_feature_to_use="a",
            vmin=None,
            vmax=None,
        )

        # should have 3 points in total placed on the axis
        df = hp.axes["a0"].node_placements
        assert df.shape[0] == 3
        assert np.array_equal(
            df.loc[:, "unique_id"].values, np.array(["n0", "n1", "n2"])
        )

        # n0 at start of axis
        assert np.allclose(
            df.loc[df.loc[:, "unique_id"] == "n0", ["x", "y"]].values,
            np.array(polar2cartesian(rho=start, phi=angle)),
        )

        # n1 at midpoint of axis
        assert np.allclose(
            df.loc[df.loc[:, "unique_id"] == "n1", ["x", "y"]].values,
            np.array(polar2cartesian(rho=(start + end) / 2, phi=angle)),
        )

        # n2 at end of axis
        assert np.allclose(
            df.loc[df.loc[:, "unique_id"] == "n2", ["x", "y"]].values,
            np.array(polar2cartesian(rho=end, phi=angle)),
        )

    @pytest.mark.parametrize("angle", [0, 120])
    @pytest.mark.parametrize("vmin_vmax", [[-1, 3], [1, 3], [1, 1.5], [-6, -5], [5, 6]])
    def test_place_nodes_on_axis_change_vmin_vmax(
        self, angle: float, vmin_vmax: list
    ) -> None:
        """
        Check that ``BaseHivePlot.place_nodes_on_axis()`` correctly shifts node placement when changing vmin and vmax.

        :param angle: angle of ``Axis`` instance.
        :param vmin_vmax: vmin and vmax values to use.
        """
        # force a rescaling on both sides
        vmin = vmin_vmax[0]
        vmax = vmin_vmax[1]

        node0 = Node(unique_id="n0", data={"a": 0})
        node1 = Node(unique_id="n1", data={"a": 1})
        node2 = Node(unique_id="n2", data={"a": 2})
        nodes = [node0, node1, node2]

        start = 1
        end = 5
        axis0 = Axis(axis_id="a0", start=start, end=end, angle=angle)

        hp = BaseHivePlot()
        hp.add_nodes(nodes=nodes)
        hp.add_axes(axes=[axis0])

        hp.place_nodes_on_axis(
            axis_id="a0",
            node_df=subset_node_collection_by_unique_ids(
                hp.nodes, ids=["n0", "n1", "n2"]
            ),
            sorting_feature_to_use="a",
            vmin=vmin,
            vmax=vmax,
        )

        # should have 3 points in total placed on the axis
        df = hp.axes["a0"].node_placements
        assert df.shape[0] == 3
        assert np.array_equal(
            df.loc[:, "unique_id"].values, np.array(["n0", "n1", "n2"])
        )

        # scaled positions check
        for node_id in ["n0", "n1", "n2"]:
            node_val = subset_node_collection_by_unique_ids(
                node_collection=hp.nodes, ids=node_id
            )["a"].to_numpy()[0]
            # scale to [vmin, vmax] dim
            node_val = np.minimum(node_val, vmax)
            node_val = np.maximum(node_val, vmin)
            # scale to vmin = 0, vmax = 1
            scaled_position = (node_val - vmin) / (vmax - vmin)
            # rescale to size and shift of real axes
            new_rho = scaled_position * (end - start) + start
            assert np.allclose(
                df.loc[df.loc[:, "unique_id"] == node_id, ["x", "y"]].values,
                np.array(polar2cartesian(rho=new_rho, phi=angle)),
            )

    @pytest.mark.parametrize("angle", [0, 120])
    def test_place_nodes_on_axis_one_point(self, angle: float) -> None:
        """
        Check that ``BaseHivePlot.place_nodes_on_axis()`` correctly places a single point.

        Specifically, that the point is placed at the midpoint of an axis when no vmin or vmax are specified.

        :param angle: angle of ``Axis`` instance.
        """
        node0 = Node(unique_id="n0", data={"a": 0})
        nodes = [node0]

        start = 1
        end = 5
        axis0 = Axis(axis_id="a0", start=start, end=end, angle=angle)

        hp = BaseHivePlot()
        hp.add_nodes(nodes=nodes)
        hp.add_axes(axes=[axis0])

        hp.place_nodes_on_axis(
            axis_id="a0",
            node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["n0"]),
            sorting_feature_to_use="a",
            vmin=None,
            vmax=None,
        )

        # should have 3 points in total placed on the axis
        df = hp.axes["a0"].node_placements
        assert df.shape[0] == 1
        assert np.array_equal(df.loc[:, "unique_id"].values, np.array(["n0"]))

        # n0 at midpoint of axis
        assert np.allclose(
            df.loc[df.loc[:, "unique_id"] == "n0", ["x", "y"]].values,
            np.array(polar2cartesian(rho=(start + end) / 2, phi=angle)),
        )

    def test_place_nodes_on_axis_resetting_curves(self) -> None:
        """
        Check that ``BaseHivePlot.place_nodes_on_axis()`` kills any existing Bezier curves that connect to that axis.

        (The logic being, if you place the nodes, presumably they've been moved to different locations. Thus, one
        would need to redraw the Bezier curves (or at least for this method, one would not want to plot old, unrelated
        Bezier curves).
        """
        hp, edges = three_axis_basehiveplot_example()

        # connect some of the edges
        a_b_tag = hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
        b_c_tag = hp.connect_axes(edges=edges, axis_id_1="B", axis_id_2="C")

        # should have real curves for A <-> B and B <-> C  connections
        assert hp.hive_plot_edges["A"]["B"][a_b_tag]["curves"].size > 0, (
            "Should be non-empty numpy array of Bezier curves at this point."
        )
        assert hp.hive_plot_edges["B"]["A"][a_b_tag]["curves"].size > 0, (
            "Should be non-empty numpy array of Bezier curves at this point."
        )
        assert hp.hive_plot_edges["C"]["B"][b_c_tag]["curves"].size > 0, (
            "Should be non-empty numpy array of Bezier curves at this point."
        )
        assert hp.hive_plot_edges["B"]["C"][b_c_tag]["curves"].size > 0, (
            "Should be non-empty numpy array of Bezier curves at this point."
        )

        # re-placing A's nodes with a new sorting procedure should kill any A <-> B curves but not and B <-> C
        hp.place_nodes_on_axis(axis_id="A", sorting_feature_to_use="b")

        assert "curves" not in hp.hive_plot_edges["A"]["B"][a_b_tag], (
            "Bezier curves between 'A' -> 'B' should have been dropped when re-placing nodes."
        )
        assert "curves" not in hp.hive_plot_edges["B"]["A"][a_b_tag], (
            "Bezier curves between 'A' <- 'B' should have been dropped when re-placing nodes."
        )
        assert hp.hive_plot_edges["B"]["C"][b_c_tag]["curves"].size > 0, (
            "Bezier curves between 'B' and 'C' should be unaffected by messing with 'A'."
        )

    @pytest.mark.parametrize("a1_to_a2", [True, False])
    @pytest.mark.parametrize("a2_to_a1", [True, False])
    def test_add_edge_ids_example(self, a1_to_a2: bool, a2_to_a1: bool) -> None:
        """
        Test ``BaseHivePlot.add_edge_ids()`` correctly assigns edges with friendly example.

        :param a1_to_a2: whether to add edges from the first to second axis.
        :param a2_to_a1: whether to add edges from the second to first axis.
        """
        hp, edges = three_axis_basehiveplot_example()

        edge_combinations = list(combinations(hp.axes.keys(), 2))

        # starting with no edges
        assert hp.hive_plot_edges == {}

        for source, sink in edge_combinations:
            # testing appropriate key existence

            if a1_to_a2:
                # the [source][sink] key should not exist before
                try:
                    assert source not in hp.hive_plot_edges
                except AssertionError:
                    assert sink not in hp.hive_plot_edges[source]

            if a2_to_a1:
                # the [sink][source] key should not exist before
                try:
                    assert sink not in hp.hive_plot_edges
                except AssertionError:
                    assert source not in hp.hive_plot_edges[sink]

            # appropriate keys should now exist after
            try:
                tag = hp.add_edge_ids(
                    edges=edges,
                    axis_id_1=source,
                    axis_id_2=sink,
                    a1_to_a2=a1_to_a2,
                    a2_to_a1=a2_to_a1,
                )
            except ValueError:
                if not a1_to_a2 and not a2_to_a1:
                    assert True
                else:
                    pytest.fail(
                        f"Should not trigger a ValueError with a1_to_a2={a1_to_a2} and a2_to_a1={a2_to_a1}"
                    )

            if a1_to_a2:
                assert sink in hp.hive_plot_edges[source]
            if a2_to_a1:
                assert source in hp.hive_plot_edges[sink]

            # testing appropriate edge existence

            if a1_to_a2:
                # edges in source (first) column should all be in source axis
                assert np.isin(
                    hp.hive_plot_edges[source][sink][tag]["ids"][:, 0],
                    hp.axes[source].node_placements.unique_id.values,
                ).all()

                # edges in sink (second) column should all be in sink axis
                assert np.isin(
                    hp.hive_plot_edges[source][sink][tag]["ids"][:, 1],
                    hp.axes[sink].node_placements.unique_id.values,
                ).all()

            if a2_to_a1:
                # edges in first column should all be in source axis
                assert np.isin(
                    hp.hive_plot_edges[sink][source][tag]["ids"][:, 0],
                    hp.axes[sink].node_placements.unique_id.values,
                ).all()

                # edges in second column should all be in sink axis
                assert np.isin(
                    hp.hive_plot_edges[sink][source][tag]["ids"][:, 1],
                    hp.axes[source].node_placements.unique_id.values,
                ).all()

    @pytest.mark.parametrize(
        "hp_function",
        [
            three_axis_basehiveplot_example,
            two_axis_zero_crossing_basehiveplot_example,
            repeated_nodes_basehiveplot_example,
            repeat_axis_basehiveplot_example,
        ],
    )
    @pytest.mark.parametrize("short_arc", [True, False])
    def test_add_edge_curves_between_axes_example(
        self, hp_function: callable, short_arc: bool
    ) -> None:
        """
        Test that ``BaseHivePlot.add_edge_curves_between_axes()`` correctly builds edge curves with friendly examples.

        Do this by checking the polar angle of the discretized curves.

        :param hp_function: which graph-generating function to use.
        :param short_arc: whether to take the minimum angle arc or not when drawing edges.
        """
        hp, edges = hp_function()
        edge_combinations = list(combinations(hp.axes.keys(), 2))

        num_steps = 100

        for a0, a1 in edge_combinations:
            tag = hp.add_edge_ids(edges=edges, axis_id_1=a0, axis_id_2=a1)
            hp.add_edge_curves_between_axes(
                axis_id_1=a0,
                axis_id_2=a1,
                num_steps=num_steps,
                tag=tag,
                short_arc=short_arc,
            )

            # check the arcs in both directions
            for [i, j] in [[a0, a1], [a1, a0]]:
                curves = hp.hive_plot_edges[i][j][tag]["curves"].copy().astype(float)
                # drop the spacer values
                curves = curves[~np.isnan(curves)].reshape(-1, 2)

                # get polar coordinates
                rho, phi = cartesian2polar(x=curves[:, 0], y=curves[:, 1])

                # distance from origin should be smaller than the largest two rho values for the two connecting points
                #  (it can be smaller than the minimum)
                ids = hp.hive_plot_edges[i][j][tag]["ids"].copy()
                for k in range(ids.shape[0]):
                    # curve from and two node IDs
                    node_id_i = ids[k, 0]
                    node_id_j = ids[k, 1]

                    # rho values for those two nodes
                    rho_i = (
                        hp.axes[i]
                        .node_placements.set_index("unique_id")
                        .loc[node_id_i, "rho"]
                    )
                    rho_j = (
                        hp.axes[j]
                        .node_placements.set_index("unique_id")
                        .loc[node_id_j, "rho"]
                    )

                    # the curve of interest (drop the np.nan at the end)
                    temp_curve = rho[k * num_steps : (k + 1) * num_steps]
                    assert np.round(temp_curve.max(), 4) <= np.round(
                        np.maximum(rho_i, rho_j), 4
                    ), (
                        "rho value rose above expected maximum (e.g. curve got farther from origin than expected\n"
                        f"From node {node_id_i} to node {node_id_j}\n"
                        f"rho_i {rho_i} and rho_j {rho_j}\n"
                        f"From axis {i} to axis {j}"
                    )

                # for angle expectations, we will use the definition of dot product to get the angle with the
                #  starting axis
                # use dot product definition on the discretized curves against each axis and arccos the angle
                #  no discretized point should be more than `min angle` degrees away from an axis
                start = hp.axes[i].start
                dotted = curves[:, 0] * start[0] + curves[:, 1] * start[1]
                # rounding to avoid numerical error
                normalized = np.round(
                    dotted / (np.linalg.norm(start) * np.linalg.norm(curves, axis=1)), 5
                )
                angles = np.round(np.degrees(np.arccos(normalized)), 2)

                # if we take the short arc, angle should be bounded by the minimum angle between two axes
                #  and monotonically increasing or decreasing
                if short_arc:
                    # monotonicity check for each curve
                    for k in range(ids.shape[0]):
                        temp_angles = angles[k * num_steps : (k + 1) * num_steps]
                        diff = np.diff(temp_angles) > 0
                        # either all increasing or all decreasing
                        assert diff.sum() == diff.size or diff.sum() == 0, (
                            "Angle must be monotonic diff, since there should not be a 180 degree "
                            + "crossing of the curve\n"
                            + f"From node {ids[k, 0]} to node {ids[k, 1]}\n"
                            + f"From axis {i} to axis {j}\n"
                            + f"From angle {hp.axes[i].angle} to angle {hp.axes[j].angle}\n"
                            + f"angles:\n {temp_angles}\n"
                            + f"diff > 0:\n {diff}"
                        )

                    # bound check for angles on curves
                    angle_diff = np.abs(hp.axes[i].angle - hp.axes[j].angle)
                    if angle_diff > 180:
                        angle_diff = 360 - angle_diff
                    assert angles.max() <= angle_diff, (
                        "Maximum angle from start axis should not exceed the minimum angle between the two axes\n"
                        + f"From axis {i} to axis {j}"
                    )

                # otherwise, we guarantee an angle greater than 180 degrees
                #  dot product always returns the minimum angle, so this can't be seen
                #  BUT, passing 180 degrees will trigger a lack of monotonicity in the angles that would be
                #  otherwise impossible
                else:
                    # must have both increasing and decreasing diff
                    for k in range(ids.shape[0]):
                        temp_angles = angles[k * num_steps : (k + 1) * num_steps]
                        diff = np.diff(temp_angles) > 0
                        assert diff.sum() < diff.size, (
                            "Must be non-monotonic diff, or there was no 180 degree crossing of the curve\n"
                            + f"From node {ids[k, 0]} to node {ids[k, 1]}\n"
                            + f"From axis {i} to axis {j}\n"
                            + f"From angle {hp.axes[i].angle} to angle {hp.axes[j].angle}\n"
                            + f"angles:\n {temp_angles}\n"
                            + f"diff > 0:\n {diff}"
                        )

    def test_construct_curves(self) -> None:
        """
        Test that ``BaseHivePlot.construct_curves()`` correctly builds the correctly needed curves.

        (e.g. edges whose "ids" are specified but not yet "curves".)
        """
        hp, edges = three_axis_basehiveplot_example()

        # only add the IDs, not edges or kwargs
        tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")

        # curves should not exist yet
        assert "curves" not in hp.hive_plot_edges["A"]["B"][tag], (
            "Bezier curves between 'A' -> 'B' should not exist yet."
        )
        assert "curves" not in hp.hive_plot_edges["B"]["A"][tag], (
            "Bezier curves between 'A' <- 'B' should not exist yet."
        )

        # add curves for all specified IDs
        hp.construct_curves()

        # should have real curves for the above A <-> B connections, but nothing else
        assert hp.hive_plot_edges["A"]["B"][tag]["curves"].size > 0, (
            "Should be non-empty numpy array of Bezier curves between 'A' -> 'B' at this point."
        )
        assert hp.hive_plot_edges["B"]["A"][tag]["curves"].size > 0, (
            "Should be non-empty numpy array of Bezier curves between 'A' <- 'B' at this point."
        )

        assert "C" not in hp.hive_plot_edges["A"], (
            "These IDs or edges should not been defined yet"
        )

    def test_add_edge_kwargs(self) -> None:
        """
        Test that ``BaseHivePlot.add_edge_kwargs()`` correctly adds kwargs where specified.
        """
        hp, edges = three_axis_basehiveplot_example()

        tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")

        hp.add_edge_kwargs(
            axis_id_1="A", axis_id_2="B", a2_to_a1=False, tag=tag, c="C0"
        )

        # should only have generated "A" -> "B" edge_kwargs
        assert hp.hive_plot_edges["A"]["B"][tag]["edge_kwargs"] == {"c": "C0"}, (
            "Should have generated a simple dict of color for edge kwargs going from 'A' -> 'B'."
        )
        assert "edge_kwargs" not in hp.hive_plot_edges["B"]["A"][tag], (
            "Should not have any edge kwargs going from 'B' -> 'A'."
        )

    def test_connect_axes(self) -> None:
        """
        Test that ``BaseHivePlot.connect_axes()`` correctly builds edge id connections, Bezier curves, and kwargs.
        """
        hp, edges = three_axis_basehiveplot_example()

        tag = hp.connect_axes(
            edges=edges, axis_id_1="A", axis_id_2="B", a2_to_a1=False, c="C0"
        )

        # should only have generated "A" -> "B" ids, curves, and edge_kwargs
        assert hp.hive_plot_edges["A"]["B"][tag]["ids"].size > 0, (
            "Should be non-empty numpy array of node IDs representing edges between 'A' -> 'B' at this point."
        )
        assert hp.hive_plot_edges["A"]["B"][tag]["curves"].size > 0, (
            "Should be non-empty numpy array of Bezier curves between 'A' -> 'B' at this point."
        )
        assert hp.hive_plot_edges["A"]["B"][tag]["edge_kwargs"] == {"c": "C0"}, (
            "Should have generated a simple dict of color for edge kwargs going from 'A' -> 'B'."
        )

        assert "B" not in hp.hive_plot_edges, (
            "Should not have generated any edge information between 'B' to 'A' at this point."
        )

    def test_hive_plot_n_axes_example(self) -> None:
        """
        Test that ``hive_plot_n_axes()`` builds the ``BaseHivePlot`` instance we expect with a friendly example.
        """
        # generate synthetic node data from a pandas dataframe
        num_nodes = 50
        rng = np.random.default_rng(0)
        data = pd.DataFrame(
            np.c_[
                rng.uniform(low=0, high=10, size=num_nodes),
                rng.uniform(low=10, high=20, size=num_nodes),
                rng.uniform(low=20, high=30, size=num_nodes),
            ],
            columns=["low", "med", "high"],
        )

        # convert into dictionaries for later use
        node_data = data.to_dict(orient="records")

        # use the dataframe's index as unique id
        node_ids = data.index.to_numpy()

        # generate random edges
        num_edges = 200

        edges = rng.choice(node_ids, size=num_edges * 2).reshape(-1, 2)

        nodes = [
            Node(unique_id=node_id, data=node_data[i])
            for i, node_id in enumerate(node_ids)
        ]

        hp = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=nodes),
            edges=edges,
            axes_assignments=[
                np.arange(num_nodes)[: num_nodes // 3],
                np.arange(num_nodes)[num_nodes // 3 : 2 * num_nodes // 3],
                np.arange(num_nodes)[2 * num_nodes // 3 :],
            ],
            sorting_variables=["low", "med", "high"],
            axes_names=["Low", "Medium", "High"],
            repeat_axes=[False, True, False],
            vmins=[0, 0, 0],
            vmaxes=[30, 30, 30],
            suppress_deprecation_warning=True,
        )

        assert sorted(hp.axes.keys()) == [
            "High",
            "Low",
            "Medium",
            "Medium_repeat",
        ], "Should have only generated a repeat Medium axis."

        assert (
            hp.axes["Low"].node_placements["rho"].mean()
            < hp.axes["Medium"].node_placements["rho"].mean()
        ), (
            "by construction, rho values for 'Low' axis should be smaller than for 'Medium' axis."
        )

        assert (
            hp.axes["Medium"].node_placements["rho"].mean()
            < hp.axes["High"].node_placements["rho"].mean()
        ), (
            "by construction, rho values for 'Medium' axis should be smaller than for 'High' axis."
        )

        # we should have curves between these axes
        valid_curves = [
            ["Low", "Medium"],
            ["Medium", "Low"],
            ["Medium", "Medium_repeat"],
            ["Medium_repeat", "High"],
            ["High", "Medium_repeat"],
            ["High", "Low"],
            ["Low", "High"],
        ]
        for a0, a1 in valid_curves:
            assert list(hp.hive_plot_edges[a0][a1].keys()) == [0], (
                "All edge adds are unique, no unique tag specified, so all tags should be 0"
            )
            assert "curves" in hp.hive_plot_edges[a0][a1][0], (
                f"Expected Bezier curves calculated from {a0} to {a1} but did not find them."
            )

        assert "Medium" not in hp.hive_plot_edges["Medium_repeat"], (
            "Did not expect any connections between 'Medium_repeat' and 'Medium' but found them."
        )

    def test_hive_plot_n_axes_with_none_axis_assignment(self) -> None:
        """
        Confirm ``hive_plot_n_axes()`` will run with a ``None`` for one of the axis assignments in ``axes_assignments``.
        """
        node0 = Node(unique_id="0", data={"a": 1, "b": 2, "c": 3})
        node1 = Node(unique_id="1", data={"a": 2, "b": 3, "c": 4})
        node2 = Node(unique_id="2", data={"a": 3, "b": 4, "c": 5})
        node3 = Node(unique_id="3", data={"a": 4, "b": 5, "c": 6})
        node4 = Node(unique_id="4", data={"a": 5, "b": 6, "c": 7})
        node5 = Node(unique_id="5", data={"a": 6, "b": 7, "c": 8})

        nodes = [node0, node1, node2, node3, node4, node5]

        # dummy of from, to pairs
        rng = np.random.default_rng(0)
        edges = rng.choice(np.arange(len(nodes)).astype(str), size=100).reshape(-1, 2)

        hp_normal = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=nodes),
            edges=edges,
            axes_assignments=[["0", "1"], ["2", "3"], ["4", "5"]],
            sorting_variables=["a"] * 3,
            axes_names=["_", "__", "to_check"],
            suppress_deprecation_warning=True,
        )

        hp_with_none = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=nodes),
            edges=edges,
            axes_assignments=[["0", "1"], ["2", "3"], None],
            sorting_variables=["a"] * 3,
            axes_names=["_", "__", "to_check"],
            suppress_deprecation_warning=True,
        )

        hp_with_double_none = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=nodes),
            edges=edges,
            axes_assignments=[["0", "1", "2", "3"], None, None],
            sorting_variables=["a"] * 3,
            axes_names=["_", "also_to_check", "to_check"],
            suppress_deprecation_warning=True,
        )

        node_placements_normal = hp_normal.axes["to_check"].node_placements.sort_values(
            "unique_id"
        )
        normal_unique_ids = list(node_placements_normal.unique_id.values)
        normal_rho_values = list(node_placements_normal.rho.values)

        node_placements_with_none = hp_with_none.axes[
            "to_check"
        ].node_placements.sort_values("unique_id")
        with_none_unique_ids = list(node_placements_with_none.unique_id.values)
        with_none_rho_values = list(node_placements_with_none.rho.values)

        node_placements_with_double_none = hp_with_double_none.axes[
            "to_check"
        ].node_placements.sort_values("unique_id")
        with_double_none_unique_ids = list(
            node_placements_with_double_none.unique_id.values
        )
        with_double_none_rho_values = list(node_placements_with_double_none.rho.values)

        assert (
            normal_unique_ids == with_none_unique_ids == with_double_none_unique_ids
        ), "Should have chosen the same unique IDs"

        assert (
            normal_rho_values == with_none_rho_values == with_double_none_rho_values
        ), "Should have equivalent rho values"

    def test_hive_plot_n_axes_run_with_unspecified_params(self) -> None:
        """
        Confirm ``hive_plot_n_axes()`` will run with unspecified kwargs (e.g. call the defaults).
        """
        # generate synthetic node data from a pandas dataframe
        num_nodes = 50
        rng = np.random.default_rng(0)
        data = pd.DataFrame(
            np.c_[
                rng.uniform(low=0, high=10, size=num_nodes),
                rng.uniform(low=10, high=20, size=num_nodes),
                rng.uniform(low=20, high=30, size=num_nodes),
            ],
            columns=["low", "med", "high"],
        )

        # convert into dictionaries for later use
        node_data = data.to_dict(orient="records")

        # use the dataframe's index as unique id
        node_ids = data.index.to_numpy()

        # generate random edges
        num_edges = 200

        edges = rng.choice(node_ids, size=num_edges * 2).reshape(-1, 2)

        nodes = [
            Node(unique_id=node_id, data=node_data[i])
            for i, node_id in enumerate(node_ids)
        ]

        hp = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=nodes),
            edges=edges,
            axes_assignments=[
                np.arange(num_nodes)[: num_nodes // 3],
                np.arange(num_nodes)[num_nodes // 3 : 2 * num_nodes // 3],
                np.arange(num_nodes)[2 * num_nodes // 3 :],
            ],
            sorting_variables=["low", "med", "high"],
            suppress_deprecation_warning=True,
        )

        # default no repeat axes
        assert len(list(hp.axes.keys())) == 3, "Default no repeat axes."

        # default names
        assert sorted(hp.axes.keys()) == [
            "Group 1",
            "Group 2",
            "Group 3",
        ], "Default Names."

        # default placements spanning each axis
        for axis_id in hp.axes:
            assert hp.axes[axis_id].node_placements.loc[:, "rho"].min() == 1, (
                f"Minimum node placement for axis {axis_id} should be 1 (min location by construction) for default."
            )
            assert hp.axes[axis_id].node_placements.loc[:, "rho"].max() == 5, (
                f"Maximum node placement for axis {axis_id} should be 5 (max location by construction) for default."
            )

    @pytest.mark.parametrize("angle_between_repeat_axes", [40, 120, 140])
    def test_hive_plot_n_axes_repeat_axes_crossing_other_axes(
        self, angle_between_repeat_axes: float
    ) -> None:
        """
        Confirm ``hive_plot_n_axes()`` will warn when repeat axes will touch or cross other axes.

        :param angle_between_repeat_axes: angle used in between repeat axes.
        """
        # generate synthetic node data from a pandas dataframe
        num_nodes = 50
        rng = np.random.default_rng(0)
        data = pd.DataFrame(
            np.c_[
                rng.uniform(low=0, high=10, size=num_nodes),
                rng.uniform(low=10, high=20, size=num_nodes),
                rng.uniform(low=20, high=30, size=num_nodes),
            ],
            columns=["low", "med", "high"],
        )

        # convert into dictionaries for later use
        node_data = data.to_dict(orient="records")

        # use the dataframe's index as unique id
        node_ids = data.index.to_numpy()

        # generate random edges
        num_edges = 200

        edges = rng.choice(node_ids, size=num_edges * 2).reshape(-1, 2)

        nodes = [
            Node(unique_id=node_id, data=node_data[i])
            for i, node_id in enumerate(node_ids)
        ]

        # doing 3 axes so 120 degrees is the number to worry about
        if angle_between_repeat_axes >= 120:
            with pytest.warns() as record:
                hive_plot_n_axes(
                    nodes=node_collection_from_node_list(node_list=nodes),
                    edges=edges,
                    axes_assignments=[
                        np.arange(num_nodes)[: num_nodes // 3],
                        np.arange(num_nodes)[num_nodes // 3 : 2 * num_nodes // 3],
                        np.arange(num_nodes)[2 * num_nodes // 3 :],
                    ],
                    sorting_variables=["low", "med", "high"],
                    angle_between_repeat_axes=angle_between_repeat_axes,
                    suppress_deprecation_warning=True,
                )

            assert len(record) == 1, (
                f"Expected 1 warning for edge angle, got:\n{[i.message.args for i in record]}"
            )

        else:
            with warnings.catch_warnings():
                warnings.simplefilter("error")
                hive_plot_n_axes(
                    nodes=node_collection_from_node_list(node_list=nodes),
                    edges=edges,
                    axes_assignments=[
                        np.arange(num_nodes)[: num_nodes // 3],
                        np.arange(num_nodes)[num_nodes // 3 : 2 * num_nodes // 3],
                        np.arange(num_nodes)[2 * num_nodes // 3 :],
                    ],
                    sorting_variables=["low", "med", "high"],
                    angle_between_repeat_axes=angle_between_repeat_axes,
                    suppress_deprecation_warning=True,
                )

    def test_hashable_unique_ids(self) -> None:
        """
        Make sure we can pass around hashable ids and things will work.
        """
        node_list = [
            Node(unique_id=(i, j), data={i: i, j: j})
            for i in range(5)
            for j in range(5)
        ]

        axis = Axis(axis_id=("a", "b"))

        hp = BaseHivePlot()
        hp.add_nodes(node_list)
        hp.add_axes([axis])

        assert list(hp.axes.keys()) == [("a", "b")]

        # make sure nodes were placed without issue
        hp.place_nodes_on_axis(
            axis_id=("a", "b"),
            node_df=subset_node_collection_by_unique_ids(
                hp.nodes, ids=[(0, j) for j in range(5)]
            ),
            sorting_feature_to_use=0,
        )

        assert hp.axes[("a", "b")].node_placements.shape[0] == 5

    def test_no_specified_node_placement_label(self) -> None:
        """
        Confirm we can't place nodes on an axis in a ``BaseHivePlot`` without specifying a node placement label.

        Specifically, placing the node placement label before or during the placement call.
        """
        hp = BaseHivePlot()

        # build nodes
        node0 = Node(unique_id="0", data={"a": 1, "b": 2, "c": 3})
        node1 = Node(unique_id="1", data={"a": 2, "b": 3, "c": 4})
        node2 = Node(unique_id="2", data={"a": 3, "b": 4, "c": 5})
        node3 = Node(unique_id="3", data={"a": 4, "b": 5, "c": 6})
        node4 = Node(unique_id="4", data={"a": 5, "b": 6, "c": 7})
        node5 = Node(unique_id="5", data={"a": 6, "b": 7, "c": 8})

        nodes = [node0, node1, node2, node3, node4, node5]

        hp.add_nodes(nodes)

        # build axes
        axis0 = Axis(axis_id="A", start=1, end=5, angle=0)
        axis1 = Axis(axis_id="B", start=1, end=5, angle=120)
        axis2 = Axis(axis_id="C", start=1, end=5, angle=240)

        axes = [axis0, axis1, axis2]

        hp.add_axes(axes)

        # hardcoded partition of nodes assignment for now
        vmin = 0
        vmax = 10
        try:
            hp.place_nodes_on_axis(
                axis_id="A",
                node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["0", "1"]),
                sorting_feature_to_use=None,
                vmin=vmin,
                vmax=vmax,
            )
            pytest.fail(
                "Should have triggered an AssertionError for not having a sorting"
                + "procedure for the axis ever."
            )
        except AssertionError:
            assert True

    @pytest.mark.parametrize(
        "reset",
        [
            "tag_a1_to_a2_only",
            "tag_a2_to_a1_only",
            "tag_bidirectional",
            "axis_pair",
            "axis_pair_one_direction",
            "entire_from_axis",
            "entire_to_axis",
            "entire_axis",
            "all_edges",
            "invalid",
            "tag_not_found",
        ],
    )
    def test_reset_edges(self, reset: str) -> None:
        """
        Confirm we can kill any and all edge connections, curves, and kwargs in with ``BaseHivePlot.reset_edges()``.

        :param reset: which method of edge reset to consider.
        """
        hp, edges = three_axis_basehiveplot_example()

        # connect some edges
        tag = hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
        tag1 = hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
        hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="C")
        # throw in an edge kwarg for coverage
        hp.connect_axes(edges=edges, axis_id_1="C", axis_id_2="B", c="C0")

        assert len(list(hp.hive_plot_edges.keys())) > 0, (
            "Should have edge information at this point."
        )

        if reset == "tag_a1_to_a2_only":
            hp.reset_edges(axis_id_1="A", axis_id_2="B", tag=tag, a2_to_a1=False)
            assert tag not in hp.hive_plot_edges["A"]["B"], (
                "(A, B, tag) edges should've dropped."
            )
            assert tag1 in hp.hive_plot_edges["A"]["B"], (
                "(A, B, tag1) edges should've maintained."
            )
            assert tag in hp.hive_plot_edges["B"]["A"], (
                "(B, A, tag) edges should've maintained."
            )
            assert tag in hp.hive_plot_edges["A"]["C"]
            assert tag in hp.hive_plot_edges["C"]["A"]
            assert tag in hp.hive_plot_edges["B"]["C"]
            assert tag in hp.hive_plot_edges["C"]["B"]

        elif reset == "tag_a2_to_a1_only":
            hp.reset_edges(axis_id_1="A", axis_id_2="B", tag=tag, a1_to_a2=False)
            assert tag in hp.hive_plot_edges["A"]["B"], (
                "(A, B, tag) edges should've maintained."
            )
            assert tag1 in hp.hive_plot_edges["A"]["B"], (
                "(A, B, tag1) edges should've maintained."
            )
            assert tag not in hp.hive_plot_edges["B"]["A"], (
                "(B, A, tag) edges should've dropped."
            )
            assert tag in hp.hive_plot_edges["A"]["C"]
            assert tag in hp.hive_plot_edges["C"]["A"]
            assert tag in hp.hive_plot_edges["B"]["C"]
            assert tag in hp.hive_plot_edges["C"]["B"]

        elif reset == "tag_bidirectional":
            hp.reset_edges(axis_id_1="A", axis_id_2="B", tag=tag)
            assert tag not in hp.hive_plot_edges["A"]["B"], (
                "(A, B, tag) edges should've dropped."
            )
            assert tag not in hp.hive_plot_edges["B"]["A"], (
                "(B, A, tag) edges should've dropped."
            )
            assert tag1 in hp.hive_plot_edges["A"]["B"], (
                "(A, B, tag1) edges should've maintained."
            )
            assert tag in hp.hive_plot_edges["A"]["C"]
            assert tag in hp.hive_plot_edges["C"]["A"]
            assert tag in hp.hive_plot_edges["B"]["C"]
            assert tag in hp.hive_plot_edges["C"]["B"]

        elif reset == "axis_pair":
            hp.reset_edges(axis_id_1="A", axis_id_2="B")
            assert "B" not in hp.hive_plot_edges["A"], (
                "All (A, B) edges should've dropped."
            )
            assert "A" not in hp.hive_plot_edges["B"], (
                "All (B, A) edges should've dropped."
            )
            assert tag in hp.hive_plot_edges["A"]["C"]
            assert tag in hp.hive_plot_edges["C"]["A"]
            assert tag in hp.hive_plot_edges["B"]["C"]
            assert tag in hp.hive_plot_edges["C"]["B"]

        elif reset == "axis_pair_one_direction":
            hp.reset_edges(axis_id_1="A", axis_id_2="B", a2_to_a1=False)
            assert "B" not in hp.hive_plot_edges["A"], (
                "All (A, B) edges should've dropped."
            )
            assert tag in hp.hive_plot_edges["B"]["A"]
            assert tag in hp.hive_plot_edges["A"]["C"]
            assert tag in hp.hive_plot_edges["C"]["A"]
            assert tag in hp.hive_plot_edges["B"]["C"]
            assert tag in hp.hive_plot_edges["C"]["B"]

        elif reset == "entire_from_axis":
            hp.reset_edges(axis_id_1="A", a2_to_a1=False)
            assert "A" not in hp.hive_plot_edges, "All (A, *) edges should've dropped."
            assert tag in hp.hive_plot_edges["B"]["A"]
            assert tag in hp.hive_plot_edges["C"]["A"]
            assert tag in hp.hive_plot_edges["B"]["C"]
            assert tag in hp.hive_plot_edges["C"]["B"]

        elif reset == "entire_to_axis":
            hp.reset_edges(axis_id_1="A", a1_to_a2=False)
            for a0 in hp.hive_plot_edges:
                assert "A" not in hp.hive_plot_edges[a0], (
                    "All (*, A) edges should've dropped."
                )
            assert tag in hp.hive_plot_edges["A"]["C"]
            assert tag in hp.hive_plot_edges["A"]["B"]
            assert tag in hp.hive_plot_edges["B"]["C"]
            assert tag in hp.hive_plot_edges["C"]["B"]

        elif reset == "entire_axis":
            hp.reset_edges(axis_id_1="A")
            assert "A" not in hp.hive_plot_edges, "All (A, *) edges should've dropped."
            for a0 in hp.hive_plot_edges:
                assert "A" not in hp.hive_plot_edges[a0], (
                    "All (*, A) edges should've dropped."
                )
            assert tag in hp.hive_plot_edges["B"]["C"]
            assert tag in hp.hive_plot_edges["C"]["B"]

        # should delete all edges
        elif reset == "all_edges":
            hp.reset_edges()
            assert len(list(hp.hive_plot_edges.keys())) == 0, (
                "Should have no edge information at this point."
            )

        elif reset == "invalid":
            # do just tag as example of invalid option
            try:
                hp.reset_edges(tag=tag)
                pytest.fail("Should've triggerd NotImplementedError")
            except NotImplementedError:
                assert True

        elif reset == "tag_not_found":
            try:
                hp.reset_edges(axis_id_1="A", axis_id_2="B", tag="non_existent_tag")
                pytest.fail("Should've triggerd ValueError from non existent tag")
            except ValueError:
                assert True

            # make sure it works both ways (bidirectional non existent will always fail on on the a1_to_a2 side
            try:
                hp.reset_edges(
                    axis_id_1="A", axis_id_2="B", tag="non_existent_tag", a1_to_a2=False
                )
                pytest.fail("Should've triggerd ValueError from non existent tag")
            except ValueError:
                assert True

        else:
            raise NotImplementedError

    @pytest.mark.parametrize("edge_method", ["curves", "kwargs"])
    def test_add_edge_curves_or_kwargs_trigger_keyerror(self, edge_method: str) -> None:
        """
        Test that two method calls each trigger a ``KeyError``.

        Test ``BaseHivePlot.add_edge_curves_between_axes()`` and ``BaseHivePlot.add_edge_kwargs()``.

        Specifically when attempting to run between axes with unspecified edge IDs between which to connect.

        :param edge_method: string specifying which method to call.
        """
        hp, edges = three_axis_basehiveplot_example()

        if edge_method == "curves":
            call = hp.add_edge_curves_between_axes
        elif edge_method == "kwargs":
            call = hp.add_edge_kwargs
        else:
            raise NotImplementedError

        # certainly if we haven't specified any edge IDs to connect, we will trigger the KeyError
        try:
            call(axis_id_1="A", axis_id_2="B", a2_to_a1=False, tag=0)
            pytest.fail("Should have triggered KeyError.")
        except KeyError:
            assert True

        # also, if we haven't specified a certain direction but ask for those curves,
        #  we should still trigger the KeyError
        tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B", a2_to_a1=False)

        # a1 to a2 should not trigger an error, but a2 to a1 should since the IDs weren't created
        try:
            # only A -> B, which exists
            call(axis_id_1="A", axis_id_2="B", a2_to_a1=False, tag=tag)
            assert True
        except KeyError:
            pytest.fail(
                "These edge IDs were already specified, and therefore should not have triggered a KeyError."
            )

        try:
            # only B -> A, which does not exist
            call(axis_id_1="A", axis_id_2="B", a1_to_a2=False, tag=tag)
            pytest.fail(
                "These edge IDs were NOT yet specified, and therefore this should have triggered a KeyError."
            )
        except KeyError:
            assert True

    def test_add_edge_curves_between_axes_at_least_one_direction_of_edges(self) -> None:
        """
        ``BaseHivePlot.add_edge_curves_between_axes()`` must have at least one of ``a1_to_a2`` / ``a2_to_a1`` be True.
        """
        hp, edges = three_axis_basehiveplot_example()

        tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")

        try:
            hp.add_edge_curves_between_axes(
                axis_id_1="A", axis_id_2="B", a1_to_a2=False, a2_to_a1=False, tag=tag
            )
            pytest.fail("Un-allowed behavior. Should have triggered a ValueError.")
        except ValueError:
            assert True

    @pytest.mark.parametrize(
        "edge_directions", [[True, True], [True, False], [False, True]]
    )
    def test_add_edge_curves_between_axes_no_tag(self, edge_directions: list) -> None:
        """
        Make sure ``BaseHivePlot.add_edge_curves_between_axes()`` tolerates no tag when there's one tag (and only 1).

        :param edge_directions: list specifying the parameter values `a1_to_a2` and `a2_to_a1` for
            ``add_edge_curves_between_axes()``.
        """
        a1_to_a2 = edge_directions[0]
        a2_to_a1 = edge_directions[1]

        hp, edges = three_axis_basehiveplot_example()

        tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")

        hp.add_edge_curves_between_axes(axis_id_1="A", axis_id_2="B")
        assert "curves" in hp.hive_plot_edges["A"]["B"][tag]

        tag2 = hp.add_edge_ids(
            edges=edges, axis_id_1="A", axis_id_2="B", tag="second_tag"
        )
        try:
            hp.add_edge_curves_between_axes(
                axis_id_1="A", axis_id_2="B", a1_to_a2=a1_to_a2, a2_to_a1=a2_to_a1
            )
            pytest.fail(
                "Should be ambiguous which tag to update and trigger a ValueError"
            )
        except ValueError:
            assert True

        assert "curves" not in hp.hive_plot_edges["A"]["B"][tag2]
        hp.add_edge_curves_between_axes(axis_id_1="A", axis_id_2="B", tag=tag2)
        assert "curves" in hp.hive_plot_edges["A"]["B"][tag2]

    def test_add_edge_kwargs_preserve_existing_kwargs(self) -> None:
        """
        Make sure ``BaseHivePlot.add_edge_kwargs()`` doesn't drop previous kwargs with multiple calls that add kwargs.
        """
        hp, edges = three_axis_basehiveplot_example()
        tag = hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B", c="blue")
        hp.add_edge_kwargs(axis_id_1="A", axis_id_2="B", lw=2)
        hp.add_edge_kwargs(axis_id_1="A", axis_id_2="B", ls="--")
        assert sorted(hp.hive_plot_edges["A"]["B"][tag]["edge_kwargs"]) == [
            "c",
            "ls",
            "lw",
        ]

    def test_add_edge_ids_non_unique_tag(self) -> None:
        """
        Make sure ``BaseHivePlot.add_edge_ids()`` yells when trying to add a non-unique tag of edges to an axis pair.
        """
        hp, edges = three_axis_basehiveplot_example()
        tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")
        hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B", tag="new_tag")
        try:
            hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B", tag=tag)
            pytest.fail(
                "Should yell for trying to add non-unique tag of edges to an axis pair"
            )
        except ValueError:
            assert True

    @pytest.mark.parametrize(
        "edge_directions", [[True, True], [True, False], [False, True]]
    )
    def test_add_edge_kwargs_no_tag(self, edge_directions: list) -> None:
        """
        Make sure ``BaseHivePlot.add_edge_kwargs()`` tolerates no tag when there's one tag (and only 1).

        :param edge_directions: list specifying the parameter values `a1_to_a2` and `a2_to_a1` for
            ``add_edge_kwargs()``.
        """
        a1_to_a2 = edge_directions[0]
        a2_to_a1 = edge_directions[1]

        hp, edges = three_axis_basehiveplot_example()

        tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")

        hp.add_edge_kwargs(axis_id_1="A", axis_id_2="B", c="blue")
        assert "edge_kwargs" in hp.hive_plot_edges["A"]["B"][tag]

        tag2 = hp.add_edge_ids(
            edges=edges, axis_id_1="A", axis_id_2="B", tag="second_tag"
        )
        try:
            hp.add_edge_kwargs(
                axis_id_1="A",
                axis_id_2="B",
                a1_to_a2=a1_to_a2,
                a2_to_a1=a2_to_a1,
                c="black",
            )
            pytest.fail(
                "Should be ambiguous which tag to update and trigger a ValueError"
            )
        except ValueError:
            assert True

        assert "edge_kwargs" not in hp.hive_plot_edges["A"]["B"][tag2]
        hp.add_edge_kwargs(axis_id_1="A", axis_id_2="B", tag=tag2)
        assert "edge_kwargs" in hp.hive_plot_edges["A"]["B"][tag2]

    def test_add_edge_kwargs_no_edges(self) -> None:
        """
        Make sure ``BaseHivePlot.add_edge_kwargs()`` warns / runs as expected when there are no edges.
        """
        # generate synthetic node data from a pandas dataframe
        num_nodes = 15
        rng = np.random.default_rng(0)
        data = pd.DataFrame(
            np.c_[
                rng.uniform(low=0, high=10, size=num_nodes),
                rng.uniform(low=10, high=20, size=num_nodes),
                rng.uniform(low=20, high=30, size=num_nodes),
            ],
            columns=["low", "med", "high"],
        )

        # convert into dictionaries for later use
        node_data = data.to_dict(orient="records")

        # use the dataframe's index as unique id
        node_ids = data.index.to_numpy()

        nodes = [
            Node(unique_id=node_id, data=node_data[i])
            for i, node_id in enumerate(node_ids)
        ]

        # generate random edges
        num_edges = 0

        edges = rng.choice(node_ids, size=num_edges * 2).reshape(-1, 2)

        hp = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=nodes),
            edges=edges,
            axes_assignments=[
                np.arange(num_nodes)[: num_nodes // 3],
                np.arange(num_nodes)[num_nodes // 3 : 2 * num_nodes // 3],
                np.arange(num_nodes)[2 * num_nodes // 3 :],
            ],
            sorting_variables=["low", "med", "high"],
            repeat_axes=[True] * 3,
            axes_names=["Low", "Medium", "High"],
            orient_angle=-30,
            all_edge_kwargs={"alpha": 1},
            suppress_deprecation_warning=True,
        )

        with warnings.catch_warnings():
            warnings.simplefilter("error")
            hp.add_edge_kwargs(axis_id_1="High", axis_id_2="Medium_repeat", c="C6")

        with warnings.catch_warnings():
            warnings.simplefilter("error")
            hp.add_edge_kwargs(axis_id_1="High", axis_id_2="High_repeat", c="C6")

        with warnings.catch_warnings():
            warnings.simplefilter("error")
            hp.add_edge_kwargs(axis_id_1="High_repeat", axis_id_2="High", c="C6")

        # remove just the tagged edges to get specific warnings
        hp.reset_edges(axis_id_1="Medium", axis_id_2="Low_repeat", tag=0)
        with pytest.warns() as record:
            hp.add_edge_kwargs(axis_id_1="Medium", axis_id_2="Low_repeat", c="C6")
        assert len(record) == 2, (
            "Should have yelled at both directions for having no tagged edges"
            + f"\n{[i.message.args for i in record]}"
        )

        # remove just some edge structure to get specific warnings
        hp.reset_edges(axis_id_1="Low")

        with pytest.warns() as record:
            hp.add_edge_kwargs(axis_id_1="High_repeat", axis_id_2="Low", c="C6")
        assert len(record) == 2, (
            "Should have yelled at both directions for having no edges"
            + f"\n{[i.message.args for i in record]}"
        )

        with pytest.warns() as record:
            hp.add_edge_kwargs(axis_id_1="Low", axis_id_2="High_repeat", c="C6")
        assert len(record) == 2, (
            "Should have yelled at both directions for having no edges"
            + f"\n{[i.message.args for i in record]}"
        )

        # remove all edge structure, same warnings should persist without errors running
        hp.reset_edges()

        with pytest.warns() as record:
            hp.add_edge_kwargs(axis_id_1="High", axis_id_2="Medium_repeat", c="C6")
        assert len(record) == 2, (
            "Should have yelled at both directions for having no edges"
            + f"\n{[i.message.args for i in record]}"
        )

        with pytest.warns() as record:
            hp.add_edge_kwargs(axis_id_1="High", axis_id_2="High_repeat", c="C6")
        assert len(record) == 1, (
            "Should have yelled once about repeat axes specifically"
            + f"\n{[i.message.args for i in record]}"
        )

        with pytest.warns() as record:
            hp.add_edge_kwargs(axis_id_1="High_repeat", axis_id_2="High", c="C6")
        assert len(record) == 1, (
            "Should have yelled once about repeat axes specifically"
            + f"\n{[i.message.args for i in record]}"
        )

    def test_copy(self) -> None:
        """
        Make sure ``BaseHivePlot.copy()`` plays nice.
        """
        hp, edges = three_axis_basehiveplot_example()

        hp_copy = hp.copy()

        hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B", a1_to_a2=False)

        assert len(list(hp_copy.hive_plot_edges.keys())) == 0
        assert len(list(hp.hive_plot_edges.keys())) == 1

        hp_copy.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B", a2_to_a1=False)

        assert len(list(hp_copy.hive_plot_edges.keys())) == 1
        assert "A" in hp_copy.hive_plot_edges

        assert len(list(hp.hive_plot_edges.keys())) == 1
        assert "B" in hp.hive_plot_edges

    def test_to_json(self) -> None:
        """
        Make sure ``hiveplotlib.BaseHivePlot.to_json()`` returns all the relevant information without error.
        """
        # generate synthetic node data from a pandas dataframe
        num_nodes = 15
        rng = np.random.default_rng(0)
        data = pd.DataFrame(
            np.c_[np.ones(num_nodes), np.ones(num_nodes), np.ones(num_nodes)],
            columns=["low", "med", "high"],
        )

        # convert into dictionaries for later use
        node_data = data.to_dict(orient="records")

        # use the dataframe's index as unique id
        node_ids = data.index.to_numpy()

        nodes = [
            Node(unique_id=node_id, data=node_data[i])
            for i, node_id in enumerate(node_ids)
        ]

        # generate random edges
        num_edges = 1

        edges = rng.choice(node_ids, size=num_edges * 2).reshape(-1, 2)

        # force all nodes to end points of each axis
        hp = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=nodes),
            edges=edges,
            axes_assignments=[
                np.arange(num_nodes)[: num_nodes // 3],
                np.arange(num_nodes)[num_nodes // 3 : 2 * num_nodes // 3],
                np.arange(num_nodes)[2 * num_nodes // 3 :],
            ],
            sorting_variables=["low", "med", "high"],
            repeat_axes=[True] * 3,
            axes_names=["Low", "Medium", "High"],
            orient_angle=-30,
            vmins=[0] * 3,
            vmaxes=[1] * 3,
            all_edge_kwargs={"alpha": 1, "c": "black"},
            suppress_deprecation_warning=True,
        )

        hp_json = hp.to_json()

        # check our outputs
        hp_dict = json.loads(hp_json)

        assert sorted(hp_dict.keys()) == ["axes", "edges"]

        for axis in hp_dict["axes"]:
            # check axes locations preserved
            assert list(hp.axes[axis].start) == hp_dict["axes"][axis]["start"]
            assert list(hp.axes[axis].end) == hp_dict["axes"][axis]["end"]

            # check node locations in Cartesian space preserved
            for val in ["x", "y"]:
                json_values = hp_dict["axes"][axis]["nodes"][val]
                # should all be same position
                assert np.unique(json_values).size == 1
                # should be same as what's in the hive plot
                assert (
                    json_values[0] == hp.axes[axis].node_placements[val].to_numpy()[0]
                )

        # check the one edge we drew
        arr_list = [
            np.array(arr).astype(float)
            for arr in hp_dict["edges"]["High"]["Medium_repeat"]["0"]["curves"]
        ]
        # add the NaN values back in to each curve to be comparable
        arr_list = [np.vstack([arr, [np.nan, np.nan]]) for arr in arr_list]
        # concatenate back together
        arr = np.vstack(arr_list)
        assert np.allclose(
            arr,
            hp.hive_plot_edges["High"]["Medium_repeat"][0]["curves"],
            rtol=0.0,
            atol=0.0,
            equal_nan=True,
        )
        assert arr.shape == (101, 2)
        # starts at "High" endpoint
        assert list(arr[0, :]) == list(hp.axes["High"].end)
        # ends at "Medium_repeat" endpoint (remember the NaN spacer so index -2)
        assert list(arr[-2, :]) == list(hp.axes["Medium_repeat"].end)

        # check the kwargs
        assert hp_dict["edges"]["High"]["Medium_repeat"]["0"]["edge_kwargs"] == {
            "alpha": 1,
            "c": "black",
        }

    def test_to_json_example_hp(self) -> None:
        """
        Make sure ``hiveplotlib.BaseHivePlot.to_json()`` returns the same output as stored on disk.

        This output is used in documentation (linking to the file as a URL from Gitlab), so this test will check that we
        maintain that file accordingly.
        """
        hp = example_base_hive_plot(
            num_nodes=15,
            num_edges=30,
            seed=0,
            suppress_deprecation_warning=True,
        )
        hp_json_as_dict = json.loads(hp.to_json())

        with open("./data/example_hive_plot.json", "r") as f:
            expected_hp_json_as_dict = json.load(f)

        message = (
            "Expected Hive Plot JSON different from actual hive plot JSON on disk. This could have happened for a few "
            "reasons.\n"
            "1. Something has changed with the ``hiveplotlib.datasets.example_hive_plot()`` method.\n"
            "2. Something has changed with the ``hiveplotlib.HivePlot.to_json()`` method.\n"
            "3. Something has changed with lower-level hive plot calculations / data structures.\n"
            "4. A package dependency has changed the values somehow (e.g. sig figs, or 'code likes to break'...).\n\n"
            "If this change was *INTENTIONAL*, simply re-run the following python snippet from the root of the "
            "repository:\n\n"
            "```{python}\n"
            "from hiveplotlib.datasets import example_hive_plot\n"
            "hp = example_hive_plot(num_nodes=15, num_edges=30, seed=0)\n"
            "hp_json = hp.to_json()\n"
            "with open('./data/example_hive_plot.json', 'w') as f:\n"
            "    f.write(hp_json)\n"
            "```\n"
        )

        assert hp_json_as_dict == expected_hp_json_as_dict, message

    def test_place_nodes_on_axis_removed_unique_ids(self) -> None:
        """
        Test ``place_nodes_on_axis()``method fails when trying to use ``unique_ids`` parameter.
        """
        hp = example_base_hive_plot(
            num_nodes=15,
            num_edges=30,
            seed=0,
            suppress_deprecation_warning=True,
        )
        try:
            hp.place_nodes_on_axis(axis_id="A", unique_ids=[0, 1])
            pytest.fail(
                reason="Should have failed when trying to use the no-longer-supported `unique_is` parameter."
            )
        except TypeError:
            assert True


class TestHivePlotNAxesKwargs:
    """
    Tests that various kwarg options in ``hive_plot_n_axes()`` are cooperating.
    """

    def setup_method(self) -> None:
        """
        Perform setup.
        """
        rng = np.random.default_rng(0)

        num_nodes = 51
        num_edges = 100

        # a and b columns move in sync with eventual node id (index), c is random
        data = pd.DataFrame(
            np.c_[
                np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
                np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
                rng.uniform(low=0, high=10, size=num_nodes),
            ],
            columns=["a", "b", "c"],
        )

        # convert into dictionaries for later use
        node_data = data.to_dict(orient="records")

        # use the dataframe's index as unique id
        node_ids = data.index.to_numpy()

        self.nodes = [
            Node(unique_id=node_id, data=node_data[i])
            for i, node_id in enumerate(node_ids)
        ]

        self.edges = rng.choice(node_ids, size=num_edges * 2).reshape(-1, 2)

        # randomly split node ids into 3 groups
        axes_assignments = node_ids.copy()
        rng.shuffle(axes_assignments)
        self.axes_assignments = list(axes_assignments.reshape(3, -1))

        self.sorting_variables = ["a", "b", "c"]
        self.axes_names = ["A", "B", "C"]

    def test_all_edge_kwargs(self) -> None:
        """
        Make sure the ``all_edge_kwargs`` parameter behaves as expected.
        """
        hp = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=self.nodes),
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            suppress_deprecation_warning=True,
        )

        # shouldn't have any dotted line styles
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                if "ls" in hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]:
                    assert (
                        hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]["ls"] != "dotted"
                    ), "No lines should be dotted here"

        hp = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=self.nodes),
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            all_edge_kwargs={"ls": "dotted"},
            ccw_edge_kwargs={},  # ccw_edge_kwargs defaults to dashed
            suppress_deprecation_warning=True,
        )

        # EVERY line should have dotted style
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                assert hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]["ls"] == "dotted", (
                    "All lines should be dotted here"
                )

    def test_cw_edge_kwargs(self) -> None:
        """
        Make sure the ``cw_edge_kwargs`` parameter behaves as expected.
        """
        hp = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=self.nodes),
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            suppress_deprecation_warning=True,
        )

        # shouldn't have any dotted line styles
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                if "ls" in hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]:
                    assert (
                        hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]["ls"] != "dotted"
                    ), "No lines should be dotted here"

        hp = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=self.nodes),
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            cw_edge_kwargs={"ls": "dotted"},
            ccw_edge_kwargs={},  # ccw_edge_kwargs defaults to dashed
            suppress_deprecation_warning=True,
        )

        # EVERY cw line should have dotted style
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                if (a0, a1) in [("B", "A"), ("C", "B"), ("A", "C")]:
                    assert (
                        hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]["ls"] == "dotted"
                    ), "All cw lines should be dotted here"
                elif "ls" in hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]:
                    assert (
                        hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]["ls"] != "dotted"
                    ), "No ccw lines should be dotted here"

    def test_ccw_edge_kwargs(self) -> None:
        """
        Make sure the ``ccw_edge_kwargs`` parameter behaves as expected.
        """
        hp = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=self.nodes),
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            suppress_deprecation_warning=True,
        )

        # shouldn't have any dotted line styles
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                if "ls" in hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]:
                    assert (
                        hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]["ls"] != "dotted"
                    ), "No lines should be dotted here"

        hp = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=self.nodes),
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            ccw_edge_kwargs={"ls": "dotted"},
            suppress_deprecation_warning=True,
        )

        # EVERY ccw line should have dotted style
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                if (a0, a1) in [("A", "B"), ("B", "C"), ("C", "A")]:
                    assert (
                        hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]["ls"] == "dotted"
                    ), "All ccw lines should be dotted here"
                elif "ls" in hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]:
                    assert (
                        hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]["ls"] != "dotted"
                    ), "No cw lines should be dotted here"

    def test_repeat_edge_kwargs(self) -> None:
        """
        Make sure the ``repeat_edge_kwargs`` parameter behaves as expected.
        """
        hp = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=self.nodes),
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            repeat_axes=[True, True, True],
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            suppress_deprecation_warning=True,
        )

        # shouldn't have any dotted line styles
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                if "ls" in hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]:
                    assert (
                        hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]["ls"] != "dotted"
                    ), "No lines should be dotted here"

        hp = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=self.nodes),
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            repeat_axes=[True, True, True],
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            repeat_edge_kwargs={"ls": "dotted"},
            suppress_deprecation_warning=True,
        )

        # EVERY repeat axis line should have dotted style
        repeat_axes = ["A_repeat", "B_repeat", "C_repeat"]
        repeat_axis_pairs = list(zip(self.axes_names, repeat_axes)) + list(
            zip(repeat_axes, self.axes_names)
        )
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                if (a0, a1) in repeat_axis_pairs:
                    assert (
                        hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]["ls"] == "dotted"
                    ), "All repeat lines should be dotted here"
                elif "ls" in hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]:
                    assert (
                        hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]["ls"] != "dotted"
                    ), "No non-repeat lines should be dotted here"

        # make sure we trigger warnings on redundant kwargs, check kwargs

        with pytest.warns() as record:
            hp = hive_plot_n_axes(
                nodes=node_collection_from_node_list(node_list=self.nodes),
                edges=[self.edges] * 2,
                axes_assignments=self.axes_assignments,
                repeat_axes=[True, True, True],
                sorting_variables=self.sorting_variables,
                axes_names=self.axes_names,
                ccw_edge_kwargs={},
                all_edge_kwargs={"c": "red"},
                repeat_edge_kwargs={"c": "blue"},
                suppress_deprecation_warning=True,
            )

        assert len(record) > 0, (
            "Expected warnings per pair of repeat axes each direction to connect about conflicting kwargs"
        )
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                for i in range(2):
                    if (a0, a1) in repeat_axis_pairs:
                        assert (
                            hp.hive_plot_edges[a0][a1][i]["edge_kwargs"]["c"] == "blue"
                        ), "Wrong kwarg"
                    else:
                        assert (
                            hp.hive_plot_edges[a0][a1][i]["edge_kwargs"]["c"] == "red"
                        ), "Wrong kwarg"

        # check that unique all_edge_kwargs propagate as expected
        with warnings.catch_warnings():
            warnings.simplefilter("error")
            hp = hive_plot_n_axes(
                nodes=node_collection_from_node_list(node_list=self.nodes),
                edges=[self.edges] * 2,
                axes_assignments=self.axes_assignments,
                repeat_axes=[True, True, True],
                sorting_variables=self.sorting_variables,
                axes_names=self.axes_names,
                ccw_edge_kwargs={},
                all_edge_kwargs={"lw": 2},
                repeat_edge_kwargs={"c": "blue"},
                suppress_deprecation_warning=True,
            )
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                for i in range(2):
                    assert hp.hive_plot_edges[a0][a1][i]["edge_kwargs"]["lw"] == 2, (
                        "Wrong kwarg"
                    )

    def test_edge_list_kwargs(self) -> None:
        """
        Make sure ``edge_list_kwargs`` parameter behaves as expected.
        """
        hp = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=self.nodes),
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            repeat_axes=[True, True, True],
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            ccw_edge_kwargs={},
            edge_list_kwargs=None,
            suppress_deprecation_warning=True,
        )
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                assert (
                    len(list(hp.hive_plot_edges[a0][a1][0]["edge_kwargs"].keys())) == 0
                ), (
                    f"Found edge kwargs but expected None {hp.hive_plot_edges[a0][a1][0]['edge_kwargs']}"
                )

        hp = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=self.nodes),
            edges=[self.edges] * 2,
            axes_assignments=self.axes_assignments,
            repeat_axes=[True, True, True],
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            ccw_edge_kwargs={},
            edge_list_kwargs=None,
            suppress_deprecation_warning=True,
        )
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                assert (
                    len(list(hp.hive_plot_edges[a0][a1][0]["edge_kwargs"].keys())) == 0
                ), "Shouldn't have any edge kwargs"

        hp = hive_plot_n_axes(
            nodes=node_collection_from_node_list(node_list=self.nodes),
            edges=[self.edges] * 2,
            axes_assignments=self.axes_assignments,
            repeat_axes=[True, True, True],
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            ccw_edge_kwargs={},
            edge_list_kwargs=[{"c": "blue"}, None],
            suppress_deprecation_warning=True,
        )
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                assert hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]["c"] == "blue", (
                    "Wrong kwarg"
                )
                assert (
                    len(list(hp.hive_plot_edges[a0][a1][1]["edge_kwargs"].keys())) == 0
                ), "Shouldn't have any edge kwargs"

        try:
            hive_plot_n_axes(
                nodes=node_collection_from_node_list(node_list=self.nodes),
                edges=[self.edges] * 2,
                axes_assignments=self.axes_assignments,
                repeat_axes=[True, True, True],
                sorting_variables=self.sorting_variables,
                axes_names=self.axes_names,
                ccw_edge_kwargs={},
                edge_list_kwargs=[{"c": "blue"}] * 3,
                suppress_deprecation_warning=True,
            )
            pytest.fail("Shouldn't allow wrong number of `edge_list_kwargs`")
        except AssertionError:
            assert True

        # repeat_edge_kwargs clashes with edge_list_kwargs
        with pytest.warns() as record:
            hp = hive_plot_n_axes(
                nodes=node_collection_from_node_list(node_list=self.nodes),
                edges=[self.edges] * 2,
                axes_assignments=self.axes_assignments,
                repeat_axes=[True, True, True],
                sorting_variables=self.sorting_variables,
                axes_names=self.axes_names,
                ccw_edge_kwargs={},
                repeat_edge_kwargs={"c": "red"},
                edge_list_kwargs=[{"c": "blue"}] * 2,
                suppress_deprecation_warning=True,
            )

        assert len(record) > 0, (
            "Expected warnings per pair of axes each direction to connect about conflicting kwargs"
        )
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                for i in range(2):
                    assert (
                        hp.hive_plot_edges[a0][a1][i]["edge_kwargs"]["c"] == "blue"
                    ), "Wrong kwarg"

        # all_edge_kwargs clashes with edge_list_kwargs
        with pytest.warns() as record:
            hp = hive_plot_n_axes(
                nodes=node_collection_from_node_list(node_list=self.nodes),
                edges=[self.edges] * 2,
                axes_assignments=self.axes_assignments,
                repeat_axes=[True, True, True],
                sorting_variables=self.sorting_variables,
                axes_names=self.axes_names,
                ccw_edge_kwargs={},
                all_edge_kwargs={"c": "red"},
                edge_list_kwargs=[{"c": "blue"}] * 2,
                suppress_deprecation_warning=True,
            )

        assert len(record) > 0, (
            "Expected warnings per pair of axes each direction to connect about conflicting kwargs"
        )
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                for i in range(2):
                    assert (
                        hp.hive_plot_edges[a0][a1][i]["edge_kwargs"]["c"] == "blue"
                    ), "Wrong kwarg"

        # cw_edge_kwargs clashes with edge_list_kwargs
        with pytest.warns() as record:
            hp = hive_plot_n_axes(
                nodes=node_collection_from_node_list(node_list=self.nodes),
                edges=[self.edges] * 2,
                axes_assignments=self.axes_assignments,
                repeat_axes=[True, True, True],
                sorting_variables=self.sorting_variables,
                axes_names=self.axes_names,
                ccw_edge_kwargs={},
                cw_edge_kwargs={"c": "red"},
                edge_list_kwargs=[{"c": "blue"}] * 2,
                suppress_deprecation_warning=True,
            )

        assert len(record) > 0, (
            "Expected warnings per pair of axes each direction to connect about conflicting kwargs"
        )
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                for i in range(2):
                    assert (
                        hp.hive_plot_edges[a0][a1][i]["edge_kwargs"]["c"] == "blue"
                    ), "Wrong kwarg"

        # ccw_edge_kwargs clashes with edge_list_kwargs
        with pytest.warns() as record:
            hp = hive_plot_n_axes(
                nodes=node_collection_from_node_list(node_list=self.nodes),
                edges=[self.edges] * 2,
                axes_assignments=self.axes_assignments,
                repeat_axes=[True, True, True],
                sorting_variables=self.sorting_variables,
                axes_names=self.axes_names,
                ccw_edge_kwargs={"c": "red"},
                edge_list_kwargs=[{"c": "blue"}] * 2,
                suppress_deprecation_warning=True,
            )

        assert len(record) > 0, (
            "Expected warnings per pair of axes each direction to connect about conflicting kwargs"
        )
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                for i in range(2):
                    assert (
                        hp.hive_plot_edges[a0][a1][i]["edge_kwargs"]["c"] == "blue"
                    ), "Wrong kwarg"


class TestMultipleEdgeSets:
    """
    Tests focused on checking for appropriate behavior when adding multiple sets of edges between one pair of axes.
    """

    def test_add_edge_ids_2_sets(self) -> None:
        """
        Check that ``BaseHivePlot.add_edge_ids()`` plays nice when adding 2 sets of edges.
        """
        hp, edges = three_axis_basehiveplot_example()

        num_edges = edges.shape[0]
        tag_0 = hp.add_edge_ids(
            edges=edges[:num_edges, :], axis_id_1="A", axis_id_2="B", tag=5
        )
        assert tag_0 == 5
        assert list(hp.hive_plot_edges["A"]["B"].keys()) == [tag_0]
        assert list(hp.hive_plot_edges["B"]["A"].keys()) == [tag_0]

        tag_1 = hp.add_edge_ids(
            edges=edges[num_edges:, :],
            axis_id_1="A",
            axis_id_2="B",
            tag=10,
            a2_to_a1=False,
        )
        assert tag_1 == 10
        assert sorted(hp.hive_plot_edges["A"]["B"].keys()) == [tag_0, tag_1]
        assert list(hp.hive_plot_edges["B"]["A"].keys()) == [tag_0], (
            f"Should not have added B -> A with tag {tag_1}"
        )

    @pytest.mark.parametrize("a2_to_a1", [True, False])
    def test_add_edge_curves_between_axes_2_sets(self, a2_to_a1: bool) -> None:
        """
        Check tht ``BaseHivePlot.add_edge_curves_between_axes()`` plays nice when adding 2 sets of edges.

        :param a2_to_a1: whether to construct the axis 2 -> axis 1 curves (True) or not (False).
        """
        hp, edges = three_axis_basehiveplot_example()

        num_edges = edges.shape[0]
        # build out IDs in both directions, but check curves as we add
        tag_0 = hp.add_edge_ids(
            edges=edges[:num_edges, :], axis_id_1="A", axis_id_2="B", tag=5
        )
        tag_1 = hp.add_edge_ids(
            edges=edges[num_edges:, :], axis_id_1="A", axis_id_2="B", tag=10
        )

        hp.add_edge_curves_between_axes(
            axis_id_1="A", axis_id_2="B", tag=tag_0, a2_to_a1=a2_to_a1
        )
        assert "curves" in hp.hive_plot_edges["A"]["B"][tag_0]
        a_b_tag_0_curves = hp.hive_plot_edges["A"]["B"][tag_0]["curves"].copy()
        if a2_to_a1:
            assert "curves" in hp.hive_plot_edges["B"]["A"][tag_0]
        else:
            assert "curves" not in hp.hive_plot_edges["B"]["A"][tag_0]

        assert "curves" not in hp.hive_plot_edges["A"]["B"][tag_1], (
            f"Tag {tag_1} curves should not exist yet"
        )
        assert "curves" not in hp.hive_plot_edges["B"]["A"][tag_1], (
            f"Tag {tag_1} curves should not exist yet"
        )

        hp.add_edge_curves_between_axes(
            axis_id_1="A", axis_id_2="B", tag=tag_1, a2_to_a1=a2_to_a1
        )

        # check equal arrays but tolerating the nan spacers
        assert np.allclose(
            a_b_tag_0_curves,
            hp.hive_plot_edges["A"]["B"][tag_0]["curves"],
            rtol=0.0,
            atol=0.0,
            equal_nan=True,
        ), (
            f"Tag {tag_0} curves should not be altered by doing anything to Tag {tag_1} curves"
        )

        assert "curves" in hp.hive_plot_edges["A"]["B"][tag_1], (
            f"Tag {tag_1} curves should now exist"
        )
        if a2_to_a1:
            assert "curves" in hp.hive_plot_edges["B"]["A"][tag_1], (
                f"Tag {tag_1} curves should now exist"
            )
        else:
            assert "curves" not in hp.hive_plot_edges["B"]["A"][tag_1], (
                f"Tag {tag_1} curves should not exist in this case"
            )

    def test_construct_curves_2_sets(self) -> None:
        """
        Check that ``BaseHivePlot.construct_curves()`` plays nice when adding 2 sets of edges.
        """
        hp, edges = three_axis_basehiveplot_example()

        num_edges = edges.shape[0]
        # build out IDs in both directions, but check curves as we add
        tag_0 = hp.add_edge_ids(
            edges=edges[:num_edges, :], axis_id_1="A", axis_id_2="B", tag=5
        )
        tag_1 = hp.add_edge_ids(
            edges=edges[num_edges:, :], axis_id_1="A", axis_id_2="B", tag=10
        )

        assert "curves" not in hp.hive_plot_edges["A"]["B"][tag_0]
        assert "curves" not in hp.hive_plot_edges["A"]["B"][tag_1]
        assert "curves" not in hp.hive_plot_edges["B"]["A"][tag_0]
        assert "curves" not in hp.hive_plot_edges["B"]["A"][tag_1]

        hp.construct_curves()

        assert "curves" in hp.hive_plot_edges["A"]["B"][tag_0]
        assert "curves" in hp.hive_plot_edges["A"]["B"][tag_1]
        assert "curves" in hp.hive_plot_edges["B"]["A"][tag_0]
        assert "curves" in hp.hive_plot_edges["B"]["A"][tag_1]

    def test_add_edge_kwargs_2_sets(self) -> None:
        """
        Check that ``BaseHivePlot.add_edge_kwargs()`` plays nice when there are 2 sets of edges.
        """
        hp, edges = three_axis_basehiveplot_example()

        num_edges = edges.shape[0]
        # build out IDs in both directions, but check curves as we add
        tag_0 = hp.add_edge_ids(
            edges=edges[:num_edges, :], axis_id_1="A", axis_id_2="B", tag=5
        )
        tag_1 = hp.add_edge_ids(
            edges=edges[num_edges:, :], axis_id_1="A", axis_id_2="B", tag=10
        )

        assert "edge_kwargs" not in hp.hive_plot_edges["A"]["B"][tag_0]
        assert "edge_kwargs" not in hp.hive_plot_edges["B"]["A"][tag_0]

        assert "edge_kwargs" not in hp.hive_plot_edges["A"]["B"][tag_1]
        assert "edge_kwargs" not in hp.hive_plot_edges["B"]["A"][tag_1]

        try:
            hp.add_edge_kwargs(axis_id_1="A", axis_id_2="B", tag=None)
            pytest.fail(
                "Should've triggered an error for not specifying a non-None tag"
            )
        except ValueError:
            assert True

        # just tag_0 changes first
        hp.add_edge_kwargs(axis_id_1="A", axis_id_2="B", tag=tag_0, c="cornflowerblue")

        assert "edge_kwargs" in hp.hive_plot_edges["A"]["B"][tag_0]
        assert "edge_kwargs" in hp.hive_plot_edges["B"]["A"][tag_0]

        assert list(hp.hive_plot_edges["A"]["B"][tag_0]["edge_kwargs"].keys()) == ["c"]
        assert (
            hp.hive_plot_edges["A"]["B"][tag_0]["edge_kwargs"]["c"] == "cornflowerblue"
        )

        assert list(hp.hive_plot_edges["B"]["A"][tag_0]["edge_kwargs"].keys()) == ["c"]
        assert (
            hp.hive_plot_edges["B"]["A"][tag_0]["edge_kwargs"]["c"] == "cornflowerblue"
        )

        assert "edge_kwargs" not in hp.hive_plot_edges["A"]["B"][tag_1]
        assert "edge_kwargs" not in hp.hive_plot_edges["B"]["A"][tag_1]

        # one direction of tag_1 changes
        hp.add_edge_kwargs(
            axis_id_1="A", axis_id_2="B", a2_to_a1=False, tag=tag_1, c="seagreen"
        )

        assert "edge_kwargs" in hp.hive_plot_edges["A"]["B"][tag_1]
        assert "edge_kwargs" not in hp.hive_plot_edges["B"]["A"][tag_1]

        assert list(hp.hive_plot_edges["A"]["B"][tag_1]["edge_kwargs"].keys()) == ["c"]
        assert hp.hive_plot_edges["A"]["B"][tag_1]["edge_kwargs"]["c"] == "seagreen"

        # bidirectional tag_1 changes, switch the existing color too
        hp.add_edge_kwargs(axis_id_1="A", axis_id_2="B", tag=tag_1, c="maroon", lw=2)

        assert "edge_kwargs" in hp.hive_plot_edges["A"]["B"][tag_1]
        assert "edge_kwargs" in hp.hive_plot_edges["B"]["A"][tag_1]

        assert sorted(hp.hive_plot_edges["A"]["B"][tag_1]["edge_kwargs"].keys()) == [
            "c",
            "lw",
        ]
        assert hp.hive_plot_edges["A"]["B"][tag_1]["edge_kwargs"]["c"] == "maroon"

        assert list(hp.hive_plot_edges["B"]["A"][tag_1]["edge_kwargs"].keys()) == [
            "c",
            "lw",
        ]
        assert hp.hive_plot_edges["B"]["A"][tag_1]["edge_kwargs"]["c"] == "maroon"

        # tag_0 should be unaffected by all of this
        assert list(hp.hive_plot_edges["A"]["B"][tag_0]["edge_kwargs"].keys()) == ["c"]
        assert (
            hp.hive_plot_edges["A"]["B"][tag_0]["edge_kwargs"]["c"] == "cornflowerblue"
        )

        assert list(hp.hive_plot_edges["B"]["A"][tag_0]["edge_kwargs"].keys()) == ["c"]
        assert (
            hp.hive_plot_edges["B"]["A"][tag_0]["edge_kwargs"]["c"] == "cornflowerblue"
        )

    def test_connect_axes_2_sets(self) -> None:
        """
        Check that ``BaseHivePlot.connect_axes()`` plays nice when there are 2 sets of edges.
        """
        hp, edges = three_axis_basehiveplot_example()

        num_edges = edges.shape[0]

        tag_0 = hp.connect_axes(
            edges=edges[:num_edges, :], axis_id_1="A", axis_id_2="B", c="khaki"
        )
        assert tag_0 == 0
        tag_1 = hp.connect_axes(
            edges=edges[num_edges:, :],
            axis_id_1="A",
            axis_id_2="B",
            tag=10,
            c="darksalmon",
            lw=5,
        )

        assert list(hp.hive_plot_edges["A"]["B"][tag_0]["edge_kwargs"].keys()) == ["c"]
        assert hp.hive_plot_edges["A"]["B"][tag_0]["edge_kwargs"]["c"] == "khaki"

        assert list(hp.hive_plot_edges["B"]["A"][tag_0]["edge_kwargs"].keys()) == ["c"]
        assert hp.hive_plot_edges["B"]["A"][tag_0]["edge_kwargs"]["c"] == "khaki"

        assert sorted(hp.hive_plot_edges["A"]["B"][tag_1]["edge_kwargs"].keys()) == [
            "c",
            "lw",
        ]
        assert hp.hive_plot_edges["A"]["B"][tag_1]["edge_kwargs"]["c"] == "darksalmon"

        assert list(hp.hive_plot_edges["B"]["A"][tag_1]["edge_kwargs"].keys()) == [
            "c",
            "lw",
        ]
        assert hp.hive_plot_edges["B"]["A"][tag_1]["edge_kwargs"]["c"] == "darksalmon"


def test_connect_axes_change_control_pt_rho() -> None:
    """
    Check that ``BaseHivePlot.connect_axes()`` changes edges when we alter the ``control_rho_scale`` parameter.
    """
    # set up example with a single edge
    num_axes = 2
    nodes, node_splits, edges = example_nodes_and_edges(
        num_axes=num_axes, num_edges=1, seed=1
    )

    hp = BaseHivePlot()
    hp.add_nodes(nodes)

    for i in range(num_axes):
        temp_axis = Axis(
            axis_id=i,
            start=1,
            end=5,
            angle=30 + 120 * i,
            long_name=f"Axis {i}",
        )
        hp.add_axes(temp_axis)
        hp.place_nodes_on_axis(
            axis_id=i,
            node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=node_splits[i]),
            sorting_feature_to_use="low",
        )

    # make default hp edge first
    default_hp = hp.copy()
    default_hp.connect_axes(
        edges=edges,
        axis_id_1=0,
        axis_id_2=1,
    )

    # build lower rho edge
    low_rho_hp = hp.copy()
    low_rho_hp.connect_axes(
        edges=edges,
        axis_id_1=0,
        axis_id_2=1,
        control_rho_scale=0.5,
    )

    # build higher rho edge
    high_rho_hp = hp.copy()
    high_rho_hp.connect_axes(
        edges=edges,
        axis_id_1=0,
        axis_id_2=1,
        control_rho_scale=1.5,
    )

    # grab edge, but skip the first and last points (which will be equal because node points)
    #  plus, skip the nan, nan record at the end.
    default_edge_cartesian = default_hp.hive_plot_edges[1][0][0]["curves"][1:-2, :]
    default_rhos = cartesian2polar(
        default_edge_cartesian[:, 0], default_edge_cartesian[:, 1]
    )[0]

    # grab edge, but skip the first and last points (which will be equal because node points)
    #  plus, skip the nan, nan record at the end.
    low_rho_edge_cartesian = low_rho_hp.hive_plot_edges[1][0][0]["curves"][1:-2, :]
    low_rho_rhos = cartesian2polar(
        low_rho_edge_cartesian[:, 0], low_rho_edge_cartesian[:, 1]
    )[0]

    # grab edge, but skip the first and last points (which will be equal because node points)
    #  plus, skip the nan, nan record at the end.
    high_rho_edge_cartesian = high_rho_hp.hive_plot_edges[1][0][0]["curves"][1:-2, :]
    high_rho_rhos = cartesian2polar(
        high_rho_edge_cartesian[:, 0], high_rho_edge_cartesian[:, 1]
    )[0]

    assert (low_rho_rhos < default_rhos).all()
    assert (default_rhos < high_rho_rhos).all()


def test_connect_axes_change_control_pt_angle() -> None:
    """
    Check that ``BaseHivePlot.connect_axes()`` changes edges when we alter the ``control_angle_shift`` param.
    """
    # set up example with a single edge
    num_axes = 2
    nodes, node_splits, edges = example_nodes_and_edges(
        num_axes=num_axes, num_edges=1, seed=1
    )

    hp = BaseHivePlot()
    hp.add_nodes(nodes)

    for i in range(num_axes):
        temp_axis = Axis(
            axis_id=i,
            start=1,
            end=5,
            angle=30 + 120 * i,
            long_name=f"Axis {i}",
        )
        hp.add_axes(temp_axis)
        hp.place_nodes_on_axis(
            axis_id=i,
            node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=node_splits[i]),
            sorting_feature_to_use="low",
        )

    # make default hp edge first
    default_hp = hp.copy()
    default_hp.connect_axes(
        edges=edges,
        axis_id_1=0,
        axis_id_2=1,
    )

    # build lower angle edge
    low_angle_hp = hp.copy()
    low_angle_hp.connect_axes(
        edges=edges,
        axis_id_1=0,
        axis_id_2=1,
        control_angle_shift=-20,
    )

    # build higher angle edge
    high_angle_hp = hp.copy()
    high_angle_hp.connect_axes(
        edges=edges,
        axis_id_1=0,
        axis_id_2=1,
        control_angle_shift=20,
    )

    # grab edge, but skip the first and last points (which will be equal because node points)
    #  plus, skip the nan, nan record at the end.
    default_edge_cartesian = default_hp.hive_plot_edges[1][0][0]["curves"][1:-2, :]
    default_angles = cartesian2polar(
        default_edge_cartesian[:, 0], default_edge_cartesian[:, 1]
    )[1]

    # grab edge, but skip the first and last points (which will be equal because node points)
    #  plus, skip the nan, nan record at the end.
    low_angle_edge_cartesian = low_angle_hp.hive_plot_edges[1][0][0]["curves"][1:-2, :]
    low_angle_angles = cartesian2polar(
        low_angle_edge_cartesian[:, 0], low_angle_edge_cartesian[:, 1]
    )[1]

    # grab edge, but skip the first and last points (which will be equal because node points)
    #  plus, skip the nan, nan record at the end.
    high_angle_edge_cartesian = high_angle_hp.hive_plot_edges[1][0][0]["curves"][
        1:-2, :
    ]
    high_angle_angles = cartesian2polar(
        high_angle_edge_cartesian[:, 0], high_angle_edge_cartesian[:, 1]
    )[1]

    assert (low_angle_angles < default_angles).all()
    assert (default_angles < high_angle_angles).all()


@pytest.mark.parametrize("wrong_name_column", [True, False])
@pytest.mark.parametrize("from_to", ["from", "to"])
def test_add_second_edge_set(wrong_name_column: bool, from_to: str) -> None:
    """
    Test adding a second set of edges to an existing hive plot.

    The ``False-"from"`` and ``False-"to"`` parameter options are the same test, keeping for DRY code though.

    :param wrong_name_column: whether to add a right or wrong (i.e. different) column name to either "from edges" or
        "to edges". A different column name for the second edge set should trigger an ``AssertionError``.
    :param from_to: whether to change "from edges" or "to edges".
    """
    first_edge_count = 100
    second_edge_count = 50

    hp = example_hive_plot(num_edges=first_edge_count)
    if wrong_name_column:
        if from_to == "from":
            second_edge_set = example_edges(
                nodes=hp.nodes,
                num_edges=second_edge_count,
                from_column_name="different_wrong_column_name",
            )
        elif from_to == "to":
            second_edge_set = example_edges(
                nodes=hp.nodes,
                num_edges=second_edge_count,
                to_column_name="different_wrong_column_name",
            )
        else:
            raise NotImplementedError()
    else:
        second_edge_set = example_edges(
            nodes=hp.nodes,
            num_edges=second_edge_count,
        )

    if wrong_name_column:
        try:
            hp.add_edges(edges=second_edge_set)
            pytest.fail("Different column name should have raised `AssertionError`")
        except AssertionError:
            assert len(hp.edges) == first_edge_count, (
                "Should have errored out and only kept first edge set."
            )
    else:
        hp.add_edges(edges=second_edge_set)
        assert len(hp.edges) == first_edge_count + second_edge_count, (
            "Should have accepted first and second edge sets."
        )


class TestHivePlot:
    """
    Tests for the higher-level ``HivePlot`` class.
    """

    def test_instantiate_hive_plot(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance.
        """
        data = example_node_data()
        node_collection = NodeCollection(data=data, unique_id_column="unique_id")
        partition_variable = node_collection.create_partition_variable(
            data_column="low", cutoffs=3
        )
        # pass numpy array of edges for test coverage
        edges = example_edges(nodes=node_collection).export_edge_array()
        HivePlot(
            nodes=node_collection,
            edges=edges,
            partition_variable=partition_variable,
            sorting_variables="low",
        )

    def test_instantiate_partition_variables(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance with custom ``partition_variable`` input.
        """
        # default hive plot partitions into 3 axes
        hp = example_hive_plot()
        assert len(hp.axes) == 3

        # make different node partition with a different split (should result in different number of axes)
        num_expected_axes = 5
        nodes = example_node_collection()
        larger_partition = nodes.create_partition_variable(
            data_column="low",
            cutoffs=num_expected_axes,
        )
        edges = example_edges(nodes=nodes)
        hp = HivePlot(
            nodes=nodes,
            edges=edges,
            partition_variable=larger_partition,
            sorting_variables="low",
        )
        assert len(hp.axes) == num_expected_axes

    def test_instantiate_invalid_partition_variables(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance with invalid partition variable input.
        """
        # two ways to do this input wrong, first is in the underlying setup of the NodeCollection
        try:
            example_hive_plot(partition_data_column="invalid_column")
            pytest.fail("should have failed on invalid data column")
        except InvalidPartitionVariableError:
            assert True

        # second is on instantiation of the HivePlot
        nodes = example_node_collection()
        nodes.create_partition_variable(
            data_column="low",
            cutoffs=3,
        )
        edges = example_edges(nodes=nodes)
        try:
            HivePlot(
                nodes=nodes,
                edges=edges,
                partition_variable="non_existent_partition",
                sorting_variables="low",
            )
            pytest.fail("Should have failed on invalid partition")
        except InvalidPartitionVariableError:
            assert True

    def test_instantiate_sorting_variables_sort_all_axes(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance with custom ``sorting_variables`` input on all axes.
        """
        hp_low = example_hive_plot(sorting_variables="low")
        hp_med = example_hive_plot(sorting_variables="med")
        for axis in hp_low.axes:
            low_data = hp_low.axes[axis].node_placements.sort_values("rho")
            med_data = hp_med.axes[axis].node_placements.sort_values("rho")

            # should have same nodes but different ordering / positioning
            assert len(low_data) == len(med_data)

            low_ids = low_data.unique_id.to_list()
            med_ids = med_data.unique_id.to_list()
            assert set(low_ids).difference(set(med_ids)) == set()
            assert low_ids != med_ids

    def test_instantiate_sorting_variables_sort_one_axis(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance with different ``sorting_variables`` input on just one axis.
        """
        hp_all_low = example_hive_plot(sorting_variables="low")
        med_sorted_axis = "B"
        hp_one_med = example_hive_plot(
            sorting_variables={
                "A": "low",
                "B": "med",
                "C": "low",
            }
        )
        for axis in hp_all_low.axes:
            low_data = hp_all_low.axes[axis].node_placements.sort_values("rho")
            med_data = hp_one_med.axes[axis].node_placements.sort_values("rho")

            # should have same nodes and ordering / positioning, but different ordering for `med_sorted_axis`
            assert len(low_data) == len(med_data)

            low_ids = low_data.unique_id.to_list()
            med_ids = med_data.unique_id.to_list()
            assert set(low_ids).difference(set(med_ids)) == set()
            if axis == med_sorted_axis:
                assert low_ids != med_ids
            else:
                assert low_ids == med_ids

    def test_instantiate_invalid_sorting_variables(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance with invalid ``sorting_variables`` input.
        """
        # two ways to do this input wrong, first is wrong hashable
        try:
            example_hive_plot(sorting_variables="invalid_column")
            pytest.fail("should have failed on invalid data column")
        except InvalidSortingVariableError:
            assert True

        # second is wrong dict value
        try:
            example_hive_plot(
                sorting_variables={
                    "A": "low",
                    "B": "invalid_column",
                    "C": "low",
                }
            )
            pytest.fail("should have failed on invalid data column")
        except InvalidSortingVariableError:
            assert True

    def test_instantiate_sorting_variables_insufficient_sorting(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance with a custom, incomplete dict of ``sorting_variables``.

        (If instantitating a custom dict sorting, all variables must be specified.)
        """
        try:
            example_hive_plot(
                sorting_variables={
                    "A": "low",
                    "C": "low",
                },
            )
            pytest.fail(
                "Insufficiently specified `sorting_variables` should have failed."
            )
        except MissingSortingVariableError:
            example_hive_plot(
                sorting_variables={
                    "A": "low",
                    "B": "low",
                    "C": "low",
                },
            )
            assert True, "Fully specified sorting should have played nice."

    def test_instantiate_sorting_variables_overspecified_sorting(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance with a custom, over-complete dict of ``sorting_variables``.

        (If instantitating a custom dict sorting, all variables must be specified, tolerating but ignoring extra
        variables.)
        """
        hp = example_hive_plot()
        extra_hp = example_hive_plot(
            sorting_variables={
                "A": "low",
                "B": "low",
                "C": "low",
                "my_extra_nonexistent_variable": "low",
            },
        )

        # these hive plots should be the same
        for axis in extra_hp.axes:
            hp_data = hp.axes[axis].node_placements.sort_values("rho")
            extra_hp_data = extra_hp.axes[axis].node_placements.sort_values("rho")

            # should have same nodes and ordering / positioning for all axes
            assert len(hp_data) == len(extra_hp_data)

            low_ids = hp_data.unique_id.to_list()
            med_ids = extra_hp_data.unique_id.to_list()
            assert set(low_ids).difference(set(med_ids)) == set()
            assert low_ids == med_ids

    def test_instantiate_backend_invalid_option(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance with an invalid ``backend`` input.
        """
        try:
            example_hive_plot(backend="potato")
            pytest.fail("Should have raised AssertionError on invalid backend option")
        except AssertionError:
            assert True

    @pytest.mark.parametrize("repeat_axes", [True, False, 1, 2])
    def test_instantiate_repeat_axes(self, repeat_axes: Union[bool, int]) -> None:
        """
        Test instantiating a ``HivePlot`` instance with custom ``repeat_axes`` input.

        :param repeat_axes: boolean param or specification of the number of values to provide to the ``repeat_axes``
            parameter on instantiation of a ``HivePlot`` instance.
        """
        axes_names = ["custom_name_A", "custom_name_B", "custom_name_C"]
        # if repeat_axes is not a bool, then we will revise the repeat_axes input to the HivePlot call
        if isinstance(repeat_axes, bool):
            repeat_axes_value = repeat_axes
        elif repeat_axes == 1:
            repeat_axes_value = axes_names[0]
        elif repeat_axes == 2:
            repeat_axes_value = axes_names[:2]
        else:
            raise NotImplementedError

        hp = example_hive_plot(labels=axes_names, repeat_axes=repeat_axes_value)

        if repeat_axes is True:
            expected_axes = axes_names + [f"{i}_repeat" for i in axes_names]
        elif repeat_axes is False:
            expected_axes = axes_names
        elif repeat_axes == 1:
            expected_axes = axes_names + [f"{repeat_axes_value}_repeat"]
        elif repeat_axes == 2:
            expected_axes = axes_names + [f"{i}_repeat" for i in repeat_axes_value]

        assert set(hp.axes.keys()) == set(expected_axes), (
            "Did not create expected repeat axes."
        )

    def test_instantiate_axes_order(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance with custom ``axes_order`` input.
        """
        hp = example_hive_plot()
        standard_order = {
            "A": 0,
            "B": 120,
            "C": 240,
        }
        for k in standard_order:
            assert hp.axes[k].angle == standard_order[k]

        hp_switched_order = example_hive_plot(axes_order=["B", "C", "A"])
        switched_order = {
            "B": 0,
            "C": 120,
            "A": 240,
        }
        for k in switched_order:
            assert hp_switched_order.axes[k].angle == switched_order[k]

        # make sure nodes followed axes
        for k in ["A", "B", "C"]:
            assert np.array_equal(
                hp.axes[k].node_placements.unique_id.values,
                hp_switched_order.axes[k].node_placements.unique_id.values,
            )

    def test_instantiate_axes_order_incomplete_order(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance with an invalid / incomplete ``axes_order`` input.
        """
        try:
            example_hive_plot(axes_order=["B", "A"])
            pytest.fail("Test should have failed on incomplete axes specifications")
        except InvalidAxesOrderError:
            assert True

    def test_instantiate_axes_order_overspecified_order(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance with an invalid / overspecified ``axes_order`` input.
        """
        try:
            example_hive_plot(axes_order=["B", "A", "C", "nonexistent_axis"])
            pytest.fail("Test should have failed on incomplete axes specifications")
        except InvalidAxesOrderError:
            assert True

    def test_instantiate_axis_kwargs_vmin_vmax_store_inference(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance with ``axis_kwargs`` checking storage of vmin / vmax inference.

        Tests that we correctly store whether "vmin" / "vmax" values were inferred or not..
        """
        hp = example_hive_plot(
            axis_kwargs={
                "A": {"vmin": -5},
                "B": {"vmax": 500},
            }
        )

        assert not hp.axes["A"].inferred_vmin
        assert hp.axes["A"].inferred_vmax

        assert hp.axes["B"].inferred_vmin
        assert not hp.axes["B"].inferred_vmax

        for ax in ["A", "B"]:
            max_rho = hp.axes[ax].node_placements.rho.max()
            min_rho = hp.axes[ax].node_placements.rho.min()
            if ax == "A":
                assert min_rho > hp.axes[ax].polar_start, (
                    "extra small `vmin` should nudge nodes away from inner edge of axis"
                )
            if ax == "B":
                assert max_rho < hp.axes[ax].polar_end, (
                    "extra large `vmax` should nudge nodes away from outer edge of axis"
                )
            full_rho_range = hp.axes[ax].polar_end - hp.axes[ax].polar_start
            assert max_rho - min_rho < full_rho_range, (
                "one of `vmin` or `vmax` should be out of range of data, so node data should NOT span full rho range."
            )

    def test_instantiate_axis_kwargs_polar_rho_values(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance with various custom ``axis_kwargs`` input that mess with polar rho.

        Tests changes to both "start" / "end" and "vmin" / "vmax" parameters.
        """
        hp = example_hive_plot()

        hp_different_kwargs = example_hive_plot(
            axis_kwargs={
                "A": {"start": -5, "end": 10},
                "B": {"vmin": -500, "vmax": 500},
            }
        )
        # A values should span larger rho range than before
        default_a_rho_min = hp.axes["A"].node_placements.rho.min()
        default_a_rho_max = hp.axes["A"].node_placements.rho.max()
        different_a_rho_min = hp_different_kwargs.axes["A"].node_placements.rho.min()
        different_a_rho_max = hp_different_kwargs.axes["A"].node_placements.rho.max()
        assert different_a_rho_min < default_a_rho_min, (
            "A axis should now start with lower rho per our 'start' changes."
        )
        assert different_a_rho_max > default_a_rho_max, (
            "A axis should now end with higher rho per our 'end' changes."
        )

        # B values should span tiny rho per vmin and vmax changes
        default_b_rho_range = (
            hp.axes["B"].node_placements.rho.max()
            - hp.axes["B"].node_placements.rho.min()
        )
        different_b_rho_range = (
            hp_different_kwargs.axes["B"].node_placements.rho.max()
            - hp_different_kwargs.axes["B"].node_placements.rho.min()
        )
        assert different_b_rho_range < default_b_rho_range, (
            "B axis range of nodes should be compressed per our vmin and vmax changes."
        )

    def test_instantiate_axis_kwargs_sorting_variable_order_of_operations(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance with custom ``axis_kwargs`` prioritizes axis kwargs sorting variable.
        """
        hp = example_hive_plot(sorting_variables="low")
        low_indices_rho_values = (
            hp.axes["A"].node_placements.sort_values("rho").unique_id.to_numpy()
        )

        hp_different_sorting_var = example_hive_plot(
            axis_kwargs={
                "A": {"sorting_variable": "med"},
            }
        )
        different_indices_rho_values = (
            hp_different_sorting_var.axes["A"]
            .node_placements.sort_values("rho")
            .unique_id.to_numpy()
        )

        comparable_hp = example_hive_plot(sorting_variables="med")
        comparable_indices_rho_values = (
            comparable_hp.axes["A"]
            .node_placements.sort_values("rho")
            .unique_id.to_numpy()
        )

        assert not np.array_equal(
            low_indices_rho_values, different_indices_rho_values
        ), (
            "Axis should have different sorting variable ('low' vs 'med') and thus different node placement rho values."
        )

        assert np.array_equal(
            comparable_indices_rho_values, different_indices_rho_values
        ), (
            "Both hive plots should have same sorting variable ('med') and thus the same node placement rho values."
        )

    @pytest.mark.parametrize("repeat_axes", [True, False])
    @pytest.mark.parametrize("rotation", [0, 30])
    def test_instantiate_axis_kwargs_rotation_effect(
        self, repeat_axes: bool, rotation: int
    ) -> None:
        """
        Test instantiating a ``HivePlot`` instance with custom ``axis_kwargs`` angle prioritizes over rotation.

        :param repeat_axes: parameter value for ``repeat_axes``.
        :param rotation: parameter value for ``rotation``.
        """
        hp = example_hive_plot(repeat_axes=repeat_axes, rotation=rotation)

        fixed_angle = 277

        hp_different_kwargs = example_hive_plot(
            repeat_axes=repeat_axes,
            rotation=rotation,
            axis_kwargs={
                "C": {"angle": fixed_angle},
            },
        )
        if repeat_axes:
            fixed_angle -= hp.angle_between_repeat_axes / 2
            fixed_angle_repeat = fixed_angle + hp.angle_between_repeat_axes
            assert (
                hp_different_kwargs.axes["C_repeat"].angle
                == fixed_angle_repeat
                != hp.axes["C_repeat"].angle
            ), "C axis should be at explicit angle and different from default angle."
        assert (
            hp_different_kwargs.axes["C"].angle == fixed_angle != hp.axes["C"].angle
        ), "C axis should be at explicit angle and different from default angle."

    @pytest.mark.parametrize("repeat_axes", [True, False])
    def test_instantiate_axis_kwargs_repeat_effect(self, repeat_axes: bool) -> None:
        """
        Test instantiating a ``HivePlot`` instance with custom ``axis_kwargs`` long name keeps name for repeat axis.

        :param repeat_axes: parameter value for ``repeat_axes``.
        """
        hp = example_hive_plot(
            repeat_axes=repeat_axes,
            axis_kwargs={
                "A": {"long_name": "Potato"},
            },
        )

        assert hp.axes["A"].long_name == "Potato", "Should have changed long name"

        if repeat_axes:
            assert hp.axes["A_repeat"].long_name == "Potato", (
                "Should have changed main and repeat axes long names."
            )

    def test_instantiate_axis_kwargs_invalid_axis(self) -> None:
        """
        Test instantiating a ``HivePlot`` instance with custom ``axis_kwargs`` prioritizes axis kwargs sorting variable.
        """
        try:
            example_hive_plot(
                sorting_variables="low",
                axis_kwargs={"Non-existent Axis": {"long_name": "failure"}},
            )
            pytest.fail(reason="Should have failed on invalid axis name")
        except InvalidAxisNameError:
            assert True

    @pytest.mark.parametrize("repeat_axes", [True, False])
    def test_instantiate_rotation(self, repeat_axes: bool) -> None:
        """
        Test instantiating a ``HivePlot`` instance with custom ``rotation`` input.

        :param repeat_axes: parameter value for ``repeat_axes``.
        """
        hp = example_hive_plot(
            repeat_axes=repeat_axes,
        )

        rotation = 43

        hp_rotated = example_hive_plot(
            repeat_axes=repeat_axes,
            rotation=rotation,
        )

        for axis in hp.axes:
            assert (hp.axes[axis].angle + rotation) % 360 == hp_rotated.axes[
                axis
            ].angle, f"Unexpected angle on axis {axis}"

    @pytest.mark.parametrize("angle_between", [23, 37])
    def test_instantiate_angle_between_repeat_axes(self, angle_between: float) -> None:
        """
        Test instantiating a ``HivePlot`` instance with custom ``angle_between_repeat_axes`` input.

        :param angle_between: parameter value for ``repeat_axes``.
        """
        hp = example_hive_plot()

        hp_angle_between = example_hive_plot(
            repeat_axes=True,
            angle_between_repeat_axes=angle_between,
        )

        for axis in hp.axes:
            angle_diff = (
                hp_angle_between.axes[f"{axis}_repeat"].angle
                - hp_angle_between.axes[axis].angle
            ) % 360

            assert angle_diff == angle_between

    def test_no_naming_axis_repeat(self) -> None:
        """
        Test what happens when user accidentally names an axis ending in ``_repeat``.

        This naming convention is a protected name reserved for ``HivePlot`` generated repeat axes.
        """
        hp = example_hive_plot()
        error_causing_partition = hp.nodes.create_partition_variable(
            data_column="low",
            cutoffs=3,
            partition_variable_name="error_causing",
            labels=["fine", "also_fine", "error_repeat"],
        )
        try:
            hp.set_partition(
                partition_variable=error_causing_partition,
                sorting_variables="low",
            )
            pytest.fail(
                "Should have failed trying to name axis with `_repeat` in partition."
            )
        except RepeatInPartitionAxisNameError:
            assert True

    @pytest.mark.parametrize("initial_repeat_axes", [True, False, "A"])
    def test_set_repeat_axes(self, initial_repeat_axes: Union[bool, str]) -> None:
        """
        Test running the ``set_repeat_axes()`` method on an existing ``HivePlot`` instance.

        :param initial_repeat_axes: value for initial hive plot for ``repeat_axes`` parameter.
        """
        hp = example_hive_plot(repeat_axes=initial_repeat_axes)

        hp.set_repeat_axes("B")

        assert set(hp.axes.keys()) == {
            "A",
            "B",
            "B_repeat",
            "C",
        }, "Previous repeat axes were not overwritten."

    @pytest.mark.parametrize("reset_vmin_and_vmax", [True, False])
    @pytest.mark.parametrize("hardcoded_range", [True, False])
    def test_update_sorting_variables(
        self, reset_vmin_and_vmax: bool, hardcoded_range: bool
    ) -> None:
        """
        Test running the ``update_sorting_variables()`` method on an existing ``HivePlot`` instance.

        :param reset_vmin_and_vmax: value for ``reset_vmin_and_vmax`` parameter in ``update_sorting_variables()``
            method.
        :param hardcoded_range: whether to hardcode the initial vmin and vmax for an axis when instantiating the
        ``HivePlot`` instance.
        """
        kwargs = {}
        if hardcoded_range:
            kwargs["axis_kwargs"] = {"B": {"vmin": 0, "vmax": 5}}
        hp = example_hive_plot(sorting_variables="low", **kwargs)

        node_placement_ids_0 = (
            hp.axes["B"].node_placements.sort_values("rho").unique_id.to_numpy()
        )

        hp.update_sorting_variables("med", reset_vmin_and_vmax=reset_vmin_and_vmax)

        node_placement_ids_1 = (
            hp.axes["B"].node_placements.sort_values("rho").unique_id.to_numpy()
        )

        assert set(node_placement_ids_0) == set(node_placement_ids_1), (
            "Should be same node IDs on that axis."
        )

        assert not np.array_equal(node_placement_ids_0, node_placement_ids_1), (
            "Node placements should have moved."
        )

        max_rho = hp.axes["B"].node_placements.rho.max()
        min_rho = hp.axes["B"].node_placements.rho.min()
        expected_rho_range = hp.axes["B"].polar_end - hp.axes["B"].polar_start
        if hardcoded_range:
            if reset_vmin_and_vmax:
                assert max_rho - min_rho == expected_rho_range, (
                    "resetting `vmin` and `vmax` means node data should span full rho range."
                )
                assert hp.axes["B"].inferred_vmin
                assert hp.axes["B"].inferred_vmax
            else:
                assert max_rho - min_rho < expected_rho_range, (
                    "hardcoded `vmin` and `vmax` should not have changed, so node data should NOT span full rho range."
                )
                assert not hp.axes["B"].inferred_vmin
                assert not hp.axes["B"].inferred_vmax
        else:
            assert max_rho - min_rho == expected_rho_range, (
                "If we didn't hardcode the range, then node data should ALWAYS span full rho range since original "
                "`None` values for `vmin` and `vmax` will reset to span new data after changing sorting variable."
            )
            assert hp.axes["B"].inferred_vmin
            assert hp.axes["B"].inferred_vmax

    def test_set_partition(self) -> None:
        """
        Test running the ``set_partition()`` method on an existing ``HivePlot`` instance.
        """
        original_partition_column = "original_partition"
        hp = example_hive_plot(partition_variable_name=original_partition_column)

        assert set(hp.axes.keys()) == {"A", "B", "C"}

        new_cutoffs_count = 4
        new_partition_column = hp.nodes.create_partition_variable(
            data_column="low",
            cutoffs=new_cutoffs_count,
            labels=range(new_cutoffs_count),
        )

        hp.set_partition(
            partition_variable=new_partition_column,
            sorting_variables="med",
        )

        assert set(hp.axes.keys()) == set(range(new_cutoffs_count))

        # previous partition sorting variables should be preserved
        for k in hp.sorting_variables:
            if isinstance(k, str) and ("A" in k or "B" in k or "C" in k):
                assert hp.sorting_variables[k] == "low"
            else:
                assert hp.sorting_variables[k] == "med"

        hp.set_partition(
            partition_variable=original_partition_column,
            sorting_variables="high",
        )

        # previous partition sorting variables should be preserved
        for k in hp.sorting_variables:
            if isinstance(k, str) and ("A" in k or "B" in k or "C" in k):
                assert hp.sorting_variables[k] == "high"
            else:
                assert hp.sorting_variables[k] == "med"

        assert set(hp.axes.keys()) == {"A", "B", "C"}

    def test_set_partition_invalid_value(self) -> None:
        """
        Test running the ``set_partition()`` method on an existing ``HivePlot`` instance with an invalid parition value.
        """
        hp = example_hive_plot()
        try:
            hp.set_partition(
                partition_variable="non_existent_variable",
                sorting_variables="low",
            )
            pytest.fail(
                reason="Should have raised `ValueError` on invalid partition variable."
            )
        except InvalidPartitionVariableError:
            assert True

    def test_set_axes_order(self) -> None:
        """
        Test running the ``set_axes_order()`` method on an existing ``HivePlot`` instance.
        """
        hp = example_hive_plot(axis_kwargs={"A": {"end": 10}})

        expected_angles = [0, 120, 240]

        for ax, angle in zip(["A", "B", "C"], expected_angles):
            assert hp.axes[ax].angle == angle

        assert hp.axes["A"].polar_end == 10, "Axes kwargs should be preserved"

        new_order = ["B", "A", "C"]

        hp.set_axes_order(axes=new_order)

        for ax, angle in zip(new_order, expected_angles):
            assert hp.axes[ax].angle == angle

        assert hp.axes["A"].polar_end == 10, "Axes kwargs should be preserved"

    def test_set_axes_order_with_repeat(self) -> None:
        """
        Test running the ``set_axes_order()`` method on an existing ``HivePlot`` instance with a repeat axis.
        """
        repeat_axis = "A"

        hp = example_hive_plot(repeat_axes=repeat_axis, axis_kwargs={"A": {"end": 10}})

        expected_angles = [0, 120, 240]

        for ax, angle in zip(["A", "B", "C"], expected_angles):
            if ax != repeat_axis:
                assert hp.axes[ax].angle == angle
            else:
                assert (
                    hp.axes[ax].angle
                    == (angle - hp.angle_between_repeat_axes / 2) % 360
                )
                assert (
                    hp.axes[f"{ax}_repeat"].angle
                    == (angle + hp.angle_between_repeat_axes / 2) % 360
                )

        assert hp.axes["A"].polar_end == 10, "Axes kwargs should be preserved"
        assert hp.axes["A_repeat"].polar_end == 10, "Axes kwargs should be preserved"

        new_order = ["B", "A", "C"]

        hp.set_axes_order(axes=new_order)
        for ax, angle in zip(new_order, expected_angles):
            if ax != repeat_axis:
                assert hp.axes[ax].angle == angle % 360
            else:
                assert (
                    hp.axes[ax].angle
                    == (angle - hp.angle_between_repeat_axes / 2) % 360
                )
                assert (
                    hp.axes[f"{ax}_repeat"].angle
                    == (angle + hp.angle_between_repeat_axes / 2) % 360
                )

        assert hp.axes["A"].polar_end == 10, "Axes kwargs should be preserved"
        assert hp.axes["A_repeat"].polar_end == 10, "Axes kwargs should be preserved"

    @pytest.mark.parametrize(
        "rotation_angle",
        [
            0,
            30,
            180,
        ],
    )
    def test_set_rotation(self, rotation_angle: float) -> None:
        """
        Test running the ``set_rotation()`` method on an existing ``HivePlot`` instance.

        :param rotation_angle: angle to rotate hive plot.
        """
        repeat_axis = "A"

        hp = example_hive_plot(repeat_axes=repeat_axis, axis_kwargs={"A": {"end": 10}})

        expected_angles = np.array([0, 120, 240])

        for ax, angle in zip(["A", "B", "C"], expected_angles):
            if ax != repeat_axis:
                assert hp.axes[ax].angle == angle % 360
            else:
                assert (
                    hp.axes[ax].angle
                    == (angle - hp.angle_between_repeat_axes / 2) % 360
                )
                assert (
                    hp.axes[f"{ax}_repeat"].angle
                    == (angle + hp.angle_between_repeat_axes / 2) % 360
                )

        assert hp.axes["A"].polar_end == 10, "Axes kwargs should be preserved"
        assert hp.axes["A_repeat"].polar_end == 10, "Axes kwargs should be preserved"

        hp.set_rotation(rotation=rotation_angle)

        for ax, angle in zip(["A", "B", "C"], expected_angles + rotation_angle):
            if ax != repeat_axis:
                assert hp.axes[ax].angle == angle % 360
            else:
                assert (
                    hp.axes[ax].angle
                    == (angle - hp.angle_between_repeat_axes / 2) % 360
                )
                assert (
                    hp.axes[f"{ax}_repeat"].angle
                    == (angle + hp.angle_between_repeat_axes / 2) % 360
                )

        assert hp.axes["A"].polar_end == 10, "Axes kwargs should be preserved"
        assert hp.axes["A_repeat"].polar_end == 10, "Axes kwargs should be preserved"

        new_rotation_angle = -10

        hp.set_rotation(rotation=new_rotation_angle)

        for ax, angle in zip(["A", "B", "C"], expected_angles + new_rotation_angle):
            if ax != repeat_axis:
                assert hp.axes[ax].angle == angle
            else:
                assert (
                    hp.axes[ax].angle
                    == (angle - hp.angle_between_repeat_axes / 2) % 360
                )
                assert (
                    hp.axes[f"{ax}_repeat"].angle
                    == (angle + hp.angle_between_repeat_axes / 2) % 360
                )

        assert hp.axes["A"].polar_end == 10, "Axes kwargs should be preserved"
        assert hp.axes["A_repeat"].polar_end == 10, "Axes kwargs should be preserved"

    @pytest.mark.parametrize("repeat_angle", [10, 50])
    def test_set_angle_between_repeat_axes(self, repeat_angle: float) -> None:
        """
        Test running the ``set_angle_between_repeat_axes()`` method on an existing ``HivePlot`` instance.

        :param repeat_angle: angle to put between repeat axes.
        """
        repeat_axis = "A"

        hp = example_hive_plot(repeat_axes=repeat_axis, axis_kwargs={"A": {"end": 10}})

        expected_angles = np.array([0, 120, 240])

        for ax, angle in zip(["A", "B", "C"], expected_angles):
            if ax != repeat_axis:
                assert hp.axes[ax].angle == angle % 360
            else:
                assert (
                    hp.axes[ax].angle
                    == (angle - hp.angle_between_repeat_axes / 2) % 360
                )
                assert (
                    hp.axes[f"{ax}_repeat"].angle
                    == (angle + hp.angle_between_repeat_axes / 2) % 360
                )

        assert hp.axes["A"].polar_end == 10, "Axes kwargs should be preserved"
        assert hp.axes["A_repeat"].polar_end == 10, "Axes kwargs should be preserved"

        hp.set_angle_between_repeat_axes(angle=repeat_angle)

        for ax, angle in zip(["A", "B", "C"], expected_angles):
            if ax != repeat_axis:
                assert hp.axes[ax].angle == angle % 360
            else:
                assert (
                    hp.axes[ax].angle
                    == (angle - hp.angle_between_repeat_axes / 2) % 360
                )
                assert (
                    hp.axes[f"{ax}_repeat"].angle
                    == (angle + hp.angle_between_repeat_axes / 2) % 360
                )

        assert hp.axes["A"].polar_end == 10, "Axes kwargs should be preserved"
        assert hp.axes["A_repeat"].polar_end == 10, "Axes kwargs should be preserved"

        hp.set_angle_between_repeat_axes(angle=10)

        for ax, angle in zip(["A", "B", "C"], expected_angles):
            if ax != repeat_axis:
                assert hp.axes[ax].angle == angle % 360
            else:
                assert (
                    hp.axes[ax].angle
                    == (angle - hp.angle_between_repeat_axes / 2) % 360
                )
                assert (
                    hp.axes[f"{ax}_repeat"].angle
                    == (angle + hp.angle_between_repeat_axes / 2) % 360
                )

        assert hp.axes["A"].polar_end == 10, "Axes kwargs should be preserved"
        assert hp.axes["A_repeat"].polar_end == 10, "Axes kwargs should be preserved"

    def test_set_angle_between_repeat_axes_too_big(self) -> None:
        """
        Test running the ``set_angle_between_repeat_axes()`` method on an existing ``HivePlot`` instance.

        Specifically, make sure we get a warning as expected when the angle between repeat axes is too big.
        """
        repeat_axis = "A"

        hp = example_hive_plot(repeat_axes=repeat_axis, axis_kwargs={"A": {"end": 10}})

        # expecting no warning here, so force error on any warning
        with warnings.catch_warnings():
            warnings.simplefilter("error")
            hp.set_angle_between_repeat_axes(angle=50)

        with pytest.warns() as record:
            hp.set_angle_between_repeat_axes(angle=140)
        assert len(record) == 1, (
            f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
        )

    def test_set_viz_backend_invalid_option(self) -> None:
        """
        Test running the ``set_viz_backend()`` method with invalid option on an existing ``HivePlot`` instance.
        """
        hp = example_hive_plot()
        try:
            hp.set_viz_backend(backend="potato")
            pytest.fail("Should have raised AssertionError on invalid backend option")
        except AssertionError:
            assert True

    def test_update_axis(self) -> None:
        """
        Test running the ``update_axis()`` method on an existing ``HivePlot`` instance.
        """
        hp = example_hive_plot()
        hp.update_axis(
            axis_id="A",
            sorting_variable="high",
            vmin=2.1,
            vmax=None,
            start=3.1,
            end=6.2,
            angle=27,
            long_name="My super special long name",
        )

        assert hp.axes["A"].sorting_variable == "high"
        assert hp.axes["A"].vmin == 2.1
        assert not hp.axes["A"].inferred_vmin
        assert hp.axes["A"].inferred_vmax
        assert hp.axes["A"].polar_start == 3.1
        assert hp.axes["A"].polar_end == 6.2
        assert hp.axes["A"].angle == 27
        assert hp.axes["A"].long_name == "My super special long name"

    def test_build_axes_on_reset_partition(self) -> None:
        """
        Test running the ``build_axes()`` method (re)sets axes as expected on an existing ``HivePlot`` instance.

        Specifically, test the resulting axes when this method is called via changing the partition.
        """
        axis_kwargs = {
            "start": 4,
            "end": 10,
            "vmin": 17,
            "vmax": 27,
            "angle": 27,
            "long_name": "Potato",
        }
        original_partition_variable = "original_partition_variable"
        hp = example_hive_plot(
            axis_kwargs={i: axis_kwargs for i in ["A", "B", "C"]},
            partition_variable_name=original_partition_variable,
            sorting_variables="low",
        )
        for ax in hp.axes:
            assert hp.axes[ax].polar_start == axis_kwargs["start"], (
                "Should see custom axis values."
            )
            assert hp.axes[ax].polar_end == axis_kwargs["end"]
            assert hp.axes[ax].angle == axis_kwargs["angle"]
            assert hp.axes[ax].long_name == axis_kwargs["long_name"]
            assert hp.axes[ax].vmin == axis_kwargs["vmin"]
            assert hp.axes[ax].vmax == axis_kwargs["vmax"]
            assert not hp.axes[ax].inferred_vmin
            assert not hp.axes[ax].inferred_vmax

        new_partition_variable = hp.nodes.create_partition_variable(
            data_column="med", cutoffs=3
        )
        hp.set_partition(
            partition_variable=new_partition_variable, sorting_variables="med"
        )
        for i, ax in enumerate(hp.axes):
            assert hp.axes[ax].polar_start == 1, (
                "Custom axis values should have reset on new partition."
            )
            assert hp.axes[ax].polar_end == 5
            assert hp.axes[ax].angle == 120 * i
            assert hp.axes[ax].long_name == str(hp.axes[ax].axis_id)
            assert hp.axes[ax].inferred_vmin
            assert hp.axes[ax].inferred_vmax

        hp.set_partition(
            partition_variable=original_partition_variable, sorting_variables="low"
        )
        for i, ax in enumerate(hp.axes):
            assert hp.axes[ax].polar_start == 1, (
                "Back to original partition, but resetting partition should still have reset axis kwargs."
            )
            assert hp.axes[ax].polar_end == 5
            assert hp.axes[ax].angle == 120 * i
            assert hp.axes[ax].long_name == str(hp.axes[ax].axis_id)
            assert hp.axes[ax].inferred_vmin
            assert hp.axes[ax].inferred_vmax

    @pytest.mark.parametrize("repeat", ["A", True])
    @pytest.mark.parametrize("repeat_angle", [119, 120])
    def test_build_axes_warn_on_possible_naughty_repeat_axes_placements(
        self,
        repeat: Union[str, bool],
        repeat_angle: float,
    ) -> None:
        """
        Test that we warn on choosing an ``angle_between_repeat_axes`` too large for a hive plot.

        Specifically, confirm we warn when the angle between repeat axes risks a non-repeat axis in between an axis and
        its repeat.

        This occurs when the angle between repeat axes moves the repeat axis past the next axis. For example, 3 axes
        with no repeats would be at 0, 120, and 240 degrees. If we throw repeats on all axes, then each axis will split
        with the original and repeat axis each going ``angle_between_repeat_axes / 2`` degrees in opposite directions.

        In the 3 axis case with all 3 having repeats, the repeat axes will land on different, non-repeat axes when
        ``angle_between_repeat_axes`` is 120, which risks misinterpretation, so we will warn the user accordingly.

        Note: per this parametrization, even if we only do 1 repeat axis, for which 120 degree separation on the repeats
        is insufficient, we will still trigger a warning.

        :param repeat: which axis / axes to repeat.
        :param repeat_angle: angle to set between repeat axes.
        """
        hp = example_hive_plot(repeat_axes=repeat)

        if repeat_angle < 120:
            with warnings.catch_warnings():
                warnings.simplefilter("error")
                hp.set_angle_between_repeat_axes(repeat_angle)

        else:
            with pytest.warns() as record:
                hp.set_angle_between_repeat_axes(repeat_angle)
            assert len(record) == 1, (
                f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
            )

    def test_build_hive_plot_build_axes_from_scratch_true(self) -> None:
        """
        Test running ``build_hive_plot(build_axes_from_scratch=True)`` on an existing hive plot.

        Specifically, test that running that method with ``build_axes_from_scratch=True`` drops any customization with
        respect to axis placement / node placement on an axis (other than the sorting variable, that will persist).
        """
        custom_axis_kwargs = {
            "start": 5,
            "end": 10,
            "vmin": -2,
            "vmax": 15,
            "angle": 310,
            "long_name": "potato",
        }
        hp = example_hive_plot(
            sorting_variables="med",
            axis_kwargs={"A": custom_axis_kwargs},
            repeat_axes="A",
        )
        assert set(hp.axes.keys()) == {"A", "A_repeat", "B", "C"}

        for ax in hp.axes:
            assert hp.axes[ax].sorting_variable == "med"

        for ax in ["A", "A_repeat"]:
            assert hp.axes[ax].polar_start == custom_axis_kwargs["start"]
            assert hp.axes[ax].polar_end == custom_axis_kwargs["end"]
            assert hp.axes[ax].vmin == custom_axis_kwargs["vmin"]
            assert hp.axes[ax].vmax == custom_axis_kwargs["vmax"]
            assert hp.axes[ax].long_name == custom_axis_kwargs["long_name"]
            assert not hp.axes[ax].inferred_vmin
            assert not hp.axes[ax].inferred_vmax

        assert (
            hp.axes["A"].angle
            == custom_axis_kwargs["angle"] - hp.angle_between_repeat_axes / 2
        )
        assert (
            hp.axes["A_repeat"].angle
            == custom_axis_kwargs["angle"] + hp.angle_between_repeat_axes / 2
        )

        hp.build_hive_plot(build_axes_from_scratch=True)

        assert set(hp.axes.keys()) == {
            "A",
            "A_repeat",
            "B",
            "C",
        }, "Should have the same axes as before"

        for ax in hp.axes:
            assert hp.axes[ax].sorting_variable == "med", (
                "Should have same sorting variable as before"
            )

        for ax in ["A", "A_repeat"]:
            assert hp.axes[ax].polar_start == 1, (
                "Should have reverted to default polar start"
            )
            assert hp.axes[ax].polar_end == 5, (
                "Should have reverted to default polar end"
            )
            assert hp.axes[ax].long_name == "A", (
                "Should have reverted to default long name"
            )
            assert hp.axes[ax].inferred_vmin, (
                "Should have reverted to default inferring"
            )
            assert hp.axes[ax].inferred_vmax, (
                "Should have reverted to default inferring"
            )

        assert hp.axes["A"].angle == (0 - hp.angle_between_repeat_axes / 2) % 360, (
            "Should have reverted to default angle"
        )
        assert (
            hp.axes["A_repeat"].angle == (0 + hp.angle_between_repeat_axes / 2) % 360
        ), "Should have reverted to default angle"

    def test_connect_adjacent_axes_custom_repeat_axes_placement(self) -> None:
        """
        Test running the ``connect_adjacent_axes()`` method on an existing ``HivePlot`` instance.

        Specifically, make sure if we forcibly alter the order of an axis and its repeat (by explicitly setting both),
        that the resulting axes connections adjust accordingly.
        """
        hp = example_hive_plot(repeat_axes="A", rotation=120)
        assert hp.axes["A"].angle < hp.axes["A_repeat"].angle
        assert "C" in hp.hive_plot_edges["A"], (
            "Non repeat axis should connect to C axis."
        )
        assert "B" not in hp.hive_plot_edges["A"], (
            "Non repeat axis should NOT connect to B axis."
        )
        assert "B" in hp.hive_plot_edges["A_repeat"], (
            "Non repeat axis should connect to C axis."
        )
        assert "C" not in hp.hive_plot_edges["A_repeat"], (
            "Non repeat axis should NOT connect to C axis."
        )

        # switch ccw order of axis with its repeat manually
        hp = example_hive_plot(
            axis_kwargs={
                "A": {
                    "angle": 120,
                },
                "A_repeat": {"angle": 45},
            },
            repeat_axes="A",
            rotation=120,
        )

        assert hp.axes["A"].angle > hp.axes["A_repeat"].angle, (
            "Axes pair should now be flipped order ccw."
        )

        # connections should handle this accordingly
        assert "C" not in hp.hive_plot_edges["A"], (
            "Non repeat axis should NOT connect to C axis."
        )
        assert "B" in hp.hive_plot_edges["A"], (
            "Non repeat axis should connect to B axis."
        )
        assert "B" not in hp.hive_plot_edges["A_repeat"], (
            "Non repeat axis should NOT connect to C axis."
        )
        assert "C" in hp.hive_plot_edges["A_repeat"], (
            "Non repeat axis should connect to C axis."
        )

    def test_connect_adjacent_axes_edge_kwargs(self) -> None:
        """
        Test running the ``connect_adjacent_axes()`` method on an existing ``HivePlot`` instance.

        Specifically, test that the various kwargs (``repeat_kwargs``, ``cw_kwargs``, and ``ccw_kwargs``) propagate to
        the appropriate edges.
        """
        all_edge_kwargs = {"color": "blue", "ls": "dotted"}
        base_hp = example_hive_plot(
            repeat_axes=True,
            all_edge_kwargs=all_edge_kwargs,
        )

        clockwise_edge_kwargs = {"color": "green", "ls": "dotted"}
        counterclockwise_edge_kwargs = {"color": "orange", "ls": "dashed"}
        repeat_edge_kwargs = {"color": "red", "ls": "dashdot"}

        with pytest.warns() as record:
            base_hp.update_edge_plotting_keyword_arguments(
                edge_kwarg_setting="clockwise_edge_kwargs",
                **clockwise_edge_kwargs,
            )
        assert len(record) == 1, (
            f"Expected 1 warning for kwarg overlap, got:\n{[i.message.args for i in record]}"
        )
        assert "'color'" in record[0].message.args[0], (
            "Should warn about 'color' overlap variable"
        )
        assert "'ls'" in record[0].message.args[0], (
            "Should warn about 'ls' overlap variable"
        )

        with pytest.warns() as record:
            base_hp.update_edge_plotting_keyword_arguments(
                edge_kwarg_setting="counterclockwise_edge_kwargs",
                **counterclockwise_edge_kwargs,
            )
        assert len(record) == 2, (
            "Expected 2 warnings (pulled in previous warning) for kwarg overlap, "
            f"got:\n{[i.message.args for i in record]}"
        )
        assert "'color'" in record[-1].message.args[0], (
            "Should warn about 'color' overlap variable"
        )
        assert "'ls'" in record[-1].message.args[0], (
            "Should warn about 'ls' overlap variable"
        )

        with pytest.warns() as record:
            base_hp.update_edge_plotting_keyword_arguments(
                edge_kwarg_setting="repeat_edge_kwargs",
                **repeat_edge_kwargs,
            )
        assert len(record) == 3, (
            "Expected 3 warnings (pulled in previous warnings) for kwarg overlap, "
            f"got:\n{[i.message.args for i in record]}"
        )
        assert "'color'" in record[-1].message.args[0], (
            "Should warn about 'color' overlap variable"
        )
        assert "'ls'" in record[-1].message.args[0], (
            "Should warn about 'ls' overlap variable"
        )

        custom_hp = example_hive_plot(
            repeat_axes=True,
            clockwise_edge_kwargs=clockwise_edge_kwargs,
            counterclockwise_edge_kwargs=counterclockwise_edge_kwargs,
            repeat_edge_kwargs=repeat_edge_kwargs,
        )

        axes_to_check = ["A", "B", "C"]
        for i, ax in enumerate(axes_to_check):
            assert (
                base_hp.hive_plot_edges[ax][f"{ax}_repeat"][0]["edge_kwargs"]
                == custom_hp.hive_plot_edges[ax][f"{ax}_repeat"][0]["edge_kwargs"]
                == repeat_edge_kwargs
            ), "repeat edges should have repeat edge kwargs."

            # check clockwise
            previous_ax = "C" if i == 0 else axes_to_check[i - 1]
            assert (
                base_hp.hive_plot_edges[ax][f"{previous_ax}_repeat"][0]["edge_kwargs"]
                == custom_hp.hive_plot_edges[ax][f"{previous_ax}_repeat"][0][
                    "edge_kwargs"
                ]
                == clockwise_edge_kwargs
            ), "clockwise edges should have clockwise edge kwargs."

            # check counterclockwise
            next_ax = "A" if i == len(axes_to_check) - 1 else axes_to_check[i + 1]
            assert (
                base_hp.hive_plot_edges[f"{ax}_repeat"][next_ax][0]["edge_kwargs"]
                == custom_hp.hive_plot_edges[f"{ax}_repeat"][next_ax][0]["edge_kwargs"]
                == counterclockwise_edge_kwargs
            ), "counterclockwise edges should have counterclockwise edge kwargs."

    def test_set_repeat_axes_sorting_variables(self) -> None:
        """
        Test running ``set_repeat_axes()`` method on ``HivePlot`` for various ``sorting_variables`` inputs.
        """
        hp = example_hive_plot(axis_kwargs={"A": {"vmin": 5, "end": 10}})

        hp.set_repeat_axes("A")

        assert np.array_equal(
            hp.axes["A"].node_placements.sort_values("rho")["unique_id"].to_numpy(),
            hp.axes["A_repeat"]
            .node_placements.sort_values("rho")["unique_id"]
            .to_numpy(),
        ), "'A' and 'A_repeat' axes should default to the same thing."

        for ax in ["A"]:
            max_rho = hp.axes[ax].node_placements.rho.max()
            min_rho = hp.axes[ax].node_placements.rho.min()
            max_rho_repeat = hp.axes[f"{ax}_repeat"].node_placements.rho.max()
            min_rho_repeat = hp.axes[f"{ax}_repeat"].node_placements.rho.min()
            full_rho_range = hp.axes[ax].polar_end - hp.axes[ax].polar_start
            full_rho_range_repeat = (
                hp.axes[f"{ax}_repeat"].polar_end - hp.axes[f"{ax}_repeat"].polar_start
            )
            actual_rho_range = max_rho - min_rho
            actual_rho_range_repeat = max_rho_repeat - min_rho_repeat

            assert max_rho == max_rho_repeat
            assert min_rho == min_rho_repeat
            assert full_rho_range == full_rho_range_repeat
            assert actual_rho_range == actual_rho_range_repeat == 0, (
                "Both axes should scrunch all data to one end of axis."
            )

        hp.set_repeat_axes("A", sorting_variables="med")

        assert hp.axes["A"].sorting_variable == "low", "Should only change repeat axis."

        assert hp.axes["A_repeat"].sorting_variable == "med", (
            "Should only change repeat axis."
        )

        assert set(hp.axes.keys()) == {"A", "B", "C", "A_repeat"}

        assert not np.array_equal(
            hp.axes["A"].node_placements.sort_values("rho")["unique_id"].to_numpy(),
            hp.axes["A_repeat"]
            .node_placements.sort_values("rho")["unique_id"]
            .to_numpy(),
        ), "'A' and 'A_repeat' axes should sort differently now."

        assert hp.axes["A"].angle == (0 - hp.angle_between_repeat_axes / 2) % 360
        assert hp.axes["A_repeat"].angle == (0 + hp.angle_between_repeat_axes / 2) % 360

        for ax in ["A"]:
            max_rho = hp.axes[ax].node_placements.rho.max()
            min_rho = hp.axes[ax].node_placements.rho.min()
            max_rho_repeat = hp.axes[f"{ax}_repeat"].node_placements.rho.max()
            min_rho_repeat = hp.axes[f"{ax}_repeat"].node_placements.rho.min()
            full_rho_range = hp.axes[ax].polar_end - hp.axes[ax].polar_start
            full_rho_range_repeat = (
                hp.axes[f"{ax}_repeat"].polar_end - hp.axes[f"{ax}_repeat"].polar_start
            )
            actual_rho_range = max_rho - min_rho
            actual_rho_range_repeat = max_rho_repeat - min_rho_repeat

            assert max_rho == max_rho_repeat
            assert min_rho != min_rho_repeat
            assert full_rho_range == full_rho_range_repeat
            assert actual_rho_range == 0, "non repeat axis should still be scrunched."
            assert actual_rho_range_repeat == full_rho_range_repeat, (
                "repeat axis should re-range vmin and vmax with new sorting variable."
            )

        hp.set_repeat_axes("B", sorting_variables={"B": "med"})

        assert set(hp.axes.keys()) == {
            "A",
            "B",
            "C",
            "B_repeat",
        }, "Should have dropped 'A_repeat' and added 'B_repeat'."

        assert hp.axes["B"].sorting_variable == "low", "Should only change repeat axis."

        assert hp.axes["B_repeat"].sorting_variable == "med", (
            "Should only change repeat axis."
        )

        b_placements = (
            hp.axes["B"].node_placements.sort_values("rho")["unique_id"].to_numpy()
        )
        b_repeat_med_placements = (
            hp.axes["B_repeat"]
            .node_placements.sort_values("rho")["unique_id"]
            .to_numpy()
        )
        assert not np.array_equal(b_placements, b_repeat_med_placements), (
            "'B' and 'B_repeat' axes should sort differently now."
        )

        assert hp.axes["A"].angle == 0, "'A' angle should have reset"
        assert hp.axes["B"].angle == (120 - hp.angle_between_repeat_axes / 2) % 360
        assert (
            hp.axes["B_repeat"].angle == (120 + hp.angle_between_repeat_axes / 2) % 360
        )

        hp.set_repeat_axes("B", sorting_variables={"B_repeat": "high"})

        assert set(hp.axes.keys()) == {"A", "B", "C", "B_repeat"}

        assert hp.axes["B"].sorting_variable == "low", "Should only change repeat axis."

        assert hp.axes["B_repeat"].sorting_variable == "high", (
            "Should only change repeat axis."
        )

        b_placements = (
            hp.axes["B"].node_placements.sort_values("rho")["unique_id"].to_numpy()
        )
        b_repeat_high_placements = (
            hp.axes["B_repeat"]
            .node_placements.sort_values("rho")["unique_id"]
            .to_numpy()
        )
        assert not np.array_equal(b_placements, b_repeat_high_placements), (
            "'B' and 'B_repeat' axes should sort differently now."
        )

        assert not np.array_equal(b_repeat_med_placements, b_repeat_high_placements), (
            "'B_repeat' placements should have been different for different sorting variables."
        )

        assert hp.axes["B"].angle == (120 - hp.angle_between_repeat_axes / 2) % 360
        assert (
            hp.axes["B_repeat"].angle == (120 + hp.angle_between_repeat_axes / 2) % 360
        )

        hp.set_repeat_axes(
            ["B", "C"], sorting_variables={"B_repeat": "med", "C_repeat": "high"}
        )
        assert set(hp.axes.keys()) == {"A", "B", "C", "B_repeat", "C_repeat"}

        for ax in ["B", "C"]:
            assert hp.axes[ax].sorting_variable == "low", (
                "Should only change repeat axis."
            )

        assert hp.axes["B_repeat"].sorting_variable == "med", (
            "Should only change repeat axis."
        )

        assert hp.axes["C_repeat"].sorting_variable == "high", (
            "Should only change repeat axis."
        )

        assert hp.axes["B"].angle == (120 - hp.angle_between_repeat_axes / 2) % 360
        assert (
            hp.axes["B_repeat"].angle == (120 + hp.angle_between_repeat_axes / 2) % 360
        )

        assert hp.axes["C"].angle == (240 - hp.angle_between_repeat_axes / 2) % 360
        assert (
            hp.axes["C_repeat"].angle == (240 + hp.angle_between_repeat_axes / 2) % 360
        )

        for ax in ["B", "C"]:
            max_rho = hp.axes[ax].node_placements.rho.max()
            min_rho = hp.axes[ax].node_placements.rho.min()
            max_rho_repeat = hp.axes[f"{ax}_repeat"].node_placements.rho.max()
            min_rho_repeat = hp.axes[f"{ax}_repeat"].node_placements.rho.min()
            full_rho_range = hp.axes[ax].polar_end - hp.axes[ax].polar_start
            full_rho_range_repeat = (
                hp.axes[f"{ax}_repeat"].polar_end - hp.axes[f"{ax}_repeat"].polar_start
            )
            actual_rho_range = max_rho - min_rho
            actual_rho_range_repeat = max_rho_repeat - min_rho_repeat

            assert max_rho == max_rho_repeat
            assert min_rho == min_rho_repeat
            assert full_rho_range == full_rho_range_repeat
            assert actual_rho_range == actual_rho_range_repeat == full_rho_range, (
                "Both axes should span full axis."
            )

        hp.set_repeat_axes(False)

        assert set(hp.axes.keys()) == {"A", "B", "C"}
        assert hp.axes["A"].angle == 0, "'A' angle should have reset"
        assert hp.axes["B"].angle == 120, "'B' angle should have reset"
        assert hp.axes["C"].angle == 240, "'C' angle should have reset"

        hp.set_repeat_axes(True)
        assert set(hp.axes.keys()) == {
            "A",
            "B",
            "C",
            "A_repeat",
            "B_repeat",
            "C_repeat",
        }

        hp.set_repeat_axes([])
        assert set(hp.axes.keys()) == {"A", "B", "C"}
        assert hp.axes["A"].angle == 0, "'A' angle should have reset"
        assert hp.axes["B"].angle == 120, "'B' angle should have reset"
        assert hp.axes["C"].angle == 240, "'C' angle should have reset"

        hp.set_repeat_axes("A", sorting_variables={"B": "med"})
        assert set(hp.axes.keys()) == {"A", "B", "C", "A_repeat"}
        assert hp.axes["A"].sorting_variable == "low", (
            "Should have fallen back to current A sorting variable."
        )

    @pytest.mark.parametrize("custom_vmin_vmax", [True, False])
    def test_convoluted_example_0(self, custom_vmin_vmax: bool) -> None:
        """
        Test a convoluted combination of specific requests for instantiating / modifying a ``HivePlot`` instance.

        There is a wonderful (as a user) and horrible (as a maintainer) combination of calls possible to stress test.
        Rather than obsess over testing the combinatorics of every possible combination of settings and calls, in the
        least, we can institute "convoluted examples" and sanity check that we get expected outputs. This is one such
        test.

        :param custom_vmin_vmax: whether to do custom vmin and vmax values for the repeat axis.
        """
        axis_kwargs = {
            "A": {"start": 5, "end": 15, "vmin": 5},
            "A_repeat": {"start": 1, "vmin": -5, "vmax": 25, "long_name": "unchanged"},
        }
        if not custom_vmin_vmax:
            del axis_kwargs["A_repeat"]["vmin"]
            del axis_kwargs["A_repeat"]["vmax"]

        hp = example_hive_plot(
            repeat_axes="A",
            sorting_variables={
                "A": "low",
                "B": "low",
                "C": "low",
                "A_repeat": "med",
            },
            axis_kwargs=axis_kwargs,
        )

        for ax in ["A", "B", "C"]:
            assert hp.axes[ax].sorting_variable == "low"
        assert hp.axes["A_repeat"].sorting_variable == "med"

        assert hp.axes["A"].polar_start == axis_kwargs["A"]["start"]
        assert hp.axes["A"].polar_end == axis_kwargs["A"]["end"]
        assert hp.axes["A"].vmin == axis_kwargs["A"]["vmin"]
        assert not hp.axes["A"].inferred_vmin
        assert hp.axes["A"].inferred_vmax
        assert hp.axes["A"].node_placements.rho.min() == hp.axes["A"].polar_end, (
            "All nodes should be forced to end with vmin higher than max sorting variable value."
        )

        if not custom_vmin_vmax:
            assert hp.axes["A_repeat"].vmin != hp.axes["A"].vmin
            assert hp.axes["A_repeat"].vmax != hp.axes["A"].vmax
            assert hp.axes["A_repeat"].inferred_vmin
            assert hp.axes["A_repeat"].inferred_vmax
        else:
            assert (
                hp.axes["A_repeat"].vmax
                == axis_kwargs["A_repeat"]["vmax"]
                != hp.axes["A"].vmax
            )
            assert (
                hp.axes["A_repeat"].vmin
                == axis_kwargs["A_repeat"]["vmin"]
                != hp.axes["A"].vmin
            )
            assert not hp.axes["A_repeat"].inferred_vmin
            assert not hp.axes["A_repeat"].inferred_vmax
            assert (
                hp.axes["A_repeat"].node_placements.rho.max() < hp.axes["A"].polar_end
            ), "All nodes should be forced away from end with higher vmax."
        assert hp.axes["A_repeat"].polar_start == axis_kwargs["A_repeat"]["start"]

    @pytest.mark.parametrize("custom_vmin_vmax", [True, False])
    def test_convoluted_example_1(self, custom_vmin_vmax: bool) -> None:
        """
        Test a convoluted combination of specific requests for instantiating / modifying a ``HivePlot`` instance.

        There is a wonderful (as a user) and horrible (as a maintainer) combination of calls possible to stress test.
        Rather than obsess over testing the combinatorics of every possible combination of settings and calls, in the
        least, we can institute "convoluted examples" and sanity check that we get expected outputs. This is one such
        test.

        This is the same as the above ``convoluted_example_0``, except the repeat axis is given the same sorting
        variable as the non-repeat axis and the non-repeat axis no longer sets a ``vmin`` value.

        :param custom_vmin_vmax: whether to do custom vmin and vmax values for the repeat axis.
        """
        axis_kwargs = {
            "A": {"start": 5, "end": 15},
            "A_repeat": {"start": 1, "vmin": -5, "vmax": 25, "long_name": "unchanged"},
        }
        if not custom_vmin_vmax:
            del axis_kwargs["A_repeat"]["vmin"]
            del axis_kwargs["A_repeat"]["vmax"]

        hp = example_hive_plot(
            repeat_axes="A",
            sorting_variables={
                "A": "low",
                "B": "low",
                "C": "low",
                "A_repeat": "low",
            },
            axis_kwargs=axis_kwargs,
        )

        for ax in hp.axes:
            assert hp.axes[ax].sorting_variable == "low"

        assert hp.axes["A"].polar_start == axis_kwargs["A"]["start"]
        assert hp.axes["A"].polar_end == axis_kwargs["A"]["end"]
        assert hp.axes["A"].inferred_vmin
        assert hp.axes["A"].inferred_vmax
        assert hp.axes["A"].node_placements.rho.min() == hp.axes["A"].polar_start

        if not custom_vmin_vmax:
            assert hp.axes["A_repeat"].vmin == hp.axes["A"].vmin, (
                "Follow the same as A."
            )
            assert hp.axes["A_repeat"].vmax == hp.axes["A"].vmax, (
                "Follow the same as A."
            )
            assert hp.axes["A_repeat"].inferred_vmin, (
                "Repeat axis should declare it's inferring vmin just like non-repeat"
            )
            assert hp.axes["A_repeat"].inferred_vmax, (
                "Repeat axis should declare it's inferring vmax just like non-repeat"
            )
        else:
            assert (
                hp.axes["A_repeat"].vmax
                == axis_kwargs["A_repeat"]["vmax"]
                != hp.axes["A"].vmax
            )
            assert (
                hp.axes["A_repeat"].vmin
                == axis_kwargs["A_repeat"]["vmin"]
                != hp.axes["A"].vmin
            )
            assert not hp.axes["A_repeat"].inferred_vmin
            assert not hp.axes["A_repeat"].inferred_vmax
            assert (
                hp.axes["A_repeat"].node_placements.rho.max() < hp.axes["A"].polar_end
            ), "All nodes should be forced away from end with higher vmax."
        assert hp.axes["A_repeat"].polar_start == axis_kwargs["A_repeat"]["start"]

    def test_convoluted_example_2(self) -> None:
        """
        Test a convoluted combination of specific requests for instantiating / modifying a ``HivePlot`` instance.

        This test checks that we resetting the partition when there are repeat axes before, but not after doesn't cause
        an error.
        """
        hp = example_hive_plot(repeat_axes=True)
        hp.set_partition(partition_variable="partition_0", sorting_variables="low")
        assert True

    def test_custom_angle_kwarg_with_repeat_axis(self) -> None:
        """
        Test when setting the axis kwargs angle for an axis with a repeat.

        In this case, the repeat axis should "follow" the main axis around (if its angle not set in kwargs) according to
        the angle between repeat axes.
        """
        axis_kwargs = {
            "A": {"angle": 45},
        }
        hp = example_hive_plot(
            repeat_axes="A",
            axis_kwargs=axis_kwargs,
        )

        assert (
            hp.axes["A"].angle
            == (axis_kwargs["A"]["angle"] - hp.angle_between_repeat_axes // 2) % 360
        )
        assert (
            hp.axes["A_repeat"].angle
            == (hp.axes["A"].angle + hp.angle_between_repeat_axes) % 360
        )

    def test_custom_angle_kwarg_with_custom_repeat_angle(self) -> None:
        """
        Test when setting the axis kwargs angle for an axis with a repeat that also has a custom angle.

        In this case, the repeat axis should NOT "follow" the main axis around according to
        the angle between repeat axes, including if we change the angle between repeat axes.
        """
        axis_kwargs = {
            "A": {"angle": 45},
            "A_repeat": {"angle": 55},
        }
        hp = example_hive_plot(
            repeat_axes="A",
            axis_kwargs=axis_kwargs,
        )

        assert hp.axes["A"].angle == axis_kwargs["A"]["angle"]
        assert hp.axes["A_repeat"].angle == axis_kwargs["A_repeat"]["angle"]

        # setting repeat angle between axes shouldn't mess with this either
        hp.set_angle_between_repeat_axes(47)
        assert hp.axes["A"].angle == axis_kwargs["A"]["angle"]
        assert hp.axes["A_repeat"].angle == axis_kwargs["A_repeat"]["angle"]

    def test_initialize_repeat_axis_same_sorting_variable(self) -> None:
        """
        Test initialization of a ``HivePlot`` instance with a repeat axis that keeps the same sorting variable.
        """
        axis_kwargs = {
            "A": {"start": 5, "end": 15, "vmin": 5},
            "A_repeat": {"start": 1, "long_name": "unchanged"},
        }
        hp = example_hive_plot(
            repeat_axes="A",
            sorting_variables="low",
            axis_kwargs=axis_kwargs,
        )

        for ax in hp.axes:
            assert hp.axes[ax].sorting_variable == "low"

        assert hp.axes["A"].polar_start == axis_kwargs["A"]["start"]
        assert hp.axes["A"].polar_end == axis_kwargs["A"]["end"]
        assert hp.axes["A"].vmin == axis_kwargs["A"]["vmin"]
        assert not hp.axes["A"].inferred_vmin
        assert hp.axes["A"].inferred_vmax
        assert hp.axes["A"].node_placements.rho.min() == hp.axes["A"].polar_end, (
            "All nodes should be forced to end with vmin higher than max sorting variable value."
        )

        assert hp.axes["A_repeat"].polar_start == axis_kwargs["A_repeat"]["start"]
        assert hp.axes["A_repeat"].vmin == axis_kwargs["A"]["vmin"]
        assert not hp.axes["A_repeat"].inferred_vmin, (
            "Repeat axis should just copying vmin over from original axis."
        )
        assert hp.axes["A_repeat"].inferred_vmax, (
            "Repeat axis should infer, because original axis did not and it wasn't set explicitly."
        )
        assert (
            hp.axes["A_repeat"].node_placements.rho.min() == hp.axes["A"].polar_end
        ), (
            "Should have placed nodes same as original axis because same sorting variable."
        )

    def test_initialize_repeat_axis_different_sorting_variable(self) -> None:
        """
        Test initialization of a ``HivePlot`` instance with a repeat axis that keeps the same sorting variable.
        """
        axis_kwargs = {
            "A": {"start": 5, "end": 15, "vmin": 5},
            "A_repeat": {"start": 1, "long_name": "unchanged"},
        }
        hp = example_hive_plot(
            repeat_axes="A",
            sorting_variables={
                "A": "low",
                "B": "low",
                "C": "low",
                "A_repeat": "med",
            },
            axis_kwargs=axis_kwargs,
        )

        for ax in hp.axes:
            if ax == "A_repeat":
                assert hp.axes[ax].sorting_variable == "med"
            else:
                assert hp.axes[ax].sorting_variable == "low"

        assert hp.axes["A"].polar_start == axis_kwargs["A"]["start"]
        assert hp.axes["A"].polar_end == axis_kwargs["A"]["end"]
        assert hp.axes["A"].vmin == axis_kwargs["A"]["vmin"]
        assert not hp.axes["A"].inferred_vmin
        assert hp.axes["A"].inferred_vmax
        assert hp.axes["A"].node_placements.rho.min() == hp.axes["A"].polar_end, (
            "All nodes should be forced to end with vmin higher than max sorting variable value."
        )

        assert hp.axes["A_repeat"].polar_start == axis_kwargs["A_repeat"]["start"]
        assert hp.axes["A_repeat"].vmin != axis_kwargs["A"]["vmin"], (
            "Should have changed vmin."
        )
        assert hp.axes["A_repeat"].inferred_vmin, (
            "Repeat axis should have inferred because different sorting variable than original axis."
        )
        assert hp.axes["A_repeat"].inferred_vmax, (
            "Repeat axis should have inferred because different sorting variable than original axis."
        )
        assert (
            hp.axes["A_repeat"].node_placements.rho.max()
            - hp.axes["A_repeat"].node_placements.rho.min()
            == hp.axes["A_repeat"].polar_end - hp.axes["A_repeat"].polar_start
        ), "Should have placed nodes spanning axis."

    @pytest.mark.parametrize(
        "update_method",
        [
            "set_repeat_axes",
            "update_sorting_variables",
            "set_partition",
            "set_axes_order",
            "set_rotation",
            "set_angle_between_repeat_axes",
            "update_axis",
        ],
    )
    def test_warn_on_plot_without_rebuild(self, update_method: str) -> None:
        """
        Test the ``plot()`` method warns when plotting but an update method has been run with ``build_hive_plot=False``.

        Skipping building the hive plot saves edge computation if the user is running multiple hive plot update / set
        methods, but plotting will be nonsense if they don't either run the last method with ``build_hive_plot=True`` or
        run the ``build_hive_plot()`` method themselves.

        :param update_method: which method to run on the hive plot before testing plotting.
        """
        hp = example_hive_plot()
        # should be no warnings plotting normal hive plot
        with warnings.catch_warnings():
            warnings.simplefilter("error")
            hp.plot()
            plt.close()

        if update_method == "set_repeat_axes":
            hp.set_repeat_axes(
                "A",
                build_hive_plot=False,
            )
        elif update_method == "update_sorting_variables":
            hp.update_sorting_variables(
                "low",
                build_hive_plot=False,
            )
        elif update_method == "set_partition":
            hp.set_partition(
                partition_variable="partition_0",
                sorting_variables="low",
                build_hive_plot=False,
            )
        elif update_method == "set_axes_order":
            hp.set_axes_order(
                ["A", "B", "C"],
                build_hive_plot=False,
            )
        elif update_method == "set_rotation":
            hp.set_rotation(
                rotation=0,
                build_hive_plot=False,
            )
        elif update_method == "set_angle_between_repeat_axes":
            hp.set_angle_between_repeat_axes(
                angle=40,
                build_hive_plot=False,
            )
        elif update_method == "update_axis":
            hp.update_axis(
                "A",
                build_hive_plot=False,
            )

        with pytest.warns() as record:
            hp.plot()
            plt.close()
        assert any(
            "Run the `build_hive_plot()` method on your `HivePlot` instance"
            in i.message.args[0]
            for i in record
        ), f"{[i.message.args for i in record]}"

    def test_build_hive_plot_build_axes_from_scratch(self) -> None:
        """
        Test running ``HivePlot.build_hive_plot()`` method but with ``build_axes_from_scratch=True``.
        """
        default_axes_hp = example_hive_plot(repeat_axes=True)

        custom_axis_kwargs = {
            "A": {
                "start": 5,
                "end": 10,
                "vmin": -2,
                "vmax": 15,
                "angle": 45,
                "long_name": "potato",
            },
            "A_repeat": {"angle": 0},
        }

        custom_hp = example_hive_plot(
            axis_kwargs=custom_axis_kwargs,
            repeat_axes=True,
        )

        for ax in ["A", "A_repeat"]:
            assert custom_hp.axes[ax].angle == custom_axis_kwargs[ax]["angle"]
            assert custom_hp.axes[ax].polar_start == custom_axis_kwargs["A"]["start"]
            assert custom_hp.axes[ax].polar_end == custom_axis_kwargs["A"]["end"]
            assert custom_hp.axes[ax].long_name == custom_axis_kwargs["A"]["long_name"]
            assert custom_hp.axes[ax].vmin == custom_axis_kwargs["A"]["vmin"]
            assert custom_hp.axes[ax].vmax == custom_axis_kwargs["A"]["vmax"]
            assert not custom_hp.axes[ax].inferred_vmin
            assert not custom_hp.axes[ax].inferred_vmax

        custom_hp.build_hive_plot(build_axes_from_scratch=True)

        for ax in ["A", "A_repeat"]:
            assert custom_hp.axes[ax].angle == default_axes_hp.axes[ax].angle, (
                "should have dropped custom kwargs"
            )
            assert (
                custom_hp.axes[ax].polar_start == default_axes_hp.axes[ax].polar_start
            ), "should have dropped custom kwargs"
            assert custom_hp.axes[ax].polar_end == default_axes_hp.axes[ax].polar_end, (
                "should have dropped custom kwargs"
            )
            assert custom_hp.axes[ax].long_name == default_axes_hp.axes[ax].long_name, (
                "should have dropped custom kwargs"
            )
            assert custom_hp.axes[ax].vmin == default_axes_hp.axes[ax].vmin, (
                "should have dropped custom kwargs"
            )
            assert custom_hp.axes[ax].vmax == default_axes_hp.axes[ax].vmax, (
                "should have dropped custom kwargs"
            )
            assert custom_hp.axes[ax].inferred_vmin
            assert custom_hp.axes[ax].inferred_vmax

    @pytest.mark.xfail(reason="Not Implemented")
    def test_delayed_build_hive_plot(self) -> None:
        """
        Test running various ``HivePlot`` methods but with ``build_hive_plot=False``.

        Confirm that we get the same results running multiple methods with a delayed build at the end as we get when
        we run the same methods with the default rebuild after each method call.
        """
        # TODO: confirm presumed commutativity is in fact valid and prove it with testing.
        #  If some methods are NOT commutative in practice without recomputing, then we should remove the
        #   ``build_hive_plot`` flag for those methods, rebuilding every time.
        pytest.fail(reason="Not Implemented.")

    @pytest.mark.parametrize("rebuild_edges", [True, False])
    @pytest.mark.parametrize("warn_on_overlapping_kwargs", [True, False])
    def test_edge_kwargs_normal_hierarchy(
        self, rebuild_edges: bool, warn_on_overlapping_kwargs: bool
    ) -> None:
        """
        Test the edge keyword argument parameters with standard kwarg hierarchy.

        :param rebuild_edges: whether to rebuild edges from scratch (shouldn't matter either way).
        :param warn_on_overlapping_kwargs: whether to warn about overlapping kwargs.
        """
        all_edge_kwargs = {"color": "blue"}
        clockwise_edge_kwargs = {"color": "green", "ls": "dotted"}

        hp = example_hive_plot(
            repeat_axes=True, warn_on_overlapping_kwargs=warn_on_overlapping_kwargs
        )

        with warnings.catch_warnings():
            warnings.simplefilter("error")
            hp.update_edge_plotting_keyword_arguments(
                edge_kwarg_setting="all_edge_kwargs",
                **all_edge_kwargs,
            )
        if warn_on_overlapping_kwargs:
            with pytest.warns() as record:
                hp.update_edge_plotting_keyword_arguments(
                    edge_kwarg_setting="clockwise_edge_kwargs",
                    reset_edge_kwarg_setting=True,
                    rebuild_edges=rebuild_edges,
                    **clockwise_edge_kwargs,
                )
            assert len(record) == 1, (
                f"Expected 1 warning for kwarg overlap, got:\n{[i.message.args for i in record]}"
            )
            assert "'color'" in record[0].message.args[0], (
                "Should warn about 'color' overlap variable"
            )
            assert "'ls'" not in record[0].message.args[0], (
                "Should not warn about 'ls' overlap variable"
            )

        else:
            with warnings.catch_warnings():
                warnings.simplefilter("error")
                hp.update_edge_plotting_keyword_arguments(
                    edge_kwarg_setting="clockwise_edge_kwargs",
                    reset_edge_kwarg_setting=True,
                    rebuild_edges=rebuild_edges,
                    **clockwise_edge_kwargs,
                )

        # plotting should not trigger a warning
        with warnings.catch_warnings():
            warnings.simplefilter("error")
            hp.plot(
                fig_kwargs={"figsize": (4, 4)},
            )

        for i, ax in enumerate(hp.axes_order):
            first_ax = ax
            second_ax = (
                hp.axes_order[0] if ax == hp.axes_order[-1] else hp.axes_order[i + 1]
            )
            assert (
                hp.hive_plot_edges[f"{first_ax}_repeat"][f"{second_ax}"][0][
                    "edge_kwargs"
                ]
                == all_edge_kwargs
            ), "Counterclockwise edge kwargs should be unaffected."

            assert (
                hp.hive_plot_edges[f"{second_ax}"][f"{first_ax}_repeat"][0][
                    "edge_kwargs"
                ]
                == clockwise_edge_kwargs
            ), "Clockwise edge kwargs should overwrite."

            assert (
                hp.hive_plot_edges[f"{first_ax}"][f"{first_ax}_repeat"][0][
                    "edge_kwargs"
                ]
                == all_edge_kwargs
            ), "Repeat edge kwargs should be unaffected."

    def test_edge_kwargs_change_all_kwargs(self) -> None:
        """
        Test the edge keyword argument parameters with standard kwarg hierarchy.

        Make changes to ``all_edge_kwargs``, ``clockwise_edge_kwargs``, ``counterclockwise_edge_kwargs``, and
        ``repeat_edge_kwargs``.
        """
        all_edge_kwargs = {"color": "blue", "ls": "-"}
        clockwise_edge_kwargs = {"color": "green", "ls": "dotted"}
        counterclockwise_edge_kwargs = {"color": "orange", "ls": "dashed"}
        repeat_edge_kwargs = {"color": "red", "ls": "dashdot"}

        hp = example_hive_plot(repeat_axes=True)

        with warnings.catch_warnings():
            warnings.simplefilter("error")
            hp.update_edge_plotting_keyword_arguments(
                edge_kwarg_setting="all_edge_kwargs",
                **all_edge_kwargs,
            )

        for i, (kw_name, kw_dict) in enumerate(
            zip(
                [
                    "clockwise_edge_kwargs",
                    "counterclockwise_edge_kwargs",
                    "repeat_edge_kwargs",
                ],
                [
                    clockwise_edge_kwargs,
                    counterclockwise_edge_kwargs,
                    repeat_edge_kwargs,
                ],
            )
        ):
            with pytest.warns() as record:
                hp.update_edge_plotting_keyword_arguments(
                    edge_kwarg_setting=kw_name,
                    **kw_dict,
                )
            # warnings accumulate as we go, still tests 1 warning per update call though
            assert len(record) == i + 1, (
                f"Expected {i + 1} warning(s) for kwarg overlap, got:\n{[i.message.args for i in record]}"
            )

            for kw in ["'color'", "'ls"]:
                assert f"{kw}" in record[0].message.args[0], (
                    f"Should warn about {kw} overlap variable"
                )

        # plotting should not trigger a warning
        with warnings.catch_warnings():
            warnings.simplefilter("error")
            hp.plot(
                fig_kwargs={"figsize": (4, 4)},
            )

        for i, ax in enumerate(hp.axes_order):
            first_ax = ax
            second_ax = (
                hp.axes_order[0] if ax == hp.axes_order[-1] else hp.axes_order[i + 1]
            )
            assert (
                hp.hive_plot_edges[f"{first_ax}_repeat"][f"{second_ax}"][0][
                    "edge_kwargs"
                ]
                == counterclockwise_edge_kwargs
            ), "Counterclockwise edge kwargs should overwrite."

            assert (
                hp.hive_plot_edges[f"{second_ax}"][f"{first_ax}_repeat"][0][
                    "edge_kwargs"
                ]
                == clockwise_edge_kwargs
            ), "Clockwise edge kwargs should overwrite."

            assert (
                hp.hive_plot_edges[f"{first_ax}"][f"{first_ax}_repeat"][0][
                    "edge_kwargs"
                ]
                == repeat_edge_kwargs
            ), "Repeat edge kwargs should overwrite."

    @pytest.mark.parametrize("valid_hierarchy", [True, False])
    def test_set_edge_kwarg_hierarchy(self, valid_hierarchy: bool) -> None:
        """
        Test changing the ``edge_kwarg_hierarchy`` parameter.

        :param valid_hierarchy: whether to change to a valid or invalid hierarchy.
        """
        hp = example_hive_plot(repeat_axes=True)

        if valid_hierarchy:
            new_hierarchy = [
                "clockwise_edge_kwargs",
                "all_edge_kwargs",
                "counterclockwise_edge_kwargs",
                "repeat_edge_kwargs",
            ]

            hp.edge_kwarg_hierarchy = new_hierarchy
            assert True

        else:
            new_hierarchy = ["my invalid hierarchy"]
            try:
                hp.edge_kwarg_hierarchy = new_hierarchy
                pytest.fail("Should have raised `InvalidEdgeKwargHierarchyError`")
            except InvalidEdgeKwargHierarchyError:
                assert True

    def test_edge_kwargs_changed_hierarchy(self) -> None:
        """
        Test the edge keyword argument parameters with a revised kwarg hierarchy.
        """
        all_edge_kwargs = {"color": "blue"}
        clockwise_edge_kwargs = {"color": "green", "ls": "dotted"}

        hp = example_hive_plot(repeat_axes=True)

        hp.update_edge_plotting_keyword_arguments(
            edge_kwarg_setting="all_edge_kwargs",
            **all_edge_kwargs,
        )
        with pytest.warns() as record:
            hp.update_edge_plotting_keyword_arguments(
                edge_kwarg_setting="clockwise_edge_kwargs",
                reset_edge_kwarg_setting=True,
                **clockwise_edge_kwargs,
            )
        assert len(record) == 1, (
            f"Expected 1 warning for kwarg overlap, got:\n{[i.message.args for i in record]}"
        )

        hp.edge_kwarg_hierarchy = [
            "clockwise_edge_kwargs",
            "all_edge_kwargs",
            "counterclockwise_edge_kwargs",
            "repeat_edge_kwargs",
        ]

        for i, ax in enumerate(hp.axes_order):
            first_ax = ax
            second_ax = (
                hp.axes_order[0] if ax == hp.axes_order[-1] else hp.axes_order[i + 1]
            )
            assert (
                hp.hive_plot_edges[f"{first_ax}_repeat"][f"{second_ax}"][0][
                    "edge_kwargs"
                ]
                == all_edge_kwargs
            ), "Counterclockwise edge kwargs should be unaffected."

            assert (
                hp.hive_plot_edges[f"{second_ax}"][f"{first_ax}_repeat"][0][
                    "edge_kwargs"
                ]
                == clockwise_edge_kwargs | all_edge_kwargs
                != all_edge_kwargs
            ), (
                "Clockwise edge kwargs should be deprioritized against all-edge kwargs, "
                "resulting in different final kwargs."
            )

            assert (
                hp.hive_plot_edges[f"{first_ax}"][f"{first_ax}_repeat"][0][
                    "edge_kwargs"
                ]
                == all_edge_kwargs
            ), "Repeat edge kwargs should be unaffected."

    @pytest.mark.parametrize("reset_edge_kwarg_setting", [True, False])
    def test_edge_kwargs_update_twice(self, reset_edge_kwarg_setting: bool) -> None:
        """
        Test updating edge keyword argument twice via ``update_edge_plotting_keyword_arguments()`` method.

        :param reset_edge_kwarg_setting: whether to reset edge kwarg setting or compose with it.
        """
        hp = example_hive_plot()

        hp.update_edge_plotting_keyword_arguments(
            edge_kwarg_setting="all_edge_kwargs",
            color="blue",
            ls="-",
        )

        hp.update_edge_plotting_keyword_arguments(
            edge_kwarg_setting="all_edge_kwargs",
            color="green",
            reset_edge_kwarg_setting=reset_edge_kwarg_setting,
        )

        if reset_edge_kwarg_setting:
            assert hp.edge_plotting_keyword_arguments["all_edge_kwargs"] == {
                "color": "green",
            }
        else:
            assert hp.edge_plotting_keyword_arguments["all_edge_kwargs"] == {
                "color": "green",
                "ls": "-",
            }

    @pytest.mark.parametrize(
        "to_change", ["short_arc", "control_rho_scale", "control_angle_shift"]
    )
    @pytest.mark.parametrize("a1_to_a2", [True, False])
    @pytest.mark.parametrize("a2_to_a1", [True, False])
    def test_update_edges_redraw_edges(
        self,
        to_change: Literal[
            "short_arc",
            "control_rho_scale",
            "control_angle_shift",
        ],
        a1_to_a2: bool,
        a2_to_a1: bool,
    ) -> None:
        """
        Test ``update_edges()`` when redrawing the edges.

        :param to_change: which edge-redrawing parameter to change
        :param a1_to_a2: whether to update edges going from axis_id_1 to axis_id_2.
        :param a2_to_a1: whether to update edges going from axis_id_2 to axis_id_1.
        """
        # skip case where not updating either direction, should fail / do nothing
        if not a1_to_a2 and not a2_to_a1:
            return

        axis_id_1 = "A"
        axis_id_2 = "B"

        hp = example_hive_plot()

        change_kwargs = {}

        if to_change == "short_arc":
            change_kwargs[to_change] = False
        elif to_change == "control_rho_scale":
            change_kwargs[to_change] = 1.2
        elif to_change == "control_angle_shift":
            change_kwargs[to_change] = 20

        a1_a2_edges = hp.hive_plot_edges[axis_id_1][axis_id_2][0]["curves"].copy()
        a2_a1_edges = hp.hive_plot_edges[axis_id_2][axis_id_1][0]["curves"].copy()

        hp.update_edges(
            axis_id_1=axis_id_1,
            axis_id_2=axis_id_2,
            a1_to_a2=a1_to_a2,
            a2_to_a1=a2_to_a1,
            **change_kwargs,
        )

        new_a1_a2_edges = hp.hive_plot_edges[axis_id_1][axis_id_2][0]["curves"].copy()
        new_a2_a1_edges = hp.hive_plot_edges[axis_id_2][axis_id_1][0]["curves"].copy()

        if a1_to_a2:
            assert not np.array_equal(
                a1_a2_edges[~np.isnan(a1_a2_edges)].reshape(-1, 2),
                new_a1_a2_edges[~np.isnan(new_a1_a2_edges)].reshape(-1, 2),
            ), "edge curves should have changed."
        else:
            (
                np.testing.assert_array_equal(a1_a2_edges, new_a1_a2_edges),
                "edge curves should NOT have changed.",
            )

        if a2_to_a1:
            assert not np.array_equal(
                a2_a1_edges[~np.isnan(a2_a1_edges)].reshape(-1, 2),
                new_a2_a1_edges[~np.isnan(new_a2_a1_edges)].reshape(-1, 2),
            ), "edge curves should have changed."
        else:
            (
                np.testing.assert_array_equal(a2_a1_edges, new_a2_a1_edges),
                "edge curves should NOT have changed.",
            )

    @pytest.mark.parametrize("overwrite_existing_kwargs", [True, False])
    @pytest.mark.parametrize("reset_existing_kwargs", [True, False])
    @pytest.mark.parametrize("a1_to_a2", [True, False])
    @pytest.mark.parametrize("a2_to_a1", [True, False])
    def test_update_edges_modify_edge_kwargs(
        self,
        overwrite_existing_kwargs: bool,
        reset_existing_kwargs: bool,
        a1_to_a2: bool,
        a2_to_a1: bool,
    ) -> None:
        """
        Test ``update_edges()`` when changing edge kwargs.

        :param overwrite_existing_kwargs: whether to overwrite existing kwargs or not.
        :param reset_existing_kwargs: whether to reset existing kwargs or not.
        :param a1_to_a2: whether to update edges going from axis_id_1 to axis_id_2.
        :param a2_to_a1: whether to update edges going from axis_id_2 to axis_id_1.
        """
        # skip case where not updating either direction, should fail / do nothing
        if not a1_to_a2 and not a2_to_a1:
            return

        axis_id_1 = "A"
        axis_id_2 = "B"

        starting_edge_kwargs = {"color": "blue", "linewidth": 2}

        additional_edge_kwargs = {"color": "green", "alpha": 0.5}

        hp = example_hive_plot(all_edge_kwargs=starting_edge_kwargs)

        a1_a2_edge_kwargs = hp.hive_plot_edges[axis_id_1][axis_id_2][0][
            "edge_kwargs"
        ].copy()
        a2_a1_edge_kwargs = hp.hive_plot_edges[axis_id_2][axis_id_1][0][
            "edge_kwargs"
        ].copy()

        assert a1_a2_edge_kwargs == a2_a1_edge_kwargs == starting_edge_kwargs

        hp.update_edges(
            axis_id_1=axis_id_1,
            axis_id_2=axis_id_2,
            a1_to_a2=a1_to_a2,
            a2_to_a1=a2_to_a1,
            overwrite_existing_kwargs=overwrite_existing_kwargs,
            reset_existing_kwargs=reset_existing_kwargs,
            **additional_edge_kwargs,
        )

        new_a1_a2_edge_kwargs = hp.hive_plot_edges[axis_id_1][axis_id_2][0][
            "edge_kwargs"
        ].copy()
        new_a2_a1_edge_kwargs = hp.hive_plot_edges[axis_id_2][axis_id_1][0][
            "edge_kwargs"
        ].copy()

        reset_overwrite_kw = {
            True: {
                True: additional_edge_kwargs.copy(),
                False: additional_edge_kwargs.copy(),
            },
            False: {
                True: starting_edge_kwargs.copy() | additional_edge_kwargs.copy(),
                False: additional_edge_kwargs.copy() | starting_edge_kwargs.copy(),
            },
        }

        if a1_to_a2:
            assert (
                new_a1_a2_edge_kwargs
                == reset_overwrite_kw[reset_existing_kwargs][overwrite_existing_kwargs]
            )
        else:
            assert new_a1_a2_edge_kwargs == starting_edge_kwargs

        if a2_to_a1:
            assert (
                new_a2_a1_edge_kwargs
                == reset_overwrite_kw[reset_existing_kwargs][overwrite_existing_kwargs]
            )
        else:
            assert new_a2_a1_edge_kwargs == starting_edge_kwargs

    @pytest.mark.parametrize("reset_kwargs", [True, False])
    def test_custom_node_kwargs_overwrite_existing_kwargs(
        self,
        reset_kwargs: bool,
    ) -> None:
        """
        Test changing node viz kwargs in a hive plot where we do / don't overwrite existing node viz kwargs.

        :param reset_kwargs: whether to reset node viz kwargs.
        """
        hp = example_hive_plot()
        hp.update_node_viz_kwargs(ls="dashdot", c="blue")
        hp.update_node_viz_kwargs(ls="dotted", reset_kwargs=reset_kwargs)

        if reset_kwargs:
            assert hp.nodes.node_viz_kwargs == {"ls": "dotted"}
        else:
            assert hp.nodes.node_viz_kwargs == {"ls": "dotted", "c": "blue"}

    @pytest.mark.parametrize("reset_kwargs", [True, False])
    def test_custom_edge_kwargs_overwrite_existing_kwargs(
        self,
        reset_kwargs: bool,
    ) -> None:
        """
        Test changing edge viz kwargs in a hive plot where we do / don't overwrite existing edge viz kwargs.

        :param reset_kwargs: whether to reset edge viz kwargs.
        """
        hp = example_hive_plot()
        hp.update_edge_viz_kwargs(ls="dashdot", cmap="cividis")
        hp.update_edge_viz_kwargs(ls="dotted", reset_kwargs=reset_kwargs)

        if reset_kwargs:
            assert hp.edges.edge_viz_kwargs == {"ls": "dotted"}
        else:
            assert hp.edges.edge_viz_kwargs == {"ls": "dotted", "cmap": "cividis"}

    @pytest.mark.parametrize("a1_to_a2", [True, False])
    @pytest.mark.parametrize("a2_to_a1", [True, False])
    @pytest.mark.parametrize("tag", [0, None])
    def test_reset_edges(
        self,
        a1_to_a2: bool,
        a2_to_a1: bool,
        tag: int,
    ) -> None:
        """
        Test inherited ``reset_edges`` method for its overwriting of the ``edges.relevant_edges`` attribute.

        :param a1_to_a2: whether to remove edges from the first to second axis.
        :param a2_to_a1: whether to remove edges from the second to first axis.
        """
        # test kill all edges
        hp = example_hive_plot()

        assert hp.edges.relevant_edges != {}, "Should have relevant edges"
        hp.reset_edges()
        assert hp.edges.relevant_edges == {}, "Should remove any relevant edges"

        # test kill edges between 2 specific axes
        hp = example_hive_plot()

        axis_id_1 = "A"
        axis_id_2 = "B"

        assert len(hp.edges.relevant_edges[axis_id_1][axis_id_2][0]) > 0, (
            f"Should have relevant edges {axis_id_1} -> {axis_id_2}"
        )

        assert len(hp.edges.relevant_edges[axis_id_2][axis_id_1][0]) > 0, (
            f"Should have relevant edges {axis_id_2} -> {axis_id_1}"
        )

        hp.reset_edges(
            axis_id_1=axis_id_1,
            axis_id_2=axis_id_2,
            tag=tag,
            a1_to_a2=a1_to_a2,
            a2_to_a1=a2_to_a1,
        )

        if a1_to_a2:
            if tag is not None:
                assert len(hp.edges.relevant_edges[axis_id_1][axis_id_2][0]) == 0, (
                    f"Should have deleted relevant edges {axis_id_1} -> {axis_id_2}"
                )
            else:
                assert len(hp.edges.relevant_edges[axis_id_1][axis_id_2]) == 0, (
                    f"Should have deleted relevant edges {axis_id_1} -> {axis_id_2}"
                )
        else:
            assert len(hp.edges.relevant_edges[axis_id_1][axis_id_2][0]) > 0, (
                f"Should have kept relevant edges {axis_id_1} -> {axis_id_2}"
            )

        if a2_to_a1:
            if tag is not None:
                assert len(hp.edges.relevant_edges[axis_id_2][axis_id_1][0]) == 0, (
                    f"Should have deleted relevant edges {axis_id_2} -> {axis_id_1}"
                )
            else:
                assert len(hp.edges.relevant_edges[axis_id_2][axis_id_1]) == 0, (
                    f"Should have deleted relevant edges {axis_id_2} -> {axis_id_1}"
                )
        else:
            assert len(hp.edges.relevant_edges[axis_id_2][axis_id_1][0]) > 0, (
                f"Should have kept relevant edges {axis_id_2} -> {axis_id_1}"
            )

        # test kill edges to / from 1 specific axis
        hp = example_hive_plot()

        axis_id_1 = "A"
        axis_id_2 = None

        assert len(hp.edges.relevant_edges[axis_id_1]) > 0, (
            f"Should have relevant edges {axis_id_1} -> other things"
        )

        for ax in hp.axes:
            # check only OTHER axes TO axis_id_1
            if ax == axis_id_1:
                continue
            assert len(hp.edges.relevant_edges[ax][axis_id_1][0]) > 0, (
                f"Should have relevant edges {ax} -> {axis_id_1}"
            )

        hp.reset_edges(
            axis_id_1=axis_id_1,
            axis_id_2=axis_id_2,
            tag=tag,
            a1_to_a2=a1_to_a2,
            a2_to_a1=a2_to_a1,
        )

        if a1_to_a2:
            assert len(hp.edges.relevant_edges[axis_id_1]) == 0, (
                f"Should have deleted relevant edges from {axis_id_1} -> everything"
            )
        else:
            assert len(hp.edges.relevant_edges[axis_id_1]) > 0, (
                f"Should have kept relevant edges {axis_id_1} -> other things"
            )

        for ax in hp.axes:
            # check only OTHER axes TO axis_id_1
            if ax == axis_id_1:
                continue
            if a2_to_a1:
                assert len(hp.edges.relevant_edges[ax][axis_id_1]) == 0, (
                    f"Should have deleted relevant edges {ax} -> {axis_id_1}"
                )
            else:
                assert len(hp.edges.relevant_edges[ax][axis_id_1][0]) > 0, (
                    f"Should have kept relevant edges {ax} -> {axis_id_1}"
                )
