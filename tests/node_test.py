# node_test.py

"""
Tests for the ``hiveplotlib.node``.
"""

import numpy as np
import pandas as pd
import pytest

from hiveplotlib.datasets import example_node_collection
from hiveplotlib.node import (
    Node,
    dataframe_to_node_list,
    split_nodes_on_variable,
)

pytestmark = pytest.mark.unmarked


class TestNode:
    """
    Tests for ``hiveplotlib.Node`` class.
    """

    def test_add_data_example(self) -> None:
        """
        Test adding data to ``Node`` instance.
        """
        node = Node(unique_id="node1")
        data = {"a": 1, "b": 2, "c": 3}
        node.add_data(data=data)
        assert node.data == data

    def test_repr(self) -> None:
        """
        Make sure ``__repr__`` is as expected.
        """
        node = Node(unique_id="potato")
        assert "potato" in node.__repr__()


class TestNodeCollection:
    """
    Tests for ``hiveplotlib.NodeCollection`` class.
    """

    def test_instantiate_node_collection(self) -> None:
        """
        Test instantiating a ``NodeCollection`` instance.
        """
        example_node_collection()
        assert True

    def test_repr(self) -> None:
        """
        Make sure ``__repr__`` is as expected.
        """
        num_nodes = 27
        node_collection = example_node_collection(num_nodes=num_nodes)
        assert f"{num_nodes}" in node_collection.__repr__()
        assert "unique_id" in node_collection.__repr__()

    def test_len(self) -> None:
        """
        Make sure ``__len__`` is as expected.
        """
        num_nodes = 27
        node_collection = example_node_collection(num_nodes=num_nodes)
        assert len(node_collection) == num_nodes

    def test_create_partition_variable(self) -> None:
        """
        Test ``NodeCollection.create_partition_variable()`` behaves as expected.
        """
        node_collection = example_node_collection()
        partition_col_0 = node_collection.create_partition_variable(
            data_column="low",
            cutoffs=None,
            partition_variable_name=None,
        )
        number_of_cutoffs = 2
        partition_col_1 = node_collection.create_partition_variable(
            data_column="med",
            cutoffs=number_of_cutoffs,
            labels=["A", "B"],
            partition_variable_name="my_special_partition_column_name",
        )
        float_cutoff = 25
        partition_col_2 = node_collection.create_partition_variable(
            data_column="high",
            cutoffs=[float_cutoff],
            labels=None,
            partition_variable_name=None,
        )
        custom_float_group_labels = ["<", ">"]
        partition_col_3 = node_collection.create_partition_variable(
            data_column="high",
            cutoffs=[float_cutoff],
            labels=custom_float_group_labels,
            partition_variable_name=None,
        )

        # check partition names behave as expected
        assert partition_col_0 == "partition_0"
        assert "partition_0" in node_collection.data.columns.to_list()
        assert partition_col_1 == "my_special_partition_column_name"
        assert (
            "my_special_partition_column_name" in node_collection.data.columns.to_list()
        )
        assert partition_col_2 == "partition_1"
        assert "partition_1" in node_collection.data.columns.to_list()

        assert partition_col_3 == "partition_2"
        assert "partition_2" in node_collection.data.columns.to_list()

        # check each partition did what we expected
        assert (
            node_collection.data["partition_0"].unique().size
            == node_collection.data.shape[0]
        ), (
            "Partition is on unique values; all values are unique, so should be as many unique labels as data points."
        )

        assert (
            node_collection.data[partition_col_1].unique().size == number_of_cutoffs
        ), (
            f"Specified breaking into {number_of_cutoffs} groups, so should only have {number_of_cutoffs} labels."
        )

        assert node_collection.data[partition_col_2].unique().tolist() == [
            pd.Interval(-np.inf, float_cutoff),
            pd.Interval(25, np.inf),
        ], (
            f"Specified breaking along 1 float cutoff, so should have 2 labels (above and below {float_cutoff})."
        )

        assert (
            node_collection.data[partition_col_3].unique().tolist()
            == custom_float_group_labels
        ), "Specified breaking along 1 float cutoff with custom labels."


def test_split_nodes_on_variable_unique_indices() -> None:
    """
    Test ``hiveplotlib.node.split_nodes_on_variable()`` plays nice when selecting unique indices.
    """
    idxs_a = np.arange(10)
    a_nodes = [Node(unique_id=i, data={"val": "A"}) for i in idxs_a]

    idxs_b = np.arange(10) + 10
    b_nodes = [Node(unique_id=i, data={"val": "B"}) for i in idxs_b]

    all_nodes = a_nodes + b_nodes

    split = split_nodes_on_variable(all_nodes, variable_name="val")

    assert list(split.keys()) == ["A", "B"]

    assert np.array_equal(split["A"], idxs_a)
    assert np.array_equal(split["B"], idxs_b)


def test_split_nodes_on_variable_unique_splits() -> None:
    """
    Test ``hiveplotlib.node.split_nodes_on_variable()`` plays nice when splitting at specified ``cutoffs``.
    """
    idxs_a = np.arange(10)
    a_nodes = [Node(unique_id=i, data={"val": i}) for i in idxs_a]

    idxs_b = np.arange(10) + 10
    b_nodes = [Node(unique_id=i, data={"val": i}) for i in idxs_b]

    all_nodes = a_nodes + b_nodes

    labels = ["small", "med", "large"]
    cutoffs = [5, 10]
    split = split_nodes_on_variable(
        all_nodes, variable_name="val", cutoffs=cutoffs, labels=labels
    )

    split_keys = list(split.keys())
    split_keys.sort()
    assert split_keys == sorted(labels)

    assert np.array_equal(split["small"], np.arange(cutoffs[0] + 1))
    assert np.array_equal(split["med"], np.arange(cutoffs[0] + 1, cutoffs[1] + 1))
    assert np.array_equal(split["large"], np.arange(cutoffs[-1] + 1, idxs_b.max() + 1))


def test_split_nodes_on_variable_quantile_splits() -> None:
    """
    Test ``hiveplotlib.node.split_nodes_on_variable()`` plays nice when splitting at quantiles.
    """
    idxs_a = np.arange(10)
    a_nodes = [Node(unique_id=i, data={"val": i}) for i in idxs_a]

    idxs_b = np.arange(10) + 10
    b_nodes = [Node(unique_id=i, data={"val": i}) for i in idxs_b]

    idxs_c = np.arange(10) + 20
    c_nodes = [Node(unique_id=i, data={"val": i}) for i in idxs_c]

    all_nodes = a_nodes + b_nodes + c_nodes

    labels = ["small", "med", "large"]
    cutoffs = 3
    split = split_nodes_on_variable(
        all_nodes, variable_name="val", cutoffs=cutoffs, labels=labels
    )

    split_keys = list(split.keys())
    split_keys.sort()
    assert split_keys == sorted(labels)

    assert np.array(
        [len(split[lab]) == (len(all_nodes) / cutoffs) for lab in labels]
    ).all(), "Should have evenly sized quantiles of splitting of the nodes"

    assert np.array_equal(split["small"], idxs_a)
    assert np.array_equal(split["med"], idxs_b)
    assert np.array_equal(split["large"], idxs_c)


def test_dataframe_to_node_list() -> None:
    """
    Test ``hiveplotlib.node.dataframe_to_node_list()`` makes (and fails to make) lists of ``Nodes`` as expected.
    """
    num_nodes = 10

    row_vals_unique = np.arange(num_nodes)

    row_vals_not_unique = row_vals_unique.copy()
    row_vals_not_unique[1] = 0

    columns = [row_vals_unique, row_vals_not_unique]

    df = pd.DataFrame(data=np.column_stack(columns), columns=["unique", "not_unique"])

    node_list = dataframe_to_node_list(df=df, unique_id_column="unique")

    # make sure values propagated as expected to each `Node` instance
    for i, node in enumerate(node_list):
        assert node.unique_id == df["unique"].to_numpy()[i]
        assert list(node.data.keys()) == ["not_unique"]
        assert node.data["not_unique"] == df["not_unique"].to_numpy()[i]

    try:
        dataframe_to_node_list(df=df, unique_id_column="not_unique")
        pytest.fail("Should have failed on non-uniqueness")
    except AssertionError:
        assert True
