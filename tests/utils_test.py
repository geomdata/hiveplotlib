# utils_test.py

"""
Tests for ``hiveplotlib.utils.py``.
"""

import numpy as np
import pytest

from hiveplotlib import utils

pytestmark = pytest.mark.unmarked


@pytest.mark.parametrize("x_y", [[1, 0], [0, 2]])
def test_cartesian2polar_examples(x_y: list) -> None:
    """
    Test ``hiveplotlib.utils.cartesian2polar()`` on some simple examples.
    """
    rho, phi = utils.cartesian2polar(x_y[0], x_y[1])

    if x_y == [1, 0]:
        assert rho == 1
        assert phi == 0
    elif x_y == [0, 2]:
        assert rho == 2
        assert phi == 90
    else:
        raise NotImplementedError


@pytest.mark.parametrize("rho_phi", [[1, 0], [2, 90]])
def test_polar2cartesian_examples(rho_phi: list) -> None:
    """
    Test ``hiveplotlib.utils.polar2cartesian()`` on some simple examples.
    """
    x, y = utils.polar2cartesian(rho_phi[0], rho_phi[1])

    if rho_phi == [1, 0]:
        assert x == 1
        assert y == 0
    elif rho_phi == [2, 90]:
        assert round(x) == 0  # rounding to deal with numerical error
        assert round(y) == 2
    else:
        raise NotImplementedError


@pytest.mark.parametrize("start_stop", [[0, 1], [-4, 7]])
@pytest.mark.parametrize("control_point", ["start", "middle", "stop"])
def test_bezier_example(start_stop: list, control_point: str) -> None:
    """
    Test ``hiveplotlib.utils.bezier()`` with varying control point on some simple examples.

    Control point should shift average of output towards the control point value.

    :param start_stop: starting and stopping points to draw Bézier curve on.
    :param control_point: where the control point is placed (at the start value, the midpoint, or the stop value).
    """
    start = start_stop[0]
    stop = start_stop[1]

    if control_point == "start":
        control = start
    elif control_point == "middle":
        control = (start + stop) / 2
    elif control_point == "stop":
        control = stop
    else:
        raise NotImplementedError

    # 11 steps to avoid rounding issues
    bezier_output = utils.bezier(start=start, end=stop, control=control, num_steps=11)

    bezier_mean = bezier_output.mean()

    if control_point == "start":
        assert bezier_mean < (start + stop) / 2
    elif control_point == "middle":
        assert np.round(bezier_mean, 5) == (start + stop) / 2
    elif control_point == "stop":
        assert bezier_mean > (start + stop) / 2


def test_bezier_all_example() -> None:
    """
    Test ``hiveplotlib.utils.bezier_all()`` with specific example.

    Control point should shift average of output towards the control point value, so we'll do 3 non-intersecting
    curves with different direction control points.
    """
    points = np.array([[-6, -5], [-4, 7], [8, 10]])
    # control point start, middle, end
    controls = [points[0, 0], points[1, :].mean(), points[2, 1]]

    # means for reference
    means = points.mean(axis=1)

    # 11 steps to avoid rounding issues
    num_steps = 11
    bezier_output = utils.bezier_all(
        start_arr=points[:, 0],
        end_arr=points[:, 1],
        control_arr=controls,
        num_steps=num_steps,
    )

    assert bezier_output.size == num_steps * points.shape[0]

    # outputs should be distinct from each other
    pieces = np.split(bezier_output, points.shape[0])
    assert np.intersect1d(pieces[0], pieces[1]).size == 0
    assert np.intersect1d(pieces[1], pieces[2]).size == 0
    assert np.intersect1d(pieces[2], pieces[0]).size == 0

    # pieces should have their constructed biases due to control points
    assert pieces[0].mean() < means[0]
    assert pieces[1].mean() == means[1]
    assert pieces[2].mean() > means[2]
