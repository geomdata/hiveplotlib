# datashader_test.py

"""
Tests for ``hiveplotlib.viz.datashader`` module.
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pytest

from hiveplotlib import P2CP, BaseHivePlot, Node, hive_plot_n_axes, p2cp_n_axes
from hiveplotlib.datasets import example_hive_plot
from hiveplotlib.node import node_collection_from_node_list

pytestmark = pytest.mark.datashader


def three_axis_basehiveplot_example() -> BaseHivePlot:
    """
    3 axis example ``BaseHivePlot`` for use in multiple tests later and simulated edges.

    :return: ``hiveplotlib.BaseHivePlot`` instance, ``(n, 2)`` array  of edges (ids).
    """
    # build nodes
    node0 = Node(unique_id="0", data={"a": 1, "b": 2, "c": 3})
    node1 = Node(unique_id="1", data={"a": 2, "b": 3, "c": 4})
    node2 = Node(unique_id="2", data={"a": 3, "b": 4, "c": 5})

    nodes = [node0, node1, node2]

    # dummy of from, to pairs
    rng = np.random.default_rng(0)
    edges = rng.choice(np.arange(len(nodes)).astype(str), size=10).reshape(-1, 2)
    more_edges = rng.choice(np.arange(len(nodes)).astype(str), size=20).reshape(-1, 2)

    return hive_plot_n_axes(
        nodes=node_collection_from_node_list(node_list=nodes),
        edges=[edges, more_edges],
        axes_assignments=[["0"], ["1"], ["2"]],
        sorting_variables=["a", "b", "c"],
        axes_names=["A", "B", "C"],
        suppress_deprecation_warning=True,
    )


def three_axis_p2cp_example() -> P2CP:
    """
    3 axis example ``P2CP`` for use in multiple tests later.

    :return: ``hiveplotlib.P2CP`` instance.
    """
    num_nodes = 50

    # a and b columns move in sync with eventual node id (index), c is random
    rng = np.random.default_rng(0)
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            rng.uniform(low=0, high=10, size=num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    data["tag_col"] = rng.choice([True, False], size=num_nodes)

    return p2cp_n_axes(data=data, split_on="tag_col")


@pytest.mark.parametrize(
    "func", [three_axis_basehiveplot_example, three_axis_p2cp_example]
)
def test_datashade_edges_tag(func: callable) -> None:
    """
    Test ``hiveplotlib.viz.datashader.datashade_edges_mpl()`` plots different stuff for different tags.
    """
    from hiveplotlib.viz.datashader import datashade_edges_mpl

    instance = func()

    _, _, im_0 = datashade_edges_mpl(instance, tag=0, dpi=100)  # to speed up tests
    plt.close()
    _, _, im_1 = datashade_edges_mpl(instance, tag=1, dpi=100)  # to speed up tests
    plt.close()

    assert not np.array_equal(im_0.get_array().data, im_1.get_array().data)


@pytest.mark.parametrize(
    "func", [three_axis_basehiveplot_example, three_axis_p2cp_example]
)
def test_datashade_hive_plot_tag(func: callable) -> None:
    """
    Test ``hiveplotlib.viz.datashader.datashade_hive_plot_mpl()`` plots different stuff for different tags.
    """
    from hiveplotlib.viz.datashader import datashade_hive_plot_mpl

    instance = func()

    _, _, node_im_0, edge_im_0 = datashade_hive_plot_mpl(
        instance, tag=0, dpi=100
    )  # to speed up tests
    plt.close()
    _, _, node_im_1, edge_im_1 = datashade_hive_plot_mpl(
        instance, tag=1, dpi=100
    )  # to speed up tests
    plt.close()

    assert not np.array_equal(edge_im_0.get_array().data, edge_im_1.get_array().data), (
        "Edges should be different"
    )

    assert np.array_equal(node_im_0.get_array().data, node_im_1.get_array().data), (
        "Nodes should be the same"
    )


@pytest.mark.parametrize(
    "func", [three_axis_basehiveplot_example, three_axis_p2cp_example]
)
def test_datashade_check_warnings(func: callable) -> None:
    """
    Test ``hiveplotlib.viz.datashader.datashade_edges_mpl()`` warns when there are no edges or tag specified.

    This is only relevant when there are multiple tags.
    """
    from hiveplotlib.viz.datashader import datashade_edges_mpl

    instance = func()

    # trigger "no tag" warning
    with pytest.warns() as record:
        _, _, im_0 = datashade_edges_mpl(instance, dpi=100)  # to speed up tests
        plt.close()

    # confirm we get a single warning
    assert len(record) == 1, (
        f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"
    )

    # kill all edges to make sure we get a "no edges" warning
    instance.reset_edges()

    with pytest.warns() as record:
        out = datashade_edges_mpl(instance, tag=0)  # to speed up tests

    # shouldn't plot anything when there are no edges
    assert out[2] is None
    # confirm we get a single warning
    assert len(record) == 1, (
        f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"
    )


@pytest.mark.parametrize(
    "viz_function",
    ["datashade_edges_mpl", "datashade_nodes_mpl", "datashade_hive_plot_mpl"],
)
def test_expected_errors_wrong_types(viz_function: str) -> None:
    """
    Viz functions should yell if tried on non-``P2CP`` or ``HivePlot`` instances.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    import hiveplotlib.viz.datashader

    viz_function = getattr(hiveplotlib.viz.datashader, viz_function)

    node = Node(unique_id=10)

    try:
        viz_function(node)
        plt.close()
        pytest.fail(
            f"`{viz_function}` should have failed to run on an invalid input, but succeeded for some reason."
        )
    # this was supposed to fail
    except NotImplementedError:
        assert True


@pytest.mark.parametrize(
    "instance", [three_axis_basehiveplot_example, three_axis_p2cp_example]
)
@pytest.mark.parametrize("func", ["datashade_edges_mpl", "datashade_nodes_mpl"])
def test_datashade_cmap(instance: callable, func: str) -> None:
    """
    Test ``hiveplotlib.viz.datashader`` module assigns different cmaps to resulting images.
    """
    import hiveplotlib.viz.datashader

    func = getattr(hiveplotlib.viz.datashader, func)
    inst = instance()

    extra_kwargs = {"dpi": 100}  # to speed up tests
    if func == hiveplotlib.viz.datashader.datashade_edges_mpl:
        extra_kwargs["tag"] = 0

    # compose onto existing fig, ax to up our coverage while we're at it
    fig, ax = plt.subplots(figsize=(5, 5))
    _, _, im_0 = func(inst, cmap="viridis", fig=fig, ax=ax, **extra_kwargs)
    _, _, im_1 = func(inst, cmap="Blues", fig=fig, ax=ax, **extra_kwargs)

    # cmaps should be different
    assert im_0.get_cmap() != im_1.get_cmap()


@pytest.mark.parametrize("func", ["datashade_edges_mpl", "datashade_nodes_mpl"])
def test_datashade_pixel_spread(func: str) -> None:
    """
    Test ``hiveplotlib.viz.datashader`` module performs ``pixel_spread`` as expected.
    """
    import hiveplotlib.viz.datashader

    func = getattr(hiveplotlib.viz.datashader, func)
    hp = three_axis_basehiveplot_example()

    extra_kwargs = {"dpi": 100}  # to speed up tests
    if func == hiveplotlib.viz.datashader.datashade_edges_mpl:
        extra_kwargs["tag"] = 0

    _, _, im_0 = func(hp, pixel_spread=0, **extra_kwargs)
    plt.close()
    _, _, im_1 = func(hp, pixel_spread=1, **extra_kwargs)
    plt.close()
    _, _, im_2 = func(hp, pixel_spread=2, **extra_kwargs)
    plt.close()

    # sums should increase as we spread more on pixels
    assert (
        im_0.get_array().data.sum()
        < im_1.get_array().data.sum()
        < im_2.get_array().data.sum()
    )


def test_warnings_no_axes_p2cp() -> None:
    """
    Test ``hiveplotlib.viz.datashader.datashade_edges_mpl()`` hits a warning when we are missing axes for p2cps.
    """
    from hiveplotlib.viz.datashader import datashade_nodes_mpl

    p2cp = P2CP()

    with pytest.warns() as record:
        out = datashade_nodes_mpl(p2cp)
        plt.close()

    assert out[2] is None
    # confirm we get a single warning
    assert len(record) == 1, (
        f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"
    )


def test_hiveplot_viz_backend_datashader() -> None:
    """
    Test that we can call the viz backend set as ``"datashader"`` on a ``HivePlot`` instance.
    """
    hp = example_hive_plot(
        backend="datashader",
    )
    hp.plot()
    plt.close()
    assert True


def test_plot_empty_hive_plot() -> None:
    """
    Test outputs when plotting an empty (no axes) ``HivePlot`` instance.
    """
    hp = example_hive_plot(
        backend="datashader",
    )
    # kill all existing axes in hive plot (thus killing all edges and nodes too)
    hp.set_partition(
        "partition_0",
        sorting_variables="low",
        build_hive_plot=False,
    )

    with pytest.warns() as record:
        out = hp.plot()
        plt.close()
    assert len(record) == 4, (
        "Expected only 4 warnings ('intermediate changes', no axes, no edges, no nodes), "
        f"got warnings of:\n{[i.message.args for i in record]}"
    )
    for i in record:
        assert (
            "Run the `build_hive_plot()` method on your `HivePlot` instance"
            in i.message.args[0]
            or "`HivePlot.build_hive_plot()`" in i.message.args[0]
        ), f"{i.message.args}"

    # make sure outputs come out the same even when plotting empty hive plot
    assert isinstance(out[0], plt.Figure)
    assert isinstance(out[1], plt.Axes)
    assert out[2] is None
    assert out[3] is None


def test_plot_with_node_kwargs() -> None:
    """
    Test when plotting including ``node_kwargs``, which are valid for other backends, but not datashader.

    Inclusion of these should raise a warning, as size / color / alpha values for this backend are reserved for showing
    density of overlapping nodes with datashading.
    """
    hp = example_hive_plot(repeat_axes=True, backend="datashader")
    hp.nodes.data["size_10x"] = hp.nodes.data["low"].to_numpy() * 10

    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()

    extra_kwargs = {"dpi": 100}  # to speed up tests
    # should run with warning
    with pytest.warns() as record:
        hp.plot(
            node_kwargs={"c": "blue"},
            **extra_kwargs,
        )

    assert len(record) == 1, (
        "Expected only 1 warning (ignoring `node_kwargs`), "
        f"got warnings of:\n{[i.message.args for i in record]}"
    )

    assert (
        "`node_kwargs` are not valid when plotting with the `datashader` backend"
        in record[0].message.args[0]
    )
