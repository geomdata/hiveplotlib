# p2cp_test.py

"""
Tests for ``hiveplotlib.P2CP``.
"""

import json

import numpy as np
import pandas as pd
import pytest

from hiveplotlib import P2CP, p2cp_n_axes

pytestmark = pytest.mark.unmarked


class TestP2CP:
    """
    Tests for ``hiveplotlib.P2CP``.
    """

    def setup_method(self) -> None:
        """
        Perform setup.
        """
        rng = np.random.default_rng(0)

        num_nodes = 50

        # a and b columns move in sync with eventual node id (index), c is random
        data = pd.DataFrame(
            np.c_[
                np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
                np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
                rng.uniform(low=0, high=10, size=num_nodes),
            ],
            columns=["a", "b", "c"],
        )

        self.data = data

    @pytest.mark.parametrize("data_input", ["instantiate", "method_call"])
    def test_set_data(self, data_input: str) -> None:
        """
        Make sure ``hiveplotlib.P2CP.set_data()`` behaves as expected.

        :param data_input: how the data is input into the ``P2CP`` instance. "instantiate" does it on instantiation
            while "method_call" uses the ``set_data()`` method.
        """
        if data_input == "instantiate":
            p2cp = P2CP(data=self.data)
        elif data_input == "method_call":
            p2cp = P2CP()
            assert p2cp.data is None
            assert p2cp._node_collection is None

            p2cp.set_data(data=self.data)

        else:
            raise NotImplementedError

        assert np.array_equal(
            self.data.index.values,
            p2cp._node_collection.data[
                p2cp._node_collection.unique_id_column
            ].to_numpy(),
        )
        assert p2cp._node_collection.data.shape[0] == self.data.shape[0]

        # check the jth node's data
        j = 10
        node_data = p2cp._node_collection.data.iloc[j, :]
        assert node_data.unique_id == j

    def test_set_axes_assertions(self) -> None:
        """
        Make sure ``hiveplotlib.P2CP.set_axes()`` behaves as expected with assertion checks.
        """
        p2cp = P2CP(data=self.data)

        try:
            p2cp.set_axes(columns=["a", "b", "d"])
            pytest.fail("'d' column not in data, should've broken things")
        except AssertionError:
            assert True

        try:
            p2cp.set_axes(columns=["a", "b", "c"], angles=[10, 50, 270, 330])
            pytest.fail("Wrong number of `angles` should've broken things")
        except AssertionError:
            assert True

        try:
            p2cp.set_axes(columns=["a", "b", "c"], vmins=[-1, None, 10, 1e6])
            pytest.fail("Wrong number of `vmins` should've broken things")
        except AssertionError:
            assert True

        try:
            p2cp.set_axes(columns=["a", "b", "c"], vmaxes=[-1, None, 10, 1e6])
            pytest.fail("Wrong number of `vmaxes` should've broken things")
        except AssertionError:
            assert True

        try:
            p2cp.set_axes(columns=["a", "b", "c"], axis_kwargs=[{}] * 4)
            pytest.fail("Wrong number of `axis_kwargs` should've broken things")
        except AssertionError:
            assert True

    def test_set_axes_overwrite_axes_placing_nodes(self) -> None:
        """
        Make sure ``hiveplotlib.P2CP.set_axes()`` behaves as expected with overwriting existing axes.

        Also confirm we're placing our nodes in number as expected.
        """
        p2cp_list = []
        key_list = []

        p2cp = P2CP(data=self.data)
        p2cp.set_axes(columns=["a", "b"])
        assert sorted(p2cp.axes.keys()) == ["a", "b"]
        assert p2cp.axes_list == ["a", "b"]
        p2cp_list.append(p2cp.copy())
        key_list.append(["a", "b"])

        p2cp.set_axes(columns=["b"], overwrite_previously_set_axes=False)
        assert sorted(p2cp.axes.keys()) == ["a", "b", "b\nRepeat"]
        assert p2cp.axes_list == ["a", "b", "b\nRepeat"]
        p2cp_list.append(p2cp.copy())
        key_list.append(["a", "b", "b\nRepeat"])

        p2cp.set_axes(columns=["a"], overwrite_previously_set_axes=True)
        assert sorted(p2cp.axes.keys()) == ["a"]
        assert p2cp.axes_list == ["a"]
        p2cp_list.append(p2cp.copy())
        key_list.append(["a"])

        # check the underlying hiveplot instance while we're at it
        for keys, p2cp in zip(key_list, p2cp_list):
            assert sorted(p2cp._hiveplot.axes.keys()) == keys
            for key in keys:
                # all points should be on all axes by definition of P2CPs
                assert (
                    p2cp._hiveplot.axes[key].node_placements.shape[0]
                    == self.data.shape[0]
                ), (
                    f"p2cp: {p2cp._hiveplot.axes.keys()}\nour keys: {keys}\ncurrent_axis: {key}\n"
                    + f"all node sizes: {[p2cp._hiveplot.axes[k].node_placements.shape[0] for k in keys]}"
                )

    @pytest.mark.parametrize("args", ["not_None", "None"])
    def test_set_axes_optional_args(self, args: str) -> None:
        """
        Make sure ``hiveplotlib.P2CP.set_axes()`` behaves as expected with many of the optional args.

        :param args: whether to specify the ``None`` defaulted args as ``None`` or check explicit values
        """
        p2cp = P2CP(data=self.data)
        cols = ["a", "b", "c"]

        if args == "not_None":
            angles = [5, 10, 15]
            vmins = [-10, None, 10]
            vmaxes = [0, None, 20]
            axis_kwargs = [{"start": 2}, None, {"start": 3}]

        elif args == "None":
            angles = None
            vmins = None
            vmaxes = None
            axis_kwargs = None

        else:
            raise NotImplementedError

        p2cp.set_axes(
            columns=cols,
            angles=angles,
            vmins=vmins,
            vmaxes=vmaxes,
            axis_kwargs=axis_kwargs,
        )

        if args == "not_None":
            for idx, c in enumerate(cols):
                assert p2cp.axes[c]["axis"].angle == angles[idx]
                # check storing vmin and vmax values
                if vmins[idx] is None:
                    assert p2cp.axes[c]["vmin"] is None
                else:
                    assert p2cp.axes[c]["vmin"] == vmins[idx]
                if vmaxes[idx] is None:
                    assert p2cp.axes[c]["vmax"] is None
                else:
                    assert p2cp.axes[c]["vmax"] == vmaxes[idx]
                # check we placed nodes accordingly
                if idx == 0:
                    # should have altered axis polar start
                    assert (
                        p2cp._hiveplot.axes[c].polar_start == axis_kwargs[idx]["start"]
                    )
                    # should have all been clipped to max polar rho of axis (5 by default)
                    rho_values = list(
                        p2cp._hiveplot.axes[c].node_placements["rho"].values
                    )
                    unique_rho = list(set(rho_values))
                    assert len(unique_rho) == 1
                    assert unique_rho[0] == 5
                elif idx == 2:
                    # should have altered axis polar start
                    assert (
                        p2cp._hiveplot.axes[c].polar_start == axis_kwargs[idx]["start"]
                    )
                    # should have all been clipped to min polar rho of axis based on above axis_kwargs
                    rho_values = list(
                        p2cp._hiveplot.axes[c].node_placements["rho"].values
                    )
                    unique_rho = list(set(rho_values))
                    assert len(unique_rho) == 1
                    assert unique_rho[0] == axis_kwargs[idx]["start"]
                elif idx == 1:
                    # should have not altered axis polar start
                    assert p2cp._hiveplot.axes[c].polar_start == 1
                    # unconstrained by `None` values => all should place uniquely since randomly drawn
                    rho_values = list(
                        p2cp._hiveplot.axes[c].node_placements["rho"].values
                    )
                    assert len(list(set(rho_values))) == self.data.shape[0]
                else:
                    raise NotImplementedError

        elif args == "None":
            for idx, c in enumerate(cols):
                # default angles
                assert p2cp.axes[c]["axis"].angle == 120 * idx
                # check storing vmin and vmax values
                assert p2cp.axes[c]["vmin"] is None
                assert p2cp.axes[c]["vmax"] is None
                # should have not altered axis polar start
                assert p2cp._hiveplot.axes[c].polar_start == 1
                # check we placed nodes accordingly
                #  unconstrained by `None` values => all should place uniquely since randomly drawn
                rho_values = list(p2cp._hiveplot.axes[c].node_placements["rho"].values)
                assert len(list(set(rho_values))) == self.data.shape[0]

    def test_set_axes_start_angle(self) -> None:
        """
        Make sure ``hiveplotlib.P2CP.set_axes()`` behaves as expected when changing the ``start_angle`` arg.
        """
        p2cp = P2CP(data=self.data)
        cols = ["a", "b", "c"]
        desired_angles = [0, 120, 240]

        p2cp.set_axes(columns=cols, start_angle=0, overwrite_previously_set_axes=True)

        for c, a in zip(cols, desired_angles):
            assert p2cp.axes[c]["axis"].angle == a

        start_angle = 10
        desired_angles = [10, 130, 250]

        p2cp.set_axes(
            columns=cols, start_angle=start_angle, overwrite_previously_set_axes=True
        )

        for c, a in zip(cols, desired_angles):
            assert p2cp.axes[c]["axis"].angle == a

        start_angle = -10
        desired_angles = [350, 110, 230]

        p2cp.set_axes(
            columns=cols, start_angle=start_angle, overwrite_previously_set_axes=True
        )

        for c, a in zip(cols, desired_angles):
            assert p2cp.axes[c]["axis"].angle == a

        cols = ["a", "b"]
        start_angle = 90
        desired_angles = [90, 270]

        p2cp.set_axes(
            columns=cols, start_angle=start_angle, overwrite_previously_set_axes=True
        )

        for c, a in zip(cols, desired_angles):
            assert p2cp.axes[c]["axis"].angle == a

    def test_build_edges(self) -> None:
        """
        Make sure ``hiveplotlib.P2CP.build_edges()`` behaves as expected.
        """
        p2cp = P2CP(data=self.data)
        cols = ["a", "b", "c"]
        p2cp.set_axes(columns=cols)

        expected_edges = np.c_[
            p2cp._node_collection.data[
                p2cp._node_collection.unique_id_column
            ].to_numpy(),
            p2cp._node_collection.data[
                p2cp._node_collection.unique_id_column
            ].to_numpy(),
        ]
        p2cp.build_edges()

        # should only have 1 default tag of data
        assert p2cp.tags == [0]
        # should have 1 direction "loop" of data around axes
        for i, _ in enumerate(cols):
            a1 = cols[i]
            a2 = cols[(i + 1) % len(cols)]
            assert list(p2cp._hiveplot.hive_plot_edges[a1][a2].keys()) == [0], (
                "Only 1 tag of data here"
            )
            assert np.array_equal(
                p2cp._hiveplot.hive_plot_edges[a1][a2][0]["ids"], expected_edges
            )
            assert (
                list(p2cp._hiveplot.hive_plot_edges[a1][a2][0]["edge_kwargs"]) == []
            ), "Should not have any edge kwargs for this tag"

            # should only do the one direction
            assert a1 not in p2cp._hiveplot.hive_plot_edges[a2], (
                "This direction of edges shouldn't be constructed"
            )

        # add subset of data, check tags, edges, keys, kwargs, etc.
        new_tag = 1e6
        edge_subset = [2, 5, 10, 1, 16, 4]
        new_expected_edges = np.c_[edge_subset, edge_subset]
        p2cp.build_edges(indices=edge_subset, tag=new_tag, color="magenta")

        assert p2cp.tags == [0, new_tag]
        # should have 1 direction "loop" of data around axes
        for i, _ in enumerate(cols):
            a1 = cols[i]
            a2 = cols[(i + 1) % len(cols)]
            assert sorted(p2cp._hiveplot.hive_plot_edges[a1][a2].keys()) == [
                0,
                new_tag,
            ], "Should have 2 tags of data here"
            # old edges, kwargs should be unaffected
            assert np.array_equal(
                p2cp._hiveplot.hive_plot_edges[a1][a2][0]["ids"], expected_edges
            )
            assert (
                list(p2cp._hiveplot.hive_plot_edges[a1][a2][0]["edge_kwargs"]) == []
            ), "Should not have any edge kwargs for this tag"
            assert np.array_equal(
                p2cp._hiveplot.hive_plot_edges[a1][a2][new_tag]["ids"],
                new_expected_edges,
            )
            assert list(
                p2cp._hiveplot.hive_plot_edges[a1][a2][new_tag]["edge_kwargs"]
            ) == ["color"], "Should not just color edge kwargs for this tag"
            assert (
                p2cp._hiveplot.hive_plot_edges[a1][a2][new_tag]["edge_kwargs"]["color"]
                == "magenta"
            )

            # should still only have the edges in one direction
            assert a1 not in p2cp._hiveplot.hive_plot_edges[a2], (
                "This direction of edges shouldn't be constructed"
            )

        # make sure the next auto-generated tag is 1
        p2cp.build_edges()
        # should have 1 direction "loop" of data around axes
        for i, _ in enumerate(cols):
            a1 = cols[i]
            a2 = cols[(i + 1) % len(cols)]
            assert sorted(p2cp._hiveplot.hive_plot_edges[a1][a2].keys()) == [
                0,
                1,
                new_tag,
            ], "Should have 2 tags of data here"
            # 0, 1 edges, kwargs should be unaffected (since constructed the same way)
            for t in [0, 1]:
                assert np.array_equal(
                    p2cp._hiveplot.hive_plot_edges[a1][a2][t]["ids"], expected_edges
                )
                assert (
                    list(p2cp._hiveplot.hive_plot_edges[a1][a2][t]["edge_kwargs"]) == []
                ), "Should not have any edge kwargs for this tag"
            assert np.array_equal(
                p2cp._hiveplot.hive_plot_edges[a1][a2][new_tag]["ids"],
                new_expected_edges,
            )
            assert list(
                p2cp._hiveplot.hive_plot_edges[a1][a2][new_tag]["edge_kwargs"]
            ) == ["color"], "Should not just color edge kwargs for this tag"
            assert (
                p2cp._hiveplot.hive_plot_edges[a1][a2][new_tag]["edge_kwargs"]["color"]
                == "magenta"
            )

            # should still only have the edges in one direction
            assert a1 not in p2cp._hiveplot.hive_plot_edges[a2], (
                "This direction of edges shouldn't be constructed"
            )

    def test_add_edge_kwargs(self) -> None:
        """
        Make sure ``hiveplotlib.P2CP.add_edge_kwargs()`` behaves as expected.
        """
        p2cp = P2CP(data=self.data)
        cols = ["a", "b", "c"]
        p2cp.set_axes(columns=cols)

        try:
            p2cp.add_edge_kwargs(color="blue")
            pytest.fail("Should've triggered error because no tags exist yet.")
        except AssertionError:
            assert True

        p2cp.build_edges()

        for i, _ in enumerate(cols):
            a1 = cols[i]
            a2 = cols[(i + 1) % len(cols)]
            assert (
                list(p2cp._hiveplot.hive_plot_edges[a1][a2][0]["edge_kwargs"]) == []
            ), "Should not have any edge kwargs for this tag"

        p2cp.add_edge_kwargs(color="blue")
        for i, _ in enumerate(cols):
            a1 = cols[i]
            a2 = cols[(i + 1) % len(cols)]
            assert list(p2cp._hiveplot.hive_plot_edges[a1][a2][0]["edge_kwargs"]) == [
                "color"
            ], "Should have just color edge kwargs for this tag"
            assert (
                p2cp._hiveplot.hive_plot_edges[a1][a2][0]["edge_kwargs"]["color"]
                == "blue"
            )

        new_tag = 1e6
        edge_subset = [2, 5, 10, 1, 16, 4]
        p2cp.build_edges(indices=edge_subset, tag=new_tag)

        try:
            p2cp.add_edge_kwargs(ls="dotted")
            pytest.fail(
                "Should've triggered error because need to specify tag when 2 exist."
            )
        except AssertionError:
            assert True

        p2cp.add_edge_kwargs(tag=new_tag, lw=2)
        for i, _ in enumerate(cols):
            a1 = cols[i]
            a2 = cols[(i + 1) % len(cols)]
            assert list(p2cp._hiveplot.hive_plot_edges[a1][a2][0]["edge_kwargs"]) == [
                "color"
            ], "Should have just color edge kwargs for this tag"
            assert list(
                p2cp._hiveplot.hive_plot_edges[a1][a2][new_tag]["edge_kwargs"]
            ) == ["lw"], "Should have just color edge kwargs for this tag"
            assert (
                p2cp._hiveplot.hive_plot_edges[a1][a2][new_tag]["edge_kwargs"]["lw"]
                == 2
            )

    def test_reset_edges(self) -> None:
        """
        Make sure ``hiveplotlib.P2CP.reset_edges()`` behaves as expected.
        """
        p2cp = P2CP(data=self.data)
        cols = ["a", "b", "c"]
        p2cp.set_axes(columns=cols)

        t0 = p2cp.build_edges()
        t1 = p2cp.build_edges()
        for i, _ in enumerate(cols):
            a1 = cols[i]
            a2 = cols[(i + 1) % len(cols)]
            assert list(p2cp._hiveplot.hive_plot_edges[a1][a2].keys()) == [t0, t1]

        new_p2cp = p2cp.copy()
        new_p2cp.reset_edges()
        assert list(new_p2cp._hiveplot.hive_plot_edges.keys()) == [], (
            "Should've killed everything"
        )

        new_p2cp = p2cp.copy()
        new_p2cp.reset_edges(tag=t0)
        for i, _ in enumerate(cols):
            a1 = cols[i]
            a2 = cols[(i + 1) % len(cols)]
            assert list(new_p2cp._hiveplot.hive_plot_edges[a1][a2].keys()) == [t1]

    @pytest.mark.parametrize("num_groups", [1, 2])
    def test_to_json(self, num_groups: int) -> None:
        """
        Make sure ``P2CP.to_json()`` returns all the relevant information without error with one or two groups of data.

        :param num_groups: number of groups to break the two contrived data points into (1 or 2).
        """
        # work from simple toy example with only 2 data points we will place on vmin and vmax of each axis
        df = pd.DataFrame(np.repeat([0, 1], 3).reshape(-1, 3), columns=["X", "Y", "Z"])

        extra_info = {}
        if num_groups == 1:
            pass
        elif num_groups == 2:
            df["Label"] = [1, 2]
            extra_info["split_on"] = "Label"
            extra_info["indices_list_kwargs"] = [
                {"alpha": i / 10} for i in df.Label.to_numpy()
            ]
        else:
            raise NotImplementedError(
                "Test written only for 1 or 2 groups. Be sure to revise entire test if expanding "
                "the scope of test."
            )

        p2cp = p2cp_n_axes(
            data=df,
            vmins=[0] * 3,
            vmaxes=[1] * 3,
            all_edge_kwargs={"color": "black"},
            **extra_info,
        )

        p2cp_json = p2cp.to_json()

        # check our outputs
        p2cp_dict = json.loads(p2cp_json)

        assert sorted(p2cp_dict.keys()) == ["axes", "edges"]

        for axis in p2cp_dict["axes"]:
            # check axes locations preserved
            assert (
                list(p2cp.axes[axis]["axis"].start) == p2cp_dict["axes"][axis]["start"]
            )
            assert list(p2cp.axes[axis]["axis"].end) == p2cp_dict["axes"][axis]["end"]

            # check node locations in Cartesian space preserved
            for val in ["x", "y"]:
                json_values = p2cp_dict["axes"][axis]["points"][val]
                # 2 points => 2 values defined on each axis
                assert len(json_values) == 2
                # should be same as what's in the p2cp
                assert (
                    json_values[0] == p2cp._hiveplot.axes[axis].node_placements[val][0]
                )

        # check the 2 loops we drew

        # tags depend on whether we split the data
        if num_groups == 1:
            assert sorted(p2cp_dict["edges"].keys()) == ["0"]
            for a0 in p2cp_dict["edges"]["0"]["curves"]:
                for a1 in p2cp_dict["edges"]["0"]["curves"][a0]:
                    arr = np.array(p2cp_dict["edges"]["0"]["curves"][a0][a1]).astype(
                        float
                    )

                    assert np.allclose(
                        arr,
                        p2cp._hiveplot.hive_plot_edges[a0][a1][0]["curves"],
                        rtol=0.0,
                        atol=0.0,
                        equal_nan=True,
                    )

                    assert arr.shape == (202, 2)

                    # first point traverses the vmin values of each axis
                    assert list(arr[0, :]) == list(p2cp._hiveplot.axes[a0].start)
                    assert list(arr[99, :]) == list(p2cp._hiveplot.axes[a1].start)

                    # second point traverses the vmax values of each axis
                    assert list(arr[101, :]) == list(p2cp._hiveplot.axes[a0].end)
                    assert list(arr[-2, :]) == list(p2cp._hiveplot.axes[a1].end)

            # check the edge kwargs
            assert p2cp_dict["edges"]["0"]["edge_kwargs"] == {"color": "black"}

        elif num_groups == 2:
            expected_tags = ["1", "2"]
            assert sorted(p2cp_dict["edges"].keys()) == expected_tags

            for tag in expected_tags:
                for a0 in p2cp_dict["edges"][tag]["curves"]:
                    for a1 in p2cp_dict["edges"][tag]["curves"][a0]:
                        arr = np.array(
                            p2cp_dict["edges"][tag]["curves"][a0][a1]
                        ).astype(float)

                        assert np.allclose(
                            arr,
                            p2cp._hiveplot.hive_plot_edges[a0][a1][int(tag)]["curves"],
                            rtol=0.0,
                            atol=0.0,
                            equal_nan=True,
                        )

                        assert arr.shape == (101, 2)

                        if tag == "1":
                            assert list(arr[0, :]) == list(
                                p2cp._hiveplot.axes[a0].start
                            )
                            assert list(arr[-2, :]) == list(
                                p2cp._hiveplot.axes[a1].start
                            )

                        elif tag == "2":
                            assert list(arr[0, :]) == list(p2cp._hiveplot.axes[a0].end)
                            assert list(arr[-2, :]) == list(p2cp._hiveplot.axes[a1].end)

                        # check the edge kwargs
                        assert p2cp_dict["edges"][tag]["edge_kwargs"] == {
                            "color": "black",
                            "alpha": int(tag) / 10,
                        }


class TestP2cpNAxes:
    """
    Tests for ``hiveplotlib.p2cp_n_axes()``.
    """

    def setup_method(self) -> None:
        """
        Perform setup.
        """
        rng = np.random.default_rng(0)

        num_nodes = 50

        # a and b columns move in sync with eventual node id (index), c is random
        data = pd.DataFrame(
            np.c_[
                np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
                np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
                rng.uniform(low=0, high=10, size=num_nodes),
            ],
            columns=["a", "b", "c"],
        )

        self.data = data

    def test_friendly_example(self) -> None:
        """
        Make sure we "play nice" with a straightforward, baseline example.
        """
        p2cp = p2cp_n_axes(
            data=self.data, indices="all", vmins=[None, -1, 10], vmaxes=[None, 0, 11]
        )

        assert p2cp.axes_list == ["a", "b", "c"]

        # check node placements correspond to vmin and vmax values
        assert (
            p2cp._hiveplot.axes["a"].node_placements["rho"].mean()
            > p2cp._hiveplot.axes["c"].node_placements["rho"].mean()
        ), "`a` nodes should span axis, `c` nodes should be all at min"

        assert (
            p2cp._hiveplot.axes["a"].node_placements["rho"].mean()
            < p2cp._hiveplot.axes["b"].node_placements["rho"].mean()
        ), "`a` nodes should span axis, `b` nodes should be all at max"

        assert p2cp._hiveplot.axes["a"].node_placements.shape[0] == self.data.shape[0]

    def test_subset_example(self) -> None:
        """
        Make sure we "play nice" with a subsetting, example (subset of indices and axes).
        """
        subset = 10

        p2cp = p2cp_n_axes(
            data=self.data, indices=list(range(subset)), axes=["a", "c", "c"]
        )

        assert p2cp.axes_list == ["a", "c", "c\nRepeat"]

        assert p2cp._hiveplot.axes["a"].node_placements.shape[0] == self.data.shape[0]

    def test_subset_split_on(self) -> None:
        """
        Make sure we "play nice" with a split on column example.
        """
        subset = 10

        new_column_values = np.repeat(True, self.data.shape[0])
        new_column_values[:subset] = False

        self.data["new_column"] = new_column_values

        p2cp = p2cp_n_axes(data=self.data, split_on="new_column")

        assert p2cp.tags == [False, True]

        assert p2cp._hiveplot.hive_plot_edges["a"]["b"][False]["ids"].shape[0] == subset

        assert (
            p2cp._hiveplot.hive_plot_edges["a"]["b"][True]["ids"].shape[0]
            == self.data.shape[0] - subset
        )

    def test_subset_split_on_list(self) -> None:
        """
        Make sure we "play nice" with a split on example where the ``split_on`` param is provided as an external list.
        """
        subset = 10

        new_column_values = np.repeat(True, self.data.shape[0])
        new_column_values[:subset] = False

        p2cp = p2cp_n_axes(data=self.data, split_on=new_column_values)

        assert p2cp.tags == [False, True]

        assert p2cp._hiveplot.hive_plot_edges["a"]["b"][False]["ids"].shape[0] == subset

        assert (
            p2cp._hiveplot.hive_plot_edges["a"]["b"][True]["ids"].shape[0]
            == self.data.shape[0] - subset
        )

    def test_subset_split_on_list_failure(self) -> None:
        """
        Make sure if we ``split_on`` as an external list that we fail if the len is wrong.
        """
        subset = 10

        new_column_values = np.repeat(True, self.data.shape[0])
        new_column_values[:subset] = False

        try:
            p2cp_n_axes(data=self.data, split_on=new_column_values[:-1])
            pytest.fail(
                "P2CP creation should have failed due to wrong len split list, but succeeded for some reason."
            )
        except AssertionError:
            assert True

    def test_all_edge_kwargs(self) -> None:
        """
        Make sure the ``all_edge_kwargs`` parameter behaves as expected.
        """
        p2cp = p2cp_n_axes(data=self.data)

        # shouldn't have any dotted line styles
        hp = p2cp._hiveplot.copy()
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                if "ls" in hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]:
                    assert (
                        hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]["ls"] != "dotted"
                    ), "No lines should be dotted here"

        p2cp = p2cp_n_axes(data=self.data, all_edge_kwargs={"ls": "dotted"})

        # EVERY line should have dotted style
        hp = p2cp._hiveplot.copy()
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                assert hp.hive_plot_edges[a0][a1][0]["edge_kwargs"]["ls"] == "dotted", (
                    "All lines should be dotted here"
                )

    def test_indices_list_kwargs(self) -> None:
        """
        Make sure the ``indices_list_kwargs`` parameter behaves as expected.
        """
        # split into 2 sets of indices
        split = 10
        p2cp = p2cp_n_axes(
            data=self.data,
            indices=[np.arange(split), np.arange(split, self.data.shape[0])],
        )

        # shouldn't have any dotted line styles
        hp = p2cp._hiveplot.copy()
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                for tag in hp.hive_plot_edges[a0][a1]:
                    if "ls" in hp.hive_plot_edges[a0][a1][tag]["edge_kwargs"]:
                        assert (
                            hp.hive_plot_edges[a0][a1][tag]["edge_kwargs"]["ls"]
                            != "dotted"
                        ), "No lines should be dotted here"

        with pytest.warns() as record:
            p2cp = p2cp_n_axes(
                data=self.data,
                indices=[np.arange(split), np.arange(split, self.data.shape[0])],
                all_edge_kwargs={"ls": "-"},
                indices_list_kwargs=[None, {"ls": "dotted"}],
            )

        # should trigger a warning for redundant kwargs
        assert len(record) == 1, (
            f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
        )

        # EVERY second tag index line should have dotted style
        hp = p2cp._hiveplot.copy()
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                for tag in hp.hive_plot_edges[a0][a1]:
                    if tag == 1:
                        assert (
                            hp.hive_plot_edges[a0][a1][tag]["edge_kwargs"]["ls"]
                            == "dotted"
                        ), "All lines should be dotted here"
                    elif tag == 0:
                        assert (
                            hp.hive_plot_edges[a0][a1][tag]["edge_kwargs"]["ls"] == "-"
                        ), "No lines should be dotted here"
                    else:
                        raise NotImplementedError

        # check behavior when splits are done using `split_on`
        new_column_values = np.repeat(True, self.data.shape[0])
        new_column_values[:split] = False

        self.data["new_column"] = new_column_values

        # make sure we get a warning about redundant kwargs
        with pytest.warns() as record:
            p2cp = p2cp_n_axes(
                data=self.data,
                split_on="new_column",
                all_edge_kwargs={"ls": "-"},
                indices_list_kwargs=[{}, {"ls": "dotted"}],
            )
        assert len(record) == 1, (
            f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
        )

        # EVERY second tag index line should have dotted style
        hp = p2cp._hiveplot.copy()
        for a0 in hp.hive_plot_edges:
            for a1 in hp.hive_plot_edges[a0]:
                # note tags are bools here by construction
                for tag in hp.hive_plot_edges[a0][a1]:
                    if tag:
                        assert (
                            hp.hive_plot_edges[a0][a1][tag]["edge_kwargs"]["ls"]
                            == "dotted"
                        ), "All lines should be dotted here"
                    elif not tag:
                        assert (
                            hp.hive_plot_edges[a0][a1][tag]["edge_kwargs"]["ls"] == "-"
                        ), "No lines should be dotted here"
                    else:
                        raise NotImplementedError

    def test_indices_list_kwargs_skip(self) -> None:
        """
        Make sure the ``indices_list_kwargs`` parameter yells when specified when not allowed.
        """
        # only 1 set of indices, so should yell at providing multiple sets of index kwargs
        try:
            p2cp_n_axes(data=self.data, indices_list_kwargs=[{} for _ in range(2)])
            pytest.fail(
                "P2CP should have failed to initialize due to wrong numbert of index kwargs, "
                "but succeeded for some reason."
            )
        except AssertionError:
            assert True

    @pytest.mark.parametrize("wrong_len_kwargs", [[{}], [{}, {}, {}]])
    def test_indices_list_kwargs_len(self, wrong_len_kwargs: list) -> None:
        """
        Make sure the ``indices_list_kwargs`` parameter yells when specified with the wrong length.

        :param wrong_len_kwargs: below code is hardcoded for 2 set splits, so we check wrong ``indices_list_kwargs``
            using 1 and 3 sets of kwargs
        """
        # split into 2 sets of indices
        split = 10
        try:
            p2cp_n_axes(
                data=self.data,
                indices=[np.arange(split), np.arange(split, self.data.shape[0])],
                indices_list_kwargs=wrong_len_kwargs,
            )
            pytest.fail(
                "P2CP should have failed to initialize due to wrong length kwarg input, but succeeded for some reason."
            )
        except AssertionError:
            assert True

        # check behavior when splits are done using `split_on`
        new_column_values = np.repeat(True, self.data.shape[0])
        new_column_values[:split] = False

        self.data["new_column"] = new_column_values

        try:
            p2cp_n_axes(
                data=self.data,
                split_on="new_column",
                indices_list_kwargs=wrong_len_kwargs,
            )
            pytest.fail(
                "P2CP should have failed to initialize due to wrong length kwarg input, but succeeded for some reason."
            )
        except AssertionError:
            assert True
