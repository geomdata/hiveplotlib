# utils_p2cp_test.py

"""
Tests for static helper methods in ``hiveplotlib.p2cp``.
"""

import numpy as np
import pandas as pd
import pytest

from hiveplotlib.p2cp import indices_for_unique_values, split_df_on_variable

pytestmark = pytest.mark.unmarked


def test_indices_for_unique_values() -> None:
    """
    Test behavior of ``hiveplotlib.utils_p2cp.indices_for_unique_values()``.
    """
    rng = np.random.default_rng(0)

    num_nodes = 50

    subset = 10

    # a and b columns move in sync with eventual node id (index), c is random
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            rng.uniform(low=0, high=10, size=num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    new_column_values = np.repeat(True, data.shape[0])
    new_column_values[:subset] = False

    data["new_column"] = new_column_values

    split_information = indices_for_unique_values(df=data, column="new_column")

    expected_keys = [False, True]

    split_keys = list(split_information.keys())
    split_keys.sort()

    assert split_keys == expected_keys

    assert split_information[False].size == subset
    assert split_information[True].size == data.shape[0] - subset


def test_split_df_on_variable_unique_cutoffs() -> None:
    """
    Test behavior of ``hiveplotlib.utils_p2cp.split_df_on_variable()`` with unique cutoffs specified.
    """
    rng = np.random.default_rng(0)

    num_nodes = 50

    # a and b columns move in sync with eventual node id (index), c is random
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.arange(num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    cutoffs = [2, 5]
    labels = ["small", "med", "large"]
    split_information = split_df_on_variable(
        df=data, column="c", cutoffs=cutoffs, labels=labels
    )

    assert sorted(labels) == sorted(np.unique(split_information))

    assert split_information.size == data.shape[0]

    assert list(np.unique(split_information[: cutoffs[0] + 1])) == ["small"]
    assert list(np.unique(split_information[cutoffs[0] + 1 : cutoffs[1] + 1])) == [
        "med"
    ]
    assert list(np.unique(split_information[cutoffs[-1] + 1 :])) == ["large"]


def test_split_df_on_variable_quantiles() -> None:
    """
    Test behavior of ``hiveplotlib.utils_p2cp.split_df_on_variable()`` with quantile cutoffs specified.
    """
    rng = np.random.default_rng(0)

    num_nodes = 50

    # a and b columns move in sync with eventual node id (index), c is random
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.arange(num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    cutoffs = 5
    labels = np.arange(cutoffs)
    split_information = split_df_on_variable(
        df=data, column="c", cutoffs=cutoffs, labels=labels
    )

    assert sorted(labels) == sorted(np.unique(split_information))

    assert split_information.size == data.shape[0]

    # should have evenly split in order
    expected_partition = np.split(split_information, cutoffs)
    for i, arr in enumerate(expected_partition):
        assert list(np.unique(arr)) == [i]
