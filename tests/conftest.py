"""
Fixtures available in all test suites.
"""

from typing import Tuple

import numpy as np
import pandas as pd
import pytest

from hiveplotlib import P2CP, Axis, BaseHivePlot, Node, p2cp_n_axes
from hiveplotlib.node import node_collection_from_node_list


@pytest.fixture
def three_axis_basehiveplot_example() -> Tuple[BaseHivePlot, np.ndarray]:
    """
    3 axis example ``BaseHivePlot`` and simulated edges for use in multiple visualization tests later.

    :return: ``hiveplotlib.BaseHivePlot`` instance, ``(n, 2)`` array  of edges (ids).
    """
    hp = BaseHivePlot()

    # build nodes
    node0 = Node(unique_id="0", data={"a": 1, "b": 2, "c": 3})
    node1 = Node(unique_id="1", data={"a": 2, "b": 3, "c": 4})
    node2 = Node(unique_id="2", data={"a": 3, "b": 4, "c": 5})
    node3 = Node(unique_id="3", data={"a": 4, "b": 5, "c": 6})
    node4 = Node(unique_id="4", data={"a": 5, "b": 6, "c": 7})
    node5 = Node(unique_id="5", data={"a": 6, "b": 7, "c": 8})

    nodes = [node0, node1, node2, node3, node4, node5]

    hp.add_nodes(node_collection_from_node_list(nodes))

    # build axes
    axis0 = Axis(axis_id="A", start=1, end=5, angle=0)
    axis1 = Axis(axis_id="B", start=1, end=5, angle=120)
    axis2 = Axis(axis_id="C", start=1, end=5, angle=240)

    axes = [axis0, axis1, axis2]

    hp.add_axes(axes)

    # dummy of from, to pairs
    rng = np.random.default_rng(0)
    edges = rng.choice(np.arange(5).astype(str), size=100).reshape(-1, 2)

    return hp, edges


@pytest.fixture
def three_axis_p2cp_example() -> P2CP:
    """
    3 axis example ``P2CP`` for use in multiple visualization tests later.

    :return: ``hiveplotlib.P2CP`` instance.
    """
    rng = np.random.default_rng(0)

    num_nodes = 50

    # a and b columns move in sync with eventual node id (index), c is random
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            rng.uniform(low=0, high=10, size=num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    return p2cp_n_axes(data=data)
