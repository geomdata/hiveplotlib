# viz_bokeh_test.py

"""
Tests for ``hiveplotlib.viz.bokeh.py``, the ``bokeh``-backend visualization functions.
"""

import traceback
import warnings
from typing import Tuple, Union

import numpy as np
import pandas as pd
import pytest

from hiveplotlib import P2CP, BaseHivePlot, Node
from hiveplotlib.datasets import example_hive_plot
from hiveplotlib.exceptions import InvalidHoverVariableError
from hiveplotlib.node import subset_node_collection_by_unique_ids

pytestmark = pytest.mark.bokeh


@pytest.mark.parametrize(
    "viz_function", ["axes_viz", "label_axes", "node_viz", "edge_viz", "hive_plot_viz"]
)
def test_viz_functions_run_hiveplot(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray], viz_function: str
) -> None:
    """
    Make sure the baseline viz functions can run without error when run for hive plots.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    import hiveplotlib.viz.bokeh

    hp, edges = three_axis_basehiveplot_example

    # hardcoded partition of nodes assignment, placed on axes
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["0", "1"]),
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["2", "3"]),
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="C",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["4", "5"]),
        sorting_feature_to_use="c",
        vmin=vmin,
        vmax=vmax,
    )

    # connect some edges
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="C")
    # throw in an edge kwarg for coverage
    hp.connect_axes(edges=edges, axis_id_1="C", axis_id_2="B", color="blue")

    try:
        extra_kwargs = {}
        if viz_function != "label_axes":
            extra_kwargs["hover"] = False  # test hovering separately later
        viz_function = getattr(hiveplotlib.viz.bokeh, viz_function)
        viz_function(hp, **extra_kwargs)
        viz_function(
            hp,
            axes_off=False,
            **extra_kwargs,
        )  # to get full testing coverage, make sure this runs too
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail(f"`{viz_function}` should have run, but failed for some reason.")


def test_fill_in_empty_edge_kwargs(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray],
) -> None:
    """
    Make sure entirely unspecified edge kwargs doesn't break ``hiveplotlib.viz.edge_viz()``.
    """
    from hiveplotlib.viz.bokeh import edge_viz

    hp, edges = three_axis_basehiveplot_example

    # hardcoded partition of nodes assignment, placed on axes
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["0", "1"]),
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["2", "3"]),
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="C",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["4", "5"]),
        sorting_feature_to_use="c",
        vmin=vmin,
        vmax=vmax,
    )

    # connect some edges without ever touching kwargs
    #  (`HivePlot.connect_axes()` will add empty kwargs if none included)
    tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")
    hp.add_edge_curves_between_axes(axis_id_1="A", axis_id_2="B", tag=tag)

    try:
        edge_viz(instance=hp)
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail("`edge_viz` should have run, but failed for some reason.")


@pytest.mark.parametrize("viz_function", ["axes_viz", "label_axes"])
def test_warnings_no_axes_hive_plot(viz_function: str) -> None:
    """
    Make sure the baseline viz functions hits a warning when we are missing things when run for hive plots.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    import hiveplotlib.viz.bokeh

    viz_function = getattr(hiveplotlib.viz.bokeh, viz_function)
    hp = BaseHivePlot()

    with pytest.warns() as record:
        viz_function(hp)

    # confirm we get a single warning
    assert len(record) == 1, (
        f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"
    )


@pytest.mark.parametrize("viz_function", ["node_viz", "edge_viz", "hive_plot_viz"])
def test_warnings_missing_content_hive_plot(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray], viz_function: str
) -> None:
    """
    Make sure the baseline viz functions hits a warning when we are missing things when run for hive plots.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    import hiveplotlib.viz.bokeh

    viz_function = getattr(hiveplotlib.viz.bokeh, viz_function)
    hp, edges = three_axis_basehiveplot_example

    # only place nodes on 2 of the 3 axes, explicitly excluding one axis to get a warning
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["0", "1"]),
        sorting_feature_to_use="a",
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["2", "3"]),
        sorting_feature_to_use="b",
    )

    with pytest.warns() as record:
        viz_function(hp)

    # confirm we get a single warning
    if viz_function == hiveplotlib.viz.bokeh.hive_plot_viz:
        assert len(record) == 2, (
            f"Expected 2 warnings for no edges AND no nodes, got:\n{[i.message.args for i in record]}"
        )
    else:
        assert len(record) == 1, (
            f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"
        )


def test_warnings_repeat_edge_kwarg(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray],
) -> None:
    """
    Make sure ``hiveplotlib.viz.edge_viz()`` hits a warning when including a kwarg redundant to existing kwargs.

    (To warn the user that their kwarg will be ignored in deference to the existing kwarg.)
    """
    from hiveplotlib.viz.bokeh import edge_viz

    hp, edges = three_axis_basehiveplot_example
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
    hp.connect_axes(edges=edges, axis_id_1="C", axis_id_2="B")
    hp.connect_axes(
        edges=edges, axis_id_1="A", axis_id_2="C", a2_to_a1=False, line_width=1
    )
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="C", a1_to_a2=False)

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        edge_viz(instance=hp)

    with pytest.warns() as record:
        edge_viz(instance=hp, line_width=2)

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )


@pytest.mark.parametrize(
    "viz_function", ["axes_viz", "node_viz", "edge_viz", "p2cp_viz"]
)
def test_viz_functions_run_p2cp(
    three_axis_p2cp_example: P2CP, viz_function: str
) -> None:
    """
    Make sure the baseline viz functions can run without error when run for p2cps.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    import hiveplotlib.viz.bokeh

    viz_function = getattr(hiveplotlib.viz.bokeh, viz_function)
    p2cp = three_axis_p2cp_example

    try:
        with pytest.warns() as record:
            viz_function(
                p2cp, hover=True
            )  # make sure we warn as expected if inappropriately adding hover info here
        assert len(record) == 1, (
            f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
        )
        assert "Hover info not yet supported for P2CPs" in record[0].message.args[0]
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail(f"`{viz_function}` should have run, but failed for some reason.")


@pytest.mark.parametrize("tags", [None, "two", ["two"], ["one", "three"]])
def test_p2cp_legend(
    three_axis_p2cp_example: P2CP, tags: Union[str, list, None]
) -> None:
    """
    Make sure the p2cp viz functions can run without error when run for p2cps.

    :param tags: which tags of data to add to the p2cp.
    """
    from hiveplotlib.viz.bokeh import p2cp_legend, p2cp_viz

    p2cp = three_axis_p2cp_example
    p2cp.build_edges(tag="one")
    p2cp.build_edges(tag="two")
    p2cp.build_edges(tag="three", line_width=0.5)
    fig = p2cp_viz(p2cp, tags=tags)
    try:
        p2cp_legend(p2cp, fig=fig, tags=tags)
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail("`p2cp_legend` should have run, but failed for some reason.")


def test_warnings_repeat_edge_kwarg_p2cp(three_axis_p2cp_example: P2CP) -> None:
    """
    Make sure ``hiveplotlib.viz.edge_viz()`` hits a warning when including a kwarg redundant to existing kwargs.

    (To warn the user that their kwarg will be ignored in deference to the existing kwarg.)
    """
    from hiveplotlib.viz.bokeh import edge_viz

    p2cp = three_axis_p2cp_example
    p2cp.add_edge_kwargs(color="blue")

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        edge_viz(
            p2cp,
            hover=False,  # no need to test hover here
        )

    with pytest.warns() as record:
        edge_viz(
            instance=p2cp,
            color="red",
            hover=False,  # no need to test hover here
        )

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )


@pytest.mark.parametrize(
    "viz_function", ["axes_viz", "label_axes", "node_viz", "edge_viz"]
)
def test_expected_errors_wrong_types(viz_function: str) -> None:
    """
    Viz functions should yell if tried on non-``P2CP`` or ``HivePlot`` instances.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    import hiveplotlib.viz.bokeh

    viz_function = getattr(hiveplotlib.viz.bokeh, viz_function)
    node = Node(unique_id=10)

    try:
        viz_function(node)
        pytest.fail(
            f"`{viz_function}` should have failed to run on an invalid input, but succeeded for some reason."
        )
    # supposed to fail
    except NotImplementedError:
        assert True


def test_warnings_node_viz_p2cp() -> None:
    """
    Make sure ``hiveplotlib.viz.node_viz()`` hits a single warning when nodes are unspecified.

    (If done correctly, this avoids sending a P2CP user a hive plot-specific internal warning that shouldn't
    matter. Even worse, if done wrong, the user would get the same warning *per axis* hence the check for a *single*
    warning.)
    """
    from hiveplotlib.viz.bokeh import node_viz

    rng = np.random.default_rng(0)

    num_nodes = 50

    # a and b columns move in sync with eventual node id (index), c is random
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            rng.uniform(low=0, high=10, size=num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    p2cp = P2CP(data=data)

    with pytest.warns() as record:
        node_viz(
            instance=p2cp,
            hover=False,
        )

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )

    # set the axes, then should be fine
    p2cp.set_axes(list(data.columns.values))

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        node_viz(
            p2cp,
            hover=False,
        )


@pytest.mark.parametrize("viz_function", ["axes_viz", "label_axes"])
def test_warnings_no_axes_p2cp(viz_function: str) -> None:
    """
    Make sure the baseline viz functions hits a warning when we are missing things when run for p2cps.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    import hiveplotlib.viz.bokeh

    viz_function = getattr(hiveplotlib.viz.bokeh, viz_function)
    p2cp = P2CP()

    with pytest.warns() as record:
        viz_function(p2cp)

    # confirm we get a single warning
    assert len(record) == 1, (
        f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"
    )


def test_label_axes_alignments() -> None:
    """
    Make sure ``hiveplotlib.viz.label_axes()`` does a "reasonable" alignment of axes labels for many angles.

    Note, this test exists primarily for two reasons: 1) test coverage and 2) a quick reference to make a scalable,
    multi-axis figure to observe all the alignments.
    """
    from hiveplotlib.viz.bokeh import axes_viz

    rng = np.random.default_rng(0)

    num_nodes = 50

    # number of axes on final p2cp
    num_axes = 26

    # axes names will be strings of integers 0, 1, 2, ...
    #  to make names longer (to see how alignment changes), repeat each string in each title this many times
    num_repeats_per_title = 5

    np_data = [np.sort(rng.uniform(low=0, high=10, size=num_nodes))] * num_axes
    data = pd.DataFrame(
        np.column_stack(np_data),
        columns=[i * num_repeats_per_title for i in np.arange(num_axes).astype(str)],
    )

    p2cp = P2CP(data=data)

    p2cp.set_axes(data.columns.values)

    try:
        axes_viz(
            p2cp,
            hover=False,
        )
        assert True
    except:  # noqa: E722
        pytest.fail("`axes_viz` should have run, but failed for some reason.")


def test_warnings_edge_viz_p2cp() -> None:
    """
    Make sure ``hiveplotlib.viz.edge_viz()`` hits a single warning when nodes are unspecified.

    (If done correctly, this avoids sending a P2CP user a hive plot-specific internal warning that shouldn't
    matter. Even worse, if done wrong, the user would get the same warning *per axis pair* hence the check for a
    *single* warning.)
    """
    from hiveplotlib.viz.bokeh import edge_viz

    rng = np.random.default_rng(0)

    num_nodes = 50

    # a and b columns move in sync with eventual node id (index), c is random
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            rng.uniform(low=0, high=10, size=num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    p2cp = P2CP(data=data)
    p2cp.set_axes(list(data.columns.values))

    with pytest.warns() as record:
        edge_viz(
            instance=p2cp,
            hover=False,
        )

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )

    # build the edges, then should be fine
    p2cp.build_edges()

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        edge_viz(
            p2cp,
            hover=False,
        )


def test_hiveplot_viz_backend_bokeh() -> None:
    """
    Test that we can call the viz backend set as ``"bokeh"`` on a ``HivePlot`` instance.
    """
    hp = example_hive_plot(
        backend="bokeh",
    )
    hp.plot()
    assert True


def test_plot_empty_hive_plot() -> None:
    """
    Test outputs when plotting an empty (no axes) ``HivePlot`` instance.
    """
    hp = example_hive_plot(
        backend="bokeh",
    )
    # kill all existing axes in hive plot (thus killing all edges and nodes too)
    hp.set_partition(
        "partition_0",
        sorting_variables="low",
        build_hive_plot=False,
    )

    with pytest.warns() as record:
        out = hp.plot()
    assert len(record) == 4, (
        "Expected only 4 warnings ('intermediate changes', no axes, no edges, no nodes), "
        f"got warnings of:\n{[i.message.args for i in record]}"
    )
    for i in record:
        assert (
            "Run the `build_hive_plot()` method on your `HivePlot` instance"
            in i.message.args[0]
            or "`HivePlot.build_hive_plot()`" in i.message.args[0]
        ), f"{i.message.args}"

    from bokeh.plotting import figure

    assert isinstance(out, figure)


@pytest.mark.parametrize("update_node_viz_kwargs", [True, False])
def test_plot_custom_node_kwargs_from_data(update_node_viz_kwargs: bool) -> None:
    """
    Test plotting a hive plot where we update the node viz kwargs with data and non-data values.

    This test is mainly for coverage and a reference for making a highly-interpretable visual example of changing node
    kwargs.

    :param update_node_viz_kwargs: whether to update the node viz kwargs via ``HivePlot.update_node_viz_kwargs()``
        or to call the node kwarg update via the ``node_kwargs`` parameter in ``HivePlot.plot()``.
    """
    # from bokeh.models import ColorBar
    from bokeh.models import Scatter
    from bokeh.transform import linear_cmap

    hp = example_hive_plot(
        repeat_axes=True,
        backend="bokeh",
    )
    hp.nodes.data["size"] = hp.nodes.data["low"].to_numpy() * 2

    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()

    # create a color mapper
    mapper = linear_cmap(
        field_name="low",
        palette="Magma256",
        low=0,
        high=10,
    )

    node_kwargs = {
        "line_color": "black",
        "fill_color": mapper,  # based on data, but must be created above
        "size": "size",  # reference to "size" variable in node data
    }

    if update_node_viz_kwargs:
        hp.update_node_viz_kwargs(**node_kwargs)

        fig = hp.plot(
            # fig_kwargs={"width": 650, "height": 600},  # when including color bar, make wider to maintain 1:1 aspect
        )
    else:
        fig = hp.plot(node_kwargs=node_kwargs)

    # add a color bar
    # color_bar = ColorBar(color_mapper=mapper['transform'], width=8, title="Node Variable 'Low'")
    # fig.add_layout(color_bar, 'right')

    scatter_glyphs = [
        glyph.glyph for glyph in fig.renderers if isinstance(glyph.glyph, Scatter)
    ]
    for scatter_glyph in scatter_glyphs:
        assert scatter_glyph.size == "size", "Should have variable ref 'size' for size."
        color = scatter_glyph.fill_color.transform
        assert color.low == mapper.transform.low
        assert color.high == mapper.transform.high


@pytest.mark.parametrize("update_edge_viz_kwargs", [True, False])
def test_plot_custom_edge_kwargs_from_data(update_edge_viz_kwargs: bool) -> None:
    """
    Test plotting a hive plot where we update the edge viz kwargs with data and non-data values.

    This test is mainly for coverage and a reference for making a highly-interpretable visual example of changing edge
    kwargs.

    :param update_edge_viz_kwargs: whether to update the edge viz kwargs via ``HivePlot.update_edge_viz_kwargs()``
        or to call the edge kwarg update via the ``edge_kwargs`` parameter in ``HivePlot.plot()``.
    """
    # from bokeh.models import ColorBar
    from bokeh.models import MultiLine
    from bokeh.transform import linear_cmap

    hp = example_hive_plot(
        repeat_axes=True,
        backend="bokeh",
    )
    hp.edges.data["linewidth"] = hp.edges.data["low"] / 3

    # propagate extra edge data changes through to data on axes
    hp.build_hive_plot()

    # create a color mapper
    mapper = linear_cmap(
        field_name="low",
        palette="Cividis256",
        low=0,
        high=10,
    )

    edge_kwargs = {
        "line_color": mapper,  # based on data, but must be created above
        "line_width": "linewidth",  # reference to "linewidth" variable in edge data
    }

    if update_edge_viz_kwargs:
        hp.update_edge_viz_kwargs(**edge_kwargs)

        fig = hp.plot(
            # fig_kwargs={"width": 650, "height": 600},  # when including color bar, make wider to maintain 1:1 aspect
        )
    else:
        fig = hp.plot(**edge_kwargs)

    # add a color bar
    # color_bar = ColorBar(color_mapper=mapper['transform'], width=8, title="Edge Variable 'Low'")
    # fig.add_layout(color_bar, 'right')

    multi_line_glyphs = [
        glyph.glyph for glyph in fig.renderers if isinstance(glyph.glyph, MultiLine)
    ]
    for multi_line_glyph in multi_line_glyphs:
        assert multi_line_glyph.line_width == "linewidth", (
            "Should have variable ref 'linewidth' for line_width parameter."
        )
        color = multi_line_glyph.line_color.transform
        assert color.low == mapper.transform.low
        assert color.high == mapper.transform.high


def test_plotting_edge_kwarg_hierarchy_overwrite_data_kwargs() -> None:
    """
    Test that plotting kwargs from various edge kwargs play nice together.

    Specifically, test the interaction of an ``update_edges()`` call and the ``edge_viz_kwargs`` attribute edge kwargs,
    with overwriting of edge data-based kwargs.
    """
    from bokeh.models import MultiLine
    from bokeh.transform import linear_cmap

    hp = example_hive_plot(repeat_axes=True, backend="bokeh")
    hp.edges.data["linewidth"] = hp.edges.data["low"] / 3
    hp.edges.data["color"] = hp.edges.data["low"]

    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()

    # create a color mapper
    mapper = linear_cmap(
        field_name="low",
        palette="Cividis256",
        low=0,
        high=10,
    )

    edge_kwargs = {
        "line_color": mapper,  # based on data, but must be created above
        "line_width": "linewidth",  # reference to "linewidth" variable in edge data
    }

    hp.update_edge_viz_kwargs(**edge_kwargs)

    red_kwargs = {"line_color": "red", "line_width": 2}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a1_to_a2=False,
        #     short_arc=False,
        #     control_rho_scale=2.4,
        #     control_angle_shift=30,
        **red_kwargs,
    )
    blue_kwargs = {"line_color": "blue", "line_width": 10}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a2_to_a1=False,
        control_rho_scale=1.2,
        #     control_angle_shift=-30,
        **blue_kwargs,
    )

    fig = hp.plot()

    multi_line_glyphs = [
        glyph.glyph for glyph in fig.renderers if isinstance(glyph.glyph, MultiLine)
    ]
    for multi_line_glyph in multi_line_glyphs:
        if multi_line_glyph.line_width == "linewidth":
            color = multi_line_glyph.line_color.transform
            assert color.low == mapper.transform.low
            assert color.high == mapper.transform.high
        elif multi_line_glyph.line_width == red_kwargs["line_width"]:
            assert multi_line_glyph.line_color == red_kwargs["line_color"]
        elif multi_line_glyph.line_width == blue_kwargs["line_width"]:
            assert multi_line_glyph.line_color == blue_kwargs["line_color"]
        else:
            pytest.fail(
                "Should have had red, blue, or number-colored edges only, "
                f"but got {multi_line_glyph.line_color} color and {multi_line_glyph.line_width} width."
            )


def test_plotting_edge_kwarg_hierarchy_plot_overwrite() -> None:
    """
    Test that plotting kwargs from various edge kwargs play nice together.

    Specifically, test the interaction of an ``update_edges()`` call, the ``edge_viz_kwargs`` attribute, and ``plot()``
    edge kwargs.
    """
    from bokeh.models import MultiLine

    hp = example_hive_plot(repeat_axes=True, backend="bokeh")
    hp.edges.data["line_width"] = hp.edges.data["low"] / 3
    hp.edges.data["color"] = hp.edges.data["low"]

    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()

    edge_kwargs = {
        "line_color": "green",
        "line_width": 1,
    }

    hp.update_edge_viz_kwargs(**edge_kwargs)

    red_kwargs = {"line_color": "red", "line_width": 2}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a1_to_a2=False,
        #     short_arc=False,
        #     control_rho_scale=2.4,
        #     control_angle_shift=30,
        **red_kwargs,
    )
    blue_kwargs = {"line_color": "blue", "line_width": 10}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a2_to_a1=False,
        control_rho_scale=1.2,
        #     control_angle_shift=-30,
        **blue_kwargs,
    )

    with pytest.warns() as record:
        fig = hp.plot(line_width=1)

    # confirm we get two warnings
    assert len(record) == 2, (
        f"Expected only 2 warnings, got warnings of:\n{[i.message.args for i in record]}"
    )

    for r in record:
        assert "line_width" in r.message.args[0], (
            "warning should yell about repeated line_width not being overwritten."
        )

    multi_line_glyphs = [
        glyph.glyph for glyph in fig.renderers if isinstance(glyph.glyph, MultiLine)
    ]
    for multi_line_glyph in multi_line_glyphs:
        if multi_line_glyph.line_width == edge_kwargs["line_width"]:
            assert multi_line_glyph.line_color == edge_kwargs["line_color"]
        elif multi_line_glyph.line_width == red_kwargs["line_width"]:
            assert multi_line_glyph.line_color == red_kwargs["line_color"]
        elif multi_line_glyph.line_width == blue_kwargs["line_width"]:
            assert multi_line_glyph.line_color == blue_kwargs["line_color"]
        else:
            pytest.fail(
                f"Should have had red, blue, or green edges only, but got {multi_line_glyph.line_color}"
            )


@pytest.mark.parametrize(
    "hover",
    [
        True,
        False,
        "nodes",
        "edges",
        "axes",
        ["nodes", "edges"],
        ["axes"],
        "axis",
        ["axis", "nodes"],
    ],
)
def test_hive_plot_hover_tools(hover: Union[str, bool, list[str]]) -> None:
    """
    Test plotting hive plot with possible (and impossible) ``"hover"`` settings.

    :param hover: hover setting.
    """
    from bokeh.models import HoverTool

    hp = example_hive_plot(backend="bokeh")

    if hover == "axis" or hover == ["axis", "nodes"]:
        try:
            fig = hp.plot(hover=hover)
            pytest.fail(
                "Should have raised `InvalidHoverVariableError` error (needs to be 'axes' not 'axis')."
            )
        except InvalidHoverVariableError:
            assert True

    else:
        fig = hp.plot(hover=hover)

        hover_tooltips = [i for i in fig.tools if isinstance(i, HoverTool)]

        if hover is False:
            assert len(hover_tooltips) == 0, (
                "Should not have any hover tooltips when `hover=False`"
            )
        elif hover == "nodes":
            assert len(hover_tooltips) == 1, (
                "Should have 1 hover tooltip when `hover='nodes'`"
            )
            assert "Node: " in hover_tooltips[0].tooltips
        elif hover == "edges":
            assert len(hover_tooltips) == 1, (
                "Should have 1 hover tooltip when `hover='edges'`"
            )
            assert "Edge: " in hover_tooltips[0].tooltips
        elif hover == "axes" or hover == ["axes"]:
            assert len(hover_tooltips) == 1, (
                "Should have 1 hover tooltip when `hover='axes'`"
            )
            assert "Axis: " in hover_tooltips[0].tooltips
        elif hover is True:
            assert len(hover_tooltips) == 3, (
                "Should have 3 hover tooltips when `hover=True`"
            )
            for ht in hover_tooltips:
                assert (
                    "Node: " in ht.tooltips
                    or "Edge: " in ht.tooltips
                    or "Axis: " in ht.tooltips
                ), (
                    "Tooltip should either be titled 'Node: ...', 'Edge: ...', or 'Axis: ...',."
                )
        elif hover == ["nodes", "edges"]:
            assert len(hover_tooltips) == 2, (
                "Should have 2 hover tooltips when `hover=['nodes', 'edges']`"
            )
            for ht in hover_tooltips:
                assert "Node: " in ht.tooltips or "Edge: " in ht.tooltips, (
                    "Tooltip should either be titled 'Node: ...' or 'Edge: ...'."
                )
                assert "Axis: " not in ht.tooltips, (
                    "'Axis: ...' tooltip should have been skipped."
                )
        else:
            raise NotImplementedError
