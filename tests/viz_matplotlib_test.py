# viz_matplotlib_test.py

"""
Tests for ``hiveplotlib.viz.__init__.py``, specifically the ``matplotlib``-backend visualization functions.

Note, we would probably eventually want something that actually compares images
(e.g. ``pytest-mpl``), but image comparison tests, particularly when building base images
locally and comparing to images generated in the CI for `matplotlib`, almost inevitably
leads to endless failures that shouldn't be failures. For now, we will simply test that
the viz functions run on reasonably complex examples.
"""

import traceback
import warnings
from typing import Tuple, Union

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pytest
from matplotlib.collections import LineCollection, PathCollection
from matplotlib.colors import to_rgba_array

from hiveplotlib import P2CP, BaseHivePlot, Node
from hiveplotlib.datasets import example_hive_plot
from hiveplotlib.node import (
    subset_node_collection_by_unique_ids,
)
from hiveplotlib.viz import (
    axes_viz,
    edge_viz,
    hive_plot_viz,
    label_axes,
    node_viz,
    p2cp_legend,
    p2cp_viz,
)

pytestmark = pytest.mark.unmarked


@pytest.mark.parametrize(
    "viz_function", [axes_viz, label_axes, node_viz, edge_viz, hive_plot_viz]
)
def test_viz_functions_run_hiveplot(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray],
    viz_function: callable,
) -> None:
    """
    Make sure the baseline viz functions can run without error when run for hive plots.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    hp, edges = three_axis_basehiveplot_example

    # hardcoded partition of nodes assignment, placed on axes
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["0", "1"]),
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["2", "3"]),
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="C",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["4", "5"]),
        sorting_feature_to_use="c",
        vmin=vmin,
        vmax=vmax,
    )

    # connect some edges
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="C")
    # throw in an edge kwarg and leave axes on for coverage
    hp.connect_axes(edges=edges, axis_id_1="C", axis_id_2="B", color="C0")

    try:
        viz_function(hp, axes_off=False)
        plt.close()
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail(f"`{viz_function}` should have run, but failed for some reason.")


def test_fill_in_empty_edge_kwargs(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray],
) -> None:
    """
    Make sure entirely unspecified edge kwargs doesn't break ``hiveplotlib.viz.edge_viz()``.
    """
    hp, edges = three_axis_basehiveplot_example

    # hardcoded partition of nodes assignment, placed on axes
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["0", "1"]),
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["2", "3"]),
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="C",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["4", "5"]),
        sorting_feature_to_use="c",
        vmin=vmin,
        vmax=vmax,
    )

    # connect some edges without ever touching kwargs
    #  (`HivePlot.connect_axes()` will add empty kwargs if none included)
    tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")
    hp.add_edge_curves_between_axes(axis_id_1="A", axis_id_2="B", tag=tag)

    try:
        edge_viz(instance=hp)
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail("`edge_viz` should have run, but failed for some reason.")


@pytest.mark.parametrize("viz_function", [axes_viz, label_axes])
def test_warnings_no_axes_hive_plot(viz_function: callable) -> None:
    """
    Make sure the baseline viz functions hits a warning when we are missing things when run for hive plots.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    hp = BaseHivePlot()

    with pytest.warns() as record:
        viz_function(hp)
        plt.close()

    # confirm we get a single warning
    assert len(record) == 1, (
        f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"
    )


@pytest.mark.parametrize("viz_function", [node_viz, edge_viz, hive_plot_viz])
def test_warnings_missing_content_hive_plot(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray],
    viz_function: callable,
) -> None:
    """
    Make sure the baseline viz functions hits a warning when we are missing things when run for hive plots.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    hp, edges = three_axis_basehiveplot_example

    # only place nodes on 2 of the 3 axes, explicitly excluding one axis to get a warning
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["0", "1"]),
        sorting_feature_to_use="a",
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["2", "3"]),
        sorting_feature_to_use="b",
    )

    with pytest.warns() as record:
        viz_function(hp)
        plt.close()

    # confirm we get a single warning
    if viz_function == hive_plot_viz:
        assert len(record) == 2, (
            f"Expected 2 warnings for no edges AND no nodes, got:\n{[i.message.args for i in record]}"
        )
    else:
        assert len(record) == 1, (
            f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"
        )


def test_warnings_repeat_edge_kwarg(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray],
) -> None:
    """
    Make sure ``hiveplotlib.viz.edge_viz()`` hits a warning when including a kwarg redundant to existing kwargs.

    (To warn the user that their kwarg will be ignored in deference to the existing kwarg.)
    """
    hp, edges = three_axis_basehiveplot_example
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
    hp.connect_axes(edges=edges, axis_id_1="C", axis_id_2="B")
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="C", a2_to_a1=False, lw=1)
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="C", a1_to_a2=False)

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        edge_viz(instance=hp)

    with pytest.warns() as record:
        edge_viz(instance=hp, lw=2)

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )


@pytest.mark.parametrize("viz_function", [axes_viz, node_viz, edge_viz, p2cp_viz])
def test_viz_functions_run_p2cp(
    three_axis_p2cp_example: P2CP, viz_function: callable
) -> None:
    """
    Make sure the baseline viz functions can run without error when run for p2cps.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    p2cp = three_axis_p2cp_example

    try:
        viz_function(p2cp)
        plt.close()
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail(f"`{viz_function}` should have run, but failed for some reason.")


@pytest.mark.parametrize("tags", [None, "two", ["two"], ["one", "three"]])
def test_p2cp_legend(
    three_axis_p2cp_example: P2CP, tags: Union[str, list, None]
) -> None:
    """
    Make sure the p2cp viz functions can run without error when run for p2cps.

    :param tags: which tags of data to add to the p2cp.
    """
    p2cp = three_axis_p2cp_example
    p2cp.build_edges(tag="one")
    p2cp.build_edges(tag="two")
    p2cp.build_edges(tag="three", lw=0.5)
    fig, ax = p2cp_viz(p2cp, tags=tags)
    try:
        # make sure we can specify line kwargs or not
        p2cp_legend(p2cp, fig=fig, ax=ax, tags=tags, line_kwargs={"lw": 2})
        plt.close()
        p2cp_legend(p2cp, fig=fig, ax=ax, tags=tags)
        plt.close()
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail("`p2cp_legend` should have run, but failed for some reason.")


def test_warnings_repeat_edge_kwarg_p2cp(three_axis_p2cp_example: P2CP) -> None:
    """
    Make sure ``hiveplotlib.viz.edge_viz()`` hits a warning when including a kwarg redundant to existing kwargs.

    (To warn the user that their kwarg will be ignored in deference to the existing kwarg.)
    """
    p2cp = three_axis_p2cp_example
    p2cp.add_edge_kwargs(color="blue")

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        edge_viz(p2cp)

    with pytest.warns() as record:
        edge_viz(instance=p2cp, color="red")

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )


@pytest.mark.parametrize("viz_function", [axes_viz, label_axes, node_viz, edge_viz])
def test_expected_errors_wrong_types(viz_function: callable) -> None:
    """
    Viz functions should yell if tried on non-``P2CP`` or ``HivePlot`` instances.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    node = Node(unique_id=10)

    try:
        viz_function(node)
        plt.close()
        pytest.fail(
            f"`{viz_function}` should have failed on invalid input, but succeeded for some reason."
        )
    # supposed to fail
    except NotImplementedError:
        assert True


def test_warnings_node_viz_p2cp() -> None:
    """
    Make sure ``hiveplotlib.viz.node_viz()`` hits a single warning when nodes are unspecified.

    (If done correctly, this avoids sending a P2CP user a hive plot-specific internal warning that shouldn't
    matter. Even worse, if done wrong, the user would get the same warning *per axis* hence the check for a *single*
    warning.)
    """
    rng = np.random.default_rng(0)

    num_nodes = 50

    # a and b columns move in sync with eventual node id (index), c is random
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            rng.uniform(low=0, high=10, size=num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    p2cp = P2CP(data=data)

    with pytest.warns() as record:
        node_viz(instance=p2cp)

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )

    # set the axes, then should be fine
    p2cp.set_axes(list(data.columns.values))

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        node_viz(p2cp)


@pytest.mark.parametrize("viz_function", [axes_viz, label_axes])
def test_warnings_no_axes_p2cp(viz_function: callable) -> None:
    """
    Make sure the baseline viz functions hits a warning when we are missing things when run for p2cps.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    p2cp = P2CP()

    with pytest.warns() as record:
        viz_function(p2cp)
        plt.close()

    # confirm we get a single warning
    assert len(record) == 1, (
        f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"
    )


def test_label_axes_alignments() -> None:
    """
    Make sure ``hiveplotlib.viz.label_axes()`` does a "reasonable" alignment of axes labels for many angles.

    Note, this test exists primarily for two reasons: 1) test coverage and 2) a quick reference to make a scalable,
    multi-axis figure to observe all the alignments.
    """
    rng = np.random.default_rng(0)

    num_nodes = 50

    # number of axes on final p2cp
    num_axes = 26

    # axes names will be strings of integers 0, 1, 2, ...
    #  to make names longer (to see how alignment changes), repeat each string in each title this many times
    num_repeats_per_title = 5

    np_data = [np.sort(rng.uniform(low=0, high=10, size=num_nodes))] * num_axes
    data = pd.DataFrame(
        np.column_stack(np_data),
        columns=[i * num_repeats_per_title for i in np.arange(num_axes).astype(str)],
    )

    p2cp = P2CP(data=data)

    p2cp.set_axes(data.columns.values)

    try:
        axes_viz(p2cp)
        assert True
    except:  # noqa: E722
        pytest.fail("`axes_viz` should have run, but failed for some reason.")


def test_warnings_edge_viz_p2cp() -> None:
    """
    Make sure ``hiveplotlib.viz.edge_viz()`` hits a single warning when nodes are unspecified.

    (If done correctly, this avoids sending a P2CP user a hive plot-specific internal warning that shouldn't
    matter. Even worse, if done wrong, the user would get the same warning *per axis pair* hence the check for a
    *single* warning.)
    """
    rng = np.random.default_rng(0)

    num_nodes = 50

    # a and b columns move in sync with eventual node id (index), c is random
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            rng.uniform(low=0, high=10, size=num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    p2cp = P2CP(data=data)
    p2cp.set_axes(list(data.columns.values))

    with pytest.warns() as record:
        edge_viz(instance=p2cp)

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )

    # build the edges, then should be fine
    p2cp.build_edges()

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        edge_viz(p2cp)


def test_hiveplot_viz_backend_matplotlib() -> None:
    """
    Test that we can call the viz backend set as ``"matplotlib"`` on a ``HivePlot`` instance.
    """
    hp = example_hive_plot(
        backend="matplotlib",
    )
    hp.plot()
    plt.close()
    assert True


def test_plot_empty_hive_plot() -> None:
    """
    Test outputs when plotting an empty (no axes) ``HivePlot`` instance.
    """
    hp = example_hive_plot(
        backend="matplotlib",
    )
    # kill all existing axes in hive plot (thus killing all edges and nodes too)
    hp.set_partition(
        "partition_0",
        sorting_variables="low",
        build_hive_plot=False,
    )

    with pytest.warns() as record:
        out = hp.plot()
        plt.close()
    assert len(record) == 4, (
        "Expected only 4 warnings ('intermediate changes', no axes, no edges, no nodes), "
        f"got warnings of:\n{[i.message.args for i in record]}"
    )
    for i in record:
        assert (
            "Run the `build_hive_plot()` method on your `HivePlot` instance"
            in i.message.args[0]
            or "`HivePlot.build_hive_plot()`" in i.message.args[0]
        ), f"{i.message.args}"

    assert isinstance(out[0], plt.Figure)
    assert isinstance(out[1], plt.Axes)


@pytest.mark.parametrize("update_node_viz_kwargs", [True, False])
def test_plot_custom_node_kwargs_from_data(update_node_viz_kwargs: bool) -> None:
    """
    Test plotting a hive plot where we update the node viz kwargs with data and non-data values.

    This test is mainly for coverage and a reference for making a highly-interpretable visual example of changing node
    kwargs.

    :param update_node_viz_kwargs: whether to update the node viz kwargs via ``HivePlot.update_node_viz_kwargs()``
        or to call the node kwarg update via the ``node_kwargs`` parameter in ``HivePlot.plot()``.
    """
    hp = example_hive_plot(repeat_axes=True)
    hp.nodes.data["size_10x"] = hp.nodes.data["low"].to_numpy() * 10

    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()

    node_kwargs = {
        "c": "low",
        "s": "size_10x",
        "cmap": "magma",
        "vmin": 0,
        "vmax": 10,
        "edgecolor": "black",
    }

    if update_node_viz_kwargs:
        hp.update_node_viz_kwargs(**node_kwargs)
        # should run without warning
        fig, ax = hp.plot()
    else:
        fig, ax = hp.plot(node_kwargs=node_kwargs)
    fig.draw_without_rendering()  # drawing appears to actually propagate the node kwargs to the scatter call
    # check plotting values for scatter points of nodes
    scatter_points = [i for i in ax.get_children() if isinstance(i, PathCollection)]
    for sp in scatter_points:
        assert sp.get_facecolors().shape[0] > 1, (
            "Should have one color per point, making an (n, 4) color array instead of (1, 4)."
        )
        assert sp.get_sizes().size > 1, (
            "Should have one size per point, making n-sized array instead of 1."
        )
        assert sp.get_cmap().name == "magma"
        assert np.array_equal(sp.get_edgecolors(), np.array([[0, 0, 0, 0.8]])), (
            "Should have black edges with default alpha on all points."
        )
    plt.close()


@pytest.mark.parametrize("color_name", ["facecolor", "color", "c"])
def test_plot_custom_node_kwargs_handling_mpl_color_names(color_name: str) -> None:
    """
    Test plotting a hive plot where we update the node viz kwargs with corner case color names.

    Color should end up being "red" at the end regardless of ``color_name`` due to declared kwarg prioritization.

    (Using ``HivePlot.update_node_viz_kwargs()``.)

    This test is mainly for coverage.

    :param color_name: name to use when specifying color.
    """
    hp = example_hive_plot(repeat_axes=True)
    hp.nodes.data["size_10x"] = hp.nodes.data["low"].to_numpy() * 10
    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()
    hp.update_node_viz_kwargs(
        **{color_name: "red"},
    )
    # should run without warning
    _, ax = hp.plot(node_kwargs={"c": "blue"})
    # check plotting values for scatter points of nodes
    scatter_points = [i for i in ax.get_children() if isinstance(i, PathCollection)]
    for sp in scatter_points:
        assert np.array_equal(sp.get_facecolors(), np.array([[1, 0, 0, 0.8]])), (
            "Should have all red points, overwriting the provided {'c': 'blue'}."
        )
    plt.close()


@pytest.mark.parametrize("update_edge_viz_kwargs", [True, False])
def test_plot_custom_edge_kwargs_from_data(update_edge_viz_kwargs: bool) -> None:
    """
    Test plotting a hive plot where we update the edge viz kwargs with data and non-data values.

    This test is mainly for coverage and a reference for making a highly-interpretable visual example of changing edge
    kwargs.

    :param update_edge_viz_kwargs: whether to update the node viz kwargs via ``HivePlot.update_edge_viz_kwargs()``
        or to call the node kwarg update via the ``edge_kwargs`` parameter in ``HivePlot.plot()``.
    """
    hp = example_hive_plot(repeat_axes=True)
    hp.edges.data["linewidth"] = hp.edges.data["low"] / 3
    hp.edges.data["color"] = hp.edges.data["low"]

    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()

    edge_kwargs = {
        "array": "color",
        "linewidth": "linewidth",
        "cmap": "cividis",
        "clim": (0, 10),
    }

    if update_edge_viz_kwargs:
        hp.update_edge_viz_kwargs(**edge_kwargs)
        # should run without warning
        fig, ax = hp.plot()
    else:
        fig, ax = hp.plot(**edge_kwargs)
    fig.draw_without_rendering()  # drawing appears to actually propagate the node kwargs to the scatter call
    # check plotting values for edges
    lines = [i for i in ax.get_children() if isinstance(i, LineCollection)]
    for line in lines:
        assert line.get_colors().shape[0] > 1, (
            "Should have one color per point, making an (n, 4) color array instead of (1, 4)."
        )
        assert len(line.get_linewidths()) > 1, (
            "Should have line width per edge, making an (n, 1) array instead of (1, 1)."
        )
        assert line.get_cmap().name == edge_kwargs["cmap"]
    plt.close()


def test_plotting_edge_kwarg_hierarchy_overwrite_data_kwargs() -> None:
    """
    Test that plotting kwargs from various edge kwargs play nice together.

    Specifically, test the interaction of an ``update_edges()`` call and the ``edge_viz_kwargs`` attribute edge kwargs,
    with overwriting of edge data-based kwargs.
    """
    hp = example_hive_plot(repeat_axes=True)
    hp.edges.data["linewidth"] = hp.edges.data["low"] / 3
    hp.edges.data["color"] = hp.edges.data["low"]

    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()

    edge_kwargs = {
        "array": "color",
        "linewidth": "linewidth",
        "cmap": "cividis",
        "clim": (0, 10),
        "alpha": 0.65,
    }

    hp.update_edge_viz_kwargs(**edge_kwargs)

    red_kwargs = {"color": "red", "linewidth": 2}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a1_to_a2=False,
        #     short_arc=False,
        #     control_rho_scale=2.4,
        #     control_angle_shift=30,
        **red_kwargs,
    )
    blue_kwargs = {"color": "blue", "linewidth": 10}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a2_to_a1=False,
        control_rho_scale=1.2,
        #     control_angle_shift=-30,
        **blue_kwargs,
    )

    fig, ax = hp.plot()

    fig.draw_without_rendering()  # drawing appears to actually propagate the node kwargs to the scatter call

    # check plotting values for edges
    lines = [i for i in ax.get_children() if isinstance(i, LineCollection)]
    for line in lines:
        if line.get_colors().shape[0] > 1:
            assert len(line.get_linewidths()) > 1, (
                "Should have multiple line widths for these edges."
            )
            assert line.get_colors().shape[0] > 1, (
                "Should have multiple colors per line collection for these edges."
            )
        elif np.array_equal(
            line.get_colors(),
            to_rgba_array(
                red_kwargs["color"],
                alpha=edge_kwargs["alpha"],
            ),
        ):
            assert line.get_linewidths() == red_kwargs["linewidth"], (
                "Should have red kwarg line width."
            )
        elif np.array_equal(
            line.get_colors(),
            to_rgba_array(
                blue_kwargs["color"],
                alpha=edge_kwargs["alpha"],
            ),
        ):
            assert line.get_linewidths() == blue_kwargs["linewidth"], (
                "Should have blue kwarg line width."
            )
        else:
            pytest.fail(
                f"Should have had red, blue, or number-colored edges only, but got {line.get_colors()}"
            )


def test_plotting_edge_kwarg_hierarchy_plot_overwrite() -> None:
    """
    Test that plotting kwargs from various edge kwargs play nice together.

    Specifically, test the interaction of an ``update_edges()`` call, the ``edge_viz_kwargs`` attribute, and ``plot()``
    edge kwargs.
    """
    hp = example_hive_plot(repeat_axes=True)
    hp.edges.data["linewidth"] = hp.edges.data["low"] / 3
    hp.edges.data["color"] = hp.edges.data["low"]

    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()

    edge_kwargs = {
        "color": "green",
        "linewidth": 1,
        "alpha": 0.65,
    }

    hp.update_edge_viz_kwargs(**edge_kwargs)

    red_kwargs = {"color": "red", "linewidth": 2}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a1_to_a2=False,
        #     short_arc=False,
        #     control_rho_scale=2.4,
        #     control_angle_shift=30,
        **red_kwargs,
    )
    blue_kwargs = {"color": "blue", "linewidth": 10}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a2_to_a1=False,
        control_rho_scale=1.2,
        #     control_angle_shift=-30,
        **blue_kwargs,
    )

    with pytest.warns() as record:
        fig, ax = hp.plot(linewidth=1)
        plt.close()

    # confirm we get two warnings
    assert len(record) == 2, (
        f"Expected only 2 warnings, got warnings of:\n{[i.message.args for i in record]}"
    )

    for r in record:
        assert "linewidth" in r.message.args[0], (
            "warning should yell about repeated linewidth not being overwritten."
        )

    fig.draw_without_rendering()  # drawing appears to actually propagate the node kwargs to the scatter call

    # check plotting values for edges
    lines = [i for i in ax.get_children() if isinstance(i, LineCollection)]
    for line in lines:
        assert line.get_colors().shape[0] == 1, (
            "Should have one color per line collection with the 'green' and overwrite kwarg settings."
        )

        if np.array_equal(
            line.get_colors(),
            to_rgba_array(
                edge_kwargs["color"],
                alpha=edge_kwargs["alpha"],
            ),
        ):
            assert line.get_linewidths() == edge_kwargs["linewidth"], (
                "Should have default line width."
            )
        elif np.array_equal(
            line.get_colors(),
            to_rgba_array(
                red_kwargs["color"],
                alpha=edge_kwargs["alpha"],
            ),
        ):
            assert line.get_linewidths() == red_kwargs["linewidth"], (
                "Should have red kwarg line width."
            )
        elif np.array_equal(
            line.get_colors(),
            to_rgba_array(
                blue_kwargs["color"],
                alpha=edge_kwargs["alpha"],
            ),
        ):
            assert line.get_linewidths() == blue_kwargs["linewidth"], (
                "Should have blue kwarg line width."
            )
        else:
            pytest.fail(
                f"Should have had red, blue, or green edges only, but got {line.get_colors()}"
            )
