# viz_bokeh_test.py

"""
Tests for ``hiveplotlib.viz.holoviews.py``, the ``holoviews``-backend visualization functions.
"""

import traceback
import warnings
from typing import Tuple, Union

import numpy as np
import pandas as pd
import pytest
from matplotlib.collections import LineCollection, PathCollection
from matplotlib.colors import to_rgba_array

from hiveplotlib import P2CP, BaseHivePlot, Node
from hiveplotlib.datasets import example_hive_plot
from hiveplotlib.exceptions import InvalidHoverVariableError
from hiveplotlib.node import subset_node_collection_by_unique_ids

# skip these tests if no holoviews package
hv = pytest.importorskip("holoviews")

pytestmark = pytest.mark.holoviews


@pytest.mark.parametrize("backend", ["bokeh", "matplotlib"])
@pytest.mark.parametrize(
    "viz_function", ["axes_viz", "label_axes", "node_viz", "edge_viz", "hive_plot_viz"]
)
def test_viz_functions_run_hiveplot(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray],
    viz_function: str,
    backend: str,
) -> None:
    """
    Make sure the baseline viz functions can run without error when run for hive plots.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    :param backend: which holoviews backend to use.
    """
    import hiveplotlib.viz.holoviews

    hv.extension(backend)

    viz_function = getattr(hiveplotlib.viz.holoviews, viz_function)
    hp, edges = three_axis_basehiveplot_example

    # hardcoded partition of nodes assignment, placed on axes
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["0", "1"]),
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["2", "3"]),
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="C",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["4", "5"]),
        sorting_feature_to_use="c",
        vmin=vmin,
        vmax=vmax,
    )

    # connect some edges
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="C")
    # throw in an edge kwarg for coverage
    hp.connect_axes(edges=edges, axis_id_1="C", axis_id_2="B", color="blue")

    try:
        viz_function(hp)
        viz_function(
            hp, axes_off=False
        )  # to get full testing coverage, make sure this runs too
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail(f"`{viz_function}` should have run, but failed for some reason.")


@pytest.mark.parametrize("backend", ["bokeh", "matplotlib"])
def test_fill_in_empty_edge_kwargs(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray],
    backend: str,
) -> None:
    """
    Make sure entirely unspecified edge kwargs doesn't break ``hiveplotlib.viz.edge_viz()``.

    :param backend: which holoviews backend to use.
    """
    from hiveplotlib.viz.holoviews import edge_viz

    hv.extension(backend)

    hp, edges = three_axis_basehiveplot_example

    # hardcoded partition of nodes assignment, placed on axes
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["0", "1"]),
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["2", "3"]),
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="C",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["4", "5"]),
        sorting_feature_to_use="c",
        vmin=vmin,
        vmax=vmax,
    )

    # connect some edges without ever touching kwargs
    #  (`HivePlot.connect_axes()` will add empty kwargs if none included)
    tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")
    hp.add_edge_curves_between_axes(axis_id_1="A", axis_id_2="B", tag=tag)

    try:
        edge_viz(instance=hp)
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail("`edge_viz` should have run, but failed for some reason.")


@pytest.mark.parametrize("backend", ["bokeh", "matplotlib"])
@pytest.mark.parametrize("viz_function", ["axes_viz", "label_axes"])
def test_warnings_no_axes_hive_plot(
    viz_function: str,
    backend: str,
) -> None:
    """
    Make sure the baseline viz functions hits a warning when we are missing things when run for hive plots.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    :param backend: which holoviews backend to use.
    """
    import hiveplotlib.viz.holoviews

    hv.extension(backend)

    viz_function = getattr(hiveplotlib.viz.holoviews, viz_function)
    hp = BaseHivePlot()

    with pytest.warns() as record:
        viz_function(hp)

    # confirm we get a single warning
    assert len(record) == 1, (
        f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"
    )


@pytest.mark.parametrize("backend", ["bokeh", "matplotlib"])
@pytest.mark.parametrize("viz_function", ["node_viz", "edge_viz", "hive_plot_viz"])
def test_warnings_missing_content_hive_plot(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray],
    viz_function: str,
    backend: str,
) -> None:
    """
    Make sure the baseline viz functions hits a warning when we are missing things when run for hive plots.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    :param backend: which holoviews backend to use.
    """
    import hiveplotlib.viz.holoviews

    hv.extension(backend)

    viz_function = getattr(hiveplotlib.viz.holoviews, viz_function)
    hp, edges = three_axis_basehiveplot_example

    # only place nodes on 2 of the 3 axes, explicitly excluding one axis to get a warning
    hp.place_nodes_on_axis(
        axis_id="A",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["0", "1"]),
        sorting_feature_to_use="a",
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        node_df=subset_node_collection_by_unique_ids(hp.nodes, ids=["2", "3"]),
        sorting_feature_to_use="b",
    )

    with pytest.warns() as record:
        viz_function(hp)

    # confirm we get a single warning
    if viz_function == hiveplotlib.viz.holoviews.hive_plot_viz:
        assert len(record) == 2, (
            f"Expected 2 warnings for no edges AND no nodes, got:\n{[i.message.args for i in record]}"
        )
    else:
        assert len(record) == 1, (
            f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"
        )


@pytest.mark.parametrize("backend", ["bokeh", "matplotlib"])
def test_warnings_repeat_edge_kwarg(
    three_axis_basehiveplot_example: Tuple[BaseHivePlot, np.ndarray],
    backend: str,
) -> None:
    """
    Make sure ``hiveplotlib.viz.edge_viz()`` hits a warning when including a kwarg redundant to existing kwargs.

    (To warn the user that their kwarg will be ignored in deference to the existing kwarg.)

    :param backend: which holoviews backend to use.
    """
    from hiveplotlib.viz.holoviews import edge_viz

    hv.extension(backend)

    hp, edges = three_axis_basehiveplot_example
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
    hp.connect_axes(edges=edges, axis_id_1="C", axis_id_2="B")
    hp.connect_axes(
        edges=edges, axis_id_1="A", axis_id_2="C", a2_to_a1=False, line_width=1
    )
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="C", a1_to_a2=False)

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        edge_viz(instance=hp)

    with pytest.warns() as record:
        edge_viz(instance=hp, line_width=2)

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )


@pytest.mark.parametrize("backend", ["bokeh", "matplotlib"])
@pytest.mark.parametrize(
    "viz_function", ["axes_viz", "node_viz", "edge_viz", "p2cp_viz"]
)
def test_viz_functions_run_p2cp(
    three_axis_p2cp_example: P2CP,
    viz_function: str,
    backend: str,
) -> None:
    """
    Make sure the baseline viz functions can run without error when run for p2cps.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    :param backend: which holoviews backend to use.
    """
    import hiveplotlib.viz.holoviews

    hv.extension(backend)

    viz_function = getattr(hiveplotlib.viz.holoviews, viz_function)
    p2cp = three_axis_p2cp_example

    try:
        if backend == "bokeh":
            with pytest.warns() as record:
                viz_function(
                    p2cp, hover=True
                )  # make sure we warn as expected if inappropriately adding hover info here
            assert len(record) == 1, (
                f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
            )
        elif backend == "matplotlib":
            viz_function(p2cp, hover=True)  # should be disregarded with mpl backend
        else:
            raise NotImplementedError
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail(f"`{viz_function}` should have run, but failed for some reason.")


@pytest.mark.parametrize("backend", ["bokeh", "matplotlib"])
@pytest.mark.parametrize("tags", [None, "two", ["two"], ["one", "three"]])
def test_p2cp_legend(
    three_axis_p2cp_example: P2CP,
    tags: Union[str, list, None],
    backend: str,
) -> None:
    """
    Make sure the p2cp viz functions can run without error when run for p2cps.

    :param tags: which tags of data to add to the p2cp.
    :param backend: which holoviews backend to use.
    """
    from hiveplotlib.viz.holoviews import p2cp_legend, p2cp_viz

    hv.extension(backend)

    p2cp = three_axis_p2cp_example
    p2cp.build_edges(tag="one")
    p2cp.build_edges(tag="two")
    p2cp.build_edges(tag="three", color=0.5)
    fig = p2cp_viz(p2cp, tags=tags)
    try:
        p2cp_legend(fig=fig)
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail("`p2cp_legend` should have run, but failed for some reason.")


@pytest.mark.parametrize("backend", ["bokeh", "matplotlib"])
def test_warnings_repeat_edge_kwarg_p2cp(
    three_axis_p2cp_example: P2CP,
    backend: str,
) -> None:
    """
    Make sure ``hiveplotlib.viz.edge_viz()`` hits a warning when including a kwarg redundant to existing kwargs.

    (To warn the user that their kwarg will be ignored in deference to the existing kwarg.)

    :param backend: which holoviews backend to use.
    """
    from hiveplotlib.viz.holoviews import edge_viz

    hv.extension(backend)

    p2cp = three_axis_p2cp_example
    p2cp.add_edge_kwargs(color="blue")

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        edge_viz(
            p2cp,
            hover=False,  # no need to test hover here
        )

    with pytest.warns() as record:
        edge_viz(
            instance=p2cp,
            color="red",
            hover=False,  # no need to test hover here
        )

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )


@pytest.mark.parametrize("backend", ["bokeh", "matplotlib"])
@pytest.mark.parametrize(
    "viz_function", ["axes_viz", "label_axes", "node_viz", "edge_viz"]
)
def test_expected_errors_wrong_types(
    viz_function: str,
    backend: str,
) -> None:
    """
    Viz functions should yell if tried on non-``P2CP`` or ``HivePlot`` instances.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    :param backend: which holoviews backend to use.
    """
    import hiveplotlib.viz.holoviews

    hv.extension(backend)

    viz_function = getattr(hiveplotlib.viz.holoviews, viz_function)
    node = Node(unique_id=10)

    try:
        viz_function(node)
        pytest.fail(
            f"`{viz_function}` should have failed to run on an invalid input, but succeeded for some reason."
        )
    # supposed to fail
    except NotImplementedError:
        assert True


@pytest.mark.parametrize("backend", ["bokeh", "matplotlib"])
def test_warnings_node_viz_p2cp(
    backend: str,
) -> None:
    """
    Make sure ``hiveplotlib.viz.node_viz()`` hits a single warning when nodes are unspecified.

    (If done correctly, this avoids sending a P2CP user a hive plot-specific internal warning that shouldn't
    matter. Even worse, if done wrong, the user would get the same warning *per axis* hence the check for a *single*
    warning.)

    :param backend: which holoviews backend to use.
    """
    from hiveplotlib.viz.holoviews import node_viz

    hv.extension(backend)

    rng = np.random.default_rng(0)

    num_nodes = 50

    # a and b columns move in sync with eventual node id (index), c is random
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            rng.uniform(low=0, high=10, size=num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    p2cp = P2CP(data=data)

    with pytest.warns() as record:
        node_viz(
            instance=p2cp,
            hover=False,
        )

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )

    # set the axes, then should be fine
    p2cp.set_axes(list(data.columns.values))

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        node_viz(
            p2cp,
            hover=False,
        )


@pytest.mark.parametrize("backend", ["bokeh", "matplotlib"])
@pytest.mark.parametrize("viz_function", ["axes_viz", "label_axes"])
def test_warnings_no_axes_p2cp(
    viz_function: str,
    backend: str,
) -> None:
    """
    Make sure the baseline viz functions hits a warning when we are missing things when run for p2cps.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    :param backend: which holoviews backend to use.
    """
    import hiveplotlib.viz.holoviews

    hv.extension(backend)

    viz_function = getattr(hiveplotlib.viz.holoviews, viz_function)
    p2cp = P2CP()

    with pytest.warns() as record:
        viz_function(p2cp)

    # confirm we get a single warning
    assert len(record) == 1, (
        f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"
    )


@pytest.mark.parametrize("backend", ["bokeh", "matplotlib"])
def test_label_axes_alignments(
    backend: str,
) -> None:
    """
    Make sure ``hiveplotlib.viz.label_axes()`` does a "reasonable" alignment of axes labels for many angles.

    Note, this test exists primarily for two reasons: 1) test coverage and 2) a quick reference to make a scalable,
    multi-axis figure to observe all the alignments.

    :param backend: which holoviews backend to use.
    """
    from hiveplotlib.viz.holoviews import axes_viz

    hv.extension(backend)

    rng = np.random.default_rng(0)

    num_nodes = 50

    # number of axes on final p2cp
    num_axes = 26

    # axes names will be strings of integers 0, 1, 2, ...
    #  to make names longer (to see how alignment changes), repeat each string in each title this many times
    num_repeats_per_title = 5

    np_data = [np.sort(rng.uniform(low=0, high=10, size=num_nodes))] * num_axes
    data = pd.DataFrame(
        np.column_stack(np_data),
        columns=[i * num_repeats_per_title for i in np.arange(num_axes).astype(str)],
    )

    p2cp = P2CP(data=data)

    p2cp.set_axes(data.columns.values)

    try:
        axes_viz(
            p2cp,
            hover=False,
        )
        assert True
    except:  # noqa: E722
        pytest.fail("`axes_viz` should have run, but failed for some reason.")


@pytest.mark.parametrize("backend", ["bokeh", "matplotlib"])
def test_warnings_edge_viz_p2cp(
    backend: str,
) -> None:
    """
    Make sure ``hiveplotlib.viz.edge_viz()`` hits a single warning when nodes are unspecified.

    (If done correctly, this avoids sending a P2CP user a hive plot-specific internal warning that shouldn't
    matter. Even worse, if done wrong, the user would get the same warning *per axis pair* hence the check for a
    *single* warning.)

    :param backend: which holoviews backend to use.
    """
    from hiveplotlib.viz.holoviews import edge_viz

    hv.extension(backend)

    rng = np.random.default_rng(0)

    num_nodes = 50

    # a and b columns move in sync with eventual node id (index), c is random
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            rng.uniform(low=0, high=10, size=num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    p2cp = P2CP(data=data)
    p2cp.set_axes(list(data.columns.values))

    with pytest.warns() as record:
        edge_viz(
            instance=p2cp,
            hover=False,
        )

    assert len(record) == 1, (
        f"Expected 1 warning, got:\n{[i.message.args for i in record]}"
    )

    # build the edges, then should be fine
    p2cp.build_edges()

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        edge_viz(
            p2cp,
            hover=False,
        )


@pytest.mark.parametrize("backend", ["bokeh", "matplotlib"])
def test_hiveplot_viz_backend_holoviews(backend: str) -> None:
    """
    Test that we can call one of the ``holoviews`` based viz backends as on a ``HivePlot`` instance.

    Options here are ``"holoviews-bokeh"`` or ``"holoviews-matplotlib"``.

    :param backend: which holoviews backend to use.
    """
    hp = example_hive_plot(
        backend=f"holoviews-{backend}",
    )
    hp.plot()
    assert True


def test_hiveplot_viz_switch_backend() -> None:
    """
    Test that we can switch between the two ``holoviews`` viz backends on a ``HivePlot`` instance.

    Switching between  ``"holoviews-bokeh"`` and ``"holoviews-matplotlib"``.
    """
    hp = example_hive_plot(
        backend="holoviews-bokeh",
    )
    hp.plot()
    hp.set_viz_backend(backend="holoviews-matplotlib")
    hp.plot()
    assert True


@pytest.mark.parametrize("backend", ["holoviews-matplotlib", "holoviews-bokeh"])
def test_plot_empty_hive_plot(backend: str) -> None:
    """
    Test outputs when plotting an empty (no axes) ``HivePlot`` instance.

    :param backend: which backend to set in ``HivePlot`` instance.
    """
    hp = example_hive_plot(
        backend=backend,
    )
    # kill all existing axes in hive plot (thus killing all edges and nodes too)
    hp.set_partition(
        "partition_0",
        sorting_variables="low",
        build_hive_plot=False,
    )

    with pytest.warns() as record:
        out = hp.plot()
    assert len(record) == 4, (
        "Expected only 4 warnings ('intermediate changes', no axes, no edges, no nodes), "
        f"got warnings of:\n{[i.message.args for i in record]}"
    )
    for i in record:
        assert (
            "Run the `build_hive_plot()` method on your `HivePlot` instance"
            in i.message.args[0]
            or "`HivePlot.build_hive_plot()`" in i.message.args[0]
        ), f"{i.message.args}"

    assert isinstance(out, hv.Overlay)


@pytest.mark.parametrize("update_node_viz_kwargs", [True, False])
@pytest.mark.parametrize("backend", ["holoviews-matplotlib", "holoviews-bokeh"])
def test_plot_custom_node_kwargs_from_data(
    update_node_viz_kwargs: bool,
    backend: str,
) -> None:
    """
    Test plotting a hive plot where we update the node viz kwargs with data and non-data values.

    This test is mainly for coverage and a reference for making a highly-interpretable visual example of changing node
    kwargs.

    :param update_node_viz_kwargs: whether to update the node viz kwargs via ``HivePlot.update_node_viz_kwargs()``
        or to call the node kwarg update via the ``node_kwargs`` parameter in ``HivePlot.plot()``.
    :param backend: which backend to set in ``HivePlot`` instance.
    """
    hp = example_hive_plot(
        repeat_axes=True,
        backend=backend,
    )

    if hp.backend == "holoviews-bokeh":
        hp.nodes.data["size"] = hp.nodes.data["low"].to_numpy() + 5
    elif hp.backend == "holoviews-matplotlib":
        hp.nodes.data["size"] = hp.nodes.data["low"].to_numpy() * 10

    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()

    node_kwargs = {
        "color": "low",  # based on data
        "cmap": "magma",
        "clim": (0, 10),
        "colorbar": True,
        "clabel": "Node Variable 'Low'",
    }

    if hp.backend == "holoviews-matplotlib":
        node_kwargs |= {
            "edgecolor": "black",
            "s": "size",  # reference to "size" variable in node data
        }
        overlay_kwargs = {}
    elif hp.backend == "holoviews-bokeh":
        node_kwargs |= {
            "line_color": "black",
            "size": "size",  # reference to "size" variable in node data
        }
        # change width to make room for colorbar but keeping 1:1 aspect
        overlay_kwargs = {"width": 650, "height": 400}

    if update_node_viz_kwargs:
        hp.update_node_viz_kwargs(**node_kwargs)
        fig = hp.plot(
            overlay_kwargs=overlay_kwargs,
        )
    else:
        fig = hp.plot(
            node_kwargs=node_kwargs,
            overlay_kwargs=overlay_kwargs,
        )

    if hp.backend == "holoviews-bokeh":
        from bokeh.models import Scatter

        scatter_glyphs = [
            glyph.glyph
            for glyph in hv.render(fig).renderers
            if isinstance(glyph.glyph, Scatter)
        ]

        assert scatter_glyphs[0].size.field == "size", (
            "Should have variable ref 'size' for size"
        )

        color = scatter_glyphs[0].fill_color.transform
        assert color.low == node_kwargs["clim"][0]
        assert color.high == node_kwargs["clim"][1]
    elif hp.backend == "holoviews-matplotlib":
        scatter_points = [
            i
            for i in hv.render(fig).axes[0].get_children()
            if isinstance(i, PathCollection)
        ]
        for sp in scatter_points:
            assert sp.get_facecolors().shape[0] > 1, (
                "Should have one color per point, making an (n, 4) color array instead of (1, 4)."
            )
            assert sp.get_sizes().size > 1, (
                "Should have one size per point, making n-sized array instead of 1."
            )
            assert sp.get_cmap().name == "from_list", (
                "should be pulling from holoviews list"
            )
            assert np.array_equal(sp.get_edgecolors(), np.array([[0, 0, 0, 0.8]])), (
                "Should have black edges with default alpha on all points."
            )


@pytest.mark.parametrize("update_edge_viz_kwargs", [True, False])
@pytest.mark.parametrize("backend", ["holoviews-matplotlib", "holoviews-bokeh"])
def test_plot_custom_edge_kwargs_from_data(
    update_edge_viz_kwargs: bool,
    backend: str,
) -> None:
    """
    Test plotting a hive plot where we update the edge viz kwargs with data and non-data values.

    This test is mainly for coverage and a reference for making a highly-interpretable visual example of changing edge
    kwargs.

    :param update_edge_viz_kwargs: whether to update the edge viz kwargs via ``HivePlot.update_edge_viz_kwargs()``
        or to call the edge kwarg update via the ``edge_kwargs`` parameter in ``HivePlot.plot()``.
    :param backend: which backend to set in ``HivePlot`` instance.
    """
    hp = example_hive_plot(
        repeat_axes=True,
        backend=backend,
    )

    if hp.backend == "holoviews-bokeh":
        line_width_name = "line_width"
    elif hp.backend == "holoviews-matplotlib":
        line_width_name = "linewidth"

    hp.edges.data[line_width_name] = hp.edges.data["low"] / 3

    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()

    edge_kwargs = {
        "color": "low",
        line_width_name: line_width_name,
        "cmap": "cividis",
        "clim": (0, 10),
        # "colorbar": True,  # add in colorbar
        "clabel": "Edge Variable 'Low'",
    }

    # change width to make room for colorbar but keeping 1:1 aspect
    overlay_kwargs = {}
    axes_kwargs = {"axes_kwargs": {line_width_name: 0.75}}
    # if hp.backend == "holoviews-bokeh":
    # overlay_kwargs = {"width": 650, "height": 400}

    if update_edge_viz_kwargs:
        hp.update_edge_viz_kwargs(**edge_kwargs)
        fig = hp.plot(
            overlay_kwargs=overlay_kwargs,
            **axes_kwargs,
        )
    else:
        fig = hp.plot(
            overlay_kwargs=overlay_kwargs,
            **axes_kwargs,
            **edge_kwargs,
        )

    if hp.backend == "holoviews-bokeh":
        from bokeh.models import MultiLine

        multi_line_glyphs = [
            glyph.glyph
            for glyph in hv.render(fig).renderers
            if isinstance(glyph.glyph, MultiLine)
        ]

        for multi_line_glyph in multi_line_glyphs:
            assert multi_line_glyph.line_width.field == line_width_name, (
                f"Should have variable ref '{line_width_name}' for line_width parameter."
            )
            color = multi_line_glyph.line_color.transform
            assert color.low == edge_kwargs["clim"][0]
            assert color.high == edge_kwargs["clim"][1]
    elif hp.backend == "holoviews-matplotlib":
        lines = [
            i
            for i in hv.render(fig).axes[0].get_children()
            if isinstance(i, LineCollection)
        ]
        for line in lines:
            if len(line.get_linewidths()) == 1:
                assert (
                    line.get_linewidths()[0]
                    == axes_kwargs["axes_kwargs"][line_width_name]
                ), "Should be axis with expected line width."
            else:
                # holoviews mpl seems to store color info somewhere else unknown, skipping testing colors for now...
                assert len(line.get_linewidths()) > 1, (
                    "Should have line width per edge, making an (n, 1) array instead of (1, 1)."
                )
                assert line.get_cmap().name == "from_list"
                assert line.get_clim() == edge_kwargs["clim"]


@pytest.mark.parametrize("backend", ["holoviews-matplotlib", "holoviews-bokeh"])
def test_plotting_edge_kwarg_hierarchy_overwrite_data_kwargs(backend: str) -> None:
    """
    Test that plotting kwargs from various edge kwargs play nice together.

    Specifically, test the interaction of an ``update_edges()`` call and the ``edge_viz_kwargs`` attribute edge kwargs,
    with overwriting of edge data-based kwargs.

    :param backend: which backend to set in ``HivePlot`` instance.
    """
    hp = example_hive_plot(repeat_axes=True, backend=backend)

    if hp.backend == "holoviews-bokeh":
        line_width_name = "line_width"
        line_alpha_name = "line_alpha"
    elif hp.backend == "holoviews-matplotlib":
        line_width_name = "linewidth"
        line_alpha_name = "alpha"

    hp.edges.data[line_width_name] = hp.edges.data["low"] / 3

    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()

    edge_kwargs = {
        "color": "low",
        line_width_name: line_width_name,
        line_alpha_name: 0.65,
        "cmap": "cividis",
        "clim": (0, 10),
        # "colorbar": True,  # add in colorbar
        "clabel": "Edge Variable 'Low'",
    }

    hp.update_edge_viz_kwargs(**edge_kwargs)

    red_kwargs = {"color": "red", line_width_name: 2}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a1_to_a2=False,
        #     short_arc=False,
        #     control_rho_scale=2.4,
        #     control_angle_shift=30,
        **red_kwargs,
    )
    blue_kwargs = {"color": "blue", line_width_name: 10}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a2_to_a1=False,
        control_rho_scale=1.2,
        #     control_angle_shift=-30,
        **blue_kwargs,
    )

    fig = hp.plot(axes_kwargs={"linewidth": 0.5})

    if hp.backend == "holoviews-bokeh":
        from bokeh.models import MultiLine

        multi_line_glyphs = [
            glyph.glyph
            for glyph in hv.render(fig).renderers
            if isinstance(glyph.glyph, MultiLine)
        ]

        for multi_line_glyph in multi_line_glyphs:
            # checking cmaps not line colors because of hack solution to non-data edge colors
            if multi_line_glyph.line_width == red_kwargs[line_width_name]:
                assert multi_line_glyph.line_color.transform.palette == [
                    red_kwargs["color"]
                ]
            elif multi_line_glyph.line_width == blue_kwargs[line_width_name]:
                assert multi_line_glyph.line_color.transform.palette == [
                    blue_kwargs["color"]
                ]
            elif multi_line_glyph.line_width.field == line_width_name:
                color = multi_line_glyph.line_color.transform
                assert color.low == edge_kwargs["clim"][0]
                assert color.high == edge_kwargs["clim"][1]
            else:
                pytest.fail(
                    "Should have had red, blue, or number-colored edges only, "
                    f"but got {multi_line_glyph.line_color} color and {multi_line_glyph.line_width} width."
                )
    elif hp.backend == "holoviews-matplotlib":
        lines = [
            i
            for i in hv.render(fig).axes[0].get_children()
            if isinstance(i, LineCollection)
        ]
        for line in lines:
            # holoviews mpl seems to store color info somewhere else unknown, skipping testing colors for data-colored
            if len(line.get_linewidths()) > 1:
                assert line.get_cmap().name == "from_list"
                assert line.get_clim() == edge_kwargs["clim"]
            elif np.array_equal(
                line.get_colors(),
                to_rgba_array(
                    red_kwargs["color"],
                    alpha=edge_kwargs[line_alpha_name],
                ),
            ):
                assert line.get_linewidths() == red_kwargs[line_width_name], (
                    "Should have red kwarg line width."
                )
            elif np.array_equal(
                line.get_colors(),
                to_rgba_array(
                    blue_kwargs["color"],
                    alpha=edge_kwargs[line_alpha_name],
                ),
            ):
                assert line.get_linewidths() == blue_kwargs[line_width_name], (
                    "Should have blue kwarg line width."
                )
            elif np.array_equal(
                line.get_colors(),
                to_rgba_array(
                    "black",
                    alpha=0.5,
                ),
            ):
                assert line.get_linewidths() == 0.5, (
                    "Should be axis with above set axis linewidth."
                )
            else:
                pytest.fail(
                    f"Should have had red, blue, or number-colored edges only, but got {line.get_colors()}"
                )


@pytest.mark.parametrize("backend", ["holoviews-matplotlib", "holoviews-bokeh"])
def test_plotting_edge_kwarg_hierarchy_plot_overwrite(backend: str) -> None:
    """
    Test that plotting kwargs from various edge kwargs play nice together.

    Specifically, test the interaction of an ``update_edges()`` call, the ``edge_viz_kwargs`` attribute, and ``plot()``
    edge kwargs.

    :param backend: which backend to set in ``HivePlot`` instance.
    """
    hp = example_hive_plot(repeat_axes=True, backend=backend)

    if hp.backend == "holoviews-bokeh":
        line_width_name = "line_width"
        line_alpha_name = "line_alpha"
    elif hp.backend == "holoviews-matplotlib":
        line_width_name = "linewidth"
        line_alpha_name = "alpha"

    hp.edges.data[line_width_name] = hp.edges.data["low"] / 3

    # propagate extra node data changes through to data on axes
    hp.build_hive_plot()

    edge_kwargs = {"color": "green", line_width_name: 1, line_alpha_name: 0.65}

    hp.update_edge_viz_kwargs(**edge_kwargs)

    red_kwargs = {"color": "red", line_width_name: 2}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a1_to_a2=False,
        #     short_arc=False,
        #     control_rho_scale=2.4,
        #     control_angle_shift=30,
        **red_kwargs,
    )
    blue_kwargs = {"color": "blue", line_width_name: 10}
    hp.update_edges(
        axis_id_1="A_repeat",
        axis_id_2="B",
        a2_to_a1=False,
        control_rho_scale=1.2,
        #     control_angle_shift=-30,
        **blue_kwargs,
    )

    with pytest.warns() as record:
        fig = hp.plot(**{line_width_name: 1})

    # confirm we get two warnings
    assert len(record) == 2, (
        f"Expected only 2 warnings, got warnings of:\n{[i.message.args for i in record]}"
    )

    for r in record:
        assert line_width_name in r.message.args[0], (
            "warning should yell about repeated line_width not being overwritten."
        )

    if hp.backend == "holoviews-bokeh":
        from bokeh.models import MultiLine

        multi_line_glyphs = [
            glyph.glyph
            for glyph in hv.render(fig).renderers
            if isinstance(glyph.glyph, MultiLine)
        ]

        for multi_line_glyph in multi_line_glyphs:
            # checking cmaps not line colors because of hack solution to non-data edge colors
            if multi_line_glyph.line_width == edge_kwargs["line_width"]:
                assert multi_line_glyph.line_color.transform.palette == [
                    edge_kwargs["color"]
                ]
            elif multi_line_glyph.line_width == red_kwargs["line_width"]:
                assert multi_line_glyph.line_color.transform.palette == [
                    red_kwargs["color"]
                ]
            elif multi_line_glyph.line_width == blue_kwargs["line_width"]:
                assert multi_line_glyph.line_color.transform.palette == [
                    blue_kwargs["color"]
                ]
            else:
                pytest.fail(
                    f"Should have had red, blue, or green edges only, but got {multi_line_glyph.line_color}"
                )
    elif hp.backend == "holoviews-matplotlib":
        lines = [
            i
            for i in hv.render(fig).axes[0].get_children()
            if isinstance(i, LineCollection)
        ]
        for line in lines:
            if np.array_equal(
                line.get_colors(),
                to_rgba_array(
                    edge_kwargs["color"],
                    alpha=edge_kwargs[line_alpha_name],
                ),
            ):
                assert line.get_linewidths() == edge_kwargs[line_width_name], (
                    "Should have default kwarg line width."
                )
            elif np.array_equal(
                line.get_colors(),
                to_rgba_array(
                    red_kwargs["color"],
                    alpha=edge_kwargs[line_alpha_name],
                ),
            ):
                assert line.get_linewidths() == red_kwargs[line_width_name], (
                    "Should have red kwarg line width."
                )
            elif np.array_equal(
                line.get_colors(),
                to_rgba_array(
                    blue_kwargs["color"],
                    alpha=edge_kwargs[line_alpha_name],
                ),
            ):
                assert line.get_linewidths() == blue_kwargs[line_width_name], (
                    "Should have blue kwarg line width."
                )
            elif np.array_equal(
                line.get_colors(),
                to_rgba_array(
                    "black",
                    alpha=0.5,
                ),
            ):
                assert line.get_linewidths() == 1.5, (
                    "Should be axis with default axis values."
                )
            else:
                pytest.fail(
                    f"Should have had red, blue, or green edges only (or black axes), but got {line.get_colors()}"
                )


@pytest.mark.parametrize(
    "kwarg_location",
    [
        "edge_viz_kwargs",
        "hive_plot_edges",
        "contours_kwargs",
    ],
)
def test_error_on_line_color_parameter(kwarg_location: str) -> None:
    """
    Test that we cannot plot edges with the ``bokeh`` back end using ``line_color`` to specify edge color.

    (Only support using the ``color`` parameter.)

    :param kwarg_location: where ``"line_color"`` parameter added to edge kwargs.
    """
    hp = example_hive_plot(repeat_axes=True, backend="holoviews-bokeh")

    plot_kwargs = {}

    if kwarg_location == "edge_viz_kwargs":
        edge_kwargs = {"line_color": "red"}
        hp.update_edge_viz_kwargs(**edge_kwargs)
    elif kwarg_location == "hive_plot_edges":
        red_kwargs = {"line_color": "red"}
        hp.update_edges(
            axis_id_1="A_repeat",
            axis_id_2="B",
            **red_kwargs,
        )
    elif kwarg_location == "contours_kwargs":
        plot_kwargs["line_color"] = "red"
    else:
        raise NotImplementedError

    try:
        hp.plot(**plot_kwargs)
        pytest.fail("Use of 'line_color' should have raised a `ValueError`.")
    except ValueError:
        assert True


@pytest.mark.parametrize(
    "hover",
    [
        True,
        False,
        "nodes",
        "edges",
        "axes",
        ["nodes", "edges"],
        ["axes"],
        "axis",
        ["axis", "nodes"],
    ],
)
def test_hive_plot_hover_tools(hover: Union[str, bool, list[str]]) -> None:
    """
    Test plotting hive plot with possible (and impossible) ``"hover"`` settings.

    :param hover: hover setting.
    """
    from bokeh.models import HoverTool

    hp = example_hive_plot(backend="holoviews-bokeh")

    if hover == "axis" or hover == ["axis", "nodes"]:
        try:
            fig = hp.plot(hover=hover)
            pytest.fail(
                "Should have raised `InvalidHoverVariableError` error (needs to be 'axes' not 'axis')."
            )
        except InvalidHoverVariableError:
            assert True

    else:
        fig = hp.plot(hover=hover)

        hover_tooltips = [i for i in hv.render(fig).tools if isinstance(i, HoverTool)]

        if hover is False:
            assert len(hover_tooltips) == 0, (
                "Should not have any hover tooltips when `hover=False`"
            )
        elif hover == "nodes":
            assert len(hover_tooltips) == 1, (
                "Should have 1 hover tooltip when `hover='nodes'`"
            )
            assert "Node: " in hover_tooltips[0].tooltips
        elif hover == "edges":
            assert len(hover_tooltips) == 1, (
                "Should have 1 hover tooltip when `hover='edges'`"
            )
            assert "Edge: " in hover_tooltips[0].tooltips
        elif hover == "axes" or hover == ["axes"]:
            assert len(hover_tooltips) == 1, (
                "Should have 1 hover tooltip when `hover='axes'`"
            )
            assert "Axis: " in hover_tooltips[0].tooltips
        elif hover is True:
            assert len(hover_tooltips) == 3, (
                "Should have 3 hover tooltips when `hover=True`"
            )
            for ht in hover_tooltips:
                assert (
                    "Node: " in ht.tooltips
                    or "Edge: " in ht.tooltips
                    or "Axis: " in ht.tooltips
                ), (
                    "Tooltip should either be titled 'Node: ...', 'Edge: ...', or 'Axis: ...',."
                )
        elif hover == ["nodes", "edges"]:
            assert len(hover_tooltips) == 2, (
                "Should have 2 hover tooltips when `hover=['nodes', 'edges']`"
            )
            for ht in hover_tooltips:
                assert "Node: " in ht.tooltips or "Edge: " in ht.tooltips, (
                    "Tooltip should either be titled 'Node: ...' or 'Edge: ...'."
                )
                assert "Axis: " not in ht.tooltips, (
                    "'Axis: ...' tooltip should have been skipped."
                )
        else:
            raise NotImplementedError
